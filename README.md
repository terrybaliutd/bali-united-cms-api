Bali United Web API
===================
**Note :** use SuitSoccer as the core.


SuitSoccer
==========
**Note :** use SuitCMS as the core.

[![build status](https://gitlab.com/Suitmedia/suitsoccer/badges/master/build.svg)](https://gitlab.com/Suitmedia/suitsoccer/commits/master)


SuitCMS
=======

[![build status](https://gitlab.com/Suitmedia/suitcms/badges/master/build.svg)](https://gitlab.com/Suitmedia/suitcms/commits/master)

**Note: use `git clone git@gitlab.com:Suitmedia/suitcms.git --depth 1` to clone the last commit**

Platform for standard web content management system based on laravel, developed by Suitmedia. For more detail [read the documentation on wiki](https://gitlab.com/suitmedia/suitcms/wikis/home)

![SuitCMS](http://i.imgur.com/V6zSX.png)

## Main Component
* Laravel 5.1
* [Metronic 3.5][1]

## Package Dependencies
* laravelcollective/html 5.1.*
* wicochandra/captcha 1.2.*
* guzzlehttp/guzzle ~6.2.0
* barryvdh/laravel-debugbar 2.0.1
* barryvdh/laravel-elfinder 0.3.*
* bkwld/croppa ~4.2

## Requirement
* PHP >= 5.5.9
* MySQL >= 5.5

## Installation

1. Clone this repository (**`git clone git@gitlab.com:Suitmedia/suitcms.git --depth 1`**)
1. Create dan configure `.env` file based on `.env.example`
1. Run `composer install` in the root project to install all dependencies including develeopment requirement.
1. Run `php artisan key:generate` in the root project to generate new Key for new Application.
1. Run `php artisan cache:clear` in the root project to flush the application cache.
1. Run `php artisan config:cache` in the root project to create a cache file for faster configuration loading.
1. Run `php artisan migrate` in the root project to migrate main suitcms database.
1. Create username and password for admin `php artisan user:new-admin [your username] [your email] [your password]`
1. Try login in with url `/secret/login`
1. Done!

## Form Marco Collection

Detail about Macro collection can be viewed in `app/core/Suitcms/Support/macro.php`. Here are available form macro:

1. `suitOpen()` - Generate form html tag.
1. `suitModel()` - Generate Form html tag with laravel form model binding.
1. `suitSection()` - Generate section label.
1. `suitText()` - Generate text field and the label.
1. `suitNumber()` - Generate number field and the label.
1. `suitTextarea()` - Generate text area field and the label (default `rows` is `5`).
1. `suitPassword()` - Generate password field and the label.
1. `suitSelect()` - Generate select box field and the label.
1. `suitMultiSelect()` - Generate multi select field and the label.
1. `suitTokenField()` - Generate text field for token-like format and the label.
1. `suitFileInput()` - Generate file input field and the label.
1. `suitDate()` - Generate datepicker field and the label.
1. `suitDateTime()` - Generate date time picker field and the label.
1. `suitMap()` - Generate map field. (only one field with coma separated).
1. `suitFileBrowser()`- Generate text field with file browser ([elfinder][3]).
1. `suitWysiwyg()` - Generate WYSIWYG text editor (current default is from [ckeditor][2].
1. `suitSingleCheckbox()` - Generate checkbox and the label.
1. `suitSubmit()` - Generate submit button
1. `suitReset()` - Generate reset button
1. `suitBack()` - Generate back button


[1]: http://www.keenthemes.com/preview/index.php?theme=metronic   "Metronic Live Preview"
[2]: http://ckeditor.com/                                         "CKEditor"
[3]: http://elfinder.org/                                  "Elfinder"
