<?php

namespace App\API\Auth;

use App\Model\User;
use App\Services\AccessTokenService;

class UserAccess
{
    /**
     * User
     * @var \App\Model\User
     */
    protected $user;

    /**
     * Access Token Provider
     * @var \App\Services\AccessTokenService
     */
    protected $service;

    public function __construct(AccessTokenService $service, User $user = null)
    {
        $this->user = $user;
        $this->service = $service;
    }

    /**
     * Check if there is user access or not.
     * @return boolean
     */
    public function check()
    {
        return !($this->user() == null);
    }

    /**
     * Set User instance
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * Get user
     * @return \App\Model\User
     */
    public function user()
    {
        return $this->user;
    }

    /**
     * Attempt to authenticate user for access the App.
     *
     * @param string $accessToken
     * @return boolean
     */
    public function attempt($accessToken)
    {
        $user = $this->service->findUser($accessToken);
        if (!($user == null)) {
            $this->setUser($user);
            return true;
        }
        return false;
    }

    /**
     * Check user access right
     * @param  string  $access
     * @return boolean
     */
    public function hasAccess($access)
    {
        if ($this->check()) {
            return $this->user->hasAccess($access);
        }
        return false;
    }
}
