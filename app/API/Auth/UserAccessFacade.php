<?php

namespace App\API\Auth;

use Illuminate\Support\Facades\Facade as BaseFacade;

/**
 * @see \App\API\Auth\UserAccess
 */
class UserAccessFacade extends BaseFacade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'user-access';
    }
}
