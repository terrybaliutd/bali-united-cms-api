<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use GuzzleHttp\Client;
use App\Model\City;
use App\Model\Province;

class MapRajaongkirCityProvinceCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'rajaongkir:map';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Map the provinces and cities with data from rajaongkir api.";

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function __construct()
    {
        parent::__construct();
        $this->client = new Client(['base_uri' => env('RAJAONGKIR_URL', '')]);
    }

    public function fire()
    {
        $this->mapProvinces();
        $this->mapCities();
        $this->info('Synchronization Completed!!');
    }

    public function mapProvinces()
    {
        $response = $this->client->request(
            'GET',
            'province',
            [
                'headers' => [
                    'key' => env('RAJAONGKIR_API_KEY', '')
                ]
            ]
        );
        $stream = $response->getBody();
        $results = json_decode($stream->getContents(), true);

        $candidates = $results['rajaongkir']['results'];

        foreach (Province::all() as $province) {
            $shortest = -1;
            $provinceName = $province->name;
            $provinceName = trim(preg_replace("/\([^)]+\)/", "", $provinceName));
            $provinceName = str_replace(" ", "", $provinceName);

            if ($provinceName == "ACEH") {
                $closest = collect($candidates)->where('province', 'Nanggroe Aceh Darussalam (NAD)')->first();
            } else {
                foreach ($candidates as $candidate) {
                    $name = $candidate['province'];
                    $name = trim(preg_replace("/\([^)]+\)/", "", $name));
                    $name = str_replace(" ", "", $name);
                    $distance = levenshtein(strtolower($provinceName), strtolower($name), 1, 1, 1);
                    if ($distance == 0) {
                        $closest = $candidate;
                        $shortest = 0;
                        break;
                    }
                    if ($distance <= $shortest || $shortest < 0) {
                        $closest = $candidate;
                        $shortest = $distance;
                    }
                }
            }
            $province->rajaongkir_id = $closest['province_id'];
            $province->save();
            $this->info($province->name . " :: " . $closest['province'] . " | D = " . $shortest);
        }

        $this->info('Provinces synchronization complete!');
    }

    public function mapCities()
    {
        $response = $this->client->request(
            'GET',
            'city',
            [
                'headers' => [
                    'key' => env('RAJAONGKIR_API_KEY', '')
                ]
            ]
        );
        $stream = $response->getBody();
        $results = json_decode($stream->getContents(), true);

        $allCandidates = collect($results['rajaongkir']['results']);
        foreach (Province::all() as $province) {
            $cities = $province->cities;
            $candidates = $allCandidates->where('province_id', (string) $province->rajaongkir_id);
            foreach ($candidates as $candidate) {
                $name = $candidate['type'] . " " . $candidate['city_name'];
                $name = trim(preg_replace("/\([^)]+\)/", "", $name));
                $name = str_replace(" ", "", $name);

                if (strtolower($name) == "kabupatenpontianak") {
                    $closest = City::where('name', "KABUPATEN MEMPAWAH")->first();
                } else {
                    $shortest = -1;
                    foreach ($cities as $city) {
                        $cityName = str_replace(" ", "", $city->name);
                        $distance = levenshtein(strtolower($cityName), strtolower($name), 3, 3, 1);
                        if ($distance == 0) {
                            $closest = $city;
                            $shortest = 0;
                            break;
                        }
                        if ($distance <= $shortest || $shortest < 0) {
                            $closest = $city;
                            $shortest = $distance;
                        }
                    }
                }
                $closest->rajaongkir_id = $candidate['city_id'];
                $closest->save();
                $this->info($closest->name . " :: " . $candidate['city_name'] . " | D = " . $shortest);
            }
        }
        $this->info('Cities synchronization complete!');
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}
