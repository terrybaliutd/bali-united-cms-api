<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use App\Model\City;
use App\Model\User;
use App\Supports\CityProvinceFinder;

class MapUserAndCityCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'user:map-city';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Map User data with City data";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $count = 0;
        foreach (User::all() as $user) {
            $data = CityProvinceFinder::find($user);
            if ($data) {
                $user->city_id = $data['city_id'];
                $user->province_id = $data['province_id'];
                $user->save();
                $count += 1;
            }
        }
        $this->info($count . ' users data mapped!!');
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}
