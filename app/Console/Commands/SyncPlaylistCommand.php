<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use GuzzleHttp\Client;
use App\Model\Playlist;
use App\Model\User;

class SyncPlaylistCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'playlist:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Sync videos datas stored in this server's playlists with the datas in youtube server's.";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $user = User::where('email', $this->input->getArgument('email'))->first();
        if ($user) {
            $playlists = Playlist::all();
            foreach ($playlists as $playlist) {
                $playlist->synch($user);
            }
            $this->info('Playlists synchronized successfully!!');
        } else {
            $this->error('User not Found!!');
        }
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [
            ['email', InputArgument::REQUIRED, 'Admin\' email'],
        ];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}
