<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use GuzzleHttp\Client;

class SyncPointCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'user:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Sync users points stored in this server with the points stored in points server.";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $client = new Client(['base_uri' => env('POINT_API_URL')]);
        $res = $client->request('GET', 'api/users', [
            'headers' =>
            [ 'unique_code' => env('UNIQUE_CODE')]
        ]);

        $users = json_decode($res->getBody())->data;
        foreach ($users as $userRef) {
            $user = \App\Model\User::find($userRef->id);
            if ($user != null) {
                $user->points = $userRef->points;
                $user->save();
            }
        }

        $this->info('User points synchronized successfully!!');
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}
