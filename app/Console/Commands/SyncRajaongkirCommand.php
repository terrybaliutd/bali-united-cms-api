<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use GuzzleHttp\Client;
use App\Model\RajaongkirCity;
use App\Model\RajaongkirProvince;
use App\Model\RajaongkirSubdistrict;

class SyncRajaongkirCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'rajaongkir:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Sync the provinces and cities with rajaongkir api.";

    /**
     * Execute the console command.
     *
     * @return mixed
     */

    public function __construct()
    {
        parent::__construct();
        $this->client = new Client(['base_uri' => env('RAJAONGKIR_URL', '')]);
    }

    public function fire()
    {
        $this->syncProvinces();
        $this->syncCities();
        $this->syncSubdistricts();
        $this->info('Synchronization Completed!!');
    }

    public function syncProvinces()
    {
        $this->info("=== Synchronizing province data ===");
        $response = $this->client->request(
            'GET',
            'province',
            [
                'headers' => [
                    'key' => env('RAJAONGKIR_API_KEY', '')
                ]
            ]
        );
        $stream = $response->getBody();
        $results = json_decode($stream->getContents(), true);
        foreach ($results['rajaongkir']['results'] as $item) {
            $id = $item['province_id'];
            $name = $item['province'];
            RajaongkirProvince::updateOrCreate(
                ['id' => $id],
                ['name' => $name]
            );
        }
        $this->info('Provinces synchronization complete!');
    }

    public function syncCities()
    {
        $this->info("=== Synchronizing city data ===");
        $response = $this->client->request(
            'GET',
            'city',
            [
                'headers' => [
                    'key' => env('RAJAONGKIR_API_KEY', '')
                ]
            ]
        );
        $stream = $response->getBody();
        $results = json_decode($stream->getContents(), true);
        foreach ($results['rajaongkir']['results'] as $item) {
            $id = $item['city_id'];
            $name = $item['type'] . " " . $item['city_name'];
            $provinceId = $item['province_id'];
            RajaongkirCity::updateOrCreate(
                ['id' => $id],
                ['name' => $name, 'province_id' => $provinceId]
            );
        }
        $this->info('Cities synchronization complete!');
    }

    public function syncSubdistricts()
    {
        $this->info("=== Synchronizing subdistrict data ===");
        $count = 0;
        $total = RajaongkirCity::count();
        foreach (RajaongkirCity::all() as $city) {
            $response = $this->client->request(
                'GET',
                'subdistrict',
                [
                    'headers' => [
                        'key' => env('RAJAONGKIR_API_KEY', '')
                    ],
                    'query' => [
                        'city' => $city->id
                    ]
                ]
            );
            $stream = $response->getBody();
            $results = json_decode($stream->getContents(), true);
            foreach ($results['rajaongkir']['results'] as $item) {
                $id = $item['subdistrict_id'];
                $name = $item['subdistrict_name'];
                RajaongkirSubdistrict::updateOrCreate(
                    ['id' => $id],
                    ['name' => $name, 'city_id' => $city->id]
                );
            }
            $count += 1;
            $this->info('Processed '. $count . " from " . $total);
        }
        $this->info('Cities synchronization complete!');
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}
