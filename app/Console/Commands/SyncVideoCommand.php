<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;
use GuzzleHttp\Client;
use App\Model\Video;

class SyncVideoCommand extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'video:sync';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Sync videos datas stored in this server with the datas in in youtube server.";

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function fire()
    {
        $videos = Video::all();
        foreach ($videos as $video) {
            $info = Video::getInfo($video->key);
            if ($info == null) {
                continue;
            }
            $video->title = $info->title;
            $video->duration = $info->duration;
            $video->description = $info->description;
            $video->attachment = $info->thumbnails->high->url;
            $video->upload_date = (new \Carbon\Carbon($info->publishedAt))->setTimezone("Asia/Jakarta");
            $video->published = true;

            $video->save();
        }
        $this->info('Videos synchronized successfully!!');
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return [];
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return [];
    }
}
