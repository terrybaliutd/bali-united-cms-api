<?php

namespace App\Console\Commands;

use App\Model\Video;
use Illuminate\Console\Command;

class SyncVideoCommentLikeCount extends Command
{
    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'video:sync-comment-like-count';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = "Sync videos like count and comment count";
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the command.
     *
     * @return void
     */
    public function handle()
    {
        $videos = Video::get();
        foreach ($videos as $video) {
            $likeCount = $video->likes()->count();
            $commentCount = $video->comments()->count();
            $video->like_count = $likeCount;
            $video->comment_count = $commentCount;
            $video->save();
        }
    }
}
