<?php

namespace App\Console\Commands;

use App\Jobs\UpdateMatchStatus;
use App\Notifications\MatchPushNotification;
use Illuminate\Console\Command;

class UpdateMatchStatusCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'update:match-status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update current match status';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct(MatchPushNotification $notification = null)
    {
        parent::__construct();
        $this->notification = $notification;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $updateMatchStatus = new UpdateMatchStatus($this->notification);
        $updateMatchStatus->handle();

        \Log::info('Update match status completed.');
    }
}
