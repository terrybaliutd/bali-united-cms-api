<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Model\Video;
use App\Model\Playlist;
use App\Model\CitizenJournalVideo;

class UpdateVideoCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'video:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update video info';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \Log::info("UPDATE_VIDEO");
        $videos = Video::whereNull('attachment')->get();
        foreach ($videos as $video) {
            $info = Video::getInfo($video->key);
            if ($info) { // upload successful
                // Add video to Youtube playlist
                if (!empty(\Setting::get('citizen-journalism-playlist'))) {
                    \YoutubeOAuth::insertPlaylistItem([
                        'videoId' => $video->key,
                        'playlistId' => \Setting::get('citizen-journalism-playlist')
                    ]);
                }

                // Assign video to playlist
                $playlist = Playlist::where('key', \Setting::get('citizen-journalism-playlist'))->first();
                if ($playlist) {
                    $video->playlist_id = $playlist->id;
                }

                try {
                    $video->setVideoInfo($info); //update video info
                    $video->save();
                } catch (ResourceNotFoundException $e) { // video info not available yet
                    $video->save();
                }
            } else { // upload failed
                $citizenJournal = CitizenJournalVideo::where('video_id', $video->id)->first();
                if ($citizenJournal) { // set citizen journal video status back to accepted
                    $citizenJournal->status = 'accepted';
                    $citizenJournal->save();
                }
                $video->delete();
            }
        }
    }
}
