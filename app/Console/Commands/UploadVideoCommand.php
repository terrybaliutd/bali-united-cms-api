<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Model\CitizenJournalVideo;
use App\Jobs\UploadVideo;

class UploadVideoCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'video:upload';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Upload video';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \Log::info("UPLOAD_VIDEO");
        $acceptedVideos = CitizenJournalVideo::where('status', 'accepted')->get();
        foreach ($acceptedVideos as $video) {
            dispatch(new UploadVideo($video));
        }
    }
}
