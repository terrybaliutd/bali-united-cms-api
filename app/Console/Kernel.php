<?php
namespace App\Console;

use App\Console\Commands\AdminCommand;
use App\Console\Commands\SyncPointCommand;
use App\Console\Commands\SyncVideoCommand;
use App\Console\Commands\SyncPlaylistCommand;
use App\Console\Commands\SyncRajaongkirCommand;
use App\Console\Commands\MapRajaongkirCityProvinceCommand;
use App\Console\Commands\UpdateMatchStatusCommand;
use App\Console\Commands\UpdateVideoCommand;
use App\Console\Commands\UploadVideoCommand;
use App\Console\Commands\MapUserAndCityCommand;
use App\Console\Commands\SyncVideoCommentLikeCount;
use App\Model\Match;
use App\Model\Video;
use App\Notifications\LiveStreamingPushNotification;
use App\Notifications\MatchPushNotification;
use Carbon\Carbon;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{

    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        AdminCommand::class,
        SyncPointCommand::class,
        SyncVideoCommand::class,
        SyncPlaylistCommand::class,
        SyncRajaongkirCommand::class,
        UploadVideoCommand::class,
        UpdateVideoCommand::class,
        UpdateMatchStatusCommand::class,
        MapUserAndCityCommand::class,
        MapRajaongkirCityProvinceCommand::class,
        SyncVideoCommentLikeCount::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('video:upload')->everyThirtyMinutes();
        $schedule->command('video:update')->everyTenMinutes();
        $schedule->command('update:match-status')->everyMinute();
        $schedule->command('playlist:sync admin@admin.com')->daily();
        $schedule->command('rajaongkir:sync')->daily();
    }
}
