<?php

namespace App\Facades;

use App\Services\SafeDBService;
use Illuminate\Support\Facades\Facade;

class SafeDB extends Facade
{

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return SafeDBService::class;
    }
}
