<?php

namespace App\Http\Controllers\Admin;

use App\Model\BankAccount as Model;

class BankAccountController extends ResourceController
{
    protected $rules = [
        'bank_name' => 'required|string',
        'account_name' => 'required|string',
        'account_number' => 'required|string',
        'published' => 'required|in:0,1',
    ];

    public function __construct(Model $model)
    {
        parent::__construct($model);
    }
}
