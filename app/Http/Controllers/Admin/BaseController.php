<?php
namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Controllers\Extension\NameTrait;

abstract class BaseController extends Controller
{
    use NameTrait;

    /**
     * Custom view prefix
     * @var string
     */
    protected $viewPrefix;

    /**
     * Custom route prefix
     * @var string
     */
    protected $routePrefix;

    public function __construct()
    {
        parent::__construct();

        view()->share([
            'routePrefix' => $this->getRoutePrefix(),
            'viewPrefix' => $this->getViewPrefix(),
            'pageName' => $this->getPageName(),
            'nav' => $this->getControllerName(),
        ]);
    }

    /**
     * Get View Prefix. By default the value is plurar from and snake case of controller name
     * @return string
     */
    protected function getViewPrefix()
    {
        if ($this->viewPrefix !== null) {
            return $this->viewPrefix;
        }

        return str_plural(snake_case($this->getControllerName()));
    }

    /**
     * Get Route Prefix. By default the value is plurar from and snake case of controller name
     * @return string
     */
    protected function getRoutePrefix()
    {
        if ($this->routePrefix !== null) {
            return $this->routePrefix;
        }

        return str_plural(snake_case($this->getControllerName(), '-'));
    }
}
