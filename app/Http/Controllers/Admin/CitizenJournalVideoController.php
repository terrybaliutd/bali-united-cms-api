<?php

namespace App\Http\Controllers\Admin;

use App\Model\CitizenJournalVideo as Model;
use App\Model\User;
use Illuminate\Http\Request;

class CitizenJournalVideoController extends ResourceController
{
    public function __construct(Model $model)
    {
        parent::__construct($model);
    }

    protected function formData()
    {
        parent::formData();
        view()->share('users', User::lists('name', 'id'));
    }

    public function show($key)
    {
        $this->model = $this->find($key);
        if (empty($this->model)) {
            session()->flash(NOTIF_DANGER, 'Not Found!');
            return $this->redirectIndex();
        }
        $this->formData();

        return view(suitViewName($this->getViewPrefix().'.show'), ['model' => $this->model]);
    }

    public function accept(Request $request)
    {
        $video = Model::find($request->input('id'));
        if ($video->status == 'converted' || $video->status == 'rejected') {
            $video->status = 'accepted';
            $video->save();
            session()->flash(NOTIF_SUCCESS, 'Citizen journalism video accepted!');
            return $this->redirectIndex();
        } elseif ($video->status == 'uploaded') {
            session()->flash(NOTIF_DANGER, 'Citizen journalism video has not been converted!');
        } elseif ($video->status == 'accepted') {
            session()->flash(NOTIF_DANGER, 'Citizen journalism video has already been accepted!');
        } elseif ($video->status == 'published') {
            session()->flash(NOTIF_DANGER, 'Citizen journalism video has already been published!');
        }
        return redirect()->back();
    }

    public function reject(Request $request)
    {
        $video = Model::find($request->input('id'));
        if ($video->status == 'converted' || $video->status == 'accepted') {
            $video->status = 'rejected';
            $video->save();
            session()->flash(NOTIF_SUCCESS, 'Citizen journalism video rejected!');
            return $this->redirectIndex();
        } elseif ($video->status == 'uploaded') {
            session()->flash(NOTIF_DANGER, 'Citizen journalism video has not been converted!');
        } elseif ($video->status == 'rejected') {
            session()->flash(NOTIF_DANGER, 'Citizen journalism video has already been rejected!');
        } elseif ($video->status == 'published') {
            session()->flash(NOTIF_DANGER, 'Citizen journalism video has already been published!');
        }
        return redirect()->back();
    }
}
