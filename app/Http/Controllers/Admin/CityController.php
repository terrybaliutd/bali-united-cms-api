<?php

namespace App\Http\Controllers\Admin;

use App\Model\City as Model;
use App\Model\Province;

class CityController extends ResourceController
{
    /**
     * Custom view prefix
     * @var string
     */
    protected $viewPrefix;

    /**
     * Custom route prefix
     * @var string
     */
    protected $routePrefix;

    /**
     * Custom page name
     * @var string
     */
    protected $pageName;

    /**
     * Form Rules
     * @var array
     */
    protected $rules = [
        'code' => 'required|string|max:32',
        'name' => 'required|string|max:200',
        'province_id' => ''
    ];

    public function __construct(Model $model)
    {
        parent::__construct($model);
    }

    protected function beforeValidate()
    {
        parent::beforeValidate();
        // Put your form input preprocessing
        // e.g. $this->form->filterInput('trim', ['title', 'description']);
    }

    protected function formData()
    {
        parent::formData();
        // put your view data here
        // e.g. view()->share('list', $list);
        view()->share('provinces', Province::lists('name', 'id')->all());
    }

    protected function formRules()
    {
        // add strict form rules
        parent::formRules();
    }

    protected function doSave()
    {
        // Before all data being fill and save
        // Usually for belongsTo Relation

        $province = Province::find($this->form->all()['province_id']);
        if ($province) {
            $this->model->provice()->associate($province);
        }
        parent::doSave();

        // Put your logic after model being saved
    }
}
