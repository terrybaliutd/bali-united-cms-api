<?php

namespace App\Http\Controllers\Admin;

use App\Model\Club as Model;

class ClubController extends ResourceController
{
    protected $rules = [
        'api_id' => 'string|max:255',
        'name' => 'required|string|max:200',
        'nick_name' => 'string|max:100',
        'official_name' => 'required|string|max:200',
        'abbreviation' => 'string|max:5',
        'published' => 'required|boolean',
        'established_at' => 'date',
        'home_base' => 'string|max:200',
        'logo_url' => 'string',
        'manager' => 'string|max:200',
        'supporter_nick_name' => 'string|max:200',
        'stadium_name' => 'string|max:200',
    ];

    public function __construct(Model $model)
    {
        parent::__construct($model);
    }

    protected function beforeValidate()
    {
        parent::beforeValidate();
    }
}
