<?php

namespace App\Http\Controllers\Admin;

use App\Model\Comment as Model;

class CommentController extends ResourceController
{
    public function __construct(Model $model)
    {
        parent::__construct($model);
    }

    public function show($key)
    {
        $this->model = $this->find($key);
        if (empty($this->model)) {
            session()->flash(NOTIF_DANGER, 'Not Found!');
            return $this->redirectIndex();
        }
        $this->formData();

        return view(suitViewName($this->getViewPrefix().'.show'), ['model' => $this->model]);
    }
}
