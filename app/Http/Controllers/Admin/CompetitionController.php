<?php

namespace App\Http\Controllers\Admin;

use App\Model\Competition as Model;

class CompetitionController extends ResourceController
{
    protected $rules = [
        'name' => 'required|string|max:200',
        'long_name' => 'required|string|max:255',
        'order' => 'integer',
        'is_default' => 'required|boolean',
        'published' => 'required|boolean',
        'api_id' => 'string',
        'clubs' => 'array',
        'slug' => 'alpha_dash|max:255|unique:competitions,slug',
    ];

    public function __construct(Model $model)
    {
        parent::__construct($model);
    }
}
