<?php

namespace App\Http\Controllers\Admin;

use App\Model\CustomNotification as Model;

class CustomNotificationController extends ResourceController
{
    protected $rules = [
        'title' => 'required',
        'body' => 'required',
        'published' => 'required|in:0,1',
    ];

    public function __construct(Model $model)
    {
        parent::__construct($model);
    }
}
