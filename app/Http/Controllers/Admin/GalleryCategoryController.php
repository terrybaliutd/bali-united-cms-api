<?php

namespace App\Http\Controllers\Admin;

use App\Model\GalleryCategory as Model;
use App\Model\GalleryItem;

class GalleryCategoryController extends ResourceController
{
    protected $rules = [
        'image' => '',
        'parent_id' => '',
        'title' => 'required|max:100',
        'slug' => 'alpha_dash|unique:gallery_categories,slug',
        'published' => 'required|in:0,1',
        'items' => 'array',
        'items.*.id' => 'integer',
        'items.*.title' => 'required|max:100',
        'items.*.type' => 'required',
        'items.*.attachment' => 'required',
        'items.*.published' => 'required|in:0,1',
        'items.*.featured' => 'required|in:0,1',
        'items.*.tags' => '',
        'items.*.match_id' => 'integer',
        'items.*.match_id_manual' => 'integer',
        'items.*.club_tags' => 'array'
    ];

    public function __construct(Model $model)
    {
        parent::__construct($model);
    }

    protected function formData()
    {
        view()->share('parentList', $this->parentList()->toArray());
        view()->share('typeList', GalleryItem::getTypes());
    }

    protected function formRules()
    {
        $this->rules['parent_id'] .= 'in:' . $this->implode($this->parentList()->toArray());
        $this->rules['items.*.type'] .= '|in:' . $this->implode(array_keys(GalleryItem::getTypes()));
        parent::formRules();
    }

    protected function parentList()
    {
        $categories = Model::published();
        return $this->model->generateAdminCategoryList($categories);
    }

    protected function doSave()
    {
        $parentCategory = $this->model->find($this->form->input('parent_id'));
        $this->model->parent()->associate($parentCategory);
        parent::doSave();

        $galleryItems = [];
        if (array_key_exists('items', $this->form->all())) {
            foreach ($this->form->all()['items'] as $item) {
                if (array_key_exists('id', $item)) {
                    $galleryItems[] = $item['id'];
                    $rawItem = Gallery::find($item['id']);
                    if ($rawItem) {
                        $rawItem->fill($item);
                        $rawItem->save();
                        $this->model->galleries()->save($rawItem);
                    }
                } else {
                    $rawItem = Gallery::create($item);
                    $this->model->galleries()->save($rawItem);
                    $galleryItems[] = $rawItem->id;
                    $rawItem->save();
                }
            }
        }
        Gallery::where('gallery_category_id', $this->model->id)->whereNotIn('id', $galleryItems)->delete();
    }
}
