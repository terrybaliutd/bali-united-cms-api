<?php

namespace App\Http\Controllers\Admin;

use App\Model\HtmlTemplate as Model;
use Illuminate\Support\Collection;

class HtmlTemplateController extends ResourceController
{
    protected $rules = [
        'title' => 'required|string|max:255',
        'description' => 'required|string|max:255',
        'html' => 'required|string'
    ];

    public function __construct(Model $model)
    {
        parent::__construct($model);
    }

    protected function beforeValidate()
    {
        parent::beforeValidate();
        $this->form->filterInput('trim', ['title', 'description']);
    }

    public function templateList()
    {
        $data = $this->model->orderBy('title', 'asc')->get(['title', 'description', 'id']);

        $res = new Collection;
        foreach ($data as $template) {
            $res->push([
                'title' => $template->title,
                'description' => $template->description,
                'url' => suitRoute('template.detail', $template)
            ]);
        }

        return $res;
    }

    public function template($key)
    {
        $this->model = $this->find($key);

        return response($this->model->html);
    }
}
