<?php

namespace App\Http\Controllers\Admin;

use App\Model\Club;
use App\Model\Competition;
use App\Model\Match as Model;
use App\Model\MatchClubInfo;
use App\Model\MatchHighlight;
use App\Model\MatchStatistic;
use App\Model\MatchLineUp;
use App\Model\Membership;
use App\Model\Player;
use App\Model\Season;

class MatchController extends ResourceController
{
    protected $rules = [
        'highlights' => 'array',
        'status' => 'string',
        'venue' => 'string',
        'additional_info' => 'string',
        'tv_info' => 'string',
        'referees' => 'string',
        'audience' => 'integer|min:0',
        'weather_info' => 'string|max:255',
        'preview_match_text' => 'string|max:65535',
        'post_match_text' => 'string|max:65535',
        'start_at' => 'required|date',
        'suspended_players' => 'string',
        'banner' => '',
        'ticket_url' => 'url',
        'live_video_id' => '',
        'competition_id' => 'required|string',
        'season_id' => 'required|string',
        'prediction_home_win' => 'integer|min:0',
        'prediction_draw' => 'integer|min:0',
        'prediction_away_win' => 'integer|min:0',
        'published' => 'required|in:0,1',
        // Home club rules
        'home_club_id' => 'required',
        'home_coach' => 'string',
        'home_formation' => 'string',
        'home_score' => 'integer|min:0',
        'home_lineups' => 'array',
        'home_lineups.*.player_name' => 'required|string',
        'home_lineups.*.position_code' => 'required|string|max:5',
        'home_lineups.*.shirt_number' => 'required|integer',
        'home_lineups.*.is_primary' => 'required|in:0,1',
        //statistic
        'home_statistic_corner' => 'integer|min:0',
        'home_statistic_foul' => 'integer|min:0',
        'home_statistic_offside' => 'integer|min:0',
        'home_statistic_red_card' => 'integer|min:0',
        'home_statistic_yellow_card' => 'integer|min:0',
        'home_statistic_tackle_success' => 'integer|min:0',
        'home_statistic_passing_accuracy' => 'integer|min:0',
        'home_statistic_shot_on_goal' => 'integer|min:0',
        'home_statistic_total_shots' => 'integer|min:0',
        'home_statistic_ball_possession' => 'integer|min:0',
        // Away club rules
        'away_club_id' => 'required',
        'away_coach' => 'string',
        'away_formation' => 'string',
        'away_score' => 'integer|min:0',
        'away_lineups' => 'array',
        'away_lineups.*.player_name' => 'required|string',
        'away_lineups.*.position_code' => 'required|string|max:5',
        'away_lineups.*.shirt_number' => 'required|integer',
        'away_lineups.*.is_primary' => 'required|in:0,1',
        //statistic
        'away_statistic_corner' => 'integer|min:0',
        'away_statistic_foul' => 'integer|min:0',
        'away_statistic_offside' => 'integer|min:0',
        'away_statistic_red_card' => 'integer|min:0',
        'away_statistic_yellow_card' => 'integer|min:0',
        'away_statistic_tackle_success' => 'integer|min:0',
        'away_statistic_passing_accuracy' => 'integer|min:0',
        'away_statistic_shot_on_goal' => 'integer|min:0',
        'away_statistic_total_shots' => 'integer|min:0',
        'away_statistic_ball_possession' => 'integer|min:0',
        // Highlight rules
        'highlights.*.id' => 'integer',
        'highlights.*.time_in_minute' => '',
        'highlights.*.time_in_second' => '',
        'highlights.*.event_description' => '',
        'highlights.*.player_name' => 'required|string',
        'highlights.*.support_player_name' => 'string',
        'highlights.*.stage_name' => '',
        'highlights.*.club_type' => ''
    ];

    public function __construct(Model $model)
    {
        parent::__construct($model);

        view()->share('competitions', Competition::orderBy('is_default', 'desc')->lists('long_name', 'id'));
        view()->share('clubs', Club::published()->lists('name', 'id'));
        view()->share('position_codes', Player::POSISTION_CODES);
        view()->share('seasons', Season::lists('name', 'id'));
    }

    protected function formData()
    {
        view()->share('statusList', Model::getStatusList());
        view()->share('events', MatchHighlight::getCodes());
    }

    protected function formRules()
    {
        $this->rules['status'] .= '|in:' . $this->implode(Model::getStatusList());
        parent::formRules();
    }

    protected function doSave()
    {
        parent::doSave();

        $key = $this->model->getKeyName();
        $highlightIds = [];

        if (array_key_exists('highlights', $this->form->all())) {
            foreach ($this->form->all()['highlights'] as $highlight) {
                $rawHighlight = null;

                if (array_key_exists($key, $highlight)) {
                    $rawHighlight = MatchHighlight::find($highlight[$key]);
                    if ($rawHighlight == null) {
                        continue;
                    }
                } else {
                    $rawHighlight = new MatchHighlight();
                    if ($this->model->status == Model::STATUS_PLAYING) {
                        $this->form = $this->updateMatchScore($this->form, $highlight);
                        $rawHighlight->sendNotification($highlight, $this->model->getKey());
                    }
                }

                $rawHighlight->fill($highlight + ['match_id' => $this->model->getKey()]);
                $rawHighlight->save();

                $this->model->highlights()->save($rawHighlight);
                $highlightIds[] = $rawHighlight->id;
            }
        }

        $this->model->highlights()->whereNotIn($key, $highlightIds)->delete();

        $matchClubHome = $this->getMatchClubInfoObject('home_id');
        $matchClubHome = $matchClubHome->saveWithMatch(
            $this->form->all(),
            'home'
        );
        $this->model->clubInfos()->save($matchClubHome);

        //save statistic
        $matchStatisticHome = $this->getMatchStatisticObject('home_id');

        $matchStatisticHome = $matchStatisticHome->saveWithClubInfo(
            $this->form->all() + ['match_club_info_id' => $this->model->homeClub->getKey()],
            'home'
        );

        $this->processMatchLineUp($matchClubHome, 'home');


        $matchClubAway = $this->getMatchClubInfoObject('away_id');
        $matchClubAway = $matchClubAway->saveWithMatch(
            $this->form->all(),
            'away'
        );
        $this->model->clubInfos()->save($matchClubAway);

        //save statistic
        $matchStatisticAway = $this->getMatchStatisticObject('away_id');
        $matchStatisticAway = $matchStatisticAway->saveWithClubInfo(
            $this->form->all() + ['match_club_info_id' => $this->model->awayClub->getKey()],
            'away'
        );
        $this->processMatchLineUp($matchClubAway, 'away');
    }

    protected function getMatchClubInfoObject($key)
    {
        if (array_key_exists($key, request()->all())) {
            return MatchClubInfo::find(request()->{$key});
        }
        return app(MatchClubInfo::class);
    }

    protected function getMatchStatisticObject($key)
    {
        if (array_key_exists($key, request()->all())) {
            $statistic = MatchClubInfo::find(request()->{$key})->statistic()->get()->first();
            if ($statistic !== null) {
                return $statistic;
            } else {
                return app(MatchStatistic::class);
            }
        }
        return app(MatchStatistic::class);
    }

    protected function processMatchLineUp($clubInfo, $clubType)
    {
        $clubType = $clubType . "_lineups";
        $key = $clubInfo->getKeyName();
        if (array_key_exists($clubType, $this->form->all())) {
            foreach ($this->form->all()[$clubType] as $lineup) {
                $rawLineup = null;

                if (array_key_exists($key, $lineup)) {
                    $rawLineup = MatchLineUp::find($lineup[$key]);
                    if ($rawLineup == null) {
                        continue;
                    }
                } else {
                    $rawLineup = new MatchLineUp();
                }

                $rawLineup->fill($lineup);

                $clubInfo->lineups()->save($rawLineup);
                $lineupIds[] = $rawLineup->id;
            }
            $clubInfo->lineups()->whereNotIn($key, $lineupIds)->delete();
        }
    }

    private function updateMatchScore($request, $highlight)
    {
        $homeScore = $request->all()['home_score'];
        $awayScore = $request->all()['away_score'];
        if ($highlight['event_description'] == 'goal') {
            if ($highlight['club_type'] == 'home') {
                $request->merge(['home_score' => ($homeScore + 1)]);
            } else {
                $request->merge(['away_score' => ($awayScore + 1)]);
            }
        } elseif ($highlight['event_description'] == 'own_goal') {
            if ($highlight['club_type'] == 'home') {
                $request->merge(['away_score' => ($awayScore + 1)]);
            } else {
                $request->merge(['home_score' => ($homeScore + 1)]);
            }
        }
        return $request;
    }
}
