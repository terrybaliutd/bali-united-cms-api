<?php

namespace App\Http\Controllers\Admin;

use App\Model\Membership as Model;

class MembershipController extends ResourceController
{
    protected $rules = [
        'name' => 'required|string',
        'prefix' => 'required|string',
        'ribbon_path' => 'required|string',
        'text_label' => 'required|string',
    ];

    public function __construct(Model $model)
    {
        parent::__construct($model);
    }
}
