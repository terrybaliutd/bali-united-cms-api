<?php

namespace App\Http\Controllers\Admin;

use App\Model\Membership;
use App\Model\News as Model;

class NewsController extends ResourceController
{
    protected $rules = [
        'slug' => 'alpha_dash',
        'published' => 'required|in:0,1',
        'featured' => 'required|in:0,1',
        'news_date' => 'required',
        'title' => 'required',
        'description' => '',
        'content' => '',
        'image' => '',
        'tags' => '',
        'attachment_file' => '',
        'match_id' => 'integer',
        'membership_id' => 'integer',
    ];

    public function __construct(Model $model)
    {
        parent::__construct($model);
        view()->share('memberships', Membership::lists('name', 'id'));
    }
}
