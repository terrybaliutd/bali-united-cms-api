<?php

namespace App\Http\Controllers\Admin;

use App\Model\NewsListAds as Model;

class NewsListAdsController extends ResourceController
{
    protected $rules = [
        'attachment' => 'required',
        'title' => 'required|string',
        'url' => 'required|url',
        'priority' => 'required|integer',
        'published' => 'required|in:0,1',
    ];

    public function __construct(Model $model)
    {
        parent::__construct($model);
    }
}
