<?php

namespace App\Http\Controllers\Admin;

use App\Model\Notification as Model;

class NotificationController extends ResourceController
{
    protected $rules = [
        'latest_match' => 'required|boolean',
        'upcoming_match' => 'required|boolean',
        'new_news' => 'required|boolean',
        'new_video' => 'required|boolean',
        'live_video' => 'required|boolean',
    ];

    public function __construct(Model $model)
    {
        parent::__construct($model);
    }
}
