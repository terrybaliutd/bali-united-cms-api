<?php

namespace App\Http\Controllers\Admin;

use App\Model\Player as Model;

class PlayerController extends ResourceController
{
    protected $rules = [
        'api_id' => '',
        'name' => 'required|string|max:200',
        'short_name' => 'required|string|max:100',
        'position_code' => 'required|string|max:5',
        'shirt_number' => 'required|integer',
        'birth_place' => 'string',
        'birth_date' => 'date',
        'nationality' => 'string|size:2',
        'weight' => 'integer|min:0',
        'height' => 'integer|min:0',
        'photo_profile' => 'string',
        'photo_action' => 'string',

        'slug' => 'alpha_dash|unique:players,slug',
        'published' => 'required|in:0,1',
    ];

    public function __construct(Model $model)
    {
        parent::__construct($model);
        view()->share('position_codes', Model::POSISTION_CODES);
    }
}
