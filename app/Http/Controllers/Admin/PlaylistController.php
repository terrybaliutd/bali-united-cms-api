<?php

namespace App\Http\Controllers\Admin;

use App\Model\Playlist as Model;

class PlaylistController extends ResourceController
{
    protected $rules = [
        'key' => 'required|string',
        'title' => 'required|string',
        'published' => 'required|in:0,1',
        'description' => 'string',
        'attachment' => 'required',
        'position' => 'integer',
    ];

    public function __construct(Model $model)
    {
        parent::__construct($model);
    }

    public function synchronize($playlistId)
    {
        $playlist = Model::where("id", $playlistId)->first();
        $result = $playlist->synch(\Auth::user());
        return \Redirect::back()->with(
            NOTIF_SUCCESS,
            "Total Videos in Playlist : " . $result['total'] . ",  New Videos : " . $result['new']
        );
    }

    public function getInfo()
    {
        $url = \Request::input('url');
        if (empty($this->model)) {
            session()->flash(NOTIF_DANGER, 'URL Empty!');
            return \Redirect::back();
        }
        $this->model = Model::createFromUrl($url);
        if (empty($this->model)) {
            session()->flash(NOTIF_DANGER, 'Playlist Not Found!');
            return \Redirect::back()->withInput();
        }

        $this->formData();

        return view(suitViewName($this->getViewPrefix().'.edit'), ['model' => $this->model, 'url' => $url]);
    }
}
