<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests;
use App\Model\Post;
use App\Model\PostComment as Model;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\Request;

class PostCommentController extends ResourceController
{
    public function __construct(Model $model)
    {
        parent::__construct($model);
    }

    protected $rules = [
        'user_id' => 'required|integer',
        'post_id' => 'required|integer',
        'parent_id' => 'integer',
        'content' => 'required|string',
    ];

    protected function indexByPost($key)
    {
        $post = Post::find($key);
        if (empty($post)) {
            session()->flash(NOTIF_DANGER, "Post not found.");
            return $this->redirectToPostsIndex();
        }

        if (\Input::ajax()) {
            $searchBy = request('search.value');
            $orderBy = new Collection(request('order'));
            $orderBy = $orderBy->lists('dir', 'column');
            $columnList = new Collection(request('columns'));
            $columnList = $columnList->lists('name', 'data');
            foreach ($orderBy as $key => $order) {
                if (isset($columnList[$key])) {
                    $orderBy[$columnList[$key]] = $order;
                }
                unset($orderBy[$key]);
            }

            $length = request('length', $this->defaultRow);
            if ($length < 0) {
                $length = $this->paginateQuery()->count();
            }
            $scopes = [];
            foreach (request()->query() as $key => $value) {
                if (starts_with($key, '__')) {
                    $key = ltrim($key, '_');
                    $scopes[$key] = $value;
                }
            }

            $models = $this->model->where('post_id', $post->getKey())
                ->newQuery()
                ->filter($searchBy, $orderBy, $scopes)
                ->newestCreated()
                ->paginate($length, [$this->model->getTable() . '.*']);

            $total = $this->model->where('post_id', $post->getKey())
                ->newQuery()
                ->count();

            return response()->view(
                suitViewName($this->getViewPrefix() . ".index-ajax"),
                compact('models', 'post', 'total'),
                200,
                ["Content-Type" => "application/json"]
            );
        }

        view()->share('post', $post);

        return view(suitViewName($this->getViewPrefix() . ".index"));
    }

    protected function createByPost($key)
    {
        $post = Post::find($key);
        $user = \Auth::user();
        if (empty($post)) {
            session()->flash(NOTIF_DANGER, 'Post not found.');
            return $this->redirectToPostsIndex();
        }

        view()->share([
            'post' => $post
        ]);

        return view(suitViewName($this->getViewPrefix() . ".create"), [
            'model' => $this->model,
            'user' => $user,
            'postComments' => $post->postComments()->lists('content', 'id')
        ]);
    }

    protected function store()
    {
        $post = Post::find(request('post_id'));
        if (empty($post)) {
            session()->flash(NOTIF_DANGER, 'Post not found.');
            return $this->redirectToPostsIndex();
        }

        $this->beforeValidate();
        if (!$this->form->validate()) {
            return redirect()->back();
        }
        $this->afterValidate();

        $this->model = $this->model->newInstance();
        $this->model->fill($this->form->all());

        $this->doSave();

        return redirect()->to(suitRoute("post-comments.index", $post));
    }

    public function edit($key)
    {
        $this->model = $this->find($key);
        $user = \Auth::user();
        if (empty($this->model)) {
            session()->flash(NOTIF_DANGER, 'Not Found!');
            return redirect()->to(\URL::previous());
        }

        view()->share([
            'post' => $this->model->post,
            'user' => $user
        ]);

        $this->formData();

        return view(suitViewName($this->getViewPrefix() . '.edit'), ['model' => $this->model]);
    }

    protected function update($key)
    {
        $this->model = $this->find($key);

        $this->beforeValidate();
        if (!$this->form->validate()) {
            return redirect()->back();
        }
        $this->afterValidate();

        $this->model->fill($this->form->all());
        $this->doSave();

        session()->flash(NOTIF_SUCCESS, '' . $this->getControllerName() . ' information Updated.');
        return redirect()->to(suitRoute("post-comments.index", $this->model->post));
    }

    protected function destroy($key)
    {
        $this->model = $this->find($key);
        $post = $this->model->post;

        if (empty($this->model)) {
            session()->flash(NOTIF_DANGER, 'Not Found!');
            return redirect()->back();
        }

        $this->doDelete();
        session()->flash(NOTIF_SUCCESS, '' . ucfirst($this->getEntityName()) . ' information deleted.');
        return redirect()->to(suitRoute('post-comments.index', $post));
    }

    protected function getEntityName()
    {
        $cntName = $this->getControllerName();
        $entity = preg_replace('/(?<=\\w)(?=[A-Z])/', " $1", $cntName);
        $entity = trim($entity);
        $entity = strtolower($entity);

        return $entity;
    }

    protected function redirectToPostsIndex()
    {
        return redirect()->route(suitRouteName('posts.index'));
    }
}
