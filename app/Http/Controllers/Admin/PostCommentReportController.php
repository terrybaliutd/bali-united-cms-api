<?php

namespace App\Http\Controllers\Admin;

use App\Model\PostCommentReport;

class PostCommentReportController extends ResourceController
{
    public function __construct(PostCommentReport $model)
    {
        parent::__construct($model);
    }
}
