<?php

namespace App\Http\Controllers\Admin;

use App\Model\Post as Model;
use App\Model\PostTag;
use App\Model\Tag;
use App\Model\User;

class PostController extends ResourceController
{
    protected $rules = [
        'owner_id' => 'required|integer',
        'type' => 'required|string',
        'title' => 'required|string',
        'slug' => 'string',
        'image' => '',
        'excerpt' => 'string',
        'content' => 'string',
        'video_key' => 'string',
        'published' => 'required|in:0,1',
        'tags' => 'array',
        'video_status' => 'string',
        'video_status_description' => 'string'
    ];

    public function __construct(Model $model)
    {
        parent::__construct($model);
        view()->share('types', $this->model->getTypeOptions());
        view()->share('users', User::lists('name', 'id'));
        view()->share('tags', Tag::lists('name', 'id'));
    }

    protected function store()
    {
        $this->beforeValidate();
        if (!$this->form->validate()) {
            return redirect()->back();
        }
        $this->afterValidate();
        $this->model = $this->model->newInstance();
        $this->model->fill($this->form->all());

        $this->doSave();
        $this->setTags($this->model, $this->form->get('tags'));

        session()->flash(NOTIF_SUCCESS, 'New ' . $this->getControllerName() . ' information created.');
        return redirect(route('backend.posts.show', [$this->model]));
    }

    protected function update($key)
    {
        $this->model = $this->find($key);

        $this->beforeValidate();
        if (!$this->form->validate()) {
            return redirect()->back();
        }
        $this->afterValidate();

        $this->model->fill($this->form->all());
        $this->doSave();
        $this->deleteTags($this->model);
        $this->setTags($this->model, $this->form->get('tags'));

        session()->flash(NOTIF_SUCCESS, '' . $this->getControllerName() . ' information Updated.');
        return redirect(route('backend.posts.show', [$key]));
    }

    protected function show($key)
    {
        $post = $this->find($key);
        if (empty($post)) {
            session()->flash(NOTIF_DANGER, 'Post not found.');
            return $this->redirectIndex();
        }
        $postTags = $post->postTags()->get()->implode('name', ', ');

        return view(suitViewName($this->getViewPrefix() . '.show'), [
            'post' => $post,
            'postTags' => $postTags
        ]);
    }

    public function edit($key)
    {
        $this->model = $this->find($key);
        if (empty($this->model)) {
            session()->flash(NOTIF_DANGER, 'Not Found!');
            return $this->redirectIndex();
        }
        $this->formData();
        $this->model['tags'] = array_pluck($this->model->postTags()->get(), 'id');
        return view(suitViewName($this->getViewPrefix() . '.edit'), ['model' => $this->model]);
    }

    private function setTags($post, $tags)
    {
        if ($tags) {
            foreach ($tags as $tag) {
                $postTag = new PostTag();
                $postTag->post_id = $post->id;
                $postTag->tag_id = $tag;
                $postTag->save();
            }
        }
    }

    private function deleteTags($post)
    {
        PostTag::where('post_id', $post->id)->delete();
    }
}
