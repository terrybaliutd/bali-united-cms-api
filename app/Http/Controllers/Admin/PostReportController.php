<?php

namespace App\Http\Controllers\Admin;

use App\Model\PostReport;

class PostReportController extends ResourceController
{
    public function __construct(PostReport $model)
    {
        parent::__construct($model);
    }
}
