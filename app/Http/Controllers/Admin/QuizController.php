<?php

namespace App\Http\Controllers\Admin;

use App\Model\Quiz as Model;

class QuizController extends ResourceController
{
    protected $rules = [
        'video_id' => 'required|integer',
        'title' => 'required|string|max:255',
        'description' => 'required|string',
        'question' => 'required|string',
        'image'=> '',
        'option_1' => 'required|string|max:255',
        'option_2' => 'required|string|max:255',
        'option_3' => 'required|string|max:255',
        'option_4' => 'string|max:255',
        'answer' => 'integer|in:1,2,3,4',
        'transaction_type_id' => 'required|integer',
        'published' => 'integer|in:0,1'
    ];

    public function __construct(Model $model)
    {
        parent::__construct($model);
    }

    protected function paginateQuery()
    {
        return parent::paginateQuery()->orderBy('video_id');
    }

    protected function formData()
    {
        parent::formData();
        view()->share('videos', \App\Model\Video::lists('title', 'id'));
        view()->share('transaction_types', $this->model->transactionTypeList());
    }

    protected function formRules()
    {
        $this->rules['video_id'] .= '|in:' . implode(",", \App\Model\Video::lists('id')->toArray());
        parent::formRules();
    }

    protected function doSave()
    {
        parent::doSave();
        $this->model->point = $this->model->getTransactionType();
        $this->model->save();
    }

    public function import()
    {
        $file = request()->file('file');
        $datas = \Excel::load($file, function () {
        })->get();

        $transTypes = (new Model)->transactionTypeList();
        $minPoint = min($transTypes);
        $minId = array_search($minPoint, $transTypes);

        $count = 0;
        foreach ($datas as $data) {
            $video = \App\Model\Video::where('title', $data->video)->first();
            if ($video) {
                if (Model::where('title', $data->title)->where('video_id', $video->id)->first()) {
                    continue;
                }

                $pointId = array_search($data->point, $transTypes);

                if (!$pointId) {
                    $pointId = $minId;
                }

                $count++;
                $quiz = new Model;
                $quiz->video()->associate($video);
                $quiz->title = $data->title;
                $quiz->description = $data->description;
                $quiz->question = $data->question;
                $quiz->option_1 = $data->option_1;
                $quiz->option_2 = $data->option_2;
                $quiz->option_3 = $data->option_3;
                $quiz->option_4 = $data->option_4;
                $quiz->answer = $data->correct_answer_the_option_number_that_represents_the_correct_answer_1_2_3_or_4;
                $quiz->transaction_type_id = $pointId;
                $quiz->point = $transTypes[$pointId];
                $quiz->published = false;
                $quiz->save();
            }
        }
        return Redirect(route('backend.quizzes.index'))->with(
            NOTIF_SUCCESS,
            "Total Quiz in Excel : " . count($datas). ",  New Quiz : " . $count
        );
    }

    public function importForm()
    {
        return view(suitViewName($this->getViewPrefix().'.import'));
    }
}
