<?php

namespace App\Http\Controllers\Admin;

use App\Model\QuizUser as Model;

class QuizUserController extends ResourceController
{
    public function __construct(Model $model)
    {
        parent::__construct($model);
    }

    protected function paginateQuery()
    {
        return parent::paginateQuery()->orderBy('quiz_id');
    }
}
