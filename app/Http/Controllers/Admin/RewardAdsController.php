<?php

namespace App\Http\Controllers\Admin;

use App\Model\RewardAds as Model;

class RewardAdsController extends ResourceController
{
    protected $rules = [
        'title' => 'required|string',
        'url' => 'required|string',
        'image' => 'required',
        'priority' => 'required|integer',
        'published' => 'required|in:0,1',
    ];

    public function __construct(Model $model)
    {
        parent::__construct($model);
    }
}
