<?php

namespace App\Http\Controllers\Admin;

use App\Model\Competition;
use App\Model\Season as Model;

class SeasonController extends ResourceController
{
    protected $rules = [
        'name' => 'required|string|max:200',
        'order' => 'integer',
        'is_default' => 'required|boolean',
        'published' => 'required|boolean',
        'competitions' => 'array',
        'competitions.*' => '',
        'slug' => 'alpha_dash|max:255|unique:competitions,slug',
    ];

    public function __construct(Model $model)
    {
        parent::__construct($model);
        view()->share('competitions', Competition::lists('long_name', 'id'));
    }

    protected function formRules()
    {
        $this->rules['competitions.*'] .= 'in:' . $this->implode(Competition::lists('id')->all());
        parent::formRules();
    }

    protected function doSave()
    {
        parent::doSave();
        $this->model->competitions()->sync($this->form->all()['competitions']);
    }

    public function getSeasonByCompetition()
    {
        $competitionId = request()->competition_id;

        return Competition::find($competitionId)->seasons;
    }
}
