<?php

namespace App\Http\Controllers\Admin;

use App\Model\Club;
use App\Model\Competition;
use App\Model\Season;
use App\Model\Standing as Model;

class StandingController extends ResourceController
{
    protected $rules = [
        'club_id' => 'required|integer',
        'competition_id' => 'required|integer',
        'season_id' => 'required|integer',
        'position' => 'integer',
        'matches' => 'integer',
        'goal' => 'integer',
        'goal_conceded' => 'integer',
        'goal_diff' => 'integer',
        'win' => 'integer',
        'lose' => 'integer',
        'draw' => 'integer',
        'point' => 'integer',
        'priority' => 'integer',
        'published' => 'required|in:0,1'
    ];

    public function __construct(Model $model)
    {
        parent::__construct($model);
        view()->share('competitions', Competition::lists('long_name', 'id'));
        view()->share('seasons', Season::lists('name', 'id'));
        view()->share('clubs', Club::published()->lists('name', 'id'));
    }
}
