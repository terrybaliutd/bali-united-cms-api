<?php

namespace App\Http\Controllers\Admin;

use App\Model\StickerCategory as Model;
use App\Model\StickerItem;

class StickerCategoryController extends ResourceController
{
    protected $rules = [
        'title' => 'required|max:255',
        'slug' => 'alpha_dash|unique:sticker_categories,slug',
        'image' => 'required',
        'expired_date' => 'required',
        'published' => 'required|in:0,1',
        'items' => 'array',
        'items.*.id' => 'integer',
        'items.*.title' => 'required|max:255',
        'items.*.image' => 'required',
    ];

    public function __construct(Model $model)
    {
        parent::__construct($model);
    }

    protected function formRules()
    {
        if ($this->model->exists) {
            foreach (['slug'] as $key) {
                $this->rules[$key] .= ','.$this->model->getKey();
            }
        }
        parent::formRules();
    }

    protected function doSave()
    {
        parent::doSave();

        $galleryItems = [];
        if (array_key_exists('items', $this->form->all())) {
            foreach ($this->form->all()['items'] as $item) {
                if (array_key_exists('id', $item)) {
                    $galleryItems[] = $item['id'];
                    $rawItem = StickerItem::find($item['id']);
                    if ($rawItem) {
                        $rawItem->fill($item);
                        $rawItem->save();
                        $this->model->items()->save($rawItem);
                    }
                } else {
                    $rawItem = $this->model->items()->getRelated();
                    $rawItem->fill($item);
                    $rawItem->category()->associate($this->model);
                    $rawItem->save();
                    $galleryItems[] = $rawItem->id;
                }
            }
        }
        StickerItem::where('sticker_category_id', $this->model->id)->whereNotIn('id', $galleryItems)->delete();
    }
}
