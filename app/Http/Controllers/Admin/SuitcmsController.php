<?php
namespace App\Http\Controllers\Admin;

class SuitcmsController extends BaseController
{

    /**
     * Create a new Suitcms Controller
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();  //Invoke Base Controller constructor
    }

    public function index()
    {
        view()->share('navDashboard', true);
        return \View::make(suitViewName('index'));
    }
}
