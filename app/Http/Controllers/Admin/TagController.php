<?php

namespace App\Http\Controllers\Admin;

use App\Model\Tag as Model;

class TagController extends ResourceController
{
    protected $rules = [
        'name' => 'required|string',
        'description' => 'string',
    ];

    public function __construct(Model $model)
    {
        parent::__construct($model);
    }
}
