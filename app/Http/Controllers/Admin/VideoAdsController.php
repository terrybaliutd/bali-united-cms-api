<?php

namespace App\Http\Controllers\Admin;

use App\Model\Membership;
use App\Model\VideoAds as Model;

class VideoAdsController extends ResourceController
{
    protected $rules = [
        'key' => 'required|string',
        'duration' => 'required|string',
        'title' => 'required|string',
        'published' => 'required|in:0,1',
        'attachment' => 'required',
        'membership_id' => '',
        'description' => 'string',
        'priority' => 'required|integer',
    ];

    public function __construct(Model $model)
    {
        parent::__construct($model);
        view()->share('memberships', Membership::lists('name', 'id'));
    }

    public function getInfo()
    {
        $url = \Request::input('url');
        if (empty($url)) {
            session()->flash(NOTIF_DANGER, 'URL Empty!');
            return \Redirect::back();
        }

        $this->model = Model::createFromUrl($url);
        if (empty($this->model)) {
            session()->flash(NOTIF_DANGER, 'Video Not Found!');
            return \Redirect::back()->withInput();
        }

        $this->formData();

        return view(suitViewName($this->getViewPrefix().'.edit'), ['model' => $this->model, 'url' => $url]);
    }
}
