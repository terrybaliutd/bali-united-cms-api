<?php

namespace App\Http\Controllers\Admin;

use App\Model\Match;
use App\Model\Membership;
use App\Model\Playlist;
use App\Model\Video as Model;

class VideoController extends ResourceController
{
    protected $rules = [
        'key' => 'required|string|unique:videos,key',
        'playlist_id' => 'numeric',
        'match_id' => 'integer',
        'duration' => 'required|string',
        'title' => 'required|string',
        'featured' => 'required|in:0,1',
        'published' => 'required|in:0,1',
        'attachment' => 'required',
        'description' => 'string',
        'is_live' => 'in:0,1',
        'start_time' => 'date',
        'upload_date' => 'date',
        'membership_id' => 'integer',
    ];

    public function __construct(Model $model)
    {
        parent::__construct($model);
        view()->share([
            'playlists' => Playlist::lists('title', 'id'),
            'matches' => Match::with('competition', 'season', 'clubInfos', 'clubInfos.club')->get()->pluck('name', 'id'),
            'memberships' => Membership::lists('name', 'id')
        ]);
    }

    protected function formRules()
    {
        if ($this->model->exists) {
            foreach (['key'] as $key) {
                $this->rules[$key] .= ','.$this->model->getKey();
            }
        }
        parent::formRules();
    }

    public function paginateQuery()
    {
        return parent::paginateQuery();
    }

    public function getInfo()
    {
        $url = \Request::input('url');
        if (empty($url)) {
            session()->flash(NOTIF_DANGER, 'URL Empty!');
            return \Redirect::back();
        }

        $this->model = Model::createFromUrl($url);
        if (empty($this->model)) {
            session()->flash(NOTIF_DANGER, 'Video Not Found!');
            return \Redirect::back()->withInput();
        }

        $this->formData();

        return view(suitViewName($this->getViewPrefix().'.edit'), ['model' => $this->model, 'url' => $url]);
    }

    public function doSave()
    {
        if (!$this->model->exists) {
            $this->model->publisher()->associate(\Auth::user());
        }
        if ($this->form->all()['playlist_id']) {
            $this->model->playlist()->associate($this->form->all()['playlist_id']);
        }
        if ($this->form->all()['match_id']) {
            $this->model->match()->associate($this->form->all()['match_id']);
        }
        if (!$this->model->is_live) {
            $this->model->start_time = null;
        }
        $this->model->save();
    }
}
