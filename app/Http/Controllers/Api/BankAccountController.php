<?php

namespace App\Http\Controllers\Api;

use App\Repositories\BankAccountRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class BankAccountController extends RepoResourceController
{
    public function __construct(BankAccountRepository $model)
    {
        parent::__construct($model);
        $model->setAdditionalQuery(function (Builder $q) {
            return $q->published();
        });
    }
}
