<?php

namespace App\Http\Controllers\Api;

use App\Model\CitizenJournalVideo;
use App\Jobs\ConvertVideo;
use Illuminate\Http\Request;

class CitizenJournalVideoController extends ResourceController
{
    public function __construct(CitizenJournalVideo $model)
    {
        parent::__construct($model);
    }

    protected function createQueryFromRequest(Request $request)
    {
        return parent::createQueryFromRequest($request)
            ->where('status', 'published')
            ->with(['vid', 'user']);
    }

    public function postVideo(Request $request)
    {
        $mimes = [
            'video/mp4',
            'video/x-flv',
            'application/x-mpegURL',
            'video/MP2T',
            'video/3gpp',
            'video/quicktime',
            'video/x-msvideo',
            'video/x-ms-wmv',
            'video/x-matroska'
        ];
        $mimetypes = 'mimetypes:' . implode(',', $mimes);

        $validator = \Validator::make($request->all(), [
            'title' => 'required|string|max:255',
            'description' => 'required|string',
            'video' => $mimetypes,
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 400,
                'message' => $validator->errors(),
                'result' => null
            ]);
        }

        $user = \UserAccess::user();

        $videoTemp = $request->file('video')->move('files/');

        $video = new CitizenJournalVideo;
        $video->user_id = $user->id;
        $video->title = $request->get('title');
        $video->description = $request->get('description');
        $video->video = $videoTemp;
        $video->status = 'uploaded';
        $video->save();

        exec("rm " . $videoTemp->getPathname());

        $this->dispatch(new ConvertVideo($video));

        return response()->json([
            'status' => 200,
            'message' => 'Video successfully uploaded!',
            'result' => [
                'data' => $video
            ]
        ]);
    }
}
