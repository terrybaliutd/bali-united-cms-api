<?php

namespace App\Http\Controllers\Api;

use App\Model\City;

class CityController extends ResourceController
{
    public function __construct(City $model)
    {
        parent::__construct($model);
    }
}
