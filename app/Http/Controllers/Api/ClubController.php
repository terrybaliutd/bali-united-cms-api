<?php

namespace App\Http\Controllers\Api;

use App\Model\Club;
use Illuminate\Http\Request;

class ClubController extends ResourceController
{
    public function __construct(Club $model)
    {
        parent::__construct($model);
    }

    protected function createQueryFromRequest(Request $request)
    {
        return parent::createQueryFromRequest($request)->published()->orderBy('created_at', 'desc');
    }
}
