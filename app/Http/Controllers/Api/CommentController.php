<?php

namespace App\Http\Controllers\Api;

use App\Model\Comment;
use App\Model\Video;
use Illuminate\Http\Request;

class CommentController extends ResourceController
{
    public function __construct(Comment $model)
    {
        parent::__construct($model);
    }

    protected function createQueryFromRequest(Request $request)
    {
        $query = parent::createQueryFromRequest($request)->with([
            'user',
            'stickerItem'
        ]);

        if ($request->get('video_id')) {
            $query = $query->where('video_id', $request->get('video_id'));
        }

        return $query->orderBy('created_at', 'desc');
    }

    public function postComment(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'video_id' => 'required|exists:videos,id',
            'message' => 'required_without_all:sticker_item_id|string',
            'sticker_item_id' => 'required_without_all:message|integer'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 400,
                'message' => $validator->errors()
            ]);
        }

        $user = \UserAccess::user();

        $comment = new Comment();
        $comment->user_id = $user->id;
        $comment->fill($request->all());
        if ($request->message) {
            $comment->type = Comment::TYPE_MESSAGE;
        } else {
            $comment->type = Comment::TYPE_STICKER;
        }
        $comment->save();

        $video = $comment->video()->first();
        $video->comment_count++;
        $video->save();

        return response()->json([
            'status' => 200,
            'message' => 'Comment successfully created!',
            'data' => $comment->load('user', 'video')
        ]);
    }
}
