<?php

namespace App\Http\Controllers\Api;

use App\Model\Competition;
use Illuminate\Http\Request;

class CompetitionController extends ResourceController
{
    public function __construct(Competition $model)
    {
        parent::__construct($model);
    }

    protected function createQueryFromRequest(Request $request)
    {
        return parent::createQueryFromRequest($request)->with('seasons')
                                                       ->published()->orderBy('order', 'asc');
    }
}
