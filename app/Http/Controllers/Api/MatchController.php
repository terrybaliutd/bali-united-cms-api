<?php

namespace App\Http\Controllers\Api;

use App\API\Auth\UserAccess;
use App\Model\Match;
use Illuminate\Http\Request;

class MatchController extends ResourceController
{
    public function __construct(Match $model)
    {
        parent::__construct($model);
    }

    protected function createQueryFromRequest(Request $request)
    {
//        $user = \UserAccess::user();
        return parent::createQueryFromRequest($request)
            ->published()
            ->with(['competition', 'season', 'clubInfos', 'clubInfos.club'])
            ->orderBy('start_at', 'asc');
    }

    public function matches(Request $request)
    {
        return $this->index($request);
    }

    public function results(Request $request)
    {

        $perPage = $this->getRequestPerPage($request);

        $query = parent::createQueryFromRequest($request)
            ->published()
            ->with(['competition', 'season', 'clubInfos', 'clubInfos.club'])
            ->orderBy('start_at', 'desc')
            ->where('status', 'Finished')->orWhere('status', 'Playing');

        $paginator = $query->paginate($perPage);
        $paginator->appends($request->query());

        $visibleFields = $this->getVisibleFields($request);
        $hiddenFields = $this->getHiddenFields($request);
        $this->filterVisible($paginator, $visibleFields, $hiddenFields);

        $result = $paginator->toArray();
        $idx = 0;
        foreach ($result['data'] as $matchData) {
            if ($matchData['status'] == 'Playing') {
                $result['data'][$idx]['home_club']['score'] = '--';
                $result['data'][$idx]['away_club']['score'] = '--';
            }
            $idx = $idx+1;
        }

        return response()->json([
            'status' => 200,
            'message' => $this->getPageName() . ' successfully fetch',
            'result' => $result
        ]);
    }

    public function fixtures(Request $request)
    {
        $perPage = $this->getRequestPerPage($request);

        $query = $this->createQueryFromRequest($request)
            ->where(function ($query) {
                $query->where('status', 'Schedule');
            });

        $paginator = $query->paginate($perPage);
        $paginator->appends($request->query());

        $visibleFields = $this->getVisibleFields($request);
        $hiddenFields = $this->getHiddenFields($request);
        $this->filterVisible($paginator, $visibleFields, $hiddenFields);

        return response()->json([
            'status' => 200,
            'message' => $this->getPageName() . ' successfully fetch',
            'result' => $paginator->toArray()
        ]);
    }

    public function table()
    {
    }
}
