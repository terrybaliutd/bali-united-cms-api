<?php

namespace App\Http\Controllers\Api;

use App\Model\UserMembership;
use App\Repositories\MatchFixturesRepository;
use App\Services\AccessTokenService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class MatchFixturesRepoController extends RepoResourceController
{
    public function __construct(MatchFixturesRepository $model)
    {
        parent::__construct($model);

        $model->setAdditionalQuery(function (Builder $q) {
            return $q->published()
                     ->with(['competition', 'season', 'clubInfos', 'clubInfos.club'])
                     ->where('status', 'Schedule')
                     ->orderBy('start_at', 'asc');
        });
    }
}
