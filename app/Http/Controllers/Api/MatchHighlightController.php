<?php

namespace App\Http\Controllers\Api;

use App\Model\MatchHighlight;
use App\Model\MatchClubInfo;
use Illuminate\Http\Request;

class MatchHighlightController extends ResourceController
{
    public function __construct(MatchHighlight $model)
    {
        parent::__construct($model);
    }

    protected function createQueryFromRequest(Request $request)
    {
        return parent::createQueryFromRequest($request)->with(['match'])
            ->orderBy('time_in_minute', 'desc');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function highlight(Request $request)
    {
        $query = $this->createQueryFromRequest($request)
            ->where('match_id', $request->id)
            ->orderBy('time_in_minute', 'desc');
        $highlights = $query->get();
        return $highlights;
    }
}
