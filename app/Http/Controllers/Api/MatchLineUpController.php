<?php

namespace App\Http\Controllers\Api;

use App\Model\MatchLineUp;
use App\Model\Match;
use App\Model\MatchClubInfo;
use Illuminate\Http\Request;

class MatchLineUpController extends ResourceController
{
    public function __construct(MatchClubInfo $model)
    {
        parent::__construct($model);
    }

    protected function createQueryFromRequest(Request $request)
    {
        return parent::createQueryFromRequest($request)->with(['match', 'club', 'primary', 'subtitutions']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function lineUp(Request $request)
    {
        return Match::find($request->id)->clubInfos()->get();
    }
}
