<?php

namespace App\Http\Controllers\Api;

use App\API\Auth\UserAccess;
use App\Model\UserMembership;
use App\Repositories\MatchResultRepository;
use App\Services\AccessTokenService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class MatchResultRepoController extends RepoResourceController
{
    public function __construct(MatchResultRepository $model)
    {
        parent::__construct($model);

        $model->setAdditionalQuery(function (Builder $q) {

            return $q->published()
                ->with(['competition', 'season'])
                ->orderBy('start_at', 'desc')
                ->where(function ($query) {
                    $query->where('status', 'Finished')
                        ->orWhere('status', 'Playing');
                });
        });
    }
}
