<?php

namespace App\Http\Controllers\Api;

use App\Model\MatchStatistic;
use App\Model\MatchClubInfo;
use App\Model\Match;
use Illuminate\Http\Request;

class MatchStatisticController extends ResourceController
{
    public function __construct(MatchClubInfo $model)
    {
        parent::__construct($model);
    }

    protected function createQueryFromRequest(Request $request)
    {
        return parent::createQueryFromRequest($request)->with(['match', 'club', 'statistic']);
    }

    public function statistic(Request $request)
    {
        return Match::find($request->id)->clubInfos()->with(['statistic'])->get();
    }
}
