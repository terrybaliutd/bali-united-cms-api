<?php

namespace App\Http\Controllers\Api;

use App\Repositories\MatchTicketsRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class MatchTicketsRepoController extends RepoResourceController
{
    public function __construct(MatchTicketsRepository $model)
    {
        parent::__construct($model);
        $model->setAdditionalQuery(function (Builder $q) {
            return $q->published()
                     ->with(['competition', 'season', 'clubInfos', 'clubInfos.club'])
                     ->where('status', 'Schedule')
                     ->whereNotNull('ticket_url')
                     ->where('ticket_url', "!=", "")
                     ->where('start_at', ">=", \Carbon\Carbon::now())
                     ->orderBy('start_at', 'asc');
        });
    }
}
