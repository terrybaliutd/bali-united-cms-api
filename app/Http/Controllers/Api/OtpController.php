<?php

namespace App\Http\Controllers\Api;


use App\API\Auth\UserAccess;
use App\Model\Membership;
use App\Model\Notification;
use App\Model\Otp;
use App\Model\SocialAccount;
use App\Model\User;
use App\Model\UserMembership;
use App\Services\AccessTokenService;
use App\Supports\CityProvinceFinder;
use App\Supports\ImageRotator;
use Auth;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Hash;
use Illuminate\Auth\Guard;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Mail;
use Mockery\Exception;

class OtpController extends ResourceController
{
    const INFOBIP_API_URL = "https://api.infobip.com/sms/1/text/single";
    const INFOBIP_FROM = "BaliUnited";
    const INFOBIP_MESSAGE = "Your otp code is %s";

    protected $user;
    protected $otp;
//    protected $auth;
//    protected $client;
//    protected $accessTokenService;
//    use ResetsPasswords;

    public function __construct(Otp $model) {
        parent::__construct($model);
        $this->otp = $model;
    }

    public function verifyOtp(Request $request) {

        $validator = \Validator::make($request->all(), [
            'otp_code' => 'required|integer',
            'type' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 401,
                'message' => $validator->errors()->first(),
                'result' => null
            ]);
        }

        $user = \UserAccess::user();

        $otp = Otp::where('otp_code', $request->otp_code)->where('user_id', $user->id)->where('type', $request->type)->where('verified', false)->first();

        if ($otp) {
            $otp->verified = true;
            $otp->save();

            switch ($otp->type) {
                case Otp::TYPE_CHANGE_PHONE:
                    $previous_user = User::where('phone_number', $this->parse_phone_number($otp->object_string))->first();

                    $user->phone_number = $otp->object_string;
                    $user->mobile_phone_status = User::USER_VERIFIED;
                    $user->save();

                    if ($previous_user) {
                        $previous_user->mobile_phone_status = User::USER_NOT_VERIFIED;
                        $previous_user->phone_number = null;
                        $previous_user->save();

                        UserMembership::where('user_id', $previous_user->id)->delete();
                    }

                    UserMembership::where('user_id', $user->id)->delete();

                    $membership = Membership::where('prefix', 'like', '%'.$this->getPhoneMembershipPrefix($otp->object_string).'%')->first();
                    if ($membership) {
                        $user_membership = new UserMembership();
                        $user_membership->user_id = $user->id;
                        $user_membership->membership_id = $membership->id;
                        $user_membership->save();
                    }
                    break;
                case Otp::TYPE_REGISTER:
                    $user->mobile_phone_status = User::USER_VERIFIED;
                    $user->save();
                    break;
            }

            return response()->json([
                'status' => Otp::OTP_VERIFICATION_SUCCESS_CODE,
                'message' => "OTP Verified",
            ]);
        } else {
            return response()->json([
                'status' => Otp::OTP_VERIFICATION_ERROR_CODE,
                'message' => "Otp verification failed",
                'result' => null
            ]);
        }
    }

    public function getPhoneMembershipPrefix($phone_number) {
        $prefix = substr($phone_number, 0, 2);
        if ($prefix == "62") {
            return substr($phone_number, 0, 5);
        } else if ($prefix == "08") {
            return substr($phone_number, 0, 4);
        } else {
            return substr($phone_number, 0, 4);
        }
    }


    //Object field for storing temporary value, example if type is PHONE_CHANGE will update user phone_number with object_string
    public function generateOtp(Request $request) {

        $validator = \Validator::make($request->all(), [
            'type' => 'required|string',
            'object_string' => 'required|string|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 401,
                'message' => $validator->errors()->first(),
                'result' => null
            ]);
        }

        $user = \UserAccess::user();

        if (!$this->verify_phone_number($request->object_string)) {
            return $this->toJson(Otp::OTP_INVALID_PHONE_NUMBER, [
                'message' => "Invalid phone number",
                'result' => null
            ]);
        }


        try {
            if ($this->parse_phone_number($request->object_string) == $this->parse_phone_number($user->phone_number)) {
                return $this->toJson(Otp::OTP_INVALID_PHONE_NUMBER, [
                    'message' => "Same phone number registered",
                    'result' => null
                ]);
            }
        } catch (Exception $e) {}

        $otp = Otp::where('user_id', $user->id)
            ->where('created_at', '>', Carbon::now()->subDay())
            ->where('created_at', '<', Carbon::now())->where('verified', false)->get();

        if (count($otp) >= Otp::OTP_MAX_COUNT) {
            return $this->toJson(Otp::OTP_ERROR_CODE_MAX_REQUEST, [
                'message' => "You have reach today OTP limit",
                'result' => null
            ]);
        }

        $otp = Otp::where('user_id', $user->id)->where('verified', false)->orderBy('created_at', 'desc')->first();

        if ($otp && strtotime($otp->created_at) >= strtotime('-1 minutes')) {
            return $this->toJson(Otp::OTP_ERROR_CODE_LAST_REQUEST, [
                'message' => "You have to wait for next OTP request",
                'result' => null
            ]);
        }

        $otp = new Otp();
        $otp->user_id = $user->id;
        $otp->type = $request->type;
        $otp->object_string = $this->parse_phone_number($request->object_string);
        $otp->otp_code = rand(111111,999999);
        $otp->save();

        $result = $this->send_sms(json_encode(array(
            'from' => self::INFOBIP_FROM,
            'to' => $this->parse_phone_number($otp->object_string),
            'text' => sprintf(self::INFOBIP_MESSAGE, $otp->otp_code)
        )));

        return response()->json([
            'status' => Otp::OTP_SUCCESS_CODE,
            'result' => $result,
            'message' => 'Otp Successfully Generated. Message sent to : '. $this->parse_phone_number($otp->object_string),
        ]);
    }

    public function verify_phone_number($phone_string) {
        $prefix = substr($phone_string, 0, 2);
        if (($prefix != "62" && $prefix != "08") || strlen($phone_string) < 9 || strlen($phone_string) > 13) {
            return false;
        }
        return true;
    }

    public function parse_phone_number($phone_string) {
        $prefix = substr($phone_string, 0, 2);
        if ($prefix != "62") {
            $phone_string = substr_replace($phone_string, "628", 0, 2);
        }
        return $phone_string;
    }

    public function send_sms($json_string) {
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => self::INFOBIP_API_URL,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $json_string,
            CURLOPT_HTTPHEADER => array(
                "accept: application/json",
                "authorization: Basic  dGVsdGljczpibHUzdDAwdGgxMjM=",
                "cache-control: no-cache",
                "content-type: application/json",
                "postman-token: 7da03a73-a896-b83c-dd65-de40d4e23ce4"
            ),
        ));

        $response = json_decode(curl_exec($curl));
        $err = curl_error($curl);

        if (@$response->messages[0]->status->groupName === Otp::OTP_SMS_REJECTED) {
            return $response;
        } else {
            return $response;
        }
    }
}
