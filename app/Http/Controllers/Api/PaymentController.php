<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;

class PaymentController extends ResourceController
{
    protected $client;

    public function __construct()
    {
        $this->client = new Client(['base_uri' => env('POINT_API_URL', '')]);
    }

    public function show(Request $request, $key)
    {
        $stream = $this->sendRequestToPointServer($request);
        $result = json_decode($stream->getContents(), true);
        return $result;
    }

    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'receiver_bank_name' => 'required',
            'date' => 'required|date_format:"Y-m-d"',
            'sender_bank_name' => 'required',
            'sender_account_number' => 'required',
            'sender_account_name' => 'required',
            'payment_amount' => 'required|integer',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 400,
                'message' => $validator->errors(),
                'result' => null
            ]);
        }

        $stream = $this->sendRequestToPointServer($request);
        $result = json_decode($stream->getContents(), true);
        return $result;
    }

    private function sendRequestToPointServer(Request $request)
    {
        $res = $this->client->request(
            $request->method(),
            $request->path(),
            [
                'headers' =>
                    [
                        'unique_code' => env('UNIQUE_CODE'),
                    ],
                'json' => $request->all(),
            ]
        );
        $stream = $res->getBody();
        return $stream;
    }
}
