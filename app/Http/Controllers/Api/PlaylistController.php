<?php

namespace App\Http\Controllers\Api;

use App\Model\Playlist;
use Illuminate\Http\Request;

class PlaylistController extends ResourceController
{
    public function __construct(Playlist $model)
    {
        parent::__construct($model);
    }

    protected function createQueryFromRequest(Request $request)
    {
        return parent::createQueryFromRequest($request)->with('videos')->published()->orderBy('position', 'asc');
    }
}
