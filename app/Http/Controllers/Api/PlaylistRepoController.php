<?php

namespace App\Http\Controllers\Api;

use App\Model\Playlist;

use App\Repositories\PlaylistRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class PlaylistRepoController extends RepoResourceController
{
    public function __construct(PlaylistRepository $model)
    {
        parent::__construct($model);
        $model->setAdditionalQuery(function (Builder $q) {
            return $q->published()->orderBy('position', 'asc');
        });
    }

    public function index(Request $request)
    {
        $cacheKey = 'playlist:7f4dbd4:' . md5(json_encode($request->query()));
        $cachedData = \Cache::get($cacheKey, false);

        if ($cachedData !== false) {
            return $cachedData;
        }

        $result = parent::index($request);
        \Cache::add($cacheKey, $result, 1);
        return $result;
    }
}
