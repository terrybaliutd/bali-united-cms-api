<?php

namespace App\Http\Controllers\Api;

use App\Model\Post;
use App\Model\PostComment;
use App\Model\PostCommentLike;
use App\Model\PostCommentReport;
use Auth;
use Illuminate\Http\Request;

class PostCommentController extends ResourceController
{

    public function __construct(PostComment $model)
    {
        parent::__construct($model);
    }

    protected function createQueryFromRequest(Request $request)
    {
        return parent::createQueryFromRequest($request)
            ->with('sticker')
            ->orderBy('created_at', 'desc');
    }

    public function store(Request $request, $key)
    {
        $validator = \Validator::make($request->all(), [
            'content' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->toJson(400, [
                'message' => $validator->errors()->first(),
            ]);
        }
        $user = \UserAccess::user();
        auth()->loginUsingId($user->id);
        $post = Post::find($key);
        $post->total_comment++;
        $post->save();

        if (!$post) {
            return $this->toJson(400, [
                'message' => "Post not found.",
                'result' => null
            ]);
        }

        $postComment = new PostComment();
        $postComment->fill($request->all());
        $postComment->user_id = $user->id;
        $postComment->post_id = $post->id;
        $postComment->save();

        return $this->toJson(200, [
            'message' => 'Successfully commented on a post.',
            'result' => $postComment
        ]);
    }

    public function storeReply(Request $request, $key)
    {
        $validator = \Validator::make($request->all(), [
            'content' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->toJson(400, [
                'message' => $validator->errors()->first(),
            ]);
        }

        $user = \UserAccess::user();
        auth()->loginUsingId($user->id);
        $postComment = PostComment::find($key);
        $post = $postComment->post()->first();
        $post->total_comment++;
        $post->save();

        if (!$postComment) {
            return $this->toJson(400, [
                'message' => 'Comment not found.',
                'result' => null
            ]);
        }

        $reply = new PostComment();
        $reply->fill($request->all());
        $reply->user_id = $user->id;
        $reply->post_id = $postComment->post_id;
        $reply->parent_id = $postComment->id;
        $reply->save();

        return $this->toJson(200, [
            'message' => 'Successfully replied on a comment.',
            'result' => $reply,
        ]);
    }

    public function update(Request $request, $key)
    {
        $postComment = PostComment::find($key);

        if ($postComment == null) {
            return $this->toJson(400, [
                'message' => "Post not found.",
                'result' => null
            ]);
        }

        $validator = \Validator::make($request->all(), [
            'content' => 'required'
        ]);

        if ($validator->fails()) {
            return $this->toJson(400, [
                'message' => $validator->errors()->first(),
                'result' => null
            ]);
        }

        $user = \UserAccess::user();
        auth()->loginUsingId($user->id);
        $postComment->content = $request->content;
        $postComment->save();

        return $this->toJson(200, [
            'message' => 'Successfully edited your comment.',
            'result' => $postComment,
        ]);
    }

    public function destroy($key)
    {
        $postComment = PostComment::find($key);

        if ($postComment == null) {
            return $this->toJson(400, [
                'message' => "Post not found.",
                'result' => null
            ]);
        }

        $post = $postComment->post()->first();
        $post->total_comment--;
        $post->save();
        $postComment->delete();

        return $this->toJson(200, [
            'message' => 'This comment is successfully deleted.',
            'result' => null
        ]);
    }

    public function like($key)
    {
        $key = (int)$key;
        $user = \UserAccess::user();
        $postComment = PostComment::find($key);
        if (!$postComment) {
            return $this->toJson(400, [
                'message' => 'Comment not found.',
            ]);
        }

        $like = PostCommentLike::where("post_comment_id", $postComment->id)->where("user_id", $user->id)->first();

        if ($like) {
            PostCommentLike::where("post_comment_id", $postComment->id)->where("user_id", $user->id)->delete();
            $postComment->total_like--;
            $postComment->save();

            return $this->toJson(200, [
                'message' => 'Successfully removed \'like\'.',
                "result" => [
                    "post_comment_id" => $postComment->id,
                    "user_id" => $user->id
                ],
            ]);
        } else {
            $like = new PostCommentLike;
            $like->post_comment_id = $postComment->id;
            $like->user_id = $user->id;
            $like->save();
            $postComment->total_like++;
            $postComment->save();

            return $this->toJson(200, [
                'message' => 'Successfully liked the comment.',
                "result" => [
                    "post_comment_id" => $postComment->id,
                    "user_id" => $user->id
                ],
            ]);
        }
    }

    public function report(Request $request, $key)
    {
        $validator = \Validator::make($request->all(), [
            'type' => 'required|string',
            'reason' => 'required|string',
        ]);

        if ($validator->fails()) {
            return $this->toJson(400, [
                'message' => $validator->errors()->first(),
                'result' => null
            ]);
        }

        $key = (int)$key;
        $user = \UserAccess::user();
        $postComment = PostComment::find($key);

        if (!$postComment) {
            return $this->toJson(400, [
                "message" => "The comment is not found on this post.",
                'result' => null
            ]);
        }

        auth()->loginUsingId($user->id);
        $postCommentReport = new PostCommentReport();
        $postCommentReport->fill($request->all());
        $postCommentReport->user_id = $user->id;
        $postCommentReport->post_comment_id = $postComment->id;
        $postCommentReport->save();

        return $this->toJson(200, [
            "message" => "Report added succesfully",
            "result" => $postCommentReport
        ]);
    }
}
