<?php

namespace App\Http\Controllers\Api;

use App\Model\Post;
use App\Model\PostLike;
use App\Model\PostReport;
use App\Model\PostTag;
use App\Model\Tag;
use App\Model\User;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;

class PostController extends ResourceController
{
    public function __construct(Post $model)
    {
        parent::__construct($model);
    }

    protected function createQueryFromRequest(Request $request)
    {
        return parent::createQueryFromRequest($request)
                    ->with(['owner'])
                    ->published()
                    ->orderBy('created_at', 'desc');
    }

    public function show(Request $request, $key)
    {
        $query = $this->createQueryFromRequest($request);

        $user = \UserAccess::user();

        $model = $query->findOrFailByUrlKey($key);
        $model->has_liked = PostLike::isLikedBy($model, $user);

        return response()->json([
            'status' => 200,
            'message' => $this->getPageName() . ' successfully fetch',
            'result' => $model
        ]);
    }

    public function store(Request $request)
    {
        if ($request->tags) {
            $request->merge([
                'tags' => explode(",", $request->tags)
            ]);
        }

        $validator = \Validator::make($request->all(), [
            'type' => 'required|string|in:photo,meme,article',
            'title' => 'required|string',
            'image' => 'required',
            'excerpt' => 'string',
            'content' => 'string',
            'tags' => 'array'
        ]);

        if ($validator->fails()) {
            return $this->toJson(400, [
                'message' => $validator->errors()->first(),
                'result' => null
            ]);
        }

        $user = \UserAccess::user();
        auth()->loginUsingId($user->id);
        $post = new Post();
        $post->fill($request->all());
        $post->owner_id = $user->id;
        $post->published_at = Carbon::now();
        $post->save();
        $this->setTags($post, $request->tags);

        return $this->toJson(200, [
            'message' => 'New ' . $post->getLabel() . ' has been created.',
            'result' => $post
        ]);
    }

    public function postVideo(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'user_id' => 'required|exists:users,id',
            'type' => 'required|string|in:video',
            'title' => 'required|string',
            'excerpt' => 'string',
            'video_key' => 'required|string',
            'video_status' => 'string'
        ]);

        if ($validator->fails()) {
            return $this->toJson(400, [
                'message' => $validator->errors()->first(),
                'result' => null
            ]);
        }

        $user = User::find($request->user_id);
        if ($user == null) {
            return $this->toJson(400, [
                'message' => 'User Not Found',
                'result' => null
            ]);
        }
        auth()->loginUsingId($user->id);

        $post = new Post();
        $post->owner_id = $user->id;
        $post->fill($request->all());
        $post->image = $this->generateThumbnailVideo($request->video_key);
        $post->save();

        return $this->toJson(200, [
            'message' => 'New ' . $post->getLabel() . ' has been created.',
            'result' => $post
        ]);
    }

    public function update(Request $request, $key)
    {
        $user = \UserAccess::user();
        $post = Post::find($key);
        if ($post == null) {
            return $this->toJson(400, [
                'message' => $post->getLabel() . ' not found.',
                'result' => null
            ]);
        }

        if ($request->tags) {
            $request->merge([
                'tags' => explode(",", $request->tags)
            ]);
        }

        $validator = \Validator::make($request->all(), [
            'title' => 'required|string',
            'image' => 'required',
            'excerpt' => 'string',
            'content' => 'string',
            'tags' => 'array'
        ]);

        if ($validator->fails()) {
            return $this->toJson(400, [
                'message' => $validator->errors()->first(),
                'result' => null
            ]);
        }

        auth()->loginUsingId($user->id);
        $post->fill($request->all());
        $post->save();

        $this->deleteTags($post);
        $this->setTags($post, $request->tags);

        return $this->toJson(200, [
            'message' => 'Post content is updated successfully.',
            'result' => $post,
        ]);
    }

    public function destroy($key)
    {
        $post = Post::find($key);
        if (!$post) {
            return $this->toJson(400, [
                'message' => 'Post not found.',
            ]);
        }

        $post->delete();
        return $this->toJson(200, [
            'message' => 'Post is deleted successfully.',
        ]);
    }

    public function like($key)
    {
        $key = (int)$key;
        $user = \UserAccess::user();
        $post = Post::find($key);
        if (!$post) {
            return $this->toJson(400, [
                "message" => "Post Not Found",
            ]);
        }

        $like = PostLike::where("post_id", $post->id)->where("user_id", $user->id)->first();

        if ($like) {
            PostLike::where("post_id", $post->id)->where("user_id", $user->id)->delete();
            $post->total_like--;
            $post->save();
            $content = [
                "message" => "Like removed successfully",
                "result" => [
                    "post_id" => $post->id,
                    "user_id" => $user->id
                ]
            ];

            return $this->toJson(200, $content);
        } else {
            $like = new PostLike;
            $like->post_id = $post->id;
            $like->user_id = $user->id;
            $like->save();
            $post->total_like++;
            $post->save();
            $content = [
                "message" => "Like added successfully",
                "result" => [
                    "post_id" => $post->id,
                    "user_id" => $user->id
                ]
            ];
            return $this->toJson(200, $content);
        }
    }

    public function report(Request $request, $key)
    {
        $validator = \Validator::make($request->all(), [
            'type' => 'required|string',
            'reason' => 'required|string',
        ]);

        if ($validator->fails()) {
            return $this->toJson(400, [
                'message' => $validator->errors()->first(),
                'result' => null
            ]);
        }

        $key = (int)$key;
        $user = \UserAccess::user();
        $post = Post::find($key);

        if (!$post) {
            return $this->toJson(400, [
                'message' => 'Post not found.',
                'result' => null
            ]);
        }

        auth()->loginUsingId($user->id);
        $postReport = new PostReport();
        $postReport->fill($request->all());
        $postReport->user_id = $user->id;
        $postReport->post_id = $post->id;
        $postReport->save();

        return $this->toJson(200, [
            "message" => "Report added succesfully",
            "result" => $postReport
        ]);
    }

    public function search(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'key' => 'required|string',
        ]);

        if ($validator->fails()) {
            return $this->toJson(400, [
                'message' => $validator->errors()->first(),
                'result' => null
            ]);
        }

        $perPage = $this->getRequestPerPage($request);

        $query = $this->createQueryFromRequest($request)
            ->where('title', 'like', '%'.$request->key.'%')
            ->orWhereHas('postTags', function ($query) use ($request) {
                $query->where('name', 'like', '%'.$request->key.'%');
            });
        $paginator = $query->paginate($perPage);
        $paginator->appends($request->query());

        $visibleFields = $this->getVisibleFields($request);
        $hiddenFields = $this->getHiddenFields($request);
        $this->filterVisible($paginator, $visibleFields, $hiddenFields);

        return response()->json([
            'status' => 200,
            'message' => $this->getPageName() . ' successfully fetch',
            'result' => $paginator->toArray()
        ]);
    }

    private function generateThumbnailVideo($videoKey)
    {
        $video = \YoutubeData::getVideoInfo($videoKey);
        return $video->snippet->thumbnails->maxres->url;
    }


    private function setTags($post, $tags)
    {
        if ($tags) {
            foreach ($tags as $value) {
                $tag = Tag::where('name', $value)->first();
                if (!$tag) {
                    $tag = new Tag();
                    $tag->name = trim($value);
                    $tag->description = trim($value);
                    $tag->save();
                }
                $postTag = new PostTag();
                $postTag->post_id = $post->id;
                $postTag->tag_id = $tag->id;
                $postTag->save();
            }
        }
    }

    private function deleteTags($post)
    {
        PostTag::where('post_id', $post->id)->delete();
    }
}
