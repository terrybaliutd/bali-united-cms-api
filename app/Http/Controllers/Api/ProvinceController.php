<?php

namespace App\Http\Controllers\Api;

use App\Model\Province;

class ProvinceController extends ResourceController
{
    public function __construct(Province $model)
    {
        parent::__construct($model);
    }
}
