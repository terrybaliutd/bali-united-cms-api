<?php

namespace App\Http\Controllers\Api;

use App\Model\Comment;
use App\Model\Quiz;
use App\Model\QuizUser;
use App\Model\User;
use App\Services\AccessTokenService;
use App\Supports\QuizPointUpdater;
use Illuminate\Http\Request;

class QuizController extends ResourceController
{
    protected $accessTokenService;

    public function __construct(
        Quiz $model,
        AccessTokenService $accessTokenService
    ) {
        parent::__construct($model);
        $this->accessTokenService = $accessTokenService;
    }

    protected function createQueryFromRequest(Request $request)
    {
        $query = parent::createQueryFromRequest($request)->published()->with(['quizUsers']);

        if ($request->get('access_token')) {
            // if user has not done the quiz, the quiz_users attribute will be empty
            // otherwise, the quiz_users attribute will return the corresponding QuizUsers object
            $user = $this->accessTokenService->findUser($request->access_token);

            $query = $query->with(['quizUsers' => function ($query) use ($user) {
                $query->where('user_id', $user->id);
            }]);
        }

        return $query;
    }

    public function postQuiz(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'quiz_id' => 'required|exists:quizzes,id',
            'answer' => 'required|integer|in:1,2,3,4'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 400,
                'message' => $validator->errors(),
                'result' => null
            ]);
        }

        $user = \UserAccess::user();
        $quiz = Quiz::findOrFail($request->get('quiz_id'));
        
        $quizUser = QuizUser::where('quiz_id', $request->get('quiz_id'))
            ->where('user_id', $user->id)
            ->first();

        if ($quizUser) {
            return response()->json([
                'status' => 412,
                'message' => 'Quiz has already been answered.',
                'result' => null
            ]);
        }

        $quizUser = $this->createQuizUser($request, $quiz, $user);
        $comment = $this->createComment($quiz, $user);
        
        try {
            \SafeDB::transaction(function () use ($comment, $quiz, $quizUser, $user) {
                $quizUser->save();
                $comment->save();

                if ($quiz->answer == request()->get('answer')) {
                    $updater = new QuizPointUpdater($quiz, $user);
                    $updater->execute();
                }
            }, 20);
        } catch (Exception $e) {
            return [
                "status" => 500,
                "message" => "postQuizError: " . $e->getMessage(),
                "result" => null
            ];
        }

        // Prepare Json Result
        $answer = (int)($quiz->answer == $request->get('answer'));
        $status = "QUIZ";
        $message = "Jawaban Anda ";
        $message .= ($answer) ? 'Benar!' : 'Salah!';
        
        return response()->json([
            'status' => 200,
            'message' => 'Quiz successfully answered!',
            'result' => [
                'status' => $status,
                'message' => $message,
                'answer' => $answer, // 1 if the answer is correct and 0 otherwise
                'data' => $quizUser
            ]
        ]);
    }

    protected function createComment(Quiz $quiz, User $user)
    {
        $comment = new Comment;
        $comment->video_id = $quiz->video->id;
        $comment->user_id = $user->id;
        $comment->message = $user->name . " has joined the quiz!";
        $comment->type = Comment::TYPE_NOTIFICATION;

        return $comment;
    }

    protected function createQuizUser(Request $request, Quiz $quiz, User $user)
    {
        $quizUser = new QuizUser;
        $quizUser->quiz_id = $quiz->id;
        $quizUser->user_id = $user->id;
        $quizUser->answer = $request->get('answer');

        return $quizUser;
    }
}
