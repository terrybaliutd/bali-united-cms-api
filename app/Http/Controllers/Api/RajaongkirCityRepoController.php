<?php

namespace App\Http\Controllers\Api;

use App\Repositories\RajaongkirCityRepository;
use Illuminate\Database\Eloquent\Builder;

class RajaongkirCityRepoController extends RepoResourceController
{
    public function __construct(RajaongkirCityRepository $model)
    {
        parent::__construct($model);
    }
}
