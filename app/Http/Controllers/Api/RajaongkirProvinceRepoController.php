<?php

namespace App\Http\Controllers\Api;

use App\Repositories\RajaongkirProvinceRepository;
use Illuminate\Database\Eloquent\Builder;

class RajaongkirProvinceRepoController extends RepoResourceController
{
    public function __construct(RajaongkirProvinceRepository $model)
    {
        parent::__construct($model);
    }
}
