<?php

namespace App\Http\Controllers\Api;

use App\Repositories\RajaongkirSubdistrictRepository;
use Illuminate\Database\Eloquent\Builder;

class RajaongkirSubdistrictRepoController extends RepoResourceController
{
    public function __construct(RajaongkirSubdistrictRepository $model)
    {
        parent::__construct($model);
    }
}
