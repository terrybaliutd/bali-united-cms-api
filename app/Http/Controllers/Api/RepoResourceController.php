<?php

namespace App\Http\Controllers\Api;

use App\Repositories\BaseRepository;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class RepoResourceController extends Controller
{
    protected $repo;

    public function __construct(BaseRepository $repo)
    {
        $this->repo = $repo;
    }

    public function index(Request $request)
    {
        return $this->returnData($request, $this->repo->getIndex($request->query()));
    }

    public function show(Request $request, $key)
    {
        return $this->returnData($request, $this->repo->getShow($request->query(), $key));
    }

    protected function isDebug(Request $request)
    {
        if ($request->has('debug') && $request->get('debug')) {
            return true;
        }
        return false;
    }

    protected function returnData($request, $data)
    {
        if ($this->isDebug($request)) {
            return view('test', [
                'datas' => $data
            ]);
        }

        if ($data instanceof LengthAwarePaginator) {
            $data = $data->toArray();
        }
        
        return $this->returnJson($data);
    }

    protected function returnJson($data)
    {
        return response()->json([
            'status' => 200,
            'message' => get_class($this->repo) . ' successfully fetch',
            'result' => $data
        ]);
    }
}
