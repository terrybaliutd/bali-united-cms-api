<?php
namespace App\Http\Controllers\Api;

use App\Http\Controllers\Extension\NameTrait;
use App\Http\Controllers\Extension\VisibilityTrait;
use App\Model\BaseModel;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use IteratorAggregate;

abstract class ResourceController extends Controller
{
    use NameTrait;
    use VisibilityTrait;

    const DEFAULT_PER_PAGE = 10;

    const MAX_PER_PAGE = 1000;

    protected $model;

    protected $filterFields = [];

    public function __construct(BaseModel $model)
    {
        parent::__construct();

        $this->model = $model;
    }

    protected function toJson($status, array $data = [])
    {
        $data = array_merge(['status' => (int) $status], $data);

        $data['result'] = array_key_exists('result', $data) ? $data['result'] : null;

        return response()->json($data);
    }

    public function index(Request $request)
    {
        $perPage = $this->getRequestPerPage($request);

        $query = $this->createQueryFromRequest($request);
        $paginator = $query->paginate($perPage);
        $paginator->appends($request->query());

        $visibleFields = $this->getVisibleFields($request);
        $hiddenFields = $this->getHiddenFields($request);
        $this->filterVisible($paginator, $visibleFields, $hiddenFields);

        return response()->json([
            'status' => 200,
            'message' => $this->getPageName() . ' successfully fetch',
            'result' => $paginator->toArray()
        ]);
    }

    public function show(Request $request, $key)
    {
        $query = $this->createQueryFromRequest($request);

        $model = $query->findOrFailByUrlKey($key);
        $visibleFields = $this->getVisibleFields($request);
        $hiddenFields = $this->getHiddenFields($request);
        $this->filterVisible($model, $visibleFields, $hiddenFields);

        return response()->json([
            'status' => 200,
            'message' => $this->getPageName() . ' successfully fetch',
            'result' => $model
        ]);
    }

    protected function getRequestPerPage(Request $request)
    {
        $perPage = $request->get('per_page', static::DEFAULT_PER_PAGE);
        if ($perPage >= static::MAX_PER_PAGE) {
            return static::MAX_PER_PAGE;
        }
        return $perPage;
    }

    protected function createQueryFromRequest(Request $request)
    {
        $params = $request->query();

        $filterParams = array_filter($params, function ($key) {
            return starts_with($key, 'filter_');
        }, ARRAY_FILTER_USE_KEY);

        foreach ($filterParams as $key => $value) {
            sscanf($key, "filter_%s", $column);

            if ($value != null && $value != "\"\"") {
                $filterParams[$column] = $value;
            }

            unset($filterParams[$key]);
        }

        $orderParams = array_filter($params, function ($key) {
            return starts_with($key, 'order_');
        }, ARRAY_FILTER_USE_KEY);

        foreach ($orderParams as $key => $value) {
            sscanf($key, "order_%s", $column);
            $column = str_replace(':', '.', $column);
            $orderParams[$column] = $value;
            unset($orderParams[$key]);
        }
        $searchQuery = $request->get('s');

        return $this->model->filter($searchQuery, $orderParams, $filterParams)->newQuery();
    }

    protected function getVisibleFields(Request $request)
    {
        return $this->parseFields($request->get('fields'));
    }

    protected function getHiddenFields(Request $request)
    {
        return $this->parseFields($request->get('hidden_fields'));
    }

    protected function parseFields($rawFields)
    {
        if (empty($rawFields)) {
            return null;
        }
        $fields = explode(',', $rawFields);
        $fields = array_map('trim', $fields);
        return array_unique($fields);
    }
}
