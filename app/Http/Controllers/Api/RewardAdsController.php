<?php

namespace App\Http\Controllers\Api;

use App\Repositories\RewardAdsRepository;

class RewardAdsController extends RepoResourceController
{
    public function __construct(RewardAdsRepository $model)
    {
        parent::__construct($model);
    }
}
