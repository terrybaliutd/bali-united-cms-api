<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use GuzzleHttp\Client;

class RewardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $client;
    //protected $pointServer;

    public function __construct()
    {
        $this->client = new Client(['base_uri' => env('POINT_API_URL', '')]);
    }

    public function index(Request $request)
    {
        $res = $this->client->request(
            'GET',
            '/api/rewards',
            [
                'headers' =>
                    [
                        'unique_code' => env('UNIQUE_CODE'),
                        'per_page' => $request->per_page,
                        'page' => $request->page
                    ],
                'json' => $request->all()
            ]
        );

        $stream = $res->getBody();
        $result = json_decode($stream->getContents(), true);
        if ($result['result']['prev_page_url']) {
            $prevPage = $result['result']['current_page'] - 1;
            $param = '?access_token='.$request->access_token.'&per_page='.$request->per_page.'&page='.$prevPage;
            $result['result']['prev_page_url'] = $request->url().$param;
        }
        if ($result['result']['next_page_url']) {
            $prevPage = $result['result']['current_page'] + 1;
            $param = '?access_token='.$request->access_token.'&per_page='.$request->per_page.'&page='.$prevPage;
            $result['result']['next_page_url'] = $request->url().$param;
        }
        return $result;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $key)
    {
        $res = $this->client->request(
            'GET',
            '/api/rewards/'.$key,
            [
                'headers' =>
                    ['unique_code' => env('UNIQUE_CODE')],
                'json' => $request->all()
            ]
        );

        $stream = $res->getBody();
        $result = json_decode($stream->getContents(), true);
        return $result;
    }
}
