<?php

namespace App\Http\Controllers\Api;

use App\Model\Video;
use App\Model\News;
use Illuminate\Http\Request;
use Illuminate\Pagination\Paginator;
use Illuminate\Pagination\LengthAwarePaginator;

class SearchController extends ResourceController
{
    protected $video;
    protected $news;

    public function __construct(Video $model, News $news)
    {
        parent::__construct($model);
        $this->video = $model;
        $this->news = $news;
    }

    public function base($models)
    {
        $items = collect($models);
        $page = \Input::get('page', 1);

        $paginatedResult = new LengthAwarePaginator(
            $items->forPage($page, static::DEFAULT_PER_PAGE),
            $items->count(),
            static::MAX_PER_PAGE,
            $page,
            ['path' => Paginator::resolveCurrentPath()]
        );

        return response()->json([
            'status' => 200,
            'message' => $this->getPageName() . ' successfully fetch',
            'result' => $paginatedResult->toArray()
        ]);
    }

    public function index(Request $request)
    {
        $keywords = explode(' ', $request->keyword);

        $news = News::searchNews($keywords);
        $videos = Video::searchVideo($keywords);

        $result = array_merge($news, $videos);

        $result = array_reverse(array_sort($result, function ($value) {
            return $value['datetime'];
        }));

        $paginatedResult = $this->base($result);

        return $paginatedResult;
    }

    public function news(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'keyword' => 'required|string',
        ]);

        if ($validator->fails()) {
            return $this->toJson(400, [
                'message' => $validator->errors()->first(),
                'result' => null
            ]);
        }

        $keywords = explode(' ', $request->keyword);

        $news = News::searchNews($keywords);

        $paginatedResult = $this->base($news);

        return $paginatedResult;
    }

    public function video(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'keyword' => 'required|string',
        ]);

        if ($validator->fails()) {
            return $this->toJson(400, [
                'message' => $validator->errors()->first(),
                'result' => null
            ]);
        }

        $playlistId = null;
        if ($request->filter_playlist_id !== null || !empty($request->filter_playlist_id)) {
            $playlistId = $request->filter_playlist_id;
        }
        $keywords = explode(' ', $request->keyword);

        $video = Video::searchVideo($keywords, $playlistId);

        $paginatedResult = $this->base($video);

        return $paginatedResult;
    }
}
