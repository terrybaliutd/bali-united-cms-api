<?php

namespace App\Http\Controllers\Api;

use App\Model\Season;
use Illuminate\Http\Request;

class SeasonController extends ResourceController
{
    public function __construct(Season $model)
    {
        parent::__construct($model);
    }

    protected function createQueryFromRequest(Request $request)
    {
        return parent::createQueryFromRequest($request)->with('competitions')
                                                       ->published()->orderBy('created_at', 'desc');
    }
}
