<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Repositories\RajaongkirCostRepository;
use Illuminate\Http\Request;

class ShippingController extends Controller
{
    protected $repository;

    public function __construct(RajaongkirCostRepository $repository)
    {
        $this->repository = $repository;
    }

    public function getCouriers()
    {
        return response()->json([
            'status' => 200,
            'message' => 'OK',
            'result' => [
                'jne',
                'pos',
                'tiki'
            ]
        ]);
    }

    public function calculateCost(Request $request)
    {
        return response()->json(
            $this->repository->getShippingCost($request->all())
        );
    }
}
