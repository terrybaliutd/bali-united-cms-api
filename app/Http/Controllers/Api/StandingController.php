<?php

namespace App\Http\Controllers\Api;

use App\Model\Competition;
use App\Model\Season;
use App\Model\Standing;
use Illuminate\Http\Request;

class StandingController extends ResourceController
{
    public function __construct(Standing $model)
    {
        parent::__construct($model);
    }

    protected function createQueryFromRequest(Request $request)
    {
        $competition = Competition::useDefault()->first();
        $season = Season::useDefault()->first();

        if ($competition === null || $season === null) {
            return [
                "status" => 404,
                "message" => "Competition or Season Not Found",
                "result" => null
            ];
        }

        $request->replace([
            'filter_competition_id' => $this->getCompetitionId($request),
            'filter_season_id' => $this->getSeasonId($request)
        ]);
        return parent::createQueryFromRequest($request)->with('club', 'competition', 'season')
                                                       ->published()
                                                       ->orderBy('position', 'asc');
    }

    protected function getCompetitionId(Request $request)
    {
        if (empty($request->filter_competition_id)) {
            $competition = Competition::useDefault()->first();
            return $competition->id;
        }
        return $request->filter_competition_id;
    }

    protected function getSeasonId(Request $request)
    {
        if (empty($request->filter_season_id)) {
            $season = Season::useDefault()->first();
            return $season->id;
        }
        return $request->filter_season_id;
    }
}
