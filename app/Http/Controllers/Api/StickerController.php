<?php

namespace App\Http\Controllers\Api;

use App\Model\StickerCategory;
use App\Model\StickerItem;
use Illuminate\Http\Request;

class StickerController extends ResourceController
{
    public function __construct(StickerCategory $model)
    {
        parent::__construct($model);
    }

    protected function createQueryFromRequest(Request $request)
    {
        return parent::createQueryFromRequest($request)->with(['items']);
    }
}
