<?php

namespace App\Http\Controllers\Api;

use App\Model\PostTag;
use App\Model\Tag;
use Illuminate\Http\Request;

class TagController extends ResourceController
{
    public function __construct(Tag $model)
    {
        parent::__construct($model);
    }

    protected function createQueryFromRequest(Request $request)
    {
        return parent::createQueryFromRequest($request);
    }

    public function posts($key)
    {
        $posts = [];
        $tag = Tag::find($key);

        if ($tag == null) {
            return $this->toJson(400, [
                'message' => "Tag not found.",
                'result' => null
            ]);
        }

        $postTags = PostTag::where('tag_id', $tag->id)->get();
        foreach ($postTags as $key => $value) {
            array_push($posts, $value->post()->first());
        }

        return $this->toJson(200, [
            'message' => 'List of Post with \'' . $tag->name . '\' Tag retrieved!',
            'result' => $posts,
        ]);
    }
}
