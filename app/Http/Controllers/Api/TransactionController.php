<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class TransactionController extends ResourceController
{
    protected $client;

    public function __construct()
    {
        $this->client = new Client(['base_uri' => env('POINT_API_URL', '')]);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $user = \UserAccess::user();

        $response = $this->client->request(
            'GET',
            '/api/transactions?filter_member_id='.$user->member_id,
            [
                'headers' => [
                    'unique_code' => 'secret'
                ]
            ]
        );
        $stream = $response->getBody();
        $result = json_decode($stream->getContents(), true);
        return $result;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'transaction_type_id' => 'required',
            'description' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 400,
                'message' => $validator->errors()
            ]);
        }

        $user = \UserAccess::user();

        if ($user->phone_number == null) {
            return response()->json([
                'status' => 400,
                'message' => 'Isi nomor telepon pada profil terlebih dahulu'
            ]);
        }

        $response = $this->client->request(
            'POST',
            '/api/transactions',
            [
                'headers' => [
                    'unique_code' => env('UNIQUE_CODE')
                ],
                'form_params' => [
                    'member_id' => $user->member_id,
                    'transaction_type_id' => $request->transaction_type_id,
                    'description' => $request->description,
                ]
            ]
        );
        if ($response->getStatusCode() == 200) {
            $stream = $response->getBody();
            $result = json_decode($stream->getContents(), true);
            if ($result['status'] === 403) {
                return response()->json([
                    'status' => 400,
                    'message' => $result['message']
                ]);
            }
            $userPoint = $result['result']['user']['points'];
            $user->points = $userPoint;
            $user->save();
        }
        return $result;
    }

    public function storeVoucher(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'transaction_type_id' => 'required',
            'description' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 400,
                'message' => $validator->errors()
            ]);
        }

        $user = \UserAccess::user();

        if (!$this->isEnoughPoint($user->member_id, $request->transaction_type_id)) {
            return response()->json([
                'status' => 400,
                'message' => 'Maaf, poin Anda tidak mencukupi'
            ]);
        }

        if ($user->phone_number == null) {
            return response()->json([
                'status' => 400,
                'message' => 'Isi nomor telepon pada profil terlebih dahulu'
            ]);
        }

        $response = $this->client->request(
            'POST',
            '/api/transactions/voucher',
            [
                'headers' => [
                    'unique_code' => env('UNIQUE_CODE')
                ],
                'form_params' => [
                    'member_id' => $user->member_id,
                    'transaction_type_id' => $request->transaction_type_id,
                    'description' => $request->description,
                    'phone' => $user->phone_number,
                ]
            ]
        );
        if ($response->getStatusCode() == 200) {
            $stream = $response->getBody();
            $result = json_decode($stream->getContents(), true);
            if ($result['status'] === 403) {
                return response()->json([
                    'status' => 400,
                    'message' => $result['message']
                ]);
            }
            $userPoint = $result['result']['user']['points'];
            $user->points = $userPoint;
            $user->save();
        }
        return $result;
    }

    public function storeMerchandise(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'transaction_type_id' => 'required',
            'description' => 'required',
            'address' => 'required',
            'phone' => 'required',
            'subdistrict' => 'required',
            'city' => 'required',
            'province' => 'required',
            'postal_code' => 'required|integer',
            'username' => 'required',
            'courier' => 'required',
            'shipping_cost' => 'required|integer',
            'service' => 'required',
            'service_description' => 'required',
            'delivery_estimation' => 'required',
            // 'note' => 'required',
        ]);


        if ($validator->fails()) {
            return response()->json([
                'status' => 400,
                'message' => $validator->errors(),
            ]);
        }

        $user = \UserAccess::user();
        $request['member_id'] = $user->member_id;

        $response = $this->client->request(
            'POST',
            '/api/transactions/merchandise',
            [
                'headers' => [
                    'unique_code' => env('UNIQUE_CODE')
                ],
                'json' => $request->all()
            ]
        );

        if ($response->getStatusCode() == 200) {
            $stream = $response->getBody();
            $result = json_decode($stream->getContents(), true);
            if ($result['status'] != 200) {
                return response()->json([
                    'status' => 400,
                    'message' => $result['message']
                ]);
            }
            $userPoint = $result['result']['user']['points'];
            $user->points = $userPoint;
            $user->save();
        }
        return $result;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Request $request, $key)
    {
        $response = $this->client->request(
            'GET',
            '/api/transactions/'.$key,
            [
                'headers' => [
                    'unique_code' => env('UNIQUE_CODE')
                ]
            ]
        );
        $stream = $response->getBody();
        $result = json_decode($stream->getContents(), true);
        return $result;
    }

    protected function getReward($rewardId)
    {
        $response = $this->client->request(
            'GET',
            '/api/rewards/'.$rewardId,
            [
                'headers' => [
                    'unique_code' => env('UNIQUE_CODE')
                ],
            ]
        );
        $stream = $response->getBody();
        $result = json_decode($stream->getContents(), true);
        return $result;
    }

    public function creditHistory(Request $request)
    {
        $user = \UserAccess::user();

        $requestToPass = $request->all();
        $requestToPass['member_id'] = $user->member_id;

        $res = $this->client->request(
            'GET',
            '/api/transactions/history/earned',
            [
                'headers' =>
                    ['unique_code' => env('UNIQUE_CODE')],
                'json' => $requestToPass
            ]
        );

        $stream = $res->getBody();
        $result = json_decode($stream->getContents(), true);
        return $result;
    }

    public function debitHistory(Request $request)
    {
        $user = \UserAccess::user();

        $requestToPass = $request->all();
        $requestToPass['member_id'] = $user->member_id;

        $res = $this->client->request(
            'GET',
            '/api/transactions/history/spent',
            [
                'headers' =>
                    ['unique_code' => env('UNIQUE_CODE')],
                'json' => $requestToPass
            ]
        );

        $stream = $res->getBody();
        $result = json_decode($stream->getContents(), true);
        return $result;
    }

    public function isEnoughPoint($memberId, $transactionTypeId)
    {
        $res = $this->client->request(
            'GET',
            '/api/transactions/isEnoughPoint?unique_code='.env('UNIQUE_CODE').
            '&member_id='.$memberId.'&transaction_type_id='.$transactionTypeId
        );

        $stream = $res->getBody();
        $result = json_decode($stream->getContents(), true);
        if ($result['status'] == '200') {
            if ($result['message'] == 'false') {
                return false;
            } else {
                return true;
            }
        }
        return true;
    }
}
