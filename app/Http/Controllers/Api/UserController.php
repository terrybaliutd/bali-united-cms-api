<?php

namespace App\Http\Controllers\Api;

use App\Model\Notification;
use App\Model\Otp;
use App\Model\SocialAccount;
use App\Model\User;
use App\Services\AccessTokenService;
use App\Supports\CityProvinceFinder;
use App\Supports\ImageRotator;
use Auth;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Hash;
use Illuminate\Auth\Guard;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Mail;
use Mockery\Exception;

class UserController extends ResourceController
{
    protected $user;
    protected $auth;
    protected $client;
    protected $accessTokenService;
    use ResetsPasswords;

    public function __construct(
        User $model,
        Guard $auth,
        AccessTokenService $accessTokenService
    ) {
        parent::__construct($model);
        $this->user = $model;
        $this->auth = $auth;
        $this->accessTokenService = $accessTokenService;
        $this->client = new Client(['base_uri' => env('POINT_API_URL', '')]);
    }

    public function login(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'email' => 'required|email|exists:users,email',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 401,
                'message' => $validator->errors()->first(),
                'result' => null
            ]);
        }

        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];

        if ($this->auth->attempt($credentials)) {
            return $this->generateToken();
        }
        return response()->json([
            'status' => 404,
            'message' => 'Failed to authenticate user',
            'result' => null
        ]);
    }

    public function loginBySocialAcc(Request $request, $provider)
    {
        if (!in_array($provider, SocialAccount::providerList())) {
            return response()->json([
                'status' => 401,
                'message' => 'Not a valid provider!',
                'result' => null
            ]);
        }

        $validator = \Validator::make($request->all(), [
            'provider_id' => 'required',
            'provider_token' => 'required',
            'name' => 'required',
            'email' => 'required|email'
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 401,
                'message' => $validator->errors()->first(),
                'result' => null
            ]);
        }

        $account = SocialAccount::whereProvider($provider)
            ->whereProviderUserId($request->provider_id)
            ->first();

        $user = User::whereEmail($request->email)->first();

        if (!$account) {
            $account = new SocialAccount([
                'provider_user_id' => $request->provider_id,
                'access_token' => $request->provider_token,
                'provider' => $provider
            ]);
        }

        if ($user && ($account->user_id != $user->id)) {
            $account = new SocialAccount([
                'provider_user_id' => $request->provider_id,
                'access_token' => $request->provider_token,
                'provider' => $provider
            ]);
        }

        if (!$user) {
            $user = new User;
            $user->fill($request->all());
            $user->active = false;
            $user->confirmation_code = str_random(30);
            $user->save();

            $usersNotification = new Notification();
            $usersNotification->user_id = $user->id;
            $usersNotification->save();

            //by pass email
//            \Mail::send('emails.verify', [
//                'confirmation_code' => $user->confirmation_code,
//                'user' => $user->name
//            ], function ($message) use ($user) {
//                $message->to($user->email)->subject('Bali United Apps : Verifikasi Email Anda');
//            });
        }

        $account->user()->associate($user);

        if ($account->isDirty()) {
            $account->save();
        }

        if ($this->auth->onceUsingId($user->id)) {
            return $this->generateToken();
        }

        return response()->json([
            'status' => 404,
            'message' => 'Failed to authenticate user',
            'result' => null
        ]);
    }

    public function generateToken()
    {
        $user = $this->auth->user();
        $this->auth->user()->login();
        $accessToken = $this->accessTokenService->create($user);
        $data = [
            'token' => $accessToken->token,
            'expired_date' => Carbon::createFromTimeStamp(strtotime($accessToken->expired_at))
                ->toIso8601String(),
            'user' => $accessToken->user
        ];

        return response()->json([
            'status' => 200,
            'message' => 'Token generated',
            'result' => $data
        ]);
    }

    public function logout(Request $request)
    {
        $user = \UserAccess::user();

        if ($user) {
            $user->logout();
            $this->auth->logout();
            $this->accessTokenService->setExpired($request->access_token);
            return [
                'status' => 200,
                'message' => 'Successfully logged out the user!',
                'result' => null
            ];
        }

        return [
            'status' => 404,
            'message' => 'User is not found!',
            'result' => null
        ];
    }

    public function register(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'email' => 'required|email|unique:users,email',
            'name' => 'required',
            'password' => 'required|basic_password|confirmed',
            'password_confirmation' => 'required|string',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 401,
                'message' => $validator->errors()->first(),
                'result' => null
            ]);
        }

        $user = app(User::class);
        $user->fill($request->all());
        $user->active = false;
        $user->confirmation_code = str_random(30);
        $user->save();

        //by pass email confirmation
//        try {
//            \Mail::send('emails.verify', [
//                'confirmation_code' => $user->confirmation_code,
//                'user' => $user->name
//            ], function ($message) use ($user) {
//                $message->to($user->email)->subject('Bali United Apps : Verifikasi Email Anda');
//            });
//        } catch (Exception $e) {}
//
//        try {
//            $usersNotification = new Notification();
//            $usersNotification->user_id = $user->id;
//            $usersNotification->save();
//        } catch (Exception $e) {}

        return response()->json([
            'status' => 200,
            'message' => 'User successfully registered!',
            'result' => null
        ]);
    }

    public function forgotPassword(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'email' => 'required|email',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 401,
                'message' => $validator->errors(),
                'result' => null,
            ]);
        }

        $user = User::where('email', $request->email)->first();

        if ($user == null) {
            return response()->json([
                'status' => 401,
                'message' => 'we could not find your email address',
                'result' => null,
            ]);
        } else {
            $this->postEmail($request);
            return response()->json([
                'status' => 200,
                'message' => 'page reset password already sent',
                'result' => null,
            ]);
        }
    }

    public function setPassword(Request $request)
    {
        $user = \UserAccess::user();

        if ($user) {
            $validator = \Validator::make($request->all(), [
                'current_password' => 'required|basic_password',
                'new_password' => 'required|basic_password|confirmed',
                'new_password_confirmation' => 'required',
            ]);

            if ($validator->fails()) {
                return response()->json([
                    'status' => 401,
                    'message' => $validator->errors(),
                    'result' => null
                ]);
            }

            if (!Hash::check($request->current_password, $user->password)) {
                return response()->json([
                    'status' => 402,
                    'message' => 'Password is not updated! Wrong current password!',
                    'result' => $user
                ]);
            }

            $user->password = $request->new_password;
            $result = $user->save();
            if ($result) {
                return response()->json([
                    'status' => 200,
                    'message' => 'Password is successfully updated',
                    'result' => $user
                ]);
            }

            return response()->json([
                'status' => 300,
                'message' => 'Password is not updated! Something goes wrong with database!',
                'result' => $user
            ]);
        }

        return [
            'status' => 404,
            'message' => 'User is not found!',
            'result' => null
        ];
    }

    public function update(Request $request)
    {
        $user = \UserAccess::user();

        $validator = \Validator::make($request->all(), [
            'name' => 'string|max:255',
            'address' => 'string|max:255',
            'gender' => 'string|in:f,m',
            'zip_code' => 'integer',
            'phone_number' => 'string|max:255',
            'profile_picture' => 'image',
            'current_password' => 'required_with:new_password|basic_password',
            'new_password' => 'basic_password|confirmed',
            'new_password_confirmation' => 'required_with:new_password',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 401,
                'message' => $validator->errors()->first(),
                'result' => null
            ]);
        }

        if ($request->new_password) {
            if (!Hash::check($request->current_password, $user->password)) {
                return response()->json([
                    'status' => 402,
                    'message' => 'Password is not updated! Wrong current password!',
                    'result' => $user
                ]);
            }

            $user->password = $request->new_password;
        }

        if ($request->profile_picture) {
            $image = $request->file('profile_picture')->move('files/');
            ImageRotator::fixOrientation($image);
            $user->attachment = $image->getPathname();
        }

        try {
            $otp_required = false;

            if ($request->phone_number != $user->phone_number) {
                $user->mobile_phone_status = User::USER_NOT_VERIFIED;
                $otp_required = true;
            }

            $user = $this->fillAttribute($request, $user);

            if ($request->profile_picture) {
                exec("rm " . $image->getPathname());
            }

            $user->save();

            $user->otp_required = $otp_required;

            return [
                "status" => 200,
                "message" => "Profile Updated Successfully!",
                "result" => $user,
            ];

        } catch (\Exception $e) {
            return [
                "status" => 400,
                "message" => $e->getMessage(),
                "result" => $user
            ];
        }
    }

    public function update_phone(Request $request)
    {
        $user = \UserAccess::user();

        $validator = \Validator::make($request->all(), [
            'phone_number' => 'string|max:255',
        ]);

        if ($validator->fails()) {
            return response()->json([
                'status' => 401,
                'message' => $validator->errors()->first(),
                'result' => null
            ]);
        }


        try {
            $otp_required = false;

            if ($request->phone_number != $user->phone_number) {
                $user->mobile_phone_status = User::USER_NOT_VERIFIED;
                $otp_required = true;
            }

            $user->save();

            $user->otp_required = $otp_required;

            return [
                "status" => 200,
                "message" => "Profile Updated Successfully!",
                "result" => $user,
            ];

        } catch (\Exception $e) {
            return [
                "status" => 400,
                "message" => $e->getMessage(),
                "result" => $user
            ];
        }
    }

    public function search(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'key' => 'required|string',
        ]);

        if ($validator->fails()) {
            return $this->toJson(400, [
                'message' => $validator->errors()->first(),
                'result' => null
            ]);
        }

        $perPage = $this->getRequestPerPage($request);

        $query = parent::createQueryFromRequest($request)->where('group_type', User::USER)
            ->where('name', 'like', '%'.$request->key.'%');
        $paginator = $query->paginate($perPage);
        $paginator->appends($request->query());

        $visibleFields = $this->getVisibleFields($request);
        $hiddenFields = $this->getHiddenFields($request);
        $this->filterVisible($paginator, $visibleFields, $hiddenFields);

        return response()->json([
            'status' => 200,
            'message' => $this->getPageName() . ' successfully fetch',
            'result' => $paginator->toArray()
        ]);
    }

    protected function createQueryFromRequest(Request $request)
    {
        return parent::createQueryFromRequest($request)->with('quizUsers', 'likes', 'notification');
    }

    protected function fillAttribute($request, $user)
    {
        $mappingRequest = $request->all();
        $mappingRequest = array_filter($mappingRequest);

        $user->fill($mappingRequest);

        $properties = [
            'name' => 'name',
            'address' => 'address',
            'zip_code' => 'zip_code',
            'phone_number' => 'phone_number',
            'city_name' => 'city',
            'province_name' => 'province',
            'subdistrict_id' => 'subdistrict_id'
        ];

        foreach ($properties as $param => $prop) {
            if (!$request->has($param)) {
                $user[$prop] = null;
            } else {
                $user[$prop] = $request[$param];
            }
        }

        //bypass sub district
        $user['subdistrict_id'] = 1;

        if (!$request->has('city_id') || !$request->has('province_id')) {
            $data = CityProvinceFinder::find($user);
            if ($data) {
                $user->city_id = $user->city_id ? $user->city_id : $data['city_id'];
                $user->province_id = $user->province_id ? $user->province_id : $data['province_id'];
            }
        }

        unset($user['phone_number']);

        return $user;
    }
}
