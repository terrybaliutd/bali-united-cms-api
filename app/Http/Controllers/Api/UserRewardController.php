<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests;
use GuzzleHttp\Client;
use Illuminate\Http\Request;

class UserRewardController extends Controller
{
    protected $client;

    public function __construct()
    {
        $this->client = new Client(['base_uri' => env('POINT_API_URL', '')]);
    }

    public function index(Request $request)
    {
        $user = \UserAccess::user();

        if (!$user) {
            return [
                "status" => 404,
                "message" => "User not found",
                "result" => null
            ];
        }

        $response = $this->client->request(
            'GET',
            '/api/user-rewards',
            [
                'query' => [
                    'unique_code' => env('UNIQUE_CODE'),
                    'member_id' => $user->member_id,
                    'per_page' => $request->per_page,
                    'page' => $request->page
                ]
            ]
        );
        $stream = $response->getBody();
        $result = json_decode($stream->getContents(), true);
        if ($result['result']['prev_page_url']) {
            $prevPage = $result['result']['current_page'] - 1;
            $param = '?access_token='.$request->access_token.'&per_page='.$request->per_page.'&page='.$prevPage;
            $result['result']['prev_page_url'] = $request->url().$param;
        }
        if ($result['result']['next_page_url']) {
            $prevPage = $result['result']['current_page'] + 1;
            $param = '?access_token='.$request->access_token.'&per_page='.$request->per_page.'&page='.$prevPage;
            $result['result']['next_page_url'] = $request->url().$param;
        }
        return $result;
    }

    public function show($key)
    {
        $user = \UserAccess::user();

        if (!$user) {
            return [
                "status" => 404,
                "message" => "User not found",
                "result" => null
            ];
        }

        $response = $this->client->request(
            'GET',
            '/api/user-rewards/'.$key,
            [
                'query' => [
                    'unique_code' => env('UNIQUE_CODE'),
                    'member_id' => $user->member_id
                ]
            ]
        );

        $stream = $response->getBody();
        $result = json_decode($stream->getContents(), true);
        return $result;
    }

    public function redeemVoucher(Request $request)
    {
        $user = \UserAccess::user();

        if (!$user) {
            return [
                "status" => 404,
                "message" => "User not found",
                "result" => null
            ];
        }

        $response = $this->client->request(
            'POST',
            '/api/user-rewards/redeem',
            [
                'query' => [
                    'unique_code' => env('UNIQUE_CODE')
                ],
                'form_params' => [
                    'member_id' => $user->member_id,
                    'id' => $request->voucher_id,
                    'merchant_code' => $request->merchant_code
                ]
            ]
        );

        $stream = $response->getBody();
        $result = json_decode($stream->getContents(), true);
        return $result;
    }
}
