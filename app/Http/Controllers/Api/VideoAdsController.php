<?php

namespace App\Http\Controllers\Api;

use App\Model\VideoAds;
use App\Model\VideoAdsUser;
use App\Repositories\VideoAdsRepository;
use App\Repositories\VideoAdsUserRepository;
use App\Supports\VideoAdsPointUpdater;
use Illuminate\Http\Request;

class VideoAdsController extends RepoResourceController
{
    protected $videoAdsUser;

    public function __construct(VideoAdsRepository $model, VideoAdsUserRepository $videoAdsUser)
    {
        parent::__construct($model);

        $this->videoAdsUser = $videoAdsUser;
    }

    public function index(Request $request)
    {
        return $this->returnData(
            $request,
            $this->repo->getVideoAdsList(\UserAccess::user())
        );
    }

    public function watch($key)
    {
        $key = (int)$key;
        $user = \UserAccess::user();

        if (!VideoAds::find($key)) {
            return [
                "status" => 404,
                "message" => "video not found",
                "result" => null
            ];
        }

        if ($this->videoAdsUser->getWatchedBy($user, $key)) {
            return [
                "status" => 403,
                "message" => "User has already watched this Video Ads!",
                "result" => null
            ];
        }

        $watch = new VideoAdsUser;
        $watch->video_ads_id = $key;
        $watch->user_id = $user->id;

        try {
            \SafeDB::transaction(function () use ($watch) {
                $watch->save();

                $updater = new VideoAdsPointUpdater($watch);
                $updater->execute();
            }, 20);

            forgetCache('VideoAdsUserRepository', \UserAccess::user());
            forgetCache('VideoAdsRepository', \UserAccess::user());

            return [
                "status" => 200,
                "message" => "Watched video ads added succesfully",
                "result" => [
                    "video_id" => $key,
                    "user_id" => $user->id
                ]
            ];
        } catch (\Exception $e) {
            return [
                "status" => 500,
                "message" => "watchedVideoError: " . $e->getMessage(),
                "result" => null
            ];
        }
    }
}
