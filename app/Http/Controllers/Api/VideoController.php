<?php

namespace App\Http\Controllers\Api;

use App\Model\Match;
use App\Model\Setting;
use App\Model\User;
use App\Model\UserMembership;
use App\Model\Video;
use App\Model\VideoLike;
use App\Model\VideoWatch;
use App\Services\AccessTokenService;
use App\Supports\VideoPointUpdater;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Mockery\Exception;

class VideoController extends ResourceController
{
    public function __construct(Video $model)
    {
        parent::__construct($model);
    }

    protected function createQueryFromRequest(Request $request)
    {
        $membership_ids = null;

        try {
            if (@$_GET['access_token']) {
                $service = new AccessTokenService();
                $user = $service->findUser(@$_GET['access_token']);
                $membership = UserMembership::where('user_id', @$user->id)->get();
                foreach ($membership as $item) {
                    $membership_ids[] = $item->membership_id;
                }
            }
        } catch (Exception $e) {}

        parent::createQueryFromRequest($request)->published()
            ->with('membership')
            ->where('membership_id', null)
            ->orWhere('membership_id', 0)
            ->with('membership');

        if (!empty($membership_ids)) {
            foreach ($membership_ids as $membership_id) {
                parent::createQueryFromRequest($request)->orWhere('membership_id', $membership_id);
            }
        }

        return parent::createQueryFromRequest($request);
    }

    protected function indexWithStatus($paginator)
    {
        return response()->json([
            'status' => 200,
            'message' => $this->getPageName() . ' successfully fetch',
            'result' => $paginator->toArray()
        ]);
    }

    public function index(Request $request)
    {
        $perPage = $this->getRequestPerPage($request);

        $query = $this->createQueryFromRequest($request)
            ->orderBy('featured', 'desc')
            ->orderBy('upload_date', 'desc')
            ->with(['match', 'playlist', 'membership']);
        $paginator = $query->paginate($perPage);
        $paginator->appends($request->query());

        $visibleFields = $this->getVisibleFields($request);
        $hiddenFields = $this->getHiddenFields($request);
        $this->filterVisible($paginator, $visibleFields, $hiddenFields);

        return response()->json([
            'status' => 200,
            'message' => $this->getPageName() . ' successfully fetch',
            'result' => $paginator->toArray()
        ]);
    }

    public function playlistVideos(Request $request)
    {
        $perPage = $this->getRequestPerPage($request);
        if (!$perPage) {
            $perPage = 10;
        }

        $query = $this->createQueryFromRequest($request)
            ->where("playlist_id", $request->id)
            ->orderBy('featured', 'desc')
            ->orderBy('upload_date', 'desc');

        $paginator = $query->paginate($perPage);
        $paginator->appends($request->query());
        return $this->indexWithStatus($paginator);
    }

    public function recent(Request $request)
    {
        $perPage = $this->getRequestPerPage($request);
        if (!$perPage) {
            $perPage = 10;
        }

        $query = $this->createQueryFromRequest($request)->orderBy('upload_date', 'desc')->with(['match']);

        $paginator = $query->paginate($perPage);
        $paginator->appends($request->query());
        return $this->indexWithStatus($paginator);
    }

    public function trending(Request $request)
    {
        $perPage = $this->getRequestPerPage($request);
        if (!$perPage) {
            $perPage = 10;
        }

        $query = Video::join("video_likes", "videos.id", "=", "video_likes.video_id")
            ->select(["videos.*", \DB::raw('count(*) as like_count')])
            ->groupBy("videos.id")
            ->where("upload_date", '>=', Carbon::now()->subMonth())
            ->orderBy("like_count", "desc");


        $paginator = $query->paginate($perPage);
        $paginator->appends($request->query());
        return $this->indexWithStatus($paginator);
    }

    public function matchVideos(Request $request)
    {
        $perPage = $this->getRequestPerPage($request);
        if (!$perPage) {
            $perPage = 10;
        }

        $query = $this->createQueryFromRequest($request)
            ->where("match_id", $request->id)
            ->with('playlist')
            ->orderBy('upload_date', 'desc');

        $paginator = $query->paginate($perPage);
        $paginator->appends($request->query());
        return $this->indexWithStatus($paginator);
    }

    public function live()
    {
        $isLive = true;
        $liveMatch = Match::published()->live()->first();
        $banner = asset(\Setting::findByName('default-banner-image')->casted_value);

        if ($liveMatch) {
            if ($liveMatch->banner != "") {
                $banner = asset($liveMatch->banner);
            }

            $video = Video::find($liveMatch->live_video_id);
            if ($video != null) {
                $video->load('match');
                return [
                    "status" => 200,
                    "message" => "Get Live Match Info",
                    "result" => [
                        "is_live" => $isLive,
                        "banner" => $banner,
                        "video" => $video,
                        "news" => null,
                    ]
                ];
            }
        }

        $isLive = false;
        $video = Video::orderBy('upload_date', 'desc')->first();
        $news = \App\Model\News::published()->orderBy('news_date', 'desc')->first();
        if ($video != null && $news != null) {
            if ($video->upload_date < $news->news_date) {
                $video = null;
                //$banner = asset($news->image);
            } else {
                $news = null;
                //$banner = asset($video->attachment);
            }
        }


        return [
            "status" => 200,
            "message" => "Get Live Match Info",
            "result" => [
                "is_live" => $isLive,
                "banner" => $banner,
                "video" => $video,
                "news" => $news,
            ]
        ];
    }

    public function like($key)
    {
        $key = (int)$key;
        $user = \UserAccess::user();
        $video = Video::find($key);

        if (!$video) {
            return [
                "status" => 404,
                "message" => "Video Not Found",
                "result" => null
            ];
        }

        $like = VideoLike::where("video_id", $key)->where("user_id", $user->id)->first();

        if ($like) {
            $like->delete();
            $video->like_count--;
            $video->save();
            return [
                "status" => 200,
                "message" => "Like removed succesfully",
                "result" => [
                    "video_id" => $key,
                    "user_id" => $user->id
                ]
            ];
        } else {
            $like = new VideoLike;
            $like->video_id = $key;
            $like->user_id = $user->id;
            $like->save();
            $video->like_count++;
            $video->save();
            return [
                "status" => 200,
                "message" => "Like added succesfully",
                "result" => [
                    "video_id" => $key,
                    "user_id" => $user->id
                ]
            ];
        }
    }

    public function watch($key)
    {
        $key = (int)$key;
        $user = \UserAccess::user();

        if (!Video::find($key)) {
            return [
                "status" => 404,
                "message" => "Video Not Found",
                "result" => null
            ];
        }

        $watched = VideoWatch::where("user_id", $user->id)->where("video_id", $key)->first();

        if ($watched) {
            return [
                "status" => 403,
                "message" => "Video has already been watched",
                "result" => [
                    "video_id" => $key,
                    "user_id" => $user->id
                ]
            ];
        }

        $watch = new VideoWatch;
        $watch->video_id = $key;
        $watch->user_id = $user->id;

        try {
            \SafeDB::transaction(function () use ($watch) {
                $watch->save();

                $updater = new VideoPointUpdater($watch);
                $updater->execute();
            }, 20);

            return [
                "status" => 200,
                "message" => "Watched video added succesfully",
                "result" => [
                    "video_id" => $key,
                    "user_id" => $user->id
                ]
            ];
        } catch (Exception $e) {
            return [
                "status" => 500,
                "message" => "watchedVideoError: " . $e->getMessage(),
                "result" => null
            ];
        }
    }

    public function show(Request $request, $key)
    {
        $query = $this->createQueryFromRequest($request)->with(['match', 'membership']);

        $user = \UserAccess::user();

        $model = $query->findOrFailByUrlKey($key);
        $model->has_liked = VideoLike::isLikedBy($model, $user);
        $model->has_watched = VideoWatch::isWatchedBy($model, $user);

        $visibleFields = $this->getVisibleFields($request);
        $hiddenFields = $this->getHiddenFields($request);
        $this->filterVisible($model, $visibleFields, $hiddenFields);

        return response()->json([
            'status' => 200,
            'message' => $this->getPageName() . ' successfully fetch',
            'result' => $model
        ]);
    }
}
