<?php

namespace App\Http\Controllers\Api;

use App\Model\UserMembership;
use App\Repositories\VideoRepository;
use App\Services\AccessTokenService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Mockery\Exception;

class VideoIndexRepoController extends RepoResourceController
{
    public function __construct(VideoRepository $model)
    {
        parent::__construct($model);

        $membership_ids = null;
        try {
            if (@$_GET['access_token']) {
                $service = new AccessTokenService();
                $user = $service->findUser(@$_GET['access_token']);
                $membership = UserMembership::where('user_id', @$user->id)->get();
                foreach ($membership as $item) {
                    $membership_ids[] = $item->membership_id;
                }
            }
        } catch (Exception $e) {}

        $model->setAdditionalQuery(function (Builder $q) use($membership_ids) {
            $q->published()
                ->where('membership_id', null)
                ->orWhere('membership_id', 0)
                ->with('membership');
            if (!empty($membership_ids)) {
                foreach ($membership_ids as $membership_id) {
                    $q->orWhere('membership_id', $membership_id);
                }
            }

            $q->orderBy('featured', 'desc')
                ->orderBy('upload_date', 'desc')
                ->with(['match', 'playlist']);

            return $q;

        });
    }
}
