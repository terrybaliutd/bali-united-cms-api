<?php

namespace App\Http\Controllers\Api;

use App\Repositories\MatchRepository;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;

class VideoLiveRepoController extends RepoResourceController
{
    public function __construct(MatchRepository $model)
    {
        parent::__construct($model);
        $model->setAdditionalQuery(function (Builder $q) {
            return $q->published()->live();
        });
    }

    public function live(Request $request)
    {
        return $this->returnData($request, $this->repo->getLiveMatch($request->query()));
    }
}
