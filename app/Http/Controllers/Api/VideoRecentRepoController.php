<?php

namespace App\Http\Controllers\Api;

use App\Model\UserMembership;
use App\Repositories\VideoRecentRepository;
use App\Services\AccessTokenService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Mockery\Exception;

class VideoRecentRepoController extends RepoResourceController
{
    public function __construct(VideoRecentRepository $model)
    {
        parent::__construct($model);
        $membership_ids = null;
        try {
            if (@$_GET['access_token']) {
                $service = new AccessTokenService();
                $user = $service->findUser(@$_GET['access_token']);
                $membership = UserMembership::where('user_id', @$user->id)->get();
                foreach ($membership as $item) {
                    $membership_ids[] = $item->membership_id;
                }
            }
        } catch (Exception $e) {}
        $model->setAdditionalQuery(function (Builder $q) use($membership_ids) {
            $q->where('membership_id', null)
                ->orWhere('membership_id', 0)
                     ->orderBy('upload_date', 'desc')
                     ->published()
                     ->with(['match', 'membership']);
            if (!empty($membership_ids)) {
                foreach ($membership_ids as $membership_id) {
                    $q->orWhere('membership_id', $membership_id);
                }
            }

            return $q;
        });
    }
}
