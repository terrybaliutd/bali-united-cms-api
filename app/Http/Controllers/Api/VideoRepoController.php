<?php

namespace App\Http\Controllers\Api;

use App\Model\UserMembership;
use App\Model\Video;
use App\Repositories\VideoRepository;
use App\Services\AccessTokenService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Mockery\Exception;

class VideoRepoController extends RepoResourceController
{
    public function __construct(VideoRepository $model)
    {
        parent::__construct($model);
        $membership_ids = null;

        try {
            if (@$_GET['access_token']) {
                $service = new AccessTokenService();
                $user = $service->findUser(@$_GET['access_token']);
                $membership = UserMembership::where('user_id', @$user->id)->get();
                foreach ($membership as $item) {
                    $membership_ids[] = $item->membership_id;
                }
            }
        } catch (Exception $e) {}

        $this->repo->setAdditionalQuery(function (Builder $query) use ($membership_ids) {
            $query->published()
                ->with('membership')
                ->where('membership_id', null)
                ->orWhere('membership_id', 0)
                ->orderBy('featured', 'desc')
                ->orderBy('upload_date', 'desc');
            if (!empty($membership_ids)) {
                foreach ($membership_ids as $membership_id) {
                    $query->orWhere('membership_id', $membership_id);
                }
            }

            return $query;
        });
    }

    public function playlistVideos(Request $request, $key)
    {
        return $this->returnData(
            $request,
            $this->repo->getPlaylistVideos($request->query(), $key)
        );
    }
}
