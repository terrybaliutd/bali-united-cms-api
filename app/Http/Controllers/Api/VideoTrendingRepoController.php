<?php

namespace App\Http\Controllers\Api;

use App\Model\UserMembership;
use App\Repositories\VideoTrendingRepository;
use App\Services\AccessTokenService;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Http\Request;
use Mockery\Exception;

class VideoTrendingRepoController extends RepoResourceController
{
    public function __construct(VideoTrendingRepository $model)
    {
        parent::__construct($model);
        $membership_ids = null;

        try {
            if (@$_GET['access_token']) {
                $service = new AccessTokenService();
                $user = $service->findUser(@$_GET['access_token']);
                $membership = UserMembership::where('user_id', @$user->id)->get();
                foreach ($membership as $item) {
                    $membership_ids[] = $item->membership_id;
                }
            }
        } catch (Exception $e) {}

        $model->setAdditionalQuery(function (Builder $q) use ($membership_ids) {
            $q->join("video_likes", "videos.id", "=", "video_likes.video_id")
                ->published()
                ->with('membership')
                ->where('membership_id', null)
                ->orWhere('membership_id', 0)
                ->select(["videos.*", \DB::raw('count(*) as like_count')])
                ->groupBy("videos.id")
                ->where("upload_date", '>=', \Carbon\Carbon::now()->subMonth())
                ->orderBy("like_count", "desc");
            if (!empty($membership_ids)) {
                foreach ($membership_ids as $membership_id) {
                    $q->orWhere('membership_id', $membership_id);
                }
            }
            return $q;
        });
    }
}
