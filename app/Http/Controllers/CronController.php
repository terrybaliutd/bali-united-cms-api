<?php

namespace App\Http\Controllers;


use App\API\Auth\UserAccess;
use App\Http\Controllers\Api\ResourceController;
use App\Model\Membership;
use App\Model\Notification;
use App\Model\Otp;
use App\Model\SocialAccount;
use App\Model\User;
use App\Model\UserMembership;
use App\Model\UserMembershipBonus;
use App\Services\AccessTokenService;
use App\Supports\CityProvinceFinder;
use App\Supports\ImageRotator;
use Auth;
use Carbon\Carbon;
use GuzzleHttp\Client;
use Hash;
use Illuminate\Auth\Guard;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;
use Mail;
use Mockery\Exception;

class CronController extends ResourceController
{

    const DEFAULT_CONVERSION = 1000;

    public function __construct() {
    }

    public function import_smartfren() {
//        $smart_fren_txt = file_get_contents(env('SMARTFREN_PROVIDER_URL'));
        $topups = array();

        foreach(preg_split("/((\r?\n)|(\r\n?))/", file_get_contents(env('SMARTFREN_PROVIDER_URL'))) as $line){
            // do stuff with $line
            $topups[] = explode(";", $line);
        }

        $fail_records = array();
        $success_records = array();

        foreach ($topups as $topup) {
            try {
                if (count($topup) >= 6) {
                    $user = User::where('phone_number', 'like', '%'.$topup[0].'%')->first();
                    if ($user) {
                        $user_membership = UserMembership::where('user_id', @$user->id)->first();
                        $membership_bonuses = UserMembershipBonus::where('object_1', $topup[3])->where('object_2', $topup[4])->where('object_3', $topup[5])->first();
                        $membership = Membership::where('id', $user_membership->membership_id)->where('is_reward', true)->first();
                        if ($user && $user_membership && $membership && !$membership_bonuses) {
                            $bonuses = new UserMembershipBonus();
                            $bonuses->user_id = $user->id;
                            $bonuses->phone_number = $topup[0];
                            $bonuses->object_amount = $topup[2];
                            $bonuses->object_date = $topup[1];
                            $bonuses->object_1 = $topup[3];
                            $bonuses->object_2 = $topup[4];
                            $bonuses->object_3 = $topup[5];
                            $bonuses->object_source = $topup[6];
                            $bonuses->save();

                            $user->points += floor($bonuses->object_amount / self::DEFAULT_CONVERSION);
                            $user->save();

                            $success_records[] = $topup;
                        } else {
                            $topup[7] = "Bonus Already Redeemed";
                            $fail_records[] = $topup;
                        }
                    } else {
                        $topup[7] = "User Not Found";
                        $fail_records[] = $topup;
                    }
                } else {
                    $topup[7] = "Data Not Valid";
                    $fail_records[] = $topup;
                }
            } catch (Exception $e) {
                $topup[7]   = "Data Not Valid";
                $fail_records[] = $topup;
            }
        }
        echo "Success: ". count($success_records);
        foreach ($success_records as $success_record) {
            echo "<br>";
            echo "number: " . @$success_record[0];
        }
        if (!empty($fail_records)) {
            echo "<br>";
            echo "<br>";
            echo "Failed: ". count($fail_records);
            echo "<br>";
            foreach ($fail_records as $fail_record) {
                echo "number: " . @$fail_record[0]. ". Reason: ". @$fail_record[7];
                echo "<br>";
            }
        }
    }

}
