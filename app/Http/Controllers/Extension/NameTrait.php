<?php

namespace App\Http\Controllers\Extension;

trait NameTrait
{
    protected $pageName;

    /**
     * Get Controller name without 'Controller' postfix
     * @return string
     */
    protected function getControllerName()
    {
        return preg_replace("/(.*)[\\\\](.*)(Controller)/", '$2', get_class($this));
    }

    /**
     * Get Page header for page title.  By default the value is uppercase word and snake case of controller name
     * @return [type] [description]
     */
    protected function getPageName()
    {
        if ($this->pageName === null) {
            $name = snake_case($this->getControllerName(), '-');
            $name = implode(' ', explode('-', $name));
            return ucwords($name);
        }
        return $this->pageName;
    }
}
