<?php

namespace App\Http\Controllers\Extension;

trait VisibilityTrait
{
    protected function filterVisible($models, $visibleFields = null, $hiddenFields = null)
    {
        if ($models instanceof Model) {
            $this->setModelVisiblity($models, $visibleFields, $hiddenFields);
            return;
        }

        if ($models instanceof IteratorAggregate ||
            is_array($models)
        ) {
            foreach ($models as $model) {
                $this->filterVisible($model, $visibleFields, $hiddenFields);
            }
        }
    }

    protected function setModelVisiblity($models, $visibleFields, $hiddenFields)
    {
        if (empty($visibleFields)) {
            $visibleFields = [];
        }
        if (empty($hiddenFields)) {
            $hiddenFields = [];
        }
        $relVisibleFields = array_filter($visibleFields, function ($value) {
            return str_contains($value, '.');
        });
        $visibleFields = array_filter($visibleFields, function ($value) {
            return !str_contains($value, '.');
        });

        $relHiddenFields = array_filter($hiddenFields, function ($value) {
            return str_contains($value, '.');
        });
        $hiddenFields = array_filter($hiddenFields, function ($value) {
            return !str_contains($value, '.');
        });

        $relVisibleFields = $this->flatArrayToNestedArray($relVisibleFields);
        $relHiddenFields = $this->flatArrayToNestedArray($relHiddenFields);

        foreach ($models->getRelations() as $relationName => $relationInstance) {
            $snakeRelationName = snake_case($relationName);
            $currentHiddenFields = array_get($relHiddenFields, $snakeRelationName);
            $currentVisibleFields = array_get($relVisibleFields, $snakeRelationName);
            $this->filterVisible($relationInstance, $currentVisibleFields, $currentHiddenFields);

            if (in_array($snakeRelationName, $visibleFields)) {
                $visibleFields[] = $relationName;
                unset($visibleFields[$snakeRelationName]);
            }
            if (in_array($snakeRelationName, $hiddenFields)) {
                $hiddenFields[] = $relationName;
                unset($hiddenFields[$snakeRelationName]);
            }
        }

        $models->setVisible($visibleFields);
        $models->setHidden(array_merge($models->getHidden(), $hiddenFields));
        $models->setDateFormat(Carbon::ISO8601);
    }

    public static function flatArrayToNestedArray($flatArray)
    {
        $result = [];
        foreach ($flatArray as $value) {
            $segments = explode('.', $value);
            $key = array_shift($segments);
            if (!isset($result[$key])) {
                $result[$key] = [];
            }
            $result[$key][] = implode('.', $segments);
        }
        return $result;
    }
}
