<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;
use \App\Model\User;

class UserController extends Controller
{

    protected $client;

    public function __construct()
    {
        $this->client = new Client(['base_uri' => env('POINT_API_URL', '')]);
    }

    public function verify($confirmationCode)
    {
        $user = User::whereConfirmationCode($confirmationCode)->first();

        if ($user) {
            $user->active = true;
            $user->confirmation_code = null;
            $user->save();

            $this->client->request(
                'POST',
                '/api/transactions/redeemFreeVoucher',
                [
                'headers' =>
                    ['unique_code' => env('UNIQUE_CODE')],
                'json' =>
                    ['member_id' => $user->member_id]
                ]
            );

            session()->flash(
                'message',
                'You have successfully verified your account. you may get back to the app for reward'
            );
        } else {
            session()->flash('message', 'Your account is already verified.');
        }
        return view('verify');
    }
}
