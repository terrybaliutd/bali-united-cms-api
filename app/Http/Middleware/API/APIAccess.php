<?php

namespace App\Http\Middleware\API;

use App\API\Exceptions\InvalidTokenException;

class APIAccess
{

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, \Closure $next)
    {
        $accessToken = $request->get('access_token');
        if ($accessToken == null) {
            return response()->json([
                'status' => 204,
                'message' => 'An access token is required to request this resource.'
            ]);
        }
        if (!\UserAccess::attempt($accessToken)) {
            return response()->json([
                'status' => 401,
                'message' => 'Access token is invalid.'
            ]);
        }
        return $next($request);
    }
}
