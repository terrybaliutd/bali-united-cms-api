<?php

namespace App\Http\Middleware;

use Closure;

class BaliUnitedStartSession
{
    /**
     * Exclude these routes from returning session cookie.
     * check 'config/cookie.php' first.
     *
     * @var [type]
     */
    protected $exclude = [
        //
    ];

    /**
     * [__construct description]
     */
    public function __construct()
    {
        $excludedRoutes = config('cookie.excluded_routes');
        $this->exclude = array_merge($excludedRoutes, $this->exclude);
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        foreach ($this->exclude as $exclude) {
            if ($request->is($exclude)) {
                config()->set('session.driver', 'array');
            }
        }
        return $next($request);
    }
}
