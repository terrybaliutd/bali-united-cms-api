<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;
use Illuminate\Session\TokenMismatchException;

class CustomCsrfToken extends BaseVerifier
{
    /**
     * Exclude these routes from returning CSRF token cookie.
     * check 'config/cookie.php' first.
     *
     * @var [type]
     */
    protected $exclude = [
    ];

    /**
     * [__construct description]
     */
    public function __construct()
    {
        $excludedRoutes = config('cookie.excluded_routes');
        $this->exclude = array_merge($excludedRoutes, $this->exclude);
    }

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     *
     * @throws \Illuminate\Session\TokenMismatchException
     */
    public function handle($request, Closure $next)
    {
        foreach ($this->exclude as $exclude) {
            if ($request->is($exclude)) {
                return $next($request);
            }
        }
        return parent::handle($request, $next);
    }
}
