<?php

\Route::match(
    ['GET', 'POST'],
    suitLoginUrl(),
    [
        'middleware' => 'admin.guest',
        'as' => suitRouteName('login'),
        'uses' => 'SessionController@login'
    ]
);

\Route::controller(suitPath('password'), 'RemindersController');

\Route::group(['prefix' => \Config::get('suitcms.prefix_url'), 'middleware' => 'admin.auth'], function () {
    // ElFinder
    \Route::get(
        'elfinder',
        [
            'as' => suitRouteName('elfinder'),
            'uses' => '\Barryvdh\Elfinder\ElfinderController@showTinyMCE4'
        ]
    );

    \Route::get('/', ['as' => suitRouteName('index'),
                      'uses' => 'SuitcmsController@index']);
    \Route::match(['GET', 'POST'], '/change-password', ['as' => suitRouteName('password'),
                                     'uses' => 'SessionController@changePassword']);
    \Route::get('/logout', ['as' => suitRouteName('logout'),
                                            'uses' => 'SessionController@logout']);

    \Route::resource('seasons', 'SeasonController', ['except' => 'show']);
    \Route::get('/season-list', [
        'as' => suitRouteName('season.list'),
        'uses' => 'SeasonController@getSeasonByCompetition'
    ]);
    \Route::resource('cities', 'CityController', ['except' => 'show']);
    \Route::resource('provinces', 'ProvinceController', ['except' => 'show']);
    \Route::resource('standings', 'StandingController', ['except' => 'show']);
    \Route::resource('gallery-categories', 'GalleryCategoryController', ['except' => 'show']);
    \Route::resource('videos', 'VideoController', ['except' => 'show']);
    \Route::resource('playlists', 'PlaylistController', ['except' => 'show']);
    \Route::resource('matches', 'MatchController', ['except' => 'show']);
    \Route::resource('competitions', 'CompetitionController', ['except' => 'show']);
    \Route::resource('news', 'NewsController', ['except' => 'show']);
    \Route::resource('players', 'PlayerController', ['except' => 'show']);
    \Route::resource('clubs', 'ClubController', ['except' => 'show']);
    \Route::resource('quizzes', 'QuizController', ['except' => 'show']);
    \Route::resource('quiz-users', 'QuizUserController', ['only' => 'index']);
    \Route::resource('pages', 'PageController', ['except' => 'show']);
    \Route::resource('users', 'UserController', ['except' => 'show']);
    \Route::resource('menus', 'MenuController', ['except' => 'show']);
    \Route::resource('settings', 'SettingController', ['except' => 'show']);
    \Route::resource('html-templates', 'HtmlTemplateController', ['except' => 'show']);
    \Route::resource('comments', 'CommentController', ['only' => ['index', 'show', 'destroy']]);
    \Route::resource('sticker-categories', 'StickerCategoryController', ['except' => 'show']);
    \Route::resource('citizen-journal-videos', 'CitizenJournalVideoController', ['except' => 'create', 'edit']);
    \Route::resource('notifications', 'NotificationController', ['except' => 'show']);
    \Route::resource('custom-notifications', 'CustomNotificationController', ['except' => 'show']);
    \Route::resource('reward-ads', 'RewardAdsController', ['except' => 'show']);
    \Route::resource('video-ads', 'VideoAdsController', ['except' => 'show']);
    \Route::post('video-ads/create', 'VideoAdsController@getInfo');
    \Route::resource('home-ads', 'HomeAdsController', ['except' => 'show']);
    \Route::resource('news-list-ads', 'NewsListAdsController', ['except' => 'show']);
    \Route::resource('video-list-ads', 'VideoListAdsController', ['except' => 'show']);
    \Route::resource('bank-accounts', 'BankAccountController', ['except' => 'show']);
    \Route::resource('memberships', 'MembershipController', ['except' => 'show']);
    \Route::resource('posts', 'PostController');
    \Route::resource('post-comments', 'PostCommentController', ['except' => 'show']);
    \Route::get('posts/{post}/comments', 'PostCommentController@indexByPost')
        ->name(suitRouteName('post-comments.index'));
    \Route::get('posts/{post}/comments/create', 'PostCommentController@createByPost')
        ->name(suitRouteName('post-comments.create'));
    \Route::resource('tags', 'TagController', ['except' => 'show']);
    \Route::resource('post-reports', 'PostReportController', ['except' => 'show']);
    \Route::resource('post-comment-reports', 'PostCommentReportController', ['except' => 'show']);
    \Route::post('citizen-journal-videos/accept', [
        'as' => suitRouteName('citizen-journal-videos.accept'),
        'uses' => 'CitizenJournalVideoController@accept'
    ]);
    \Route::post('citizen-journal-videos/reject', [
        'as' => suitRouteName('citizen-journal-videos.reject'),
        'uses' => 'CitizenJournalVideoController@reject'
    ]);

    \Route::get('quizzes/import', 'QuizController@importForm')->name("backend.quizzes.import");
    \Route::post('quizzes/import', 'QuizController@import')->name("backend.quizzes.import");

    \Route::post('videos/create', 'VideoController@getInfo');

    \Route::post('playlists/create', 'PlaylistController@getInfo');
    \Route::get('playlists/{id}/synch', 'PlaylistController@synchronize');

    //Template Route
    \Route::get(
        'template/list',
        [
            'as' => suitRouteName('template.list'),
            'uses' => 'HtmlTemplateController@templateList'
        ]
    );
    \Route::get(
        'template/{key}',
        [
            'as' => suitRouteName('template.detail'),
            'uses' => 'HtmlTemplateController@template'
        ]
    );
});
