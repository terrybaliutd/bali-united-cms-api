<?php

Route::group(['prefix' => 'api', 'namespace' => 'Api'], function () {

    Route::group(['prefix' => 'semeton-journalism', 'middleware' => 'api.access'], function () {
        Route::group(['prefix' => 'posts'], function () {
            Route::get('', [
                'as' => 'api.posts.index',
                'uses' => 'PostController@index',
            ]);

            Route::post('', [
                'as' => 'api.posts.store',
                'uses' => 'PostController@store',
            ]);

            Route::get('search', [
                'as' => 'api.posts.search',
                'uses' => 'PostController@search',
            ]);

            Route::get('{key}', [
                'as' => 'api.posts.show',
                'uses' => 'PostController@show',
            ]);

            Route::post('{key}/update', [
                'as' => 'api.posts.update',
                'uses' => 'PostController@update',
            ]);

            Route::delete('{key}', [
                'as' => 'api.posts.destroy',
                'uses' => 'PostController@destroy',
            ]);

            // Route for add/remove like on a post
            Route::post('{key}/likes', [
                'as' => 'api.posts.like',
                'uses' => 'PostController@like',
            ]);

            Route::post('{key}/comments', [
                'as' => 'api.post.comment.store',
                'uses' => 'PostCommentController@store',
            ]);

            Route::post('{key}/reports', [
                'as' => 'api.post.report.store-report',
                'uses' => 'PostController@report',
            ]);
        });

        Route::group(['prefix' => 'comments'], function () {
            Route::get('', [
                'as' => 'api.comments.index',
                'uses' => 'PostCommentController@index',
            ]);

            Route::post('{key}/update', [
                'as' => 'api.comments.update',
                'uses' => 'PostCommentController@update',
            ]);

            Route::delete('{key}', [
                'as' => 'api.comments.destroy',
                'uses' => 'PostCommentController@destroy'
            ]);

            Route::post('{key}/likes', [
                'as' => 'api.comments.like',
                'uses' => 'PostCommentController@like'
            ]);

            Route::post('{key}/replies', [
                'as' => 'api.comments.store-reply',
                'uses' => 'PostCommentController@storeReply'
            ]);

            Route::post('{key}/reports', [
                'as' => 'api.comments.report',
                'uses' => 'PostCommentController@report'
            ]);
        });

        Route::group(['prefix' => 'tags'], function () {
            Route::get('', [
                'as' => 'api.tags.index',
                'uses' => 'TagController@index',
            ]);

            Route::get('{key}', [
                'as' => 'api.tags.show',
                'uses' => 'TagController@show',
            ]);

            Route::get('{key}/posts', [
                'as' => 'api.tags.posts-index',
                'uses' => 'TagController@posts',
            ]);
        });
    });

    // Post Video API (endpoint for video-uploader-service)
    Route::post('videos-posts', [
        'as' => 'api.video.store',
        'uses' => 'PostController@postVideo'
    ]);
});
