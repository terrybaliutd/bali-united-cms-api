<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Model\CitizenJournalVideo;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;
use FFMpeg\FFMpeg;

class ConvertVideo extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $video;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(CitizenJournalVideo $video)
    {
        $this->video = $video;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $ffmpeg = FFMpeg::create([
            'ffmpeg.binaries' => exec('which ffmpeg'),
            'ffprobe.binaries' => exec('which ffprobe'),
        ]);
        $video = $ffmpeg->open(public_path().'/'.$this->video->video);
        $pathParts = pathinfo($this->video->video);
        $newFileDir = $pathParts['dirname'].'/'.$pathParts['filename'].'_converted.mp4';
        $newFileName = public_path().'/'.$newFileDir;
        $video->save(new \FFMpeg\Format\Video\X264('libmp3lame', 'libx264'), $newFileName);

        $this->video->converted_video = $newFileDir;
        $this->video->status = "converted";
        $this->video->save();
    }
}
