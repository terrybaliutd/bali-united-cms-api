<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Model\Match;
use App\Notifications\MatchPushNotification;
use Carbon\Carbon;
use Illuminate\Contracts\Bus\SelfHandling;

class UpdateMatchStatus extends Job implements SelfHandling
{
    protected $notification;

    public function __construct(MatchPushNotification $notification = null)
    {
        $this->notification = $notification;
    }

    public function handle()
    {
        // harus pakai start of day karena waktu match api dari labbola terkadang tidak di set.
        $startDate = Carbon::now()->startOfDay();
        $endDate = Carbon::now()->addDay(1)->startOfDay();

        $matches = $this->getMatchQuery($startDate, $endDate)->get();
        foreach ($matches as $match) {
            $sendNotification = false;
            // $currentStatus = $match->status;
            $matchVersus = "{$match->home_club->club->name} vs {$match->away_club->club->name}";
            if ($this->isUpcomingMatch($match) && !$match->isOnHold()) {
                $matchTime = "{$match->start_at->format('H:i')}";
                $notificationText =  "[$matchTime] {$match->tv_info}";
                $sendNotification = true;
            }

            if (!($this->notification == null) && $sendNotification) {
                if ($match->isSchedule()) {
                    $this->notification->setClickAction('com.baliutd.open_upcoming');
                    $type = 'upcoming';
                    \Log::info('send match notif upcoming');
                }
                $this->notification->send($match, $notificationText, $type, $matchVersus);
            }

            if ($this->isMatchStarted($match) && ($match->live_video_id == null)) {
                $match->status = Match::STATUS_PLAYING;
                $match->save();
            }
        }
    }

    protected function getMatchQuery($startDate, $endDate)
    {
        return Match::whereBetween('start_at', [$startDate, $endDate]);
    }

    protected function isUpcomingMatch($match)
    {
        return $match->status == Match::STATUS_SCHEDULE && (
               abs($match->start_at->subHours(1)->diffInSeconds(Carbon::now())) < 50 ||
               abs($match->start_at->subMinutes(15)->diffInSeconds(Carbon::now())) < 50
        );
    }

    protected function isLiveMatch($match)
    {
        return ($match->live_video_id != null) && $match->status == Match::STATUS_PLAYING && (
               abs($match->start_at->diffInSeconds(Carbon::now())) < 50
        );
    }

    protected function isMatchStarted($match)
    {
        return $match->status == Match::STATUS_SCHEDULE && (abs($match->start_at->diffInSeconds(Carbon::now())) < 50);
    }
}
