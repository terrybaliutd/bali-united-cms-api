<?php

namespace App\Jobs;

use App\Jobs\Job;
use App\Model\CitizenJournalVideo;
use App\Model\Video;
use App\Supports\Attachment\ResourceNotFoundException;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldQueue;

class UploadVideo extends Job implements SelfHandling, ShouldQueue
{
    use InteractsWithQueue, SerializesModels;

    protected $citizenVideo;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(CitizenJournalVideo $citizenVideo)
    {
        $this->citizenVideo = $citizenVideo;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // Upload citizenJournalVideo
        $upload = \YoutubeOAuth::upload($this->citizenVideo->converted_video, [
            'title'       => $this->citizenVideo->title,
            'description' => $this->citizenVideo->description
        ]);

        // Create new Video
        $url = "https://youtube.com/watch?v=" . $upload;
        $video = Video::createFromUrl($url);
        $video->publisher()->associate($this->citizenVideo->user_id);

        try {
            $video->save();
        } catch (ResourceNotFoundException $e) {
            $video->attachment = null;
            $video->save();
        }

        // Update citizenJournalVideo status
        $this->citizenVideo->video_id = $video->id;
        $this->citizenVideo->status = 'published';
        $this->citizenVideo->save();
    }
}
