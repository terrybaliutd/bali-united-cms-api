<?php

namespace App\Listeners;

use App\Events\UserRegistered;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class UserRegistration
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  UserRegistered  $event
     * @return void
     */
    public function handle(UserRegistered $event)
    {
        $client = new \GuzzleHttp\Client(['base_uri' => env('POINT_API_URL')]);

        $client->post('api/users', [
            'headers' => [
                'unique_code' => env('UNIQUE_CODE')
            ],
            'form_params' => [
                'email' => $event->user->email,
                'name' => $event->user->name,
                'member_id' => $event->user->member_id,
            ]
        ]);
    }
}
