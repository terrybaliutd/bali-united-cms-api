<?php

namespace App\Model;

use Carbon\Carbon;

class AccessToken extends BaseModel
{
    protected $table = 'access_tokens';

    protected $fillable = [
        'token',
        'refresh_token',
        'expired_at'
    ];

    protected $visible = [
        'token',
        'expired_at'
    ];

    protected $dates = [
        'expired_at'
    ];

    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'refresh_token' => 'string',
        'token' => 'string',
        'expired_at' => 'timestamp',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];


    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function scopeExpired($query)
    {
        return $query->where('expired_at', '<', Carbon::now());
    }

    public function scopeNotExpired($query)
    {
        return $query->where('expired_at', '>=', Carbon::now());
    }

    public function isExpired()
    {
        return $this->expired_at->lte(Carbon::now());
    }

    public function getAccessTokenOwner(array $credentials)
    {
        if (!isset($credentials['access_token'])) {
            return null;
        }

        return $this->notExpired()->whereAccessToken($credentials['access_token'])->first();
    }
}
