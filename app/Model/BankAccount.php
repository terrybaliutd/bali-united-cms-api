<?php

namespace App\Model;

use App\Model\Extension\PublishableTrait;
use App\Model\Extension\ResponseCacheTrait;
use Illuminate\Database\Eloquent\Model;

class BankAccount extends BaseModel
{
    use PublishableTrait;
    use ResponseCacheTrait;

    protected $table = 'bank_accounts';

    protected $dates = [
        'published_at',
    ];

    protected $fillable = [
        'bank_name',
        'account_number',
        'account_name',
        'published',
    ];

    protected $casts = [
        'bank_name' => 'string',
        'account_number' => 'string',
        'account_name' => 'string',
    ];

    public static function boot()
    {
        parent::boot();

        static::saved(function ($model) {
            forgetCache('BankAccountRepository');
        });
        static::deleted(function ($model) {
            forgetCache('BankAccountRepository');
        });
    }
}
