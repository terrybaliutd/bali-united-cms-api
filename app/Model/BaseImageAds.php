<?php

namespace App\Model;

use App\Model\Extension\AttachableTrait;
use App\Model\Extension\PublishableTrait;

class BaseImageAds extends BaseModel
{
    use PublishableTrait;

    protected $dates = [
        'published_at'
    ];

    protected $appends = [
        'type'
    ];

    public function getTypeAttribute()
    {
        return "ads";
    }
}
