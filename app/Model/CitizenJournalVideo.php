<?php

namespace App\Model;

use App\Model\Extension\AttachableTrait;
use App\Model\Extension\ResponseCacheTrait;

class CitizenJournalVideo extends BaseModel
{
    use AttachableTrait;
    use ResponseCacheTrait;

    protected $table = 'citizen_journal_videos';

    protected $fillable = [
        'user_id',
        'title',
        'description',
        'video',
        'converted_video',
        'status',
        'video_id'
    ];

    protected $searchField = [
        'title',
        'description',
        'status'
    ];

    protected $attachable = [
        'video' => [
            'thumb' => []
        ],
        'converted_video' => [
            'thumb' => []
        ],
    ];

    protected $hidden = [
        'video',
        'video_type',
        'video_info',
        'converted_video',
        'converted_video_type',
        'converted_video_info'
    ];

    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'title' => 'string',
        'description' => 'string',
        'status' => 'string',
        'video' => 'string',
        'video_type' => 'integer',
        'video_info' => 'string',
        'converted_video' => 'string',
        'converted_video_type' => 'integer',
        'converted_video_info' => 'string',
        'video_id' => 'integer',
    ];

    public function user()
    {
        return $this->belongsTo(\App\Model\User::class, 'user_id');
    }

    public function vid()
    {
        return $this->belongsTo(\App\Model\Video::class, 'video_id');
    }
}
