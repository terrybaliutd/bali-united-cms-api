<?php

namespace App\Model;

use App\Model\Extension\ResponseCacheTrait;
use App\Model\Province;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class City extends BaseModel implements SluggableInterface
{
    use SluggableTrait;
    use ResponseCacheTrait;

    protected $table = 'cities';

    protected $fillable = [
        'code',
        'name'
    ];

    protected $sluggable = [
        'build_from' => 'name'
    ];

    protected $urlKey = 'slug';

    protected $searchField = [
        'province.name',
        'code',
        'name'
    ];

    protected $casts = [
        'id' => 'integer',
        'province_id' => 'integer',
        'code' => 'string',
        'name' => 'string',
        'slug' => 'string',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];

    public function province()
    {
        return $this->belongsTo(Province::class, 'province_id');
    }
}
