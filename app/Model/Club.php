<?php

namespace App\Model;

use App\Model\Extension\AttachableTrait;
use App\Model\Extension\PublishableTrait;
use App\Model\Extension\ResponseCacheTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

/**
 * @property integer    id
 * @property string     api_id
 * @property string     name
 * @property string     nick_name
 * @property string     official_name
 * @property string     slug
 * @property string     abbreviation
 * @property string     home_base
 * @property string     manager
 * @property string     supporter_nick_name
 * @property string     stadium_name
 * @property string     logo_url
 * @property \Carbon\Carbon|null    establisted_at
 * @property \Carbon\Carbon|null    published_at
 * @property \Carbon\Carbon|null    created_at
 * @property \Carbon\Carbon|null    updated_at
 *
 * @property-read boolean   published
 */
class Club extends BaseModel implements SluggableInterface
{
    use AttachableTrait;
    use PublishableTrait;
    use ResponseCacheTrait;
    use SluggableTrait;

    protected $table = 'clubs';

    protected $sluggable = [
        'build_from' => 'name'
    ];

    protected $fillable = [
        'api_id',
        'name',
        'nick_name',
        'official_name',
        'abbreviation',
        'established_at',
        'home_base',
        'manager',
        'supporter_nick_name',
        'stadium_name',
        'logo_url',
        'slug',
        'published',
    ];

    protected $appends = [
        'logo_thumb',
    ];

    protected $dates = [
        'published_at',
        'created_at',
        'established_at',
    ];

    protected $urlKey = 'slug';

    protected $attachable = [
        'logo_url' => [
            'thumb' => [
                'small' => '_x70',
                'big' => '_x200'
            ]
        ]
    ];

    protected $searchField = [
        'name',
        'official_name'
    ];

    protected $casts = [
        'id' => 'integer',
        'api_id' => 'string',
        'name' => 'string',
        'nick_name' => 'string',
        'official_name' => 'string',
        'abbreviation' => 'string',
        'home_base' => 'string',
        'manager' => 'string',
        'supporter_nick_name' => 'string',
        'stadium_name' => 'string',
        'logo_url' => 'string',
        'logo_url_type' => 'integer',
        'logo_url_info' => 'string',
        'slug' => 'string',
    ];

    public function getLogoThumbAttribute()
    {
        return $this->getThumbnail('logo_url', 'small', asset('assets/img/logo-default.png'));
    }
}
