<?php

namespace App\Model;

use App\Model\Extension\ResponseCacheTrait;

class Comment extends BaseModel
{
    use ResponseCacheTrait;
    
    const TYPE_MESSAGE = 'message';
    const TYPE_STICKER = 'sticker';
    const TYPE_NOTIFICATION = 'notification';

    protected $table = 'comments';

    protected $fillable = [
        'video_id',
        'user_id',
        'message',
        'sticker_item_id',
    ];

    protected $searchField = [
        'video_id',
        'user_id',
        'message',
    ];

    protected $casts = [
        'id' => 'string',
        'video_id' => 'integer',
        'user_id' => 'integer',
        'type' => 'string',
        'sticker_item_id' => 'integer',
        'message' => 'string',
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $prefixId = $model->video_id;
            $model->id = uniqid(hash('md5', $prefixId), false);
        });
    }

    public function video()
    {
        return $this->belongsTo(Video::class, 'video_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function stickerItem()
    {
        return $this->belongsTo(StickerItem::class, 'sticker_item_id');
    }

    public function getVideoTitleAttribute()
    {
        return $this->video->title;
    }

    public function getUsernameAttribute()
    {
        return $this->user->username;
    }

    public function isMessage()
    {
        return $this->type === static::TYPE_MESSAGE;
    }

    public function isSticker()
    {
        return $this->type === static::TYPE_STICKER;
    }

    public function setAttribute($key, $value)
    {
        if (empty($value)) {
            return parent::setAttribute($key, null);
        }

        return parent::setAttribute($key, $value);
    }
}
