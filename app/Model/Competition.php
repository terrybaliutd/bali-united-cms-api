<?php

namespace App\Model;

use App\Model\Extension\OrderableTrait;
use App\Model\Extension\PublishableTrait;
use App\Model\Extension\ResponseCacheTrait;
use App\Model\Match;
use App\Model\Season;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

/**
 * @property integer    id
 * @property string     api_id
 * @property string     name
 * @property string     long_name
 * @property string     slug
 * @property integer    order
 * @property boolean    is_default
 * @property \Carbon\Carbon|null    published_at
 * @property \Carbon\Carbon|null    created_at
 * @property \Carbon\Carbon|null    updated_at
 *
 * @property-read boolean   published
 *
 * @method static Competition|Builder useDefault()
 * @method static Competition|Builder ordered()
 */
class Competition extends BaseModel implements SluggableInterface
{
    use OrderableTrait;
    use PublishableTrait;
    use ResponseCacheTrait;
    use SluggableTrait;

    protected $table = 'competitions';

    protected $sluggable = [
        'build_from' => 'long_name'
    ];

    protected $urlKey = 'slug';

    protected $fillable = [
        'name',
        'slug',
        'long_name',
        'order',
        'is_default',
        'api_id',
        'published',
    ];

    protected $dates = [
        'published_at'
    ];

    protected $appends = [
        'published'
    ];

    protected $searchField = [
        'name',
        'long_name',
        'order',
        'api_id',
        'id'
    ];

    protected $casts = [
        'id' => 'integer',
        'api_id' => 'string',
        'name' => 'string',
        'long_name' => 'string',
        'order' => 'integer',
        'is_default' => 'boolean',
        'slug' => 'string',
    ];

    public function matches()
    {
        return $this->hasMany(Match::class, 'competition_id');
    }

    public function seasons()
    {
        return $this->belongsToMany(Season::class, 'competition_season', 'competition_id', 'season_id');
    }

    public function scopeUseDefault($query)
    {
        return $query->where('is_default', true);
    }
}
