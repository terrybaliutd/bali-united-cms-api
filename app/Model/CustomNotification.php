<?php

namespace App\Model;

use App\Model\Extension\PublishableTrait;
use App\Model\Extension\ResponseCacheTrait;
use App\Notifications\CustomPushNotification;

class CustomNotification extends BaseModel
{
    use PublishableTrait;
    use ResponseCacheTrait;

    protected $table = 'custom_notifications';

    protected $fillable = [
        'title',
        'body',
        'published'
    ];

    protected $searchField = [
        'title',
        'body'
    ];

    protected $dates = [
        'published_at'
    ];

    protected static function boot()
    {
        parent::boot();

        static::created(function ($model) {
            if ($model->published) {
                $notification = app(CustomPushNotification::class);
                $notification->send($model->body, $model->title);
            }
        });
        static::updating(function ($model) {
            if ($model->isDirty('published_at') && $model->published) {
                $notification = app(CustomPushNotification::class);
                $notification->send($model->body, $model->title);
            }
        });
    }
}
