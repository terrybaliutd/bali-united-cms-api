<?php

namespace App\Model\Extension;

use Croppa;

trait AdditionalThumbnailTrait
{
    public function attributesToArray()
    {
        $attributes = parent::attributesToArray();
        $origin = $this->getThumbnailOrigin();

        if (empty($origin) || !method_exists($this, 'getThumbnailInfo')) {
            return $attributes;
        }

        foreach ($this->getAdditionalThumbnails() as $field => $thumbs) {
            foreach ($thumbs as $key => $options) {
                $options = $this->getThumbnailInfo($options);
                $attributes["{$field}_{$key}"] = asset(Croppa::url(
                    $this->getAttribute($origin),
                    $options['width'],
                    $options['height']
                ));
            }
        }

        return $attributes;
    }

    protected function getAdditionalThumbnails()
    {
        if (isset($this->additionalThumbnails) && is_array($this->additionalThumbnails)) {
            return $this->additionalThumbnails;
        }
        return [];
    }

    protected function getThumbnailOrigin()
    {
        if (isset($this->thumbnailOrigin)) {
            return $this->thumbnailOrigin;
        }

        if (isset($this->attachable) && is_array($this->attachable)) {
            reset($this->attachable);
            return key($this->attachable);
        }

        return null;
    }
}
