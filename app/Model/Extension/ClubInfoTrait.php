<?php

namespace App\Model\Extension;

use App\Model\MatchClubInfo;

trait ClubInfoTrait
{
    public function clubInfos()
    {
        return $this->hasMany(MatchClubInfo::class, 'match_id');
    }

    public function getClubInfosAttribute()
    {
        return $this->clubInfos()->with(['club'])->get();
    }

    public function getHomeClubAttribute()
    {
        return $this->clubInfos()->where('type', 'Home')->first();
    }

    public function getAwayClubAttribute()
    {
        return $this->clubInfos()->where('type', 'Away')->first();
    }
}
