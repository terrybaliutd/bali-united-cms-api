<?php

namespace App\Model\Extension;

trait ImpersonateNewsTrait
{
    public function getContentAttribute()
    {
        return '<p><a href="'. $this->getAttribute('url') .'" target="_blank">Click here to view more</a></p>';
    }

    public function getNewsDateAttribute()
    {
        return $this->getAttribute('published_at')->format($this->getDateFormat());
    }
}
