<?php

namespace App\Model\Extension;

use Croppa;

trait ImpersonateVideoTrait
{
    public function getKeyAttribute()
    {
        return null;
    }

    public function getThumbnailAttribute()
    {
        return asset(Croppa::url($this->getAttribute('attachment'), 320, 180));
    }

    public function getTimeDurationAttribute()
    {
        return '00:00';
    }

    public function getUploadDateAttribute()
    {
        return $this->getAttribute('published_at')->format($this->getDateFormat());
    }
}
