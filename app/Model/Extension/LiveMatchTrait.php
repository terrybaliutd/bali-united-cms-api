<?php

namespace App\Model\Extension;

use Carbon\Carbon;

trait LiveMatchTrait
{
    protected static function getCurrentMatchDateRange()
    {
        return [Carbon::now()->subHours(2), Carbon::now()->addMinutes(30)];
    }

    public function scopeLive($query)
    {
        $dateRange = static::getCurrentMatchDateRange();
        $startDate = $dateRange[0];
        $endDate = $dateRange[1];

        return $query->where('start_at', '>=', $startDate)
                     ->where('start_at', '<=', $endDate)
                     ->where('status', "Playing");
    }

    public function scopeRelevant($query)
    {
        return $query->where('start_at', '<=', Carbon::now()->addWeeks(2))
                     ->where('start_at', '>=', Carbon::now()->subWeeks(2))
                     ->orderBy('id', 'asc');
    }
}
