<?php

namespace App\Model\Extension;

use App\Model\User;

trait LoginTrait
{
    public function setPasswordAttribute($value)
    {
        if (!empty($value) || is_numeric($value)) {
            $this->attributes['password'] = \Hash::make($value);
        }
    }

    public function login()
    {
        if ($this->current_log_in_at !== null) {
            $this->last_log_in_at = $this->current_log_in_at;
            $this->last_log_in_ip = $this->current_log_in_ip;
        }

        $this->current_log_in_at = new \DateTime;
        $this->current_log_in_ip = \Request::getClientIp();
        $this->save();

        $this->increment('log_in_count');
    }

    public function logout()
    {
        $this->last_log_in_at = new \DateTime;
        $this->last_log_in_ip = \Request::getClientIp();
        $this->current_log_in_at = null;
        $this->current_log_in_ip = '';
        $this->save();
    }

    public function scopeNotMe($query)
    {
        if (\Auth::check()) {
            return $query->where('id', '<>', \Auth::user()->id);
        }
    }

    public function isMe()
    {
        $auth = \Auth::user();
        return get_class($this) == get_class($auth) && $this->id === $auth->id;
    }
}
