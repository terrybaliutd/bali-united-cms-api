<?php

namespace App\Model\Extension;

use Carbon\Carbon;

trait MatchStatusTrait
{
    public function isSchedule()
    {
        return $this->status == static::STATUS_SCHEDULE;
    }

    public function isPlaying()
    {
        return $this->status == static::STATUS_PLAYING;
    }

    public function isFinished()
    {
        return $this->status == static::STATUS_FINISHED && $this->start_at < Carbon::now();
    }

    public function isSuspended()
    {
        return $this->status == static::STATUS_SUSPENDED;
    }

    public function isOnHold()
    {
        return in_array($this->status, [static::STATUS_SUSPENDED, static::STATUS_POSTPONE]);
    }
}
