<?php

namespace App\Model\Extension;

use App\Model\Observers\ResponseCacheObserver;

trait ResponseCacheTrait
{
    /**
     * Attach a new observer into the current model object.
     *
     * @return void
     */
    public static function bootResponseCacheTrait()
    {
        static::observe(app(ResponseCacheObserver::class));
    }
}
