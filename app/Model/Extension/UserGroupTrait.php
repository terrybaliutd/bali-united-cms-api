<?php

namespace App\Model\Extension;

use App\Model\User;

trait UserGroupTrait
{
    public function getGroupTypeAttribute()
    {
        if (!isset($this->attributes['group_type'])) {
            return null;
        }

        $group = $this->attributes['group_type'];
        if (!is_numeric($group)) {
            return -1;
        }
        return (int)$group;
    }

    public function isSuper()
    {
        return $this->group_type === static::SUPER_ADMIN;
    }

    public function isUser()
    {
        return $this->group_type === static::USER;
    }

    public function getGroupName()
    {
        if (!in_array($this->group_type, array_keys(static::groups()))) {
            throw new \Exception('Group type [' . $this->group_type . '] is not defined');
        }

        return static::groups()[$this->group_type];
    }

    public static function groups()
    {
        return [
            static::SUPER_ADMIN => 'Super Administrator',
            static::USER => 'User'
        ];
    }
}
