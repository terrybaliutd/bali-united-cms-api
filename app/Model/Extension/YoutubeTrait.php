<?php

namespace App\Model\Extension;

use App\Model\Video;

trait YoutubeTrait
{
    public static function getInfo($key)
    {
        $result = \YoutubeData::getVideoInfo($key);
        if ($result) {
            $return = $result->snippet;
            $return->duration = $result->contentDetails->duration;
            return $return;
        } else {
            return null;
        }
    }

    public static function getVideoKey($url)
    {
        $reg = '/v=([^&]+)/';
        preg_match($reg, $url, $match);
        if ($match != null) {
            return $match[1];
        } else {
            return null;
        }
    }

    public static function getMaxResThumbnail($info)
    {
        $keys = ['maxres', 'standard', 'high', 'medium', 'default'];
        foreach ($keys as $key) {
            if (property_exists($info->thumbnails, $key)) {
                return $info->thumbnails->$key;
            }
        }
    }

    public static function createFromUrl($url)
    {
        $key = Video::getVideoKey($url);
        if ($key == null) {
            return null;
        }

        $info = Video::getInfo($key);
        if ($info == null) {
            return null;
        }

        $video = new Video();
        $video->title = $info->title;
        $video->key = $key;
        $video->duration = $info->duration;
        $video->description = $info->description;
        $video->attachment = Video::getMaxResThumbnail($info)->url;
        $video->upload_date = (new \Carbon\Carbon($info->publishedAt))->setTimezone("Asia/Jakarta");
        $video->published = true;

        return $video;
    }

    public function setVideoInfo($info)
    {
        $this->duration = $info->duration;
        $this->attachment = $info->thumbnails->high->url;
        // $this->published = true;
    }

    public function getThumbnailUrlAttribute()
    {
        return asset($this->thumbnail);
    }

    public function getThumbnailAttribute()
    {
        return $this->getAttachmentSmallAttribute('attachment');
    }

    public function getAttachmentSmallAttribute()
    {
        return $this->getThumbnail('attachment', 'low');
    }

    public function getYoutubeUrlAttribute()
    {
        return "www.youtube.com/watch?v=" . $this->key;
    }

    public function getTimeDurationAttribute()
    {
        return \App\Supports\FormatDuration::convert($this->duration);
    }
}
