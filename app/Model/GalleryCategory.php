<?php

namespace App\Model;

use App\Model\Extension\AttachableTrait;
use App\Model\Extension\PublishableTrait;
use App\Model\Extension\ResponseCacheTrait;
use App\Model\Extension\TreeTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

/**
 * @property integer                id
 * @property integer                parent_id      Parent category id
 * @property string                 title          Category title
 * @property string                 slug           Model unique slug
 * @property string                 image
 * @property \Carbon\Carbon|null    published_at   Published date
 * @property \Carbon\Carbon|null    created_at     Created at
 * @property \Carbon\Carbon|null    updated_at     Updated at
 * @property-read bool              published      Is published
 *
 * @property-read \Illuminate\Database\Eloquent\Collection  $galleries      Collection of galleries
 */
class GalleryCategory extends BaseModel implements SluggableInterface
{
    use AttachableTrait;
    use PublishableTrait;
    use ResponseCacheTrait;
    use SluggableTrait;
    use TreeTrait;

    protected $table = 'gallery_categories';

    protected $sluggable = [
        'build_from' => 'title'
    ];

    protected $fillable = [
        'title',
        'published',
        'image',
    ];

    protected $urlKey = 'slug';

    protected $dates = [
        'published_at'
    ];

    protected $searchField = [
        'title',
        'parent.title',
        'created_at',
        'updated_at'
    ];

    protected $attachable = [
        'image' => [
            'thumb' => [
                'small' => '300x_'
            ]
        ]
    ];

    protected $casts = [
        'id' => 'integer',
        'parent_id' => 'integer',
        'title' => 'string',
        'slug' => 'string',
        'published_at' => 'datetime',
        'image' => 'string',
        'image_type' => 'integer',
        'image_info' => 'string',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];

    /**
     * Gallery Relationship
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function items()
    {
        return $this->hasMany('App\Model\GalleryItem', 'gallery_category_id');
    }

    public static function generateAdminCategoryList($categories, $depth = 0)
    {
        $categoryList = collect();

        if (!empty($categories)) {
            foreach ($categories as $category) {
                $title = $category->title;
                if ($depth > 0) {
                    $title = str_repeat('|--&nbsp;', $depth) . $title;
                }
                $categoryList["{$category->getKey()}"] = $title;
                $childCategoryList = static::generateAdminCategoryList($category->childs, $depth + 1);
                foreach ($childCategoryList as $key => $childCategoryTitle) {
                    $categoryList[$key] = $childCategoryTitle;
                }
            }
        }

        return $categoryList;
    }
}
