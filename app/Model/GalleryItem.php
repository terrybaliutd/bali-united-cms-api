<?php

namespace App\Model;

use App\Model\Extension\AttachableTrait;
use App\Model\Extension\PublishableTrait;
use App\Model\Extension\ResponseCacheTrait;
use App\Model\Extension\TaggableTrait;
use App\Model\GalleryCategory;

/**
 * @property integer    id
 * @property integer    gallery_category_id
 * @property string     title
 * @property string     type
 * @property boolean    featured
 * @property string     attachment
 * @property \Carbon\Carbon|null    published_at
 * @property \Carbon\Carbon|null    created_at
 * @property \Carbon\Carbon|null    updated_at
 */
class GalleryItem extends BaseModel
{

    use AttachableTrait;
    use PublishableTrait;
    use ResponseCacheTrait;
    use TaggableTrait;

    const TYPE_IMAGE = 'image';
    const TYPE_VIDEO = 'video';

    protected $fillable = [
        'title',
        'type',
        'published',
        'tags',
        'attachment',
        'featured',
    ];

    protected $searchField = [
        'title',
        'category.title',
        'type',
        'published_at',
        'created_at',
        'updated_at'
    ];

    protected $attachable = [
        'attachment' => [
            'thumb' => [
                'list' => '1024x683',
                'small' => '320x180',
                'index' => '1024x683',
                'big_preview' => '_x500',
                'small_dynamic_width' => '_x90',
                'small_dynamic_height' => '320x_'
            ]
        ]
    ];

    protected $hidden = [
        'tags',
    ];

    protected $appends = [
        'string_tags',
        'thumbnail',
        'media_url',
    ];

    protected $dates = [
        'published_at'
    ];

    protected $casts = [
        'id' => 'integer',
        'gallery_category_id' => 'integer',
        'title' => 'string',
        'type' => 'string',
        'featured' => 'boolean',
        'slug' => 'string',
        'attachment' => 'string',
        'attachment_type' => 'integer',
        'attachment_info' => 'string',
        'published_at' => 'datetime',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];

    public function category()
    {
        return $this->belongsTo(GalleryCategory::class, 'gallery_category_id');
    }

    public static function getTypes()
    {
        return [
            static::TYPE_IMAGE => 'Image',
            static::TYPE_VIDEO => 'Video',
        ];
    }

    public function getStringTagsAttribute()
    {
        return $this->getStringTags();
    }

    public function getMediaUrlAttribute()
    {
        if ($this->isVideo()) {
            return $this->getAttachmentInfo('attachment', 'embed_url');
        } else {
            return asset($this->attachment);
        }
    }

    public function getThumbnailAttribute()
    {
        return $this->getAttachmentSmallAttribute('attachment');
    }

    public function getAttachmentSmallAttribute()
    {
        return $this->getThumbnail('attachment', 'small');
    }
}
