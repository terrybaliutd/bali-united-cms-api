<?php

namespace App\Model;

use App\Model\Extension\AdditionalThumbnailTrait;
use App\Model\Extension\AttachableTrait;
use App\Model\Extension\ImpersonateNewsTrait;
use App\Model\Extension\ImpersonateVideoTrait;
use App\Model\Extension\ResponseCacheTrait;

class HomeAds extends BaseImageAds
{
    use AttachableTrait;
    use ImpersonateNewsTrait;
    use ImpersonateVideoTrait;
    use AdditionalThumbnailTrait;
    use ResponseCacheTrait;

    protected $additionalThumbnails = [
        'attachment' => [
            'list' => '1024x683',
            'index' => '1024x683',
            'big_preview' => '_x500',
            'small_dynamic_width' => '_x90',
            'small_dynamic_height' => '320x_',
        ],
        'image' => [
            'list' => '250x250',
            'detail' => '_x400',
            'big' => '1300x_',
            'home' => '1280x590',
            'news' => '600x200',
        ]
    ];

    protected $appends = [
        'content',
        'key',
        'news_date',
        'thumbnail',
        'time_duration',
        'type',
        'upload_date',
    ];

    protected $attachable = [
        'attachment' => [
            'thumb' => [
                'high' => '508x356',
                'med' => '254x178',
                'low' => '127x89',
            ]
        ]
    ];

    protected $fillable = [
        'title',
        'url',
        'attachment',
        'priority',
        'published'
    ];

    protected static function boot()
    {
        parent::boot();

        static::saved(function ($model) {
            forgetCache('VideoRecentRepository');
            forgetCache('NewsRecentRepository');
            forgetCache('HomeAdsRepository');
        });
        static::deleted(function ($model) {
            forgetCache('VideoRecentRepository');
            forgetCache('NewsRecentRepository');
            forgetCache('HomeAdsRepository');
        });
    }
}
