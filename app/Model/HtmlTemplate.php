<?php

namespace App\Model;

use App\Model\Extension\ResponseCacheTrait;
use Illuminate\Support\Str;

class HtmlTemplate extends BaseModel
{
    use ResponseCacheTrait;
    
    protected $table = 'html_templates';

    protected $fillable = [
        'name',
        'title',
        'description',
        'html',
    ];

    protected $searchField = [
        'title',
        'description'
    ];

    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'title' => 'string',
        'description' => 'string',
        'html' => 'string',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];

    protected static function boot()
    {
        parent::boot();

        static::initData();
    }

    protected static function initData()
    {
        $htmlTemplatePath = base_path('resources/views/_html-templates');

        if (!is_dir($htmlTemplatePath)) {
            return;
        }

        $filenames = scandir($htmlTemplatePath);

        foreach ($filenames as $filename) {
            $filePath = "$htmlTemplatePath/$filename";
            if (!is_file($filePath) || !Str::endsWith($filePath, '.blade.php')) {
                continue;
            }

            $rawName = explode('.', $filename)[0];
            $title = ucfirst(str_slug($rawName, ' '));

            $htmlTemplate = static::firstOrNew(['name' => $rawName]);
            if ($htmlTemplate->exists) {
                continue;
            }

            $htmlTemplate->title = $title;
            $htmlTemplate->description = $title;
            $htmlTemplate->html = view("_html-templates.$rawName")->render();
            $htmlTemplate->save();
        }
    }
}
