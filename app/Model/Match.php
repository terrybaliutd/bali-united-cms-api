<?php

namespace App\Model;

use App\Model\Competition;
use App\Model\Extension\AttachableTrait;
use App\Model\Extension\ClubInfoTrait;
use App\Model\Extension\LiveMatchTrait;
use App\Model\Extension\MatchStatusTrait;
use App\Model\Extension\PublishableTrait;
use App\Model\Extension\ResponseCacheTrait;
use App\Model\MatchClubInfo;
use App\Model\MatchHighlight;
use App\Model\Season;
use App\Model\Video;
use App\Notifications\LiveStreamingPushNotification;
use App\Notifications\MatchPushNotification;
use Carbon\Carbon;

/**
 * @property integer    id
 * @property string     api_id
 * @property integer    competition_id
 * @property integer    season_id
 * @property \Carbon\Carbon|null    start_at
 * @property string     venue
 * @property string     tv_info
 * @property string     status
 * @property integer    prediction_home_win
 * @proeprty integer    prediction_draw
 * @property integer    prediction_away_win
 * @property string     referees
 * @property integer    audience
 * @property string     preview_match_text
 * @property string     post_match_text
 * @property string     live_streaming_url
 * @property \Carbon\Carbon|null    published_at
 * @property \Carbon\Carbon|null    created_at
 * @property \Carbon\Carbon|null    updated_at
 *
 */
class Match extends BaseModel
{

    use ClubInfoTrait;
    use LiveMatchTrait;
    use PublishableTrait;
    use MatchStatusTrait;
    use AttachableTrait;
    use ResponseCacheTrait;

    const STATUS_SCHEDULE = 'Schedule';
    const STATUS_PLAYING = 'Playing';
    const STATUS_FINISHED = 'Finished';
    const STATUS_POSTPONE = 'Postponed';
    const STATUS_SUSPENDED = 'Suspended';

    protected $table = 'matches';

    protected $dates = [
        'published_at',
        'created_at',
        'start_at',
    ];

    protected $fillable = [
        'api_id',
        'competition_id',
        'season_id',
        'start_at',
        'venue',
        'tv_info',
        'published',
        'status',
        'prediction_home_win',
        'prediction_away_win',
        'prediction_draw',
        'referees',
        'preview_match_text',
        'post_match_text',
        'audience',
        'banner',
        'live_video_id',
        'ticket_url'
    ];

    protected $searchField = [
        'start_at',
        'competition.long_name'
    ];

    protected $casts = [
        'id' => 'integer',
        'competition_id' => 'integer',
        'season_id' => 'integer',
        'api_id' => 'string',
        'status' => 'string',
        'venue' => 'string',
        'prediction_home_win' => 'integer',
        'prediction_draw' => 'integer',
        'prediction_away_win' => 'integer',
        'referees' => 'string',
        'preview_match_text' => 'string',
        'post_match_text' => 'string',
        'audience' => 'integer',
        'tv_info' => 'string',
        'banner' => 'string',
        'banner_type' => 'integer',
        'banner_info' => 'string',
    ];

    protected $appends = [
        'name',
        'home_club',
        'away_club',
        'club_infos'
    ];

    protected $attachable = [
        'banner' => [
            'thumb' => [
                'list' => '1024x683',
                'small' => '320x180',
                'index' => '1024x683',
                'big_preview' => '_x500',
                'small_dynamic_width' => '_x90',
                'small_dynamic_height' => '320x_'
            ]
        ]
    ];

    protected $hidden = [
        'banner',
        'banner_type',
        'banner_info',
    ];

    public static function boot()
    {
        parent::boot();

        static::updating(function ($model) {
            forgetCache('MatchRepository');
            forgetCache('MatchFixturesRepository');
            forgetCache('MatchTicketsRepository');
            forgetCache('MatchResultRepository');
            if ($model->isDirty('status') && $model->published) {
                if ($model->status == 'Finished') {
                    $notification = app(MatchPushNotification::class);
                    $notification->setClickAction('com.baliutd.open_latest');
                    $model->status = Match::STATUS_FINISHED;
                    $home = $model->home_club;
                    $away = $model->away_club;
                    $title = "{$home->club->name} {$home->score} vs {$away->score} {$away->club->name}";
                    $notificationText = "Pertandingan selesai!";
                    $type = 'latest';
                    \Log::info('Notif Send on update latest match');
                    $notification->send($model, $notificationText, $type, $title);
                }
                if ($model->status == 'Playing' && $model->live_video_id != "") {
                    $notification = app(LiveStreamingPushNotification::class);
                    $notificationText =  "Saksikan live streaming pertandingan ini melalui aplikasi Bali United!";
                    $title = "[LIVE] {$model->home_club->club->name} vs {$model->away_club->club->name}";
                    \Log::info('Notif Send on update latest match');
                    $notification->send(Video::find($model->live_video_id), $notificationText, $title);
                }

            }
        });

        // Add forgetCache to reset cache when data changed
        static::saved(function ($model) {
            forgetCache('MatchRepository');
            forgetCache('MatchFixturesRepository');
            forgetCache('MatchTicketsRepository');
            forgetCache('MatchResultRepository');
        });
        static::deleted(function ($model) {
            forgetCache('MatchRepository');
            forgetCache('MatchFixturesRepository');
            forgetCache('MatchTicketsRepository');
            forgetCache('MatchResultRepository');
        });
    }

    public function getNameAttribute()
    {
        $date = $this->start_at->format('d/m/Y');
        $competition = $this->competition->name;
        $season = $this->season->name;
        $home = $this->home_club ? $this->home_club->club->name : '-';
        $away = $this->away_club ? $this->away_club->club->name : '-';
        return "[$date][$competition][$season] $home vs $away";
    }

    public function competition()
    {
        return $this->belongsTo(Competition::class, 'competition_id');
    }

    public function season()
    {
        return $this->belongsTo(Season::class, 'season_id');
    }

    public function highlights()
    {
        return $this->hasMany(MatchHighlight::class, 'match_id');
    }

    public function liveVideo()
    {
        return $this->hasOne(Video::class, 'live_video_id');
    }

    public function video()
    {
        return $this->hasMany(Video::class, 'match_id');
    }

    public function scopeOrdered($query)
    {
        return $query->orderBy('start_at', 'asc');
    }

    public function scopeFinished($query, $currentDateTime = null)
    {
        if ($currentDateTime == null) {
            $currentDateTime = Carbon::now();
        }
        return $query->whereIn('status', [static::STATUS_FINISHED, static:: STATUS_SUSPENDED])
                     ->where('start_at', '<', $currentDateTime);
    }

    public function scopeNextMatch($query)
    {
        return $query->where('start_at', '>=', Carbon::now())->orderBy('start_at', 'asc');
    }

    public function scopePrevMatch($query)
    {
        return $query->where('start_at', '<', Carbon::now())->finished()->orderBy('start_at', 'desc');
    }

    public static function getStatusList()
    {
        return [
            static::STATUS_SCHEDULE => static::STATUS_SCHEDULE,
            static::STATUS_PLAYING => static::STATUS_PLAYING,
            static::STATUS_FINISHED => static::STATUS_FINISHED,
            static::STATUS_POSTPONE => static::STATUS_POSTPONE,
            static::STATUS_SUSPENDED => static::STATUS_SUSPENDED
        ];
    }
}
