<?php
namespace App\Model;

use App\Model\Club;
use App\Model\Extension\ResponseCacheTrait;
use App\Model\Match;
use App\Model\MatchLineUp;
use App\Model\MatchStatistic;

class MatchClubInfo extends BaseModel
{
    use ResponseCacheTrait;
    
    const TYPE_HOME = 'Home';
    const TYPE_AWAY = 'Away';

    protected $table = 'match_club_infos';

    protected $fillable = [
        'club_id',
        'match_id',
        'coach',
        'type',
        'formation',
        'score',
        'is_default'
    ];

    protected $casts = [
        'id' => 'integer',
        'match_id' => 'integer',
        'club_id' => 'integer',
        'type' => 'string',
        'coach' => 'string',
        'formation' => 'string',
        'score' => 'integer',
        'is_default' => 'integer',
    ];

    public function match()
    {
        return $this->belongsTo(Match::class, 'match_id');
    }

    public function club()
    {
        return $this->belongsTo(Club::class, 'club_id');
    }

    public function lineups()
    {
        return $this->hasMany(MatchLineUp::class, 'match_club_info_id');
    }

    public function statistic()
    {
        return $this->hasOne(MatchStatistic::class, 'match_club_info_id');
    }

    public function statisticForm()
    {
        return $this->hasOne(MatchStatistic::class, 'match_club_info_id')->get()->first();
    }

    public function primary()
    {
        return $this->hasMany(MatchLineUp::class, 'match_club_info_id')->where('is_primary', true);
    }

    public function subtitutions()
    {
        return $this->hasMany(MatchLineUp::class, 'match_club_info_id')->where('is_primary', false);
    }

    public static function getTypeList()
    {
        return [
            static::TYPE_HOME => static::TYPE_HOME,
            static::TYPE_AWAY => static::TYPE_AWAY
        ];
    }

    /**
     * Create object with given data from match
     *
     * @param   array       $data   [Data from form request]
     * @param   boolean     $type   [Type of club, home and away]
     * @return  boolean             [Return true if success]
     */
    public static function createWithMatch($data, $type)
    {
        if (!in_array($type, ['home', 'away'])) {
            return false;
        }
        $model = new static;
        $model->saveWithMatch($data, $type);
    }

    public function saveWithMatch($data, $type)
    {
        // $this->match_id = $data['match_id'];
        $this->club_id = $data[$type . '_club_id'];
        $this->coach = $data[$type . '_coach'];
        $this->formation = $data[$type . '_formation'];
        $this->score = $data[$type . '_score'];
        $this->type = $this->getTypeFromMatchForm($type);
        // $this->save();
        return $this;
    }

    protected function getTypeFromMatchForm($type)
    {
        if ($type == 'home') {
            return static::TYPE_HOME;
        } elseif ($type == 'away') {
            return static::TYPE_AWAY;
        }
        return null;
    }
}
