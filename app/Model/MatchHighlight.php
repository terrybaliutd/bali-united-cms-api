<?php
namespace App\Model;

use App\Model\Extension\ResponseCacheTrait;
use App\Model\Match;
use App\Model\Video;
use App\Notifications\HighlightPushNotification;
use App\Notifications\MatchPushNotification;

/**
 * @property integer    id
 * @property string     api_id
 * @property integer    match_id
 * @property string|null     player_name
 * @property string\null     support_player_name
 * @property integer    time_in_second
 * @property string     event_description
 * @property string     stage_name
 * @property \Carbon\Carbon|null    created_at
 * @property \Carbon\Carbon|null    updated_at
 */
class MatchHighlight extends BaseModel
{
    use ResponseCacheTrait;
    
    const GOAL_CODE = 'goal';
    const ASSIST_CODE = 'assist';
    const OWN_GOAL_CODE = 'own_goal';
    const RED_CARD_CODE = 'red_card';
    const YELLOW_CARD_CODE = 'yellow_card';
    const SUBSTITUTION_CODE = 'substitution';
    const PENALTY_GOAL_CODE = 'penalty_goal';
    const PENALTY_MISSED_CODE = 'penalty_missed';

    protected $table = 'match_highlights';

    protected $fillable = [
        'api_id',
        'player_name',
        'support_player_name',
        'time_in_second',
        'time_in_minute',
        'event_description',
        'stage_name',
        'match_id',
        'club_type'
    ];

    protected $casts = [
        'id' => 'integer',
        'api_id' => 'string',
        'match_id' => 'integer',
        'player_name' => 'string',
        'support_player_name' => 'string',
        'time_in_second' => 'integer',
        'time_in_minute' => 'integer',
        'event_description' => 'string',
        'stage_name' => 'string',
        'club_type' => 'string',
    ];

    public static function getCodes()
    {
        return [
            static::GOAL_CODE => 'Goal',
            static::ASSIST_CODE => 'Assist',
            static::OWN_GOAL_CODE => 'Own Goal',
            static::RED_CARD_CODE => 'Red Card',
            static::YELLOW_CARD_CODE => 'Yellow Card',
            static::SUBSTITUTION_CODE => 'Substitution',
            static::PENALTY_GOAL_CODE => 'Penalty Goal',
            static::PENALTY_MISSED_CODE => 'Penalty Missed'
        ];
    }

    public function sendNotification($request, $matchId)
    {
        $match = Match::find($matchId);
        if ($request['club_type'] == 'home') {
            $clubName = $match->home_club->club->name;
        } else {
            $clubName = $match->away_club->club->name;
        }
        $notificationText = "Menit ke-{$request['time_in_minute']}, {$request['player_name']}, {$clubName}";

        $title = $this->highlightNotificationTitle($request['event_description']);
        \Log::info($title);
        if ($match->live_video_id != "") {
            $notification = app(HighlightPushNotification::class);
            $notification->send($match, $notificationText, $title);
            \Log::info('Notif Send on update latest match with live video');
        } else {
            $notification = app(MatchPushNotification::class);
            $type = 'latest';
            $notification->setClickAction('com.baliutd.open_latest');
            $notification->send($match, $notificationText, $type, $title);
            \Log::info('Notif Send on update latest match without live video');
        }
    }

    public function match()
    {
        return $this->belongsTo(Match::class, 'match_id');
    }

    public function scopeOrdered($query)
    {
        return $query->orderBy('time_in_second')->orderBy('api_id');
    }

    public function scopeShortSummary($query)
    {
        return $query->whereIn('event', [
            static::GOAL_CODE,
            static::YELLOW_CARD_CODE,
            static::RED_CARD_CODE,
            static::OWN_GOAL_CODE,
            static::PENALTY_GOAL_CODE
        ]);
    }

    public function getMinuteAttribute()
    {
        return ceil($this->time_second / 60);
    }

    public function getEventCodeIcon($eventCode, $playerId)
    {
        $isActive = $playerId == $this->player_id;

        if ($isActive) {
            switch ($eventCode) {
                case static::PENALTY_GOAL_CODE:
                    return '--penalty-goal';
                case static::GOAL_CODE:
                    return 'has-goal';
                case static::ASSIST_CODE:
                    return 'has-assist';
                case static::YELLOW_CARD_CODE:
                    return '--yellow-card';
                case static::RED_CARD_CODE:
                    return '--red-card';
                case static::SUBSTITUTION_CODE:
                    return '--down';
                default:
                    return '';
            }
        } else {
            if ($eventCode == static::SUBSTITUTION_CODE) {
                return '--up';
            } else {
                return '';
            }
        }
    }

    public function scopeMatchId($query, $matchId)
    {
        return $query->where('match_id', $matchId);
    }

    private function highlightNotificationTitle($eventDescription)
    {
        switch ($eventDescription) {
            case 'goal':
                $title = "Goal - Saksikan Live sekarang";
                break;
            case 'assist':
                $title = "Assist - Saksikan Live sekarang";
                break;
            case 'own_goal':
                $title = "Own Goal - Saksikan Live sekarang";
                break;
            case 'red_card':
                $title = "Kartu Merah - Saksikan Live sekarang";
                break;
            case 'yellow_card':
                $title = "Kartu Kuning - Saksikan Live sekarang";
                break;
            case 'substitution':
                $title = "Pertukaran Pemain - Saksikan Live sekarang";
                break;
            case 'penalty_goal':
                $title = "Penalty Goal - Saksikan Live sekarang";
                break;
            default:
                $title = "Penalty Miss - Saksikan Live sekarang";
                break;
        }
        return $title;
    }
}
