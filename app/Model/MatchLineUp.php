<?php

namespace App\Model;

use App\Model\Extension\ResponseCacheTrait;
use App\Model\Player;

class MatchLineUp extends BaseModel
{
    use ResponseCacheTrait;
    
    const GK = '#FFC107';
    const DF = '#179EE5';
    const MF = '#4CD964';
    const FW = '#E53935';

    protected $table = 'match_line_ups';

    public $incrementing = false;

    public $timestamps = false;

    protected $fillable = [
        'match_club_info_id',
        'player_name',
        'position_code',
        'shirt_number',
        'is_primary',
    ];

    protected $appends = [
        'color'
    ];

    protected $casts = [
        'id' => 'string',
        'match_club_info_id' => 'integer',
        'player_name' => 'string',
        'position_code' => 'string',
        'shirt_number' => 'string',
        'is_primary' => 'boolean',
    ];

    public static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            $prefixId = $model->match_club_info_id;
            $model->id = uniqid(hash('md5', $prefixId), false);
        });
    }

    public function scopeLineUpOrdered($query)
    {
        $positionLists = array_keys(Player::POSISTION_CODES);
        $orderCase = '';
        foreach ($positionLists as $key => $position) {
            $orderCase .= "WHEN '$position' then $key ";
        }
        $query->orderBy('is_primary', 'desc');
        $query->orderByRaw("CASE position_code $orderCase ELSE {$key} END");
        return $query;
    }

    public function getColorFromPosition($position)
    {
        switch ($position) {
            case 'GK':
                return static::GK;
            case 'DF':
                return static::DF;
            case 'MF':
                return static::MF;
            case 'FW':
                return static::FW;
            default:
                return '#FFF';
        }
    }

    public function getColorAttribute()
    {
        $generalPositions = Player::getGeneralPositions();
        foreach ($generalPositions as $key => $positions) {
            if (in_array($this->position_code, $positions)) {
                return $this->getColorFromPosition($key);
            }
        }
    }
}
