<?php

namespace App\Model;

use App\Model\Extension\ResponseCacheTrait;
use App\Model\MatchClubInfo;
use Illuminate\Database\Eloquent\Model;

class MatchStatistic extends BaseModel
{
    use ResponseCacheTrait;
    
    protected $table = 'match_statistics';

    public $incrementing = false;

    protected $fillable = [
        'match_club_info_id',
        'corner',
        'foul',
        'offside',
        'red_card',
        'yellow_card',
        'tackle_success',
        'ball_possession',
        'shot_on_goal',
        'total_shots',
        'passing_accuracy'
    ];

    protected $casts = [
        'id' => 'string',
        'match_club_info_id' => 'integer',
        'corner' => 'integer',
        'foul' => 'integer',
        'offside' => 'integer',
        'red_card' => 'integer',
        'yellow_card' => 'integer',
        'tackle_success' => 'integer',
        'ball_possession' => 'integer',
        'shot_on_goal' => 'integer',
        'total_shots' => 'integer',
        'passing_accuracy' => 'integer',
    ];

    public static function boot()
    {
        parent::boot();
        static::creating(function ($model) {
            $prefixId = $model->match_club_info_id;
            $model->id = uniqid(hash('md5', $prefixId), false);
        });
    }

    public function matchClubInfo()
    {
        return $this->belongsTo(MatchClubInfo::class, 'match_club_info_id');
    }

    public static function createWithClubInfo($data, $type)
    {
        if (!in_array($type, ['home', 'away'])) {
            return false;
        }
        $model = new static;
        $model->saveWithMatchInfo($data, $type);
    }
    
    public function saveWithClubInfo($data, $type)
    {
        $this->match_club_info_id = $data['match_club_info_id'];
        $this->corner = $data[$type . '_statistic_corner'];
        $this->foul = $data[$type . '_statistic_foul'];
        $this->offside = $data[$type . '_statistic_offside'];
        $this->red_card = $data[$type . '_statistic_red_card'];
        $this->yellow_card = $data[$type . '_statistic_yellow_card'];
        $this->tackle_success = $data[$type . '_statistic_tackle_success'];
        $this->passing_accuracy = $data[$type . '_statistic_passing_accuracy'];
        $this->shot_on_goal = $data[$type . '_statistic_shot_on_goal'];
        $this->total_shots = $data[$type . '_statistic_total_shots'];
        $this->ball_possession = $data[$type . '_statistic_ball_possession'];
        $this->save();
        return $this;
    }
}
