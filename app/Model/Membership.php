<?php

namespace App\Model;

use App\Model\Extension\OrderableTrait;
use App\Model\Extension\PublishableTrait;
use App\Model\Extension\ResponseCacheTrait;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Membership extends BaseModel
{
    use PublishableTrait;
    use ResponseCacheTrait;

    protected $table = 'memberships';

    protected $fillable = [
        'name',
        'prefix',
        'ribbon_path',
        'text_label',
        'is_reward',
    ];

    protected $casts = [
        'name' => 'string',
        'prefix' => 'string',
        'ribbon_path' => 'string',
        'text_label' => 'string',
        'is_reward' => 'boolean',
    ];

    public function match()
    {
        return $this->belongsTo('App\Model\Match', 'match_id');
    }

    public static function boot()
    {
        parent::boot();

        static::saved(function ($model) {
            forgetCache('MembershipRepository');
        });
        static::deleted(function ($model) {
            forgetCache('MembershipRepository');
        });
    }

}
