<?php

namespace App\Model;

use App\Model\Extension\OrderableTrait;
use App\Model\Extension\ResponseCacheTrait;
use App\Model\Extension\TreeTrait;
use App\Model\Translation\TranslateModel;

class Menu extends TranslateModel
{
    use OrderableTrait, TreeTrait;
    use ResponseCacheTrait;

    const TYPE_MAIN = 'Main';
    const TYPE_FOOTER = 'Footer';

    protected $table = 'menus';

    public $timestamps = true;

    protected $fillable = [
        'type',
        'is_link',
        'url',
        'order',
    ];

    protected $translateField = [
        'title'
    ];

    protected $appends = [
        'real_url'
    ];

    protected $searchField = [
        'title',
        'type',
        'url',
        'parent.title'
    ];

    protected $casts = [
        'id' => 'integer',
        'parent_id' => 'integer',
        'is_link' => 'boolean',
        'type' => 'string',
        'url' => 'string',
        'order' => 'integer',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];

    public function parentListById()
    {
        $query = $this;
        if ($this->exists) {
            $query = $query->notThis(true);
        }
        return $query->select(['id', 'title'])->get()->lists('title', 'id');
    }

    public function getRealUrlAttribute()
    {
        if (!$this->is_link) {
            return '#';
        }
        if (filter_var($this->url, FILTER_VALIDATE_URL) === true) {
            return $this->url;
        }

        return url($this->url);
    }

    public function scopeOrderedMenu($query, $depth = 5)
    {
        $callback = function ($query) {
            $query->asc();
        };
        return $query->fromRoot()
                     ->withChilds(['title', 'is_link', 'url'], $callback, $depth)
                     ->select([$this->getKeyName(), 'title', 'is_link', 'url']);
    }

    public function scopeType($query, $type)
    {
        return $query->whereType($type);
    }

    public function scopeMain($query)
    {
        return $query->type(static::TYPE_MAIN);
    }

    public function scopeFooter($query)
    {
        return $query->type(static::TYPE_FOOTER);
    }

    public static function getAllMenu()
    {
        $menus = static::orderedMenu()->get();
        return $menus;
    }

    public static function typeList()
    {
        return [
            static::TYPE_MAIN,
            static::TYPE_FOOTER
        ];
    }

    public static function typeListName()
    {
        return array_combine(static::typeList(), static::typeList());
    }
}
