<?php

namespace App\Model;

use App\Model\Extension\AttachableTrait;
use App\Model\Extension\PublishableTrait;
use App\Model\Extension\ResponseCacheTrait;
use App\Model\Extension\TaggableTrait;
use App\Notifications\NewsPushNotification;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

/**
 * @property integer id
 * @property integer match_id
 * @property string slug
 * @property News[]|\Illuminate\Database\Eloquent\Collection|array|null|boolean featured
 * @property string title
 * @property string description
 * @property string content
 * @property string image
 * @property \Carbon\Carbon news_date
 * @property \Carbon\Carbon|null published_at
 * @property \Carbon\Carbon created_at
 * @property \Carbon\Carbon updated_at
 * @property-read News[]|\Illuminate\Database\Eloquent\Collection|array|null ordered
 * @method static Builder ordered()
 * @method static Builder featured()
 *
 */
class News extends BaseModel implements SluggableInterface
{
    use AttachableTrait;
    use PublishableTrait;
    use SluggableTrait;
    use ResponseCacheTrait;
    use TaggableTrait;

    protected $table = 'news';

    protected $sluggable = [
        'build_from' => 'title'
    ];

    protected $dates = [
        'published_at',
        'news_date',
        'created_at',
    ];

    // protected $urlKey = 'slug';

    protected $fillable = [
        'featured',
        'news_date',
        'slug',
        'published',
        'title',
        'description',
        'membership_id',
        'content',
        'image',
        'tags',
        'attachment_file',
        'match_id',
    ];

    protected $appends = [
        'link',
        'type'
        // 'previous_news_slug',
        // 'next_news_slug',
    ];

    protected $attachable = [
        'image' => [
            'thumb' => [
                'list' => '250x250',
                'detail' => '_x400',
                'big' => '1300x_',
                'home' => '1280x590',
                'news' => '600x200',
            ]
        ]
    ];

    protected $searchField = [
        'title',
        'description'
    ];

    protected $casts = [
        'id' => 'integer',
        'match_id' => 'integer',
        'slug' => 'string',
        'featured' => 'boolean',
        'title' => 'string',
        'description' => 'string',
        'content' => 'string',
        'image' => 'string',
        'image_type' => 'integer',
        'membership_id' => 'integer',
        'image_info' => 'string',
    ];

    protected static function boot()
    {
        parent::boot();

        static::saving(function ($model) {
            if (empty($model->description)) {
                $model->genereteDefaultDescription();
            }
        });

        static::created(function ($model) {
            forgetCache('MatchRepository');
            forgetCache('NewsRepository');
            forgetCache('NewsRecentRepository');
            if ($model->published) {
                $notification = app(NewsPushNotification::class);
                $notification->send($model, $model->title);
                \Log::info('Notif Send on create');
            }
        });
        static::updating(function ($model) {
            forgetCache('MatchRepository');
            forgetCache('NewsRepository');
            forgetCache('NewsRecentRepository');
            if ($model->isDirty('published_at') && $model->published) {
                $notification = app(NewsPushNotification::class);
                $notification->send($model, $model->title);
                \Log::info('Notif Send on update');
            }
        });

        // Add forgetCache to reset cache when data changed
        static::saved(function ($model) {
            forgetCache('MatchRepository');
            forgetCache('NewsRepository');
            forgetCache('NewsRecentRepository');
        });
        static::deleted(function ($model) {
            forgetCache('MatchRepository');
            forgetCache('NewsRepository');
            forgetCache('NewsRecentRepository');
        });
    }

    public function membership()
    {
        return $this->belongsTo(Membership::class, 'membership_id');
    }

    public function scopeFeatured($query)
    {
        return $query->where('featured', 1);
    }

    public function scopeOrdered($query)
    {
        return $query->orderBy('news_date', 'desc');
    }

    public function getAttachmentFileNameAttribute()
    {
        $fileName = basename($this->attachment_file);
        return $fileName;
    }

    public function getDescriptionAttribute()
    {
        if (empty($this->getAttributeFromArray('description'))) {
            return $this->generateDefaultDescription();
        }
        return $this->getAttributeFromArray('description');
    }

    protected function generateDefaultDescription()
    {
        $content = preg_replace('/\<(\/?br\s?\/?+|\/p)\>/i', "\n", $this->content);
        $contents = array_map('strip_tags', explode("\n", $content));
        $value = isset($contents[0])?$contents[0]:'';
        foreach ($contents as $content) {
            if (strlen($content) < 130) {
                $value .= $content;
            } else {
                break;
            }
            if (!empty($content)) {
                $value .= "\n";
            }
        }
        return $this->description = $value;
    }

    // public function getNextNewsSlugAttribute()
    // {
    //     $nextNews = static::published()->where('id', '>', $this->id)->limit(1)->first();
    //     if (!($nextNews === null)) {
    //         return $nextNews->slug;
    //     }
    // }

    // public function getPreviousNewsSlugAttribute()
    // {
    //     $prevNews = static::published()->where('id', '<', $this->id)->limit(1)->first();
    //     if (!($prevNews === null)) {
    //         return $prevNews->slug;
    //     }
    // }

    public function getLinkAttribute()
    {
        return route('api.news.show', $this);
    }

    public function getTypeAttribute()
    {
        return "news";
    }

    public static function searchNews($keywords)
    {
        $result = [];
        foreach ($keywords as $keyword) {
            $news = static::published()
                ->select(['id', 'title', 'content', 'image', 'news_date', 'published_at'])
                ->where('title', 'like', '%'.$keyword.'%')
                ->published()
                ->orderBy("news_date", "DESC")
                ->get();

            foreach ($news->unique('id') as $newsItem) {
                $value = [
                    'id' => $newsItem->id,
                    'type' => 'news',
                    'title' => $newsItem->title,
                    'description' => $newsItem->content,
                    'image' => $newsItem->getAttachment('image'),
                    'published_at' => $newsItem->published_at->toIso8601String(),
                    'datetime' => $newsItem->news_date->toIso8601String()
                ];
                $result[] = $value;
            }
        }

        return collect($result)->unique('id')->toArray();
    }
}
