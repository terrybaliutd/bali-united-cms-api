<?php

namespace App\Model;

use App\Model\Extension\AdditionalThumbnailTrait;
use App\Model\Extension\AttachableTrait;
use App\Model\Extension\ImpersonateNewsTrait;
use App\Model\Extension\ResponseCacheTrait;

class NewsListAds extends BaseImageAds
{
    use AttachableTrait;
    use ImpersonateNewsTrait;
    use AdditionalThumbnailTrait;
    use ResponseCacheTrait;

    protected $additionalThumbnails = [
        'image' => [
            'list' => '250x250',
            'detail' => '_x400',
            'big' => '1300x_',
            'home' => '1280x590',
            'news' => '600x200',
        ]
    ];

    protected $appends = [
        'type',
        'content',
        'news_date',
    ];

    protected $attachable = [
        'attachment' => [
            'thumb' => [
                'high' => '1364x456',
                'med' => '682x228',
                'low' => '341x114',
            ]
        ]
    ];

    protected $fillable = [
        'title',
        'url',
        'attachment',
        'priority',
        'published'
    ];

    protected static function boot()
    {
        parent::boot();

        static::saved(function ($model) {
            forgetCache('NewsRepository');
            forgetCache('NewsListAdsRepository');
        });
        static::deleted(function ($model) {
            forgetCache('NewsRepository');
            forgetCache('NewsListAdsRepository');
        });
    }
}
