<?php

namespace App\Model;

use App\Model\Extension\PublishableTrait;
use App\Model\Extension\ResponseCacheTrait;
use Illuminate\Database\Eloquent\Model;

class Notification extends BaseModel
{
    use PublishableTrait;
    use ResponseCacheTrait;

    protected $table = 'notifications';

    protected $fillable = [
        'user_id',
        'latest_match',
        'upcoming_match',
        'new_news',
        'new_video',
        'live_video',
    ];

    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'latest_match' => 'boolean',
        'upcoming_match' => 'boolean',
        'new_news' => 'boolean',
        'new_video' => 'boolean',
        'live_video' => 'boolean',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];

    public function user()
    {
        return $this->belongsTo(\App\Model\User::class, 'user_id');
    }
}
