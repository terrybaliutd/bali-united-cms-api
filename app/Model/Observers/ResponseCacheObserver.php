<?php

namespace App\Model\Observers;

class ResponseCacheObserver
{
    /**
     * Flush any response cache
     *
     * @return void
     */
    private function flushResponseCache()
    {
        $cacheDirectory = env('NGINX_CACHE_DIR');

        if ($cacheDirectory) {
            exec('rm -rf ' . $cacheDirectory);
        }
    }

    /**
     * Listening to any deleted events.
     *
     * @return void
     */
    public function deleted()
    {
        $this->flushResponseCache();
    }

    /**
     * Listening to any restored events.
     *
     * @return void
     */
    public function restored()
    {
        $this->flushResponseCache();
    }

    /**
     * Listening to any saved events.
     *
     * @return void
     */
    public function saved()
    {
        $this->flushResponseCache();
    }
}
