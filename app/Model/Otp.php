<?php

namespace App\Model;

class Otp extends BaseModel
{
    /**
     * User Group
     */
    const TYPE_CHANGE_PHONE = 'CHANGE_PHONE';
    const TYPE_REGISTER = 'REGISTER';
    const OTP_MAX_COUNT = 10;

    const OTP_SMS_REJECTED = "REJECTED";

    const OTP_SUCCESS_CODE = 200;
    const OTP_VERIFICATION_SUCCESS_CODE = 200;

    const OTP_VERIFICATION_ERROR_CODE = 500;
    const OTP_ERROR_CODE_MAX_REQUEST = 501;
    const OTP_INVALID_PHONE_NUMBER = 503;
    const OTP_ERROR_CODE_LAST_REQUEST = 502;

    protected $table = 'otps';

    public $timestamps = true;

    protected $fillable = [
        'user_id',
        'type',
        'otp_code',
        'object_string',
        'verified',
    ];

    public function user()
    {
        return $this->hasOne(\App\Model\User::class, 'user_id');
    }
}
