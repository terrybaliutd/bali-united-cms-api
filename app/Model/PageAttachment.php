<?php

namespace App\Model;

use App\Model\Extension\AttachableTrait;
use App\Model\Extension\ResponseCacheTrait;

class PageAttachment extends BaseModel
{
    use AttachableTrait;
    use ResponseCacheTrait;

    protected $table = 'page_attachments';

    protected $fillable = [
        'name',
        'uri'
    ];

    protected $attachable = [
        'uri' => []
    ];

    protected $casts = [
        'id' => 'integer',
        'page_id' => 'integer',
        'name' => 'string',
        'uri' => 'string',
        'uri_type' => 'integer',
        'uri_info' => 'string',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];

    public function page()
    {
        return $this->belongsTo(\App\Model\Page::class, 'page_id');
    }

    public function setAttachmentOption($option)
    {
        $this->attachable['uri'] = $option;
    }
}
