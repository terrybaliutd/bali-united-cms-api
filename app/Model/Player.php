<?php

namespace App\Model;

use App\Model\Extension\AttachableTrait;
use App\Model\Extension\PublishableTrait;
use App\Model\Extension\ResponseCacheTrait;
use Carbon\Carbon;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

/**
 * @property integer    id
 * @property string     api_id
 * @property string     name
 * @Property string     short_name
 * @property string     position_code
 * @property string     shirt_number
 * @property string     birth_place
 * @property string     brith_date
 * @property integer    weight
 * @property integer    height
 * @property string     nationality
 * @property string     photo_profile
 * @property string     photo_action
 * @property string     slug
 * @property \Carbon\Carbon|null    published_at
 * @property \Carbon\Carbon|null    created_at
 * @property \Carbon\Carbon|null    updated_at
 */
class Player extends BaseModel implements SluggableInterface
{
    use AttachableTrait;
    use PublishableTrait;
    use ResponseCacheTrait;
    use SluggableTrait;

    protected $table = 'players';

    protected $sluggable = [
        'build_from' => 'name'
    ];

    protected $dates = [
        'published_at',
        'created_at'
    ];

    protected $urlKey = 'slug';

    protected $attachable = [
        'photo_profile' => [
            'thumb' => [
                'small' => '70x70',
                'normal' => '300x300',
                'leaderboard' => '300x243'
            ]
        ],
        'photo_action' => [
            'thumb' => [
                'small' => '70x70',
                'normal' => '300x300'
            ]
        ],
    ];

    protected $fillable = [
        'api_id',
        'name',
        'short_name',
        'position_code',
        'shirt_number',
        'photo_profile',
        'photo_action',
        'slug',
        'published',
        'weight',
        'height',
        'birth_date',
        'birth_place',
        'nationality'
    ];

    const POSISTION_CODES = [
        'GK' => 'Goalkeeper',
        'LB' => 'Left Back',
        'CB' => 'Center Back',
        'RB' => 'Right Back',
        'DMF' => 'Defensive Midfield',
        'CMF' => 'Center Midfield',
        'AMF' => 'Attacking Midfield',
        'SS' => 'Second Striker',
        'CF' => 'Center Forward',
        'RM' => 'Right Midfielder',
        'LM' => 'Left Midfielder'
    ];

    protected static $generalPosition = [
        'GK' => ['GK'],
        'DF' => ['LB', 'CB', 'RB'],
        'MF' => ['DMF', 'CMF', 'AMF', 'RM', 'LM'],
        'FW' => ['SS', 'CF']
    ];

    protected $searchField = [
        'api_id',
        'name',
        'short_name'
    ];

    protected $casts = [
        'id' => 'integer',
        'api_id' => 'string',
        'name' => 'string',
        'short_name' => 'string',
        'position_code' => 'string',
        'shirt_number' => 'string',
        'birth_place' => 'string',
        'birth_date' => 'string',
        'weight' => 'integer',
        'height' => 'integer',
        'nationality' => 'string',
        'photo_profile' => 'string',
        'photo_profile_type' => 'integer',
        'photo_profile_info' => 'string',
        'photo_action' => 'string',
        'photo_action_type' => 'integer',
        'photo_action_info' => 'string',
        'slug' => 'string',
        'published_at' => 'datetime',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];

    public static function getGeneralPositions()
    {
        return static::$generalPosition;
    }
}
