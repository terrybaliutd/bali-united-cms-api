<?php

namespace App\Model;

use App\Model\Extension\AttachableTrait;
use App\Model\Extension\PublishableTrait;
use App\Model\Extension\ResponseCacheTrait;
use App\Model\Extension\TaggableTrait;
use App\Model\Video;

class Playlist extends BaseModel
{
    use AttachableTrait;
    use PublishableTrait;
    use ResponseCacheTrait;
    use TaggableTrait;

    protected $fillable = [
        'title',
        'key',
        'tags',
        'published',
        'attachment',
        'featured',
        'description',
        'position'
    ];

    protected $attachable = [
        'attachment' => [
            'thumb' => [
                'list' => '1024x683',
                'small' => '320x180',
                'index' => '1024x683',
                'big_preview' => '_x500',
                'small_dynamic_width' => '_x90',
                'small_dynamic_height' => '320x_'
            ]
        ]
    ];

    protected $dates = [
        'published_at'
    ];

    protected $appends = [
        "thumbnail",
        "link",
        "videos_link"
    ];

    protected $hidden = [
/*        "attachment",
        "attachment_type",
        "attachment_info",*/
    ];

    protected $casts = [
        'id' => 'integer',
        'key' => 'string',
        'title' => 'string',
        'description' => 'string',
        'attachment' => 'string',
        'attachment_type' => 'integer',
        'attachment_info' => 'string',
        'position' => 'integer'
    ];

    protected $searchField = [
        'title',
    ];

    protected static function boot()
    {
        parent::boot();
        // Add forgetCache to reset cache when data changed
        static::saved(function ($model) {
            forgetCache('PlaylistRepository');
        });
        static::deleted(function ($model) {
            forgetCache('PlaylistRepository');
        });
    }

    public static function getPlaylistKey($url)
    {
        $reg = '/list=([^&]+)/';
        preg_match($reg, $url, $match);
        if ($match != null) {
            return $match[1];
        } else {
            return null;
        }
    }

    public static function createFromUrl($url)
    {
        $key = Playlist::getPlaylistKey($url);
        if ($key == null) {
            return null;
        }

        $info = Playlist::getInfo($key);
        if ($info == null) {
            return null;
        }

        $playlist = new Playlist();
        $playlist->title = $info->title;
        $playlist->description = $info->description;
        $playlist->key = $key;
        $playlist->attachment = $info->thumbnails->high->url;

        return $playlist;
    }

    public static function getInfo($key)
    {
        return \YoutubeData::getPlaylistById($key)->snippet;
    }

    public function getVideos($token)
    {
        $key = $this->key;
        $playlist = \YoutubeData::getPLaylistItemsByPlaylistId($key, $token);
        return $playlist;
    }

    public function addVideo($key, $user)
    {
        $video = Video::where("key", $key)->first();
        if (!$video) {
            $video = Video::createFromUrl("v=".$key);
            if (!$video) {
                return false;
            }

            $video->publisher()->associate($user);
            $video->setPlaylistId($this->id);
            $video->published = 1;
            $video->save();
            return true;
        }
        $video->setPlaylistId($this->id);
        $video->save();
        return false;
    }

    public function updatePlaylist($videos, $user)
    {
        $count = 0;
        foreach ($videos as $video) {
            $key = $video->contentDetails->videoId;
            $count += $this->addVideo($key, $user);
        }
        return [ "total" => count($videos), "new" => $count];
    }

    public function synch($user)
    {
        $countAll = 0;
        $countNew = 0;

        $videos = $this->getVideos("");
        $info = $videos["info"];
        $res = $this->updatePlaylist($videos["results"], $user);
        $countAll += $res["total"];
        $countNew += $res["new"];

        while ($info["nextPageToken"]) {
            $videos = $this->getVideos($info["nextPageToken"]);
            $info = $videos["info"];
            $res = $this->updatePlaylist($videos["results"], $user);
            $countAll += $res["total"];
            $countNew += $res["new"];
        }

        return [ "total" => $countAll, "new" => $countNew];
    }

    public function getThumbnailUrlAttribute()
    {
        return asset($this->thumbnail);
    }

    public function getThumbnailAttribute()
    {
        return $this->getAttachmentSmallAttribute('attachment');
    }

    public function getAttachmentSmallAttribute()
    {
        return $this->getThumbnail('attachment', 'small');
    }

    public function getLinkAttribute()
    {
        return "/api/playlist/" . $this->getKey();
    }

    public function getVideosLinkAttribute()
    {
        return "/api/playlist/" . $this->getKey() . "/videos";
    }

    public function videos()
    {
        return $this->hasMany(Video::class, 'playlist_id');
    }
}
