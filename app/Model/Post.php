<?php

namespace App\Model;

use App\Model\Extension\AttachableTrait;
use App\Model\Extension\PublishableTrait;
use App\Model\Extension\ResponseCacheTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use RichanFongdasen\EloquentBlameable\BlameableTrait;

class Post extends BaseModel implements SluggableInterface
{
    use AttachableTrait;
    use BlameableTrait;
    use PublishableTrait;
    use ResponseCacheTrait;
    use SluggableTrait;

    const TYPE_PHOTO = 'photo';
    const TYPE_MEME = 'meme';
    const TYPE_VIDEO = 'video';
    const TYPE_ARTICLE = 'article';

    protected $table = 'posts';

    protected $sluggable = [
        'build_from' => 'title'
    ];

    protected $fillable = [
        'owner_id',
        'type',
        'title',
        'slug',
        'image',
        'excerpt',
        'content',
        'video_key',
        'published',
        'video_status',
        'video_status_description'
    ];

    protected $searchField = [
        'title'
    ];

    protected $dates = [
        'published_at'
    ];

    protected $attachable = [
        'image' => [
            'thumb' => [
                'small_square' => '128x128',
                'medium_square' => '256x256',
                'large_square' => '512x512',
                'xlarge_square' => '2048x2048',
                'small_cover' => '240x_',
                'normal_cover' => '360x_',
                'medium_cover' => '480x_',
                'large_cover' => '1280x_',
                'small_banner' => '_x240',
                'normal_banner' => '_x360',
                'medium_banner' => '_x480',
                'large_banner' => '_x1280'
            ]
        ]
    ];

    protected $appends = [
        'video_url'
    ];

    public function getVideoUrlAttribute()
    {
        if ($this->attributes['type'] != self::TYPE_VIDEO) {
            return null;
        }
        return "https://www.youtube.com/watch?v=" . $this->attributes['video_key'];
    }

    public function likes()
    {
        return $this->hasMany(PostLike::class, 'post_id');
    }

    public function owner()
    {
        return $this->belongsTo(User::class, 'owner_id');
    }

    public function allComments()
    {
        return $this->hasMany(PostComment::class, 'post_id');
    }

    public function postComments()
    {
        return $this->allComments()->where('parent_id', null);
    }

    public function postTags()
    {
        return $this->belongsToMany(Tag::class, 'post_tags', 'post_id', 'tag_id');
    }

    public function postReports()
    {
        return $this->hasMany(PostReport::class, 'post_id');
    }

    public function getTypeOptions()
    {
        return [
            self::TYPE_PHOTO => 'Photo',
            self::TYPE_MEME => 'Meme',
            self::TYPE_VIDEO => 'Video',
            self::TYPE_ARTICLE => 'Article'
        ];
    }

    public function getLabel()
    {
        return 'Post';
    }
}
