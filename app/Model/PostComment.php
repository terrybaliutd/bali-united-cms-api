<?php

namespace App\Model;

use App\Model\Extension\ResponseCacheTrait;
use RichanFongdasen\EloquentBlameable\BlameableTrait;

class PostComment extends BaseModel
{
    use BlameableTrait;
    use ResponseCacheTrait;

    protected $table = 'post_comments';

    protected $fillable = [
        'content',
        'user_id',
        'post_id',
        'total_like',
        'parent_id',
        'sticker_id',
        'updated_by',
        'created_by'
    ];

    protected $searchField = [
        'content'
    ];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function post()
    {
        return $this->belongsTo(Post::class, 'post_id');
    }

    public function parent()
    {
        return $this->belongsTo(PostComment::class, 'parent_id');
    }

    public function replies()
    {
        return PostComment::where('parent_id', $this->id);
    }

    public function sticker()
    {
        return $this->belongsTo(StickerItem::class, 'sticker_id');
    }

    public function getRepliesAttribute()
    {
        return $this->replies()->get();
    }

    public function postCommentReports()
    {
        return $this->hasMany(PostCommentReport::class, 'post_comment_id');
    }
}
