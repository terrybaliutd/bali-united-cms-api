<?php

namespace App\Model;

use App\Model\Extension\ResponseCacheTrait;
use Illuminate\Database\Eloquent\Model;

class PostCommentLike extends Model
{
    use ResponseCacheTrait;
    
    protected $table = 'post_comment_likes';

    protected $hidden = [
        "id",
        "created_at",
        "updated_at",
    ];

    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'post_comment_id' => 'integer',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];

    public static function isLikedBy($postComment, $user)
    {
        $like = PostLike::where("post_comment_id", $postComment->id)->where("user_id", $user->id)->first();
        if ($like) {
            return true;
        } else {
            return false;
        }
    }

    public function comment()
    {
        return $this->belongsTo(PostComment::class, 'comment_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
