<?php

namespace App\Model;

use App\Model\Extension\ResponseCacheTrait;
use RichanFongdasen\EloquentBlameable\BlameableTrait;

class PostCommentReport extends BaseModel
{
    use BlameableTrait;
    use ResponseCacheTrait;

    protected $table = 'post_comment_reports';

    protected $fillable = [
        'post_comment_id',
        'user_id',
        'type',
        'reason'
    ];

    public function postComment()
    {
        return $this->belongsTo(PostComment::class, 'post_comment_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
