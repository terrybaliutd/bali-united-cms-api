<?php

namespace App\Model;

use App\Model\Extension\ResponseCacheTrait;
use Illuminate\Database\Eloquent\Model;

class PostLike extends Model
{
    use ResponseCacheTrait;
    
    protected $table = 'post_likes';

    protected $hidden = [
        "id",
        "created_at",
        "updated_at",
    ];

    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'post_id' => 'integer',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];

    public static function isLikedBy($post, $user)
    {
        $like = PostLike::where("post_id", $post->id)->where("user_id", $user->id)->first();
        if ($like) {
            return true;
        } else {
            return false;
        }
    }

    public function post()
    {
        return $this->belongsTo(Post::class, 'post_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
