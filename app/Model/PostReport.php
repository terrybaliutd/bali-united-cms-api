<?php

namespace App\Model;

use App\Model\Extension\ResponseCacheTrait;
use RichanFongdasen\EloquentBlameable\BlameableTrait;

class PostReport extends BaseModel
{
    use BlameableTrait;
    use ResponseCacheTrait;

    protected $table = 'post_reports';

    protected $fillable = [
        'post_id',
        'user_id',
        'type',
        'reason'
    ];

    public function post()
    {
        return $this->belongsTo(Post::class, 'post_id');
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
