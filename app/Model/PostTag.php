<?php

namespace App\Model;

use App\Model\Extension\ResponseCacheTrait;
use Illuminate\Database\Eloquent\Model;

class PostTag extends BaseModel
{
    use ResponseCacheTrait;
    
    protected $table = 'post_tags';

    protected $fillable = [
        'post_id',
        'tag_id'
    ];

    public function tag()
    {
        return $this->belongsTo(Tag::class, 'tag_id');
    }

    public function post()
    {
        return $this->belongsTo(Post::class, 'post_id');
    }
}
