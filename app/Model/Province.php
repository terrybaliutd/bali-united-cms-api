<?php

namespace App\Model;

use App\Model\City;
use App\Model\Extension\ResponseCacheTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class Province extends BaseModel implements SluggableInterface
{
    use ResponseCacheTrait;
    use SluggableTrait;

    protected $table = 'provinces';

    protected $fillable = [
        'code',
        'name'
    ];

    protected $sluggable = [
        'build_from' => 'name'
    ];

    protected $urlKey = 'slug';

    protected $searchField = [
        'code',
        'name'
    ];

    protected $casts = [
        'id' => 'integer',
        'code' => 'string',
        'name' => 'string',
        'slug' => 'string',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];

    public function cities()
    {
        return $this->hasMany(City::class);
    }
}
