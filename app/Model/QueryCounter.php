<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class QueryCounter extends Model
{
    public $table = 'query_counters';

    public $fillable = [
        'url',
        'count',
        'user_id',
        'access_token'
    ];
}
