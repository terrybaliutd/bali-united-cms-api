<?php

namespace App\Model;

use App\Model\Extension\AttachableTrait;
use App\Model\Extension\PublishableTrait;
use App\Model\Extension\ResponseCacheTrait;
use App\Model\QuizUser;
use App\Model\Video;

class Quiz extends BaseModel
{
    use PublishableTrait;
    use AttachableTrait;
    use ResponseCacheTrait;

    protected $table = 'quizzes';

    protected $fillable = [
        'video_id',
        'title',
        'description',
        'question',
        'image',
        'option_1',
        'option_2',
        'option_3',
        'option_4',
        'answer',
        'transaction_type_id',
        'published'
    ];

    protected $dates = [
        'published_at'
    ];

    protected $hidden = [
        'answer'
    ];

    protected $searchField = [
        'title',
        'question',
        'video.title'
    ];

    protected $attachable = [
        'image' => [
            'thumb' => [
                'list' => '1024x683',
                'small' => '320x180',
                'index' => '1024x683',
                'big_preview' => '_x500',
                'small_dynamic_width' => '_x90',
                'small_dynamic_height' => '320x_'
            ]
        ]
    ];

    protected $casts = [
        'id' => 'integer',
        'video_id' => 'integer',
        'title' => 'string',
        'description' => 'string',
        'question' => 'string',
        'image' => 'string',
        'image_type' => 'integer',
        'image_info' => 'string',
        'option_1' => 'string',
        'option_2' => 'string',
        'option_3' => 'string',
        'option_4' => 'string',
        'answer' => 'integer',
        'transaction_type_id' => 'integer',
        'point'
    ];

    protected $appends = [
        'status'
    ];

    public function video()
    {
        return $this->belongsTo(Video::class, 'video_id');
    }

    public function quizUsers()
    {
        return $this->hasMany(QuizUser::class, 'quiz_id');
    }

    public function getStatusAttribute()
    {
        if (count($this->quizUsers) > 0) {
            return 1;
        }
        return 0;
    }

    public function getTransactionType()
    {
        if ($this->transaction_type_id === null || $this->transaction_type_id == 0) {
            return null;
        }
        $client = new \GuzzleHttp\Client(['base_uri' => env('POINT_API_URL', '')]);
        $baseUrl = 'api/transaction-types/' . $this->transaction_type_id;
        $url = $baseUrl;

        $response = $client->get($url, [
            'headers' => [
                'unique_code' => env('UNIQUE_CODE')
            ]
        ]);

        $stream = $response->getBody();
        $result = json_decode($stream->getContents(), true);
        return $result['result']['value'];
    }

    public function transactionTypeList()
    {
        $client = new \GuzzleHttp\Client(['base_uri' => env('POINT_API_URL', '')]);
        $baseUrl = 'api/transaction-types';
        $url = $baseUrl;
        $transTypes = [];
        $nextPage = true;

        while ($nextPage) {
            $response = $client->get($url, [
                'headers' => [
                    'unique_code' => env('UNIQUE_CODE')
                ],
                'query' => [
                    'filter_flag' => 0
                ]
            ]);
            if ($response->getStatusCode() == "200") {
                $result = json_decode($response->getBody(), true);
                foreach ($result['result']['data'] as $transType) {
                    $transTypes[$transType['id']] = $transType['value'];
                }
                if ($result['result']['next_page_url']) {
                    $url = $baseUrl . '?' . parse_url($result['result']['next_page_url'], PHP_URL_QUERY);
                } else {
                    $nextPage = false;
                }
            } else {
                $nextPage = false;
            }
        }

        return $transTypes;
    }
}
