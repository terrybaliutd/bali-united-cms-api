<?php

namespace App\Model;

use App\Model\Extension\ResponseCacheTrait;

class QuizUser extends BaseModel
{
    use ResponseCacheTrait;
    
    protected $table = 'quiz_users';

    protected $fillable = [
        'quiz_id',
        'user_id',
        'answer'
    ];

    protected $searchField = [
        'quiz_id',
        'user_id'
    ];

    protected $casts = [
        'quiz_id' => 'integer',
        'user_id' => 'integer',
        'answer' => 'integer',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];

    public function quiz()
    {
        return $this->belongsTo(\App\Model\Quiz::class, 'quiz_id');
    }

    public function user()
    {
        return $this->belongsTo(\App\Model\User::class, 'user_id');
    }
}
