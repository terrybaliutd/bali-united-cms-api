<?php

namespace App\Model;

use App\Model\Extension\ResponseCacheTrait;

class RajaongkirCity extends BaseModel
{
    use ResponseCacheTrait;
    
    protected $fillable = ['id', 'name', 'province_id'];

    public function province()
    {
        return $this->belongsTo(RajaongkirProvince::class, 'province_id');
    }

    public function users()
    {
        return $this->hasMany(User::class, 'city_id');
    }

    public function subdistricts()
    {
        return $this->hasMany(RajaongkirSubdistrict::class, 'city_id');
    }
}
