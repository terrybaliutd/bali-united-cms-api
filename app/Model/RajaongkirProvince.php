<?php

namespace App\Model;

use App\Model\Extension\ResponseCacheTrait;

class RajaongkirProvince extends BaseModel
{
    use ResponseCacheTrait;
    
    protected $fillable = ['id', 'name'];

    public function cities()
    {
        return $this->hasMany(RajaongkirCity::class, 'province_id');
    }

    public function users()
    {
        return $this->hasMany(User::class, 'province_id');
    }
}
