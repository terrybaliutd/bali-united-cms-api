<?php

namespace App\Model;

use App\Model\Extension\ResponseCacheTrait;

class RajaongkirSubdistrict extends BaseModel
{
    use ResponseCacheTrait;
    
    protected $fillable = ['id', 'name', 'city_id'];

    public function city()
    {
        return $this->belongsTo(RajaongkirCity::class, 'city_id');
    }

    public function users()
    {
        return $this->hasMany(User::class, 'city_id');
    }
}
