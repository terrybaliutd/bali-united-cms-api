<?php

namespace App\Model;

use App\Model\Extension\AttachableTrait;
use App\Model\Extension\PublishableTrait;
use App\Model\Extension\ResponseCacheTrait;

class RewardAds extends BaseModel
{
    use AttachableTrait;
    use PublishableTrait;
    use ResponseCacheTrait;

    protected $fillable = [
        'title',
        'url',
        'image',
        'priority',
        'published',
    ];

    protected $searchField = [
        'title'
    ];

    protected $attachable = [
        'image' => [
            'thumb' => [
                'high' => '1364x456',
                'medium' => '682x228',
                'low' => '341x114'
            ]
        ]
    ];

    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'image' => 'string',
        'priority' => 'integer',
        'image_type' => 'integer',
        'image_info' => 'string',
    ];

    public static function boot()
    {
        parent::boot();

        static::saved(function ($model) {
            forgetCache('RewardAdsRepository');
        });
        static::deleted(function ($model) {
            forgetCache('RewardAdsRepository');
        });
    }
}
