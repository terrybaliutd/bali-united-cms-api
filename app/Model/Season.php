<?php

namespace App\Model;

use App\Model\Competition;
use App\Model\Extension\OrderableTrait;
use App\Model\Extension\PublishableTrait;
use App\Model\Extension\ResponseCacheTrait;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

/**
 * @property integer    id
 * @property string     name
 * @property string     slug
 * @property integer    order
 * @property boolean    is_default
 * @property \Carbon\Carbon|null    published_at
 * @property \Carbon\Carbon|null    created_at
 * @property \Carbon\Carbon|null    updated_at
 *
 * @property-read boolean   published
 *
 * @method static Season|Builder useDefault()
 * @method static Season|Builder ordered()
 */
class Season extends BaseModel implements SluggableInterface
{
    use OrderableTrait;
    use PublishableTrait;
    use ResponseCacheTrait;
    use SluggableTrait;

    protected $table = 'seasons';

    protected $sluggable = [
        'build_from' => 'name'
    ];

    protected $urlKey = 'slug';

    protected $fillable = [
        'name',
        'slug',
        'order',
        'is_default',
        'published',
    ];

    protected $dates = [
        'published_at'
    ];

    protected $appends = [
        'published'
    ];

    protected $searchField = [
        'name',
        'order',
        'id'
    ];

    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'order' => 'integer',
        'is_default' => 'boolean',
        'slug' => 'string',
    ];

    public function competitions()
    {
        return $this->belongsToMany(Competition::class, 'competition_season', 'season_id', 'competition_id');
    }

    public function scopeUseDefault($query)
    {
        return $query->where('is_default', true);
    }
}
