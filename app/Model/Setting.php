<?php

namespace App\Model;

use App\Model\Extension\ResponseCacheTrait;

class Setting extends BaseModel
{
    use ResponseCacheTrait;
    
    protected $table = 'settings';

    protected $fillable = [
        'name',
        'title',
        'description',
        'value'
    ];

    protected $searchField = [
        'name',
        'title',
        'description',
        'value'
    ];

    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'title' => 'string',
        'description' => 'string',
        'value' => 'string',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];

    protected static function boot()
    {
        parent::boot();

        static::initData();
        static::saved(function ($model) {
            $cacheName = "setting.{$model->name}";
            forgetCache('MatchRepository');
            \Cache::forget($cacheName);
        });

        static::deleted(function ($model) {
            $cacheName = "setting.{$model->name}";
            forgetCache('MatchRepository');
            \Cache::forget($cacheName);
        });
    }

    public static function initData()
    {
        $configSettings = config('settings');
        foreach ($configSettings as $name => $configSetting) {
            $cacheName = "setting.$name";
            if (!\Cache::has($cacheName)) {
                $setting = static::findByName($name);
                if ($setting === null) {
                    $setting = new static($configSetting);
                    $setting->name = $name;
                    $setting->save();
                }
                \Cache::put($cacheName, $setting->value, 120);
            }
        }
    }

    public static function findByName($name)
    {
        return static::whereName($name)->first();
    }

    public static function get($name, $default = '')
    {
        $cacheName = "setting.$name";
        $value = \Cache::get($cacheName);
        if ($value === null) {
            $instance = static::findByName($name);
            $value = $default;

            if (!$instance === null) {
                $value = $instance->casted_value;
            }

            \Cache::put($cacheName, $value, 120);
        }

        return $value;
    }

    public function getTypeAttribute()
    {
        return config("settings.{$this->name}.type", 'suitText');
    }

    public function getCastedValueAttribute()
    {
        switch ($this->type) {
            case 'suitTokenField':
                return explode(',', $this->value);
        }

        return $this->value;
    }

    public function transactionTypeList()
    {
        $client = new \GuzzleHttp\Client(['base_uri' => env('POINT_API_URL', '')]);
        $baseUrl = 'api/transaction-types';
        $url = $baseUrl;
        $transTypes = [];
        $nextPage = true;

        while ($nextPage) {
            $response = $client->get($url, [
                'headers' => [
                    'unique_code' => env('UNIQUE_CODE')
                ],
                'query' => [
                    'filter_flag' => 0
                ]
            ]);
            if ($response->getStatusCode() == "200") {
                $result = json_decode($response->getBody(), true);
                foreach ($result['result']['data'] as $transType) {
                    $transTypes[$transType['name']] = $transType['name'];
                }
                if ($result['result']['next_page_url']) {
                    $url = $baseUrl . '?' . parse_url($result['result']['next_page_url'], PHP_URL_QUERY);
                } else {
                    $nextPage = false;
                }
            } else {
                $nextPage = false;
            }
        }
        //dd(Club::published()->lists('name', 'id'), collect($transTypes));
        return collect($transTypes);
    }
}
