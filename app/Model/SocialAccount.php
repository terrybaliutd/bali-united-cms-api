<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class SocialAccount extends BaseModel
{
    const PROVIDER_FACEBOOK = 'facebook';
    const PROVIDER_GOOGLE = 'google';

    protected $table = 'social_accounts';

    protected $fillable = [
        'user_id',
        'provider_user_id',
        'provider',
        'access_token'
    ];

    protected $casts = [
        'user_id' => 'integer',
        'provider_user_id' => 'string',
        'provider' => 'string',
        'access_token' => 'string',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];

    public function user()
    {
        return $this->belongsTo(\App\Model\User::class, 'user_id');
    }

    public static function providerList()
    {
        return [
            static::PROVIDER_FACEBOOK,
            static::PROVIDER_GOOGLE,
        ];
    }
}
