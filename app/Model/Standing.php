<?php

namespace App\Model;

use App\Model\Club;
use App\Model\Competition;
use App\Model\Extension\PublishableTrait;
use App\Model\Extension\ResponseCacheTrait;
use App\Model\Season;

/**
 * @property integer $id
 * @property integer $club_id
 * @property integer $competition_id
 * @property integer $season_id
 * @property integer $position
 * @property integer $matches
 * @property integer $goal
 * @property integer $goal_conceded
 * @property integer $goal_diff
 * @property integer $win
 * @property integer $lose
 * @property integer $draw
 * @property integer $point
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read Club $club
 * @property-read Competition $competition
 */
class Standing extends BaseModel
{
    use PublishableTrait;
    use ResponseCacheTrait;

    protected $table = 'standings';

    protected $fillable = [
        'club_id',
        'competition_id',
        'season_id',
        'position',
        'matches',
        'goal',
        'goal_conceded',
        'goal_diff',
        'win',
        'lose',
        'draw',
        'point',
        'priority',
        'published'
    ];

    protected $casts = [
        'id' => 'integer',
        'club_id' => 'integer',
        'competition_id' => 'integer',
        'season_id' => 'integer',
        'position' => 'integer',
        'matches' => 'integer',
        'goal' => 'integer',
        'goal_conceded' => 'integer',
        'goal_diff' => 'integer',
        'win' => 'integer',
        'lose' => 'integer',
        'draw' => 'integer',
        'point' => 'integer',
        'priority' => 'integer',
    ];

    protected $searchField = [
        'season.name',
        'competition.long_name',
        'club.official_name',
    ];

    protected static function boot()
    {
        parent::boot();

        static::saved(function ($model) {
            forgetCache('StandingRepository');
        });

        static::deleted(function ($model) {
            forgetCache('StandingRepository');
        });
    }

    public function club()
    {
        return $this->belongsTo(Club::class, 'club_id');
    }

    public function competition()
    {
        return $this->belongsTo(Competition::class, 'competition_id');
    }

    public function season()
    {
        return $this->belongsTo(Season::class, 'season_id');
    }

    public function scopeOrdered(Builder $query)
    {
        return $query->orderBy('priority', 'asc')
                     ->orderBy('position', 'asc')
                     ->orderBy('point', 'desc')
                     ->orderBy('goal_diff', 'desc');
    }

    public function generateGoalDiff()
    {
        return $this->goal_diff = $this->goal - $this->goal_conceded;
    }

    public function generatePoint()
    {
        return $this->point = $this->win * 3 + $this->draw;
    }
}
