<?php

namespace App\Model;

use App\Model\Extension\AttachableTrait;
use App\Model\Extension\PublishableTrait;
use App\Model\Extension\ResponseCacheTrait;
use App\Model\StickerItem;
use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;

class StickerCategory extends BaseModel implements SluggableInterface
{
    use PublishableTrait, SluggableTrait, AttachableTrait;
    use ResponseCacheTrait;

    protected $table = 'sticker_categories';

    protected $sluggable = [
        'build_from' => 'title'
    ];

    protected $fillable = [
        'title',
        'published',
        'expired_date',
        'slug',
        'image',
    ];

    protected $attachable = [
        'image' => [
            'thumb' => [
                'small' => '100x100',
                'large' => '300x300'
            ]
        ]
    ];

    protected $urlKey = 'slug';

    protected $dates = [
        'published_at'
    ];

    protected $searchField = [
        'title'
    ];

    protected $casts = [
        'id' => 'integer',
        'title' => 'string',
        'slug' => 'string',
    ];

    public function items()
    {
        return $this->hasMany('App\Model\StickerItem', 'sticker_category_id');
    }
}
