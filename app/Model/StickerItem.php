<?php

namespace App\Model;

use App\Model\Extension\AttachableTrait;
use App\Model\Extension\ResponseCacheTrait;
use App\Model\StickerCategory;

class StickerItem extends BaseModel
{
    use AttachableTrait;
    use ResponseCacheTrait;

    protected $fillable = [
        'title',
        'image'
    ];

    protected $searchField = [
        'title'
    ];

    protected $attachable = [
        'image' => [
            'thumb' => [
                'small' => '100x100',
                'large' => '300x300'
            ]
        ]
    ];

    protected $casts = [
        'id' => 'integer',
        'sticker_category_id' => 'integer',
        'title' => 'string',
        'image' => 'string',
        'image_type' => 'integer',
        'image_info' => 'string',
    ];

    public function category()
    {
        return $this->belongsTo(StickerCategory::class, 'sticker_category_id');
    }
}
