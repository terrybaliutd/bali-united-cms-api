<?php
namespace App\Model;

class Tag extends BaseModel
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tags';

    protected $urlKey = 'name';

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'description'
    ];

    protected $casts = [
        'id' => 'integer',
        'name' => 'string',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];

    /**
     * Get lists of tag (id -> name)
     *
     * @return array
     */
    public static function getTagsLists($name)
    {
        return static::whereIn('name', $name)->get()->lists('name', 'id');
    }

    public function post()
    {
        return $this->belongsToMany(Post::class, 'post_tags', 'tag_id', 'post_id');
    }

    public function getLabel()
    {
        return 'Tag';
    }
}
