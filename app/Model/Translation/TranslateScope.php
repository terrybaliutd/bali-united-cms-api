<?php
namespace App\Model\Translation;

use Illuminate\Database\Eloquent\ScopeInterface;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model as BaseModel;

class TranslateScope implements ScopeInterface
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function apply(Builder $builder, BaseModel $model)
    {
        $builder->with('langs');

        $builder->leftJoin(
            $model->getTranslateTable(),
            $model->getQualifiedKeyName(),
            '=',
            $model->getTranslateTable().'.'.$model->getTranslateForeignKey()
        )
        ->select(array_merge([$model->getTable().'.*'], $model->getQualifiedTranslateFields()))
        ->whereNested(function ($query) use ($model) {
            return $query->where("{$model->getTranslateTable()}.lang", \App::getLocale())
                         ->orWhereNull("{$model->getTranslateTable()}.lang");
        });
    }

    /**
     * Remove the scope from the given Eloquent query builder.
     *
     * @param  \Illuminate\Database\Eloquent\Builder  $builder
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @return void
     */
    public function remove(Builder $builder, BaseModel $model)
    {
        $eagerLoads = $builder->getEagerLoads();
        foreach (array_keys($eagerLoads) as $name) {
            if ($name === 'langs') {
                unset($eagerLoads['langs']);
                break;
            }
        }
        $builder->setEagerLoads($eagerLoads);

        $query = $builder->getQuery();
        foreach ($query->joins as $key => $join) {
            if ($join->table === $model->getTranslateTable()) {
                unset($query->joins[$key]);
            }
        }

        foreach ((array) $query->wheres as $key => $where) {
            if ($where['column'] === 'lang') {
                unset($query->wheres[$key]);

                $query->wheres = array_values($query->wheres);
            }
        }

        $bindings = $query->getBindings();
        foreach ($bindings as $key => $binding) {
            if ($binding === \App::getLocale()) {
                unset($bindings[$key]);
            }
        }
        $query->setBindings($bindings);
    }
}
