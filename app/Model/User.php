<?php

namespace App\Model;

use Cviebrock\EloquentSluggable\SluggableInterface;
use Cviebrock\EloquentSluggable\SluggableTrait;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\Access\Authorizable;
use App\Model\Extension\UserGroupTrait;
use App\Model\Extension\AttachableTrait;
use App\Model\Extension\LoginTrait;

class User extends BaseModel implements
    AuthenticatableContract,
    AuthorizableContract,
    CanResetPasswordContract,
    SluggableInterface
{
    use Authenticatable;
    use Authorizable;
    use CanResetPassword;
    use SluggableTrait;
    use UserGroupTrait;
    use AttachableTrait;
    use LoginTrait;

    /**
     * User Group
     */
    const USER_VERIFIED = "VERIFIED";
    const USER_NOT_VERIFIED = "NOT_VERIFIED";

    const USER = 0;
    const SUPER_ADMIN = 1;

    protected $table = 'users';

    public $timestamps = true;

    protected $sluggable = [
        'build_from' => 'username'
    ];

    protected $fillable = [
        'group_type',
        'username',
        'name',
        'email',
        'password',
        'slug',
        'about',
        'date_of_birth',
        'gender',
        'active',
        'points',
        'address',
        'city',
        'province',
        'zip_code',
        'phone_number',
        'attachment',
        'confirmation_code',
        'phone_number',
        'city_id',
        'province_id',
        'subdistrict_id',
        'status'
    ];

    protected $hidden = [
        'password',
        'remember_token',
        'confirmation_code'
    ];

    protected $searchField = [
        'username',
        'name',
    ];

    protected $appends = [
        "thumbnail"
    ];

    protected $attachable = [
        'attachment' => [
            'thumb' => [
                'list' => '1024x683',
                'small' => '320x180',
                'index' => '1024x683',
                'big_preview' => '_x500',
                'small_dynamic_width' => '_x90',
                'small_dynamic_height' => '320x_'
            ]
        ]
    ];

    protected $casts = [
        'id' => 'integer',
        'group_type' => 'integer',
        'username' => 'string',
        'email' => 'string',
        'password' => 'string',
        'slug' => 'string',
        'address' => 'string',
        'city' => 'string',
        'province' => 'string',
        'zip_code' => 'integer',
        'phone_number' => 'string',
        'name' => 'string',
        'about' => 'string',
        'date_of_birth' => 'datetime',
        'gender' => 'string',
        'attachment' => 'string',
        'attachment_type' => 'integer',
        'attachment_info' => 'string',
        'active' => 'boolean',
        'log_in_count' => 'integer',
        'current_log_in_at' => 'datetime',
        'last_log_in_at' => 'datetime',
        'current_log_in_ip' => 'string',
        'last_log_in_ip' => 'string',
        'remember_token' => 'string',
        'points' => 'integer',
        'confirmation_code' => 'string',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];

    public static function boot()
    {
        parent::boot();

        static::created(function ($model) {
            $model->member_id = 'BU-' . str_pad((string)$model->id, 10, "0", STR_PAD_LEFT);
            $model->save();

//            \Event::fire(new \App\Events\UserRegistered($model));
        });
    }

    public function getThumbnailAttribute()
    {
        return $this->getAttachmentSmallAttribute('attachment');
    }

    public function getAttachmentSmallAttribute()
    {
        return $this->getThumbnail('attachment', 'small');
    }

    public function quizUsers()
    {
        return $this->hasMany(\App\Model\QuizUser::class, 'user_id');
    }

    public function videoAdsUsers()
    {
        return $this->hasMany(\App\Model\VideoAdsUser::class, 'user_id');
    }

    public function likes()
    {
        return $this->hasMany(\App\Model\VideoLike::class, 'user_id');
    }

    public function watches()
    {
        return $this->hasMany(\App\Model\VideoWatch::class, 'user_id');
    }

    public function notification()
    {
        return $this->hasOne(\App\Model\Notification::class, 'user_id');
    }

    public function citizenJournalVideos()
    {
        return $this->hasMany(\App\Model\CitizenJournalVideo::class, 'user_id');
    }

    public function cityRef()
    {
        return $this->belongsTo(\App\Model\RajaongkirCity::class, 'city_id');
    }

    public function provinceRef()
    {
        return $this->belongsTo(\App\Model\RajaongkirProvince::class, 'province_id');
    }

    public function getName()
    {
        return $this->name;
    }

    public function listById()
    {
        $users = $this->get();
        $res = [];
        foreach ($users as $user) {
            $res[$user->getKey()] = $user->name . ' (' . $user->getGroupName() . ')';
        }
        return $res;
    }
}
