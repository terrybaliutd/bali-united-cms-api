<?php

namespace App\Model;

class UserMembership extends BaseModel
{

    protected $table = 'user_memberships';

    public $timestamps = true;

    protected $fillable = [
        'user_id',
        'membership_id',
    ];

    public function membership()
    {
        return $this->belongsTo(\App\Model\Membership::class, 'membership_id');
    }

    public function user()
    {
        return $this->belongsTo(\App\Model\User::class, 'user_id');
    }
}
