<?php

namespace App\Model;

class UserMembershipBonus extends BaseModel
{

    protected $table = 'user_membership_bonuses';

    public $timestamps = true;

    protected $fillable = [
        'user_id',
        'phone_number',
        'object_date',
        'object_amount',
        'object_1',
        'object_2',
        'object_3',
        'object_source',
    ];

    public function user()
    {
        return $this->belongsTo(\App\Model\User::class, 'user_id');
    }
}
