<?php

namespace App\Model;

use App\Model\Comment;
use App\Model\Extension\AttachableTrait;
use App\Model\Extension\PublishableTrait;
use App\Model\Extension\ResponseCacheTrait;
use App\Model\Extension\TaggableTrait;
use App\Model\Extension\YoutubeTrait;
use App\Model\Playlist;
use App\Model\User;
use App\Notifications\VideoPushNotification;

class Video extends BaseModel
{
    use AttachableTrait;
    use PublishableTrait;
    use ResponseCacheTrait;
    use TaggableTrait;
    use YoutubeTrait;

    protected $fillable = [
        'title',
        'key',
        'tags',
        'published',
        'upload_date',
        'attachment',
        'featured',
        'description',
        'duration',
        'is_live',
        'start_time',
        'like_count',
        'membership_id',
        'comment_count'
    ];

    protected $attachable = [
        'attachment' => [
            'thumb' => [
                'list' => '1024x683',
                'low' => '320x180',
                'index' => '1024x683',
                'big_preview' => '_x500',
                'small_dynamic_width' => '_x90',
                'small_dynamic_height' => '320x_'
            ]
        ]
    ];

    protected $dates = [
        'published_at',
        'upload_date'
    ];

    protected $casts = [
        'id' => 'integer',
        'publisher_id' => 'integer',
        'match_id' => 'integer',
        'key' => 'string',
        'description' => 'string',
        'playlist_id' => 'integer',
        'membership_id' => 'integer',
        'title' => 'string',
        'duration' => 'string',
        'featured' => 'boolean',
        'attachment' => 'string',
        'attachment_type' => 'integer',
        'attachment_info' => 'string',
        'is_live' => 'boolean'
    ];

    protected $appends = [
        "thumbnail",
        "youtube_url",
        "time_duration",
        "has_quiz",
        "type"
    ];

    protected $hidden = [
        "likes",
        "duration",
        "quizzes"
    ];

    protected $searchField = [
        'title',
        'playlist.title'
    ];

    protected static function boot()
    {
        parent::boot();

        static::created(function ($model) {
            forgetCache('MatchRepository');
            forgetCache('VideoRepository');
            forgetCache('VideoRecentRepository');
            forgetCache('VideoTrendingRepository');
            if ($model->published && !$model->is_live) {
                $notification = app(VideoPushNotification::class);
                $notification->send($model, "New Video Uploaded - [$model->title]");
                \Log::info('Notif Send on create');
            }
        });
        static::updating(function ($model) {
            forgetCache('MatchRepository');
            forgetCache('VideoRepository');
            forgetCache('VideoRecentRepository');
            forgetCache('VideoTrendingRepository');
            if ($model->isDirty('published_at') && $model->published && !$model->is_live) {
                $notification = app(VideoPushNotification::class);
                $notification->send($model, "New Video Uploaded - [$model->title]");
                \Log::info('Notif Send on update');
            }
        });

        // Add forgetCache to reset cache when data changed
        static::saved(function ($model) {
            forgetCache('MatchRepository');
            forgetCache('VideoRepository');
            forgetCache('VideoRecentRepository');
            forgetCache('VideoTrendingRepository');
        });
        static::deleted(function ($model) {
            forgetCache('MatchRepository');
            forgetCache('VideoRepository');
            forgetCache('VideoRecentRepository');
            forgetCache('VideoTrendingRepository');
        });
    }

    public function setMatchId($matchId)
    {
        $this->match_id = $matchId;
    }

    public function setPlaylistId($playlistId)
    {
        $this->playlist_id = $playlistId;
    }

    public function membership()
    {
        return $this->belongsTo(Membership::class, 'membership_id');
    }

    public function publisher()
    {
        return $this->belongsTo(User::class, 'publisher_id');
    }

    public function getTypeAttribute()
    {
        return "video";
    }

    public function getPlaylistId()
    {
        $playlist = $this->playlist;
        if ($playlist) {
            return $playlist->id;
        } else {
            return null;
        }
    }

    public function getHasQuizAttribute()
    {
        foreach ($this->quizzes as $quiz) {
            if ($quiz->published_at != null) {
                return true;
            }
        }
        return false;
    }

    public function quizzes()
    {
        return $this->hasMany(\App\Model\Quiz::class, 'video_id');
    }

    public function playlist()
    {
        return $this->belongsTo(Playlist::class, 'playlist_id');
    }

    public function comments()
    {
        return $this->hasMany(Comment::class, 'video_id');
    }

    public function match()
    {
        return $this->belongsTo('App\Model\Match', 'match_id');
    }

    public function likes()
    {
        return $this->hasMany('App\Model\VideoLike', 'video_id');
    }

    public function watches()
    {
        return $this->hasMany('App\Model\VideoWatch', 'video_id');
    }

    public function citizenJournal()
    {
        return $this->hasOne(\App\Model\CitizenJournalVideo::class, 'video_id');
    }

    public function userLikes()
    {
        $likes = $this->likes;
        $users = collect();
        foreach ($likes as $like) {
            $user = User::find($like->user_id);
            $users->push($user);
        }
        return $users;
    }

    public static function searchVideo($keywords, $playlistId = null)
    {
        $result = [];
        foreach ($keywords as $keyword) {
            $videos = static::select(['id', 'title', 'description', 'attachment', 'published_at', 'upload_date'])
                ->published()
                ->where('title', 'like', '%'.$keyword.'%')
                ->orderBy("upload_date", "DESC");
            if ($playlistId !== null) {
                $videos = $videos->published()->where('playlist_id', $playlistId);
            }
            $videos = $videos->get();

            foreach ($videos->unique('id') as $videoItem) {
                $value = [
                    'id' => $videoItem->id,
                    'type' => 'video',
                    'title' => $videoItem->title,
                    'description' => $videoItem->description,
                    'image' => $videoItem->getAttachment('attachment'),
                    'published_at' => $videoItem->published_at->toIso8601String(),
                    'datetime' => (new \Carbon\Carbon($videoItem->upload_date))->toIso8601String()
                ];
                $result[] = $value;
            }
        }

        return collect($result)->unique('id')->toArray();
    }
}
