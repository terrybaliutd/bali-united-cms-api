<?php

namespace App\Model;

use App\Model\Extension\AttachableTrait;
use App\Model\Extension\PublishableTrait;
use App\Model\Extension\ResponseCacheTrait;
use App\Model\Extension\YoutubeTrait;

class VideoAds extends BaseModel
{
    use AttachableTrait;
    use YoutubeTrait;
    use PublishableTrait;
    use ResponseCacheTrait;

    protected $fillable = [
        'key',
        'title',
        'attachment',
        'description',
        'duration',
        'priority',
        'published',
        'membership_id',
    ];

    protected $attachable = [
        'attachment' => [
            'thumb' => [
                'high' => '1280x720',
                'medium' => '854x480',
                'low' => '426x240',
            ]
        ]
    ];

    protected $casts = [
        'id' => 'integer',
        'key' => 'string',
        'description' => 'string',
        'title' => 'string',
        'duration' => 'string',
        'attachment' => 'string',
        'attachment_type' => 'integer',
        'attachment_info' => 'string',
        'priority' => 'integer',
        'membership_id' => 'integer',
    ];

    protected $appends = [
        "thumbnail",
        "youtube_url",
        "time_duration",
    ];

    protected $hidden = [
        "duration",
        "upload_date",
    ];

    protected $searchField = [
        'title',
    ];

    public static function boot()
    {
        parent::boot();

        static::saved(function ($model) {
            forgetCache('VideoAdsRepository');
        });
        static::deleted(function ($model) {
            forgetCache('VideoAdsRepository');
        });
    }

    public function membership()
    {
        return $this->belongsTo(Membership::class, 'membership_id');
    }
}
