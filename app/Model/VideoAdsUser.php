<?php

namespace App\Model;

use App\Model\Extension\ResponseCacheTrait;

class VideoAdsUser extends BaseModel
{
    use ResponseCacheTrait;
    
    protected $table = 'video_ads_users';

    protected $fillable = [
        'video_ads_id',
        'user_id'
    ];

    protected $casts = [
        'quiz_id' => 'integer',
        'user_id' => 'integer',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];

    public static function boot()
    {
        parent::boot();

        static::saved(function ($model) {
            forgetCache('VideoAdsUserRepository', \UserAccess::user());
            forgetCache('VideoAdsRepository', \UserAccess::user());
        });
        static::deleted(function ($model) {
            forgetCache('VideoAdsUserRepository', \UserAccess::user());
            forgetCache('VideoAdsRepository', \UserAccess::user());
        });
    }

    public function videoAds()
    {
        return $this->belongsTo(\App\Model\VideoAds::class, 'video_ads_id');
    }

    public function user()
    {
        return $this->belongsTo(\App\Model\User::class, 'user_id');
    }
}
