<?php

namespace App\Model;

use App\Model\Extension\ResponseCacheTrait;
use Illuminate\Database\Eloquent\Model;

class VideoLike extends Model
{
    use ResponseCacheTrait;
    
    protected $hidden = [
        "id",
        "created_at",
        "updated_at",
    ];

    protected $casts = [
        'id' => 'integer',
        'user_id' => 'integer',
        'video_id' => 'integer',
        'created_at' => 'timestamp',
        'updated_at' => 'timestamp',
    ];

    public static function isLikedBy($video, $user)
    {
        $like = VideoLike::where("video_id", $video->id)->where("user_id", $user->id)->first();
        if ($like) {
            return true;
        } else {
            return false;
        }
    }

    public function video()
    {
        return $this->belongsTo('App\Model\Video', 'video_id');
    }

    public function user()
    {
        return $this->belongsTo('App\Model\User', 'user_id');
    }
}
