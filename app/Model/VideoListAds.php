<?php

namespace App\Model;

use App\Model\Extension\AdditionalThumbnailTrait;
use App\Model\Extension\AttachableTrait;
use App\Model\Extension\ImpersonateVideoTrait;
use App\Model\Extension\ResponseCacheTrait;

class VideoListAds extends BaseImageAds
{
    use AttachableTrait;
    use ImpersonateVideoTrait;
    use AdditionalThumbnailTrait;
    use ResponseCacheTrait;

    protected $additionalThumbnails = [
        'attachment' => [
            'list' => '1024x683',
            'index' => '1024x683',
            'big_preview' => '_x500',
            'small_dynamic_width' => '_x90',
            'small_dynamic_height' => '320x_',
        ]
    ];

    protected $appends = [
        'key',
        'thumbnail',
        'time_duration',
        'type',
        'upload_date',
    ];

    protected $attachable = [
        'attachment' => [
            'thumb' => [
                'high' => '1364x968',
                'med' => '682x484',
                'low' => '341x242',
            ]
        ]
    ];

    protected $fillable = [
        'title',
        'url',
        'attachment',
        'priority',
        'published'
    ];

    protected static function boot()
    {
        parent::boot();

        static::saved(function ($model) {
            forgetCache('VideoRepository');
            forgetCache('VideoListAdsRepository');
        });
        static::deleted(function ($model) {
            forgetCache('VideoRepository');
            forgetCache('VideoListAdsRepository');
        });
    }
}
