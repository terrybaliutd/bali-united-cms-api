<?php

namespace App\Notifications;

use Carbon\Carbon;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Database\Eloquent\Model;

abstract class BasePushNotification
{
    protected $client;

    protected $baseUrl;

    protected $key;

    protected $retry = 3;

    protected $clickAction;

    public function __construct()
    {
        $this->client = app(Client::class);
    }

    protected function push($path, $body)
    {
        $requestPath = rtrim($this->getBaseUrl(), '/') . '/' . ltrim($path, '/');
        $headers = [
            'Content-Type' => 'application/json',
            'Authorization' => 'key=' . $this->getKey()
        ];
        $options = [
            'headers' => $headers,
            'json' => $body,
            'http_errors' => false
        ];
        return $this->client->post($requestPath, $options);
    }

    protected function getBaseUrl()
    {
        if ($this->baseUrl) {
            return $this->baseUrl;
        } elseif (config('services.firebase.url')) {
            return config('services.firebase.url');
        }
        return null;
    }

    protected function getKey()
    {
        if ($this->key) {
            return $this->key;
        } elseif (config('services.firebase.key')) {
            return config('services.firebase.key');
        }
        return null;
    }

    protected function getRetryNumber()
    {
        return $this->retry;
    }

    protected function sendData($data, $retry)
    {
        $latestException = null;
        do {
            try {
                $result = $this->push('fcm/send', $data);
                if ($result->getStatusCode() != 200) {
                    \Log::warning(
                        "Failed to send notification with status code {$result->getStatusCode()}",
                        compact('data')
                    );
                }
                $latestException = null;
                \Log::info(json_decode($result->getBody()->getContents(), true));
                break;
            } catch (RequestException $exception) {
                $latestException = $exception;
                usleep(200000); // sleep for 200 ms
            }
        } while (--$retry > 0);
        return $latestException;
    }
}
