<?php

namespace App\Notifications;

use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Model;

class CustomPushNotification extends BasePushNotification
{
    protected $clickAction = 'com.baliutd.open_home';
    
    public function setClickAction($clickAction)
    {
        $this->clickAction = $clickAction;
    }

    public function send($body, $title)
    {
        $retry = $this->getRetryNumber();
        $data = [
            'to' => "/topics/home",
            'data' => [
                'id' => null,
                'type' => null,
                'detail' => null
            ],
            'notification' => [
                'title' => $title,
                'body' => $body,
                'icon' => 'ic_notification',
                'color' => '#d91a31',
                'click_action' => $this->clickAction
            ]
        ];
        \Log::info('custom match push notif'.$body);
        $latestException = null;
        do {
            try {
                $result = $this->push('fcm/send', $data);
                if ($result->getStatusCode() != 200) {
                    \Log::warning(
                        "Failed to send notification with status code {$result->getStatusCode()}",
                        compact('data')
                    );
                }
                $latestException = null;
                \Log::info(json_decode($result->getBody()->getContents(), true));
                break;
            } catch (RequestException $exception) {
                $latestException = $exception;
                usleep(200000); // sleep for 200 ms
            }
        } while (--$retry > 0);

        if ($latestException) {
            \Log::warning('Failed to send notification with exception', ['exception' => $latestException]);
            return false;
        }
    }
}
