<?php

namespace App\Notifications;

use App\Model\Match;
use GuzzleHttp\Client;
use Illuminate\Database\Eloquent\Model;

class HighlightPushNotification extends BasePushNotification
{
    protected $clickAction = 'com.baliutd.open_home';

    public function setClickAction($clickAction)
    {
        $this->clickAction = $clickAction;
    }

    public function send(Model $match, $body, $title)
    {
        $retry = $this->getRetryNumber();
        $match->setVisible([
            'id',
            'competition_id',
            'season_id',
            'home_club',
            'away_club',
            'start_at',
            'venue',
            'status',
            'tv_info',
        ]);

        $data = [
            'to' => "/topics/home",
            'data' => [
                'id' => $match->id,
                'type' => 'home',
                'detail' => $match->toArray()
            ],
            'notification' => [
                'title' => $title,
                'body' => $body,
                'icon' => 'ic_notification',
                'color' => '#d91a31',
                'click_action' => $this->clickAction
            ]
        ];
        \Log::info('match highlight push notif'.$body);
        $latestException = null;
        do {
            try {
                $result = $this->push('fcm/send', $data);
                if ($result->getStatusCode() != 200) {
                    \Log::warning(
                        "Failed to send notification with status code {$result->getStatusCode()}",
                        compact('data')
                    );
                }
                $latestException = null;
                \Log::info(json_decode($result->getBody()->getContents(), true));
                break;
            } catch (RequestException $exception) {
                $latestException = $exception;
                usleep(200000); // sleep for 200 ms
            }
        } while (--$retry > 0);

        if ($latestException) {
            \Log::warning('Failed to send notification with exception', ['exception' => $latestException]);
            return false;
        }
    }
}
