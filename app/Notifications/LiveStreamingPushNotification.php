<?php

namespace App\Notifications;

use GuzzleHttp\Client;
use App\Model\Video;

class LiveStreamingPushNotification extends BasePushNotification
{
    protected $clickAction = 'com.baliutd.open_video';

    public function send(Video $video, $body, $title)
    {
        $retry = $this->getRetryNumber();
        $video->setVisible([
            'id',
            'title',
            'description',
            'duration',
        ]);

        $data = [
            'to' => "/topics/video",
            'data' => [
                'id' => $video->id,
                'type' => 'video',
                'detail' => $video->toArray()
            ],
            'notification' => [
                'title' => $title,
                'body' => $body,
                'icon' => 'ic_notification',
                'color' => '#d91a31',
                'click_action' => $this->clickAction
            ]
        ];
        \Log::info('Send Live Streaming Notification.');
        $latestException = null;
        do {
            try {
                $result = $this->push('fcm/send', $data);
                if ($result->getStatusCode() != 200) {
                    \Log::warning(
                        "Failed to send notification with status code {$result->getStatusCode()}",
                        compact('data')
                    );
                }
                $latestException = null;
                \Log::info(json_decode($result->getBody()->getContents(), true));
                break;
            } catch (RequestException $exception) {
                $latestException = $exception;
                usleep(200000); // sleep for 200 ms
            }
        } while (--$retry > 0);

        if ($latestException) {
            \Log::warning('Failed to send notification with exception', ['exception' => $latestException]);
            return false;
        }
    }
}
