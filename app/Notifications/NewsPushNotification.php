<?php

namespace App\Notifications;

use App\Model\News;
use GuzzleHttp\Client;

class NewsPushNotification extends BasePushNotification
{
    protected $clickAction = 'com.baliutd.open_news';

    public function send(News $news)
    {
        $retry = $this->getRetryNumber();
        $news->setVisible([
            'id',
            'title',
        ]);

        $data = [
            'to' => "/topics/news",
            'data' => [
                'id' => $news->id,
                'type' => 'news',
                'detail' => $news->toArray()
            ],
            'notification' => [
                'title' => "Baca Artikel Baru Bali United",
                'body' => $news->title,
                'icon' => 'ic_notification',
                'color' => '#d91a31',
                'click_action' => $this->clickAction
            ]
        ];

        $latestException = $this->sendData($data, $retry);

        if ($latestException) {
            \Log::warning('Failed to send notification with exception', ['exception' => $latestException]);
            return false;
        }
    }
}
