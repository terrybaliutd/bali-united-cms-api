<?php

namespace App\Notifications;

use App\Model\Video;
use GuzzleHttp\Client;

class VideoPushNotification extends BasePushNotification
{
    protected $clickAction = 'com.baliutd.open_video';

    public function send(Video $video)
    {
        $retry = $this->getRetryNumber();
        $video->setVisible([
            'id',
            'title',
            'description',
            'duration',
        ]);

        $data = [
            'to' => "/topics/video",
            'data' => [
                'id' => $video->id,
                'type' => 'video',
                'detail' => $video->toArray()
            ],
            'notification' => [
                'title' => "Saksikan Video Baru Bali United",
                'body' => $video->title,
                'icon' => 'ic_notification',
                'color' => '#d91a31',
                'click_action' => $this->clickAction
            ]
        ];

        $latestException = $this->sendData($data, $retry);

        if ($latestException) {
            \Log::warning('Failed to send notification with exception', ['exception' => $latestException]);
            return false;
        }
    }
}
