<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\API\Auth\UserAccess;
use App\Services\AccessTokenService;

class APIServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('user-access', function () {
            return new UserAccess(new AccessTokenService);
        });
    }
}
