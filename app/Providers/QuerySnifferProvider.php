<?php

namespace App\Providers;

use App\Services\QueryCounterService;
use Illuminate\Support\ServiceProvider;

class QuerySnifferProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        if (strtolower(request()->server->get('SCRIPT_FILENAME')) == 'artisan') {
            return;
        }

        if (!env('SQL_QUERY_SNIFFING')) {
            return;
        }

        \DB::listen(function ($sql, $bindings, $time) {
            app('query-counter')->incrementCounter();
            if (env('APP_ENV') == 'query-debug') {
                \Log::info($sql);
                unset($bindings);
                unset($time);
            }
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('query-counter', function () {
            return new QueryCounterService();
        });
    }
}
