<?php

namespace App\Providers;

use App\Model\VideoAds;
use App\Model\VideoAdsUser;
use App\Repositories\VideoAdsRepository;
use App\Repositories\VideoAdsUserRepository;
use Illuminate\Support\ServiceProvider;

class RepositoryProvider extends ServiceProvider
{
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('VideoAdsRepository', function () {
            return new VideoAdsRepository(
                new VideoAds,
                new VideoAdsUserRepository(new VideoAdsUser)
            );
        });
    }
}
