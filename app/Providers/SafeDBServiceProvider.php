<?php

namespace App\Providers;

use App\Services\SafeDBService;
use Illuminate\Support\ServiceProvider;

class SafeDBServiceProvider extends ServiceProvider
{
    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(SafeDBService::class, function () {
            return new SafeDBService();
        });
    }
}
