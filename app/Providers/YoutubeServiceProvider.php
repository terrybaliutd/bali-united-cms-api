<?php

namespace App\Providers;

use Illuminate\Foundation\Application;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Str;

class YoutubeServiceProvider extends ServiceProvider
{

    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     * @return void
     */
    public function boot()
    {
        if ($this->isLegacyLaravel() || $this->isOldLaravel()) {
            $this->package('alaouy/youtube', 'alaouy/youtube');
        }

        $loader = \Illuminate\Foundation\AliasLoader::getInstance();
        $loader->alias('YoutubeData', 'App\Facades\YoutubeData');
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->app['youtubeOAuth'] = $this->app->share(function () {
            return new \App\Supports\Youtube(new \Google_Client);
        });

        if ($this->isLegacyLaravel() || $this->isOldLaravel()) {
            $this->app['youtubeData'] = $this->app->share(function () {
                $key = \Config::get('alaouy/youtube::KEY');
                return new \Alaouy\Youtube\Youtube($key);
            });

            return;
        }

        //Laravel 5.1+ fix
        if (floatval(Application::VERSION) >= 5.1) {
            $this->app->bind("youtubeData", function () {
                return $this->app->make('Alaouy\Youtube\Youtube', [config('youtube.KEY')]);
            });
        } else {
            $this->app->bindShared('youtubeData', function () {
                return $this->app->make('Alaouy\Youtube\Youtube', [config('youtube.KEY')]);
            });
        }
    }

    /**
     * Get the services provided by the provider.
     *
     * @return array
     */
    public function provides()
    {
        return ['youtubeOAuth', 'youtubeData'];
    }

    public function isLegacyLaravel()
    {
        return Str::startsWith(Application::VERSION, ['4.1.', '4.2.']);
    }

    public function isOldLaravel()
    {
        return Str::startsWith(Application::VERSION, '4.0.');
    }
}
