<?php

namespace App\Repositories;

use App\Model\BankAccount;
use App\Repositories\BaseRepository;

class BankAccountRepository extends BaseRepository
{
    public function __construct(BankAccount $bankAccount)
    {
        parent::__construct($bankAccount);

        $this->implementPagination = false;
    }
}
