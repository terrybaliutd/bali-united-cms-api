<?php

namespace App\Repositories;

use App\Repositories\BaseRepository;
use App\Model\BaseModel;

abstract class BaseAdsRepository extends BaseRepository
{
    public function __construct(BaseModel $model)
    {
        parent::__construct($model);
    }

    private function generateBasicQuery()
    {
        $query = $this->model->published();

        $additionalQuery = $this->grabAdditionalQuery();
        if ($additionalQuery instanceof \Closure) {
            $query = $additionalQuery($query);
        }

        return $query;
    }

    protected function getAds()
    {
        return $this->generateBasicQuery()->get();
    }

    protected function getAdsCount()
    {
        return $this->generateBasicQuery()->count();
    }

    protected function getIndex(array $queryList)
    {
        return $this->getAds();
    }
}
