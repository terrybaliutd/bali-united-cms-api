<?php

namespace App\Repositories;

use App\Http\Controllers\Extension\NameTrait;
use App\Http\Controllers\Extension\VisibilityTrait;
use App\Model\BaseModel;
use App\Repositories\Extensions\CacheableTrait;
use Illuminate\Database\Eloquent\Builder;

abstract class BaseRepository
{
    use CacheableTrait;
    use NameTrait;
    use VisibilityTrait;

    const DEFAULT_PER_PAGE = 10;

    const MAX_PER_PAGE = 1000;

    protected $model;

    protected $filterFields = [];

    protected $implementPagination = true;

    public function __construct(BaseModel $model)
    {
        $this->model = $model;
    }

    protected function getIndex(array $queryList)
    {
        $perPage = $this->grabRequestPerPage($queryList);

        $query = $this->createQueryFromRequest($queryList);

        $additionalQuery = $this->grabAdditionalQuery();
        if ($additionalQuery instanceof \Closure) {
            $query = $additionalQuery($query);
        }

        if ($this->implementPagination) {
            $data = $this->paginate($query, $perPage);
            $data->appends($queryList);
        } else {
            $data = $query->get();
        }

        return $this->cleanUp($data, $queryList);
    }

    protected function getShow(array $queryList, $key)
    {
        $query = $this->createQueryFromRequest($queryList);

        $model = $query->findOrFailByUrlKey($key);

        return $this->cleanUp($model, $queryList);
    }

    public function setAdditionalQuery(\Closure $query)
    {
        $this->query = $query;
    }

    public function grabAdditionalQuery()
    {
        return isset($this->query) ? $this->query : null;
    }

    protected function grabRequestPerPage(array $queryList)
    {
        $perPage = isset($queryList['per_page']) ? $queryList['per_page'] : static::DEFAULT_PER_PAGE;
        if ($perPage >= static::MAX_PER_PAGE) {
            return static::MAX_PER_PAGE;
        }
        return $perPage;
    }

    protected function createQueryFromRequest(array $queryList)
    {
        $filterParams = array_filter($queryList, function ($key) {
            return starts_with($key, 'filter_');
        }, ARRAY_FILTER_USE_KEY);

        foreach ($filterParams as $key => $value) {
            sscanf($key, "filter_%s", $column);

            if ($value != null && $value != "\"\"") {
                $filterParams[$column] = $value;
            }

            unset($filterParams[$key]);
        }

        $orderParams = array_filter($queryList, function ($key) {
            return starts_with($key, 'order_');
        }, ARRAY_FILTER_USE_KEY);

        foreach ($orderParams as $key => $value) {
            sscanf($key, "order_%s", $column);
            $column = str_replace(':', '.', $column);
            $orderParams[$column] = $value;
            unset($orderParams[$key]);
        }
        $searchQuery = request()->get('s');

        return $this->model->filter($searchQuery, $orderParams, $filterParams)->newQuery();
    }

    protected function grabVisibleFields(array $queryList)
    {
        $fields = isset($queryList['fields']) ? $queryList['fields'] : '';
        return $this->parseFields($fields);
    }

    protected function grabHiddenFields(array $queryList)
    {
        $hiddenFields = isset($queryList['hidden_fields']) ? $queryList['hidden_fields'] : '';
        return $this->parseFields($hiddenFields);
    }

    protected function paginate(Builder $query, $perPage)
    {
        return $query->paginate($perPage);
    }

    protected function parseFields($rawFields)
    {
        if (empty($rawFields)) {
            return null;
        }
        $fields = explode(',', $rawFields);
        $fields = array_map('trim', $fields);
        return array_unique($fields);
    }

    protected function cleanUp($object, $queryList)
    {
        $visibleFields = $this->grabVisibleFields($queryList);
        $hiddenFields = $this->grabHiddenFields($queryList);
        $this->filterVisible($object, $visibleFields, $hiddenFields);

        return $object;
    }
}
