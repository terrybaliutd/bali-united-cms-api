<?php

namespace App\Repositories\Extensions;

use App\Model\User;
use Cache;
use Carbon\Carbon;

/**
 * Cacheable Trait
 * Automatically cache data in function which started with get or has
 *
 * usage:
 * use CacheableTrait;
 *
 * Function name which started with get or has will be cacheable automatically.
 * Visibility function which needed to be cacheable automatically MUST BE protected function
 * e.q. getSomething() or hasSomething($array, $data)
 *
 * Cache key will be yourClassName:yourMethodName:{{sha1(serialize(yourParameter))}}
 * e.q. PollRepository:getSomething:8739602554c7f3241958e3cc9b57fdecb474d508
 *
 * For forgetting cache in specific repository, add this function in helpers
 * if (!function_exists('forgetCache')) {
        function forgetCache($prefix)
        {
            Cache::forever($prefix, \Carbon\Carbon::createFromDate(2015, 1, 1)->timestamp);
        }
    }
 * And use directly forgetCache('PollRepository')
 */

trait CacheableTrait
{
    public function __call($method, $args)
    {
        if (!method_exists($this, $method)) {
            throw new \Exception("Method " . $method . " doesn't exist in " . $this->generateClass());
        }

        $cacheKey = $this->generateCacheKey($method, $args);
        if (starts_with($method, 'has') || starts_with($method, 'get')) {
            $default = ["timestamp" => Carbon::createFromDate(2015, 1, 1)->timestamp];
            $cacheKey = $this->generateCacheKey($method, $args);
            $invalidateKey = $this->generateInvalidateKey($args);
            $cacheResult = Cache::get($cacheKey, $default);
            $lastInvalidate = $this->getLastInvalidate($invalidateKey);
            if ($cacheResult['timestamp'] >= $lastInvalidate) {
                if (array_key_exists('value', $cacheResult)) {
                    return $cacheResult['value'];
                }
            }

            $result = call_user_func_array([$this, $method], $args);
            Cache::forever($cacheKey, ['timestamp' => Carbon::now()->timestamp, 'value' => $result]);
            Cache::forever($invalidateKey, Carbon::now()->timestamp);
            return $result;
        }
        return call_user_func_array([$this, $method], $args);
    }

    protected function generateCacheKey($method, $args)
    {
        $cacheKey = $this->getBaseInvalidateKey() . ':' . $method . ':';

        foreach ($args as $param) {
            if ($param instanceof User) {
                $cacheKey .= $param->getKey() . ':';
            }
        }
        return $cacheKey . sha1(serialize($args));
    }

    protected function generateClass()
    {
        $className = get_class($this);
        return last(explode('\\', $className));
    }

    protected function getBaseInvalidateKey()
    {
        return isset($this->cacheKey) ? $this->cacheKey : $this->generateClass();
    }

    protected function generateInvalidateKey($args)
    {
        $invalidateKey = $this->getBaseInvalidateKey();

        foreach ($args as $param) {
            if ($param instanceof User) {
                $invalidateKey .= ':' . $param->getKey();
            }
        }
        return $invalidateKey;
    }

    protected function getLastInvalidate($key)
    {
        $specific = Cache::get($key, Carbon::now()->timestamp);
        $global = (strpos($key, ':') !== false)
            ? Cache::get($this->getBaseInvalidateKey(), Carbon::now()->timestamp)
            : 0;

        return max($specific, $global);
    }
}
