<?php

namespace App\Repositories\Extensions;

use App\Supports\Pagination\AdsPaginator;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Pagination\Paginator;

trait HasAdsTrait
{
    /**
     * Ads Repository
     *
     * @var App\Repositories\BaseAdsRepository
     */
    protected $ads;

    /**
     * The number of Ads that should be displayed per page
     *
     * @var integer
     */
    protected $adsPerPage = 1;

    /**
     * Ads page increment
     * This variable would be used only for the home ads
     *
     * @var integer
     */
    protected $pageIncrement = 0;

    /**
     * Grab request per page value
     * Override the original method from BaseRepository
     *
     * @param  array  $queryList
     * @return integer
     */
    protected function grabRequestPerPage(array $queryList)
    {
        $decrement = ((int)$this->ads->getAdsCount() > 0) ? $this->adsPerPage : 0;

        return parent::grabRequestPerPage($queryList) - $decrement;
    }

    /**
     * Paginate the given Eloquent Query Builder
     * Override the original method from BaseRepository
     *
     * @param  Builder $query
     * @param  integer $perPage [description]
     * @return App\Supports\Pagination\AdsPaginator
     */
    protected function paginate(Builder $query, $perPage)
    {
        $pageName = 'page';
        $dbQuery = $query->getQuery();

        $total = $dbQuery->getCountForPagination();

        $dbQuery->forPage(
            $page = Paginator::resolveCurrentPage($pageName),
            $perPage
        );

        return new AdsPaginator($this->ads, $query->get(['*']), $total, $perPage, $page, [
            'path' => Paginator::resolveCurrentPath(),
            'pageName' => $pageName,
            'adsPerPage' => $this->adsPerPage,
            'pageIncrement' => $this->pageIncrement
        ]);
    }
}
