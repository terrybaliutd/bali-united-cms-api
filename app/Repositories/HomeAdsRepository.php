<?php

namespace App\Repositories;

use App\Model\HomeAds;
use App\Repositories\BaseAdsRepository;
use Illuminate\Database\Eloquent\Builder;

class HomeAdsRepository extends BaseAdsRepository
{
    public function __construct(HomeAds $model)
    {
        parent::__construct($model);

        $this->setAdditionalQuery(function (Builder $query) {
            return $query->orderBy('priority', 'asc');
        });
    }
}
