<?php

namespace App\Repositories;

use App\Model\Match;
use App\Repositories\BaseRepository;

class MatchFixturesRepository extends BaseRepository
{
    public function __construct(Match $match)
    {
        parent::__construct($match);
    }
}
