<?php

namespace App\Repositories;

use App\Model\Match;
use App\Model\Video;
use App\Repositories\BaseRepository;

class MatchRepository extends BaseRepository
{
    public function __construct(Match $match)
    {
        parent::__construct($match);
    }

    protected function getLiveMatch(array $queryList)
    {
        // $perPage = $this->grabRequestPerPage($queryList);

        $query = $this->createQueryFromRequest($queryList);
        $additionalQuery = $this->grabAdditionalQuery();
        $query = $additionalQuery($query);

        $isLive = true;
        $liveMatch = $query->first();
        $banner = asset(\Setting::findByName('default-banner-image')->casted_value);

        if ($liveMatch) {
            if ($liveMatch->banner != "") {
                $banner = asset($liveMatch->banner);
            }

            $video = Video::find($liveMatch->live_video_id);
            if ($video != null) {
                $video->load('match');
                return [
                    "is_live" => $isLive,
                    "banner" => $banner,
                    "video" => $video,
                    "news" => null,
                ];
            }
        }

        $isLive = false;
        $video = Video::orderBy('upload_date', 'desc')->first();
        $news = \App\Model\News::published()->orderBy('news_date', 'desc')->first();
        if ($video != null && $news != null) {
            if ($video->upload_date < $news->news_date) {
                $video = null;
            } else {
                $news = null;
            }
        }

        return [
            "is_live" => $isLive,
            "banner" => $banner,
            "video" => $video,
            "news" => $news,
        ];
    }
}
