<?php

namespace App\Repositories;

use App\Model\BankAccount;
use App\Model\Membership;
use App\Repositories\BaseRepository;

class MembershipRepository extends BaseRepository
{
    public function __construct(Membership $membership)
    {
        parent::__construct($membership);

        $this->implementPagination = false;
    }
}
