<?php

namespace App\Repositories;

use App\Model\NewsListAds;
use App\Repositories\BaseAdsRepository;
use Illuminate\Database\Eloquent\Builder;

class NewsListAdsRepository extends BaseAdsRepository
{
    public function __construct(NewsListAds $model)
    {
        parent::__construct($model);

        $this->setAdditionalQuery(function (Builder $query) {
            return $query->orderBy('priority', 'asc');
        });
    }
}
