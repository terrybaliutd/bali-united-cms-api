<?php

namespace App\Repositories;

use App\Model\News;
use App\Repositories\BaseRepository;
use App\Repositories\Extensions\HasAdsTrait;
use App\Repositories\HomeAdsRepository;

class NewsRecentRepository extends BaseRepository
{
    use HasAdsTrait;

    public function __construct(News $news, HomeAdsRepository $adsRepo)
    {
        parent::__construct($news);
        $this->ads = $adsRepo;

        // $this->adsPerPage = 2;
        // $this->pageIncrement = 1;
    }
}
