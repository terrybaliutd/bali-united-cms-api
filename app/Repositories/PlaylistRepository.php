<?php

namespace App\Repositories;

use App\Model\Playlist;
use App\Repositories\BaseRepository;

class PlaylistRepository extends BaseRepository
{
    public function __construct(Playlist $playlist)
    {
        parent::__construct($playlist);
    }
}
