<?php

namespace App\Repositories;

use App\Model\RajaongkirCity;
use App\Repositories\BaseRepository;

class RajaongkirCityRepository extends BaseRepository
{
    public function __construct(RajaongkirCity $model)
    {
        parent::__construct($model);

        $this->implementPagination = false;
    }
}
