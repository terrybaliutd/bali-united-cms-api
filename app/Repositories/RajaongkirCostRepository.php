<?php

namespace App\Repositories;

use App\Repositories\Extensions\CacheableTrait;
use GuzzleHttp\Client;

class RajaongkirCostRepository
{
    use CacheableTrait;

    private $client;

    public function __construct()
    {
        $this->client = new Client(['base_uri' => env('RAJAONGKIR_URL', '')]);
    }

    /**
     * Get shipping cost from RajaOngkir API
     *
     * Bali United Office Address:
     * Stadion Kapten I Wayan Dipta,
     * Buruan, Blahbatuh, Kabupaten Gianyar,
     * Bali 80581
     *
     * Default Origin for RajaOngkir API:
     * Province ID    : 1
     * City ID        : 128
     * Subdistrict ID : 1764
     *
     * @param  array  $query
     * @return array
     */
    public function getShippingCost(array $query)
    {
        $defaultCourier = \Setting::findByName('reward-shipping-default-courier')->value;
        $courier = data_get($query, 'courier', $defaultCourier);
        $destinationType = data_get($query, 'destination_type', 'subdistrict');

        $response = $this->client->request(
            'POST',
            'cost',
            [
                'headers' => [
                    'key' => env('RAJAONGKIR_API_KEY', '')
                ],
                'form_params' => [
                    'origin' => \Setting::findByName('reward-shipping-origin-id')->value,
                    'destination' => $query['destination_id'],
                    'weight' => $query['weight_in_grams'],
                    'courier' => $courier,
                    'originType' => \Setting::findByName('reward-shipping-origin-type')->value,
                    'destinationType' => $destinationType
                ]
            ]
        );
        $stream = $response->getBody();
        $result = json_decode($stream->getContents(), true);
        if ($result['rajaongkir']) {
            $result = $this->modifyRajaOngkirResult($result['rajaongkir']);
            return [
                'status' => $result['status']['code'],
                'query' => $result['query'],
                'message' => $result['status']['description'],
                'result' => $result['results']
            ];
        } else {
            return [
                'status' => 503,
                'message' => 'Service Unavailable',
                'result' => []
            ];
        }
    }

    private function modifyRajaOngkirResult($result)
    {
        $courierService = trim(strtoupper(data_get($result, 'results.0.code', '')));
        $costs = data_get($result, 'results.0.costs', []);

        if (empty($costs)) {
            return $result;
        }

        for ($i = 0, $c = count($costs); $i < $c; $i++) {
            if (($courierService == 'JNE') && ($this->replaceCtC($costs[$i]['service']) == 'YES')) {
                unset($costs[$i]);
                continue;
            }
            $costs[$i]['service'] = trim($courierService . ' ' . $this->replaceCtC($costs[$i]['service']));
        }
        sort($costs);
        $result['results'][0]['costs'] = $costs;

        return $result;
    }

    private function replaceCtC($service)
    {
        $service = strtoupper($service);

        switch ($service) {
            case 'CTC':
            case 'CTCREG':
                return 'REG';
            case 'CTCYES':
                return 'YES';
            case 'CTCOKE':
                return 'OKE';
            default:
                return $service;
        }
    }
}
