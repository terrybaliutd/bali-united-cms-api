<?php

namespace App\Repositories;

use App\Model\RajaongkirProvince;
use App\Repositories\BaseRepository;

class RajaongkirProvinceRepository extends BaseRepository
{
    public function __construct(RajaongkirProvince $model)
    {
        parent::__construct($model);

        $this->implementPagination = false;
    }
}
