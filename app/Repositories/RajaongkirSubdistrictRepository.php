<?php

namespace App\Repositories;

use App\Model\RajaongkirSubdistrict;
use App\Repositories\BaseRepository;

class RajaongkirSubdistrictRepository extends BaseRepository
{
    public function __construct(RajaongkirSubdistrict $model)
    {
        parent::__construct($model);

        $this->implementPagination = false;
    }
}
