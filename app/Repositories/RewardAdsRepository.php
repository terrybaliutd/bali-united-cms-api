<?php

namespace App\Repositories;

use App\Model\RewardAds;
use Illuminate\Database\Eloquent\Builder;

class RewardAdsRepository extends BaseAdsRepository
{
    public function __construct(RewardAds $model)
    {
        parent::__construct($model);

        $this->setAdditionalQuery(function (Builder $query) {
            return $query->orderBy('priority', 'asc');
        });
    }
}
