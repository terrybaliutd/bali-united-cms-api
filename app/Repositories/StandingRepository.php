<?php

namespace App\Repositories;

use App\Model\Standing;
use App\Repositories\BaseRepository;

class StandingRepository extends BaseRepository
{
    public function __construct(Standing $standing)
    {
        parent::__construct($standing);
    }
}
