<?php

namespace App\Repositories;

use App\Model\User;
use App\Model\VideoAds;
use Illuminate\Database\Eloquent\Builder;

class VideoAdsRepository extends BaseAdsRepository
{
    protected $videoAdsUser;

    public function __construct(VideoAds $model, VideoAdsUserRepository $videoAdsUser)
    {
        parent::__construct($model);

        $this->videoAdsUser = $videoAdsUser;

        $this->setAdditionalQuery(function (Builder $query) {
            return $query->orderBy('priority', 'asc');
        });
    }

    protected function getVideoAdsList(User $user)
    {
        $allAds = app('VideoAdsRepository')->getAds()->toArray();
        $watchedAds = [];
        $unwatchedAds = [];

        foreach ($allAds as $item) {
            $hasWatched = $this->videoAdsUser->getWatchedBy($user, $item['id']);
            if ($hasWatched) {
                $item['hasWatched'] = true;
                $watchedAds[] = $item;
            } else {
                $item['hasWatched'] = false;
                $unwatchedAds[] = $item;
            }
        }

        if (!empty($watchedAds)) {
            array_splice($unwatchedAds, count($unwatchedAds), 0, $watchedAds);
        }

        return $unwatchedAds;
    }
}
