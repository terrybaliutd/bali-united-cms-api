<?php

namespace App\Repositories;

use App\Model\User;
use App\Model\VideoAdsUser;
use App\Repositories\Extensions\CacheableTrait;

class VideoAdsUserRepository
{
    use CacheableTrait;

    private $model;

    public function __construct(VideoAdsUser $model)
    {
        $this->model = $model;
    }

    protected function getWatchedBy(User $user, $videoAdsId)
    {
        return $this->model
            ->where("video_ads_id", $videoAdsId)
            ->where("user_id", $user->id)
            ->first();
    }
}
