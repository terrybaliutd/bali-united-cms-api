<?php

namespace App\Repositories;

use App\Model\VideoListAds;
use App\Repositories\BaseAdsRepository;
use Illuminate\Database\Eloquent\Builder;

class VideoListAdsRepository extends BaseAdsRepository
{
    public function __construct(VideoListAds $model)
    {
        parent::__construct($model);

        $this->setAdditionalQuery(function (Builder $query) {
            return $query->orderBy('priority', 'asc');
        });
    }
}
