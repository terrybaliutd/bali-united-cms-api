<?php

namespace App\Repositories;

use App\Model\Video;
use App\Repositories\BaseRepository;
use App\Repositories\Extensions\HasAdsTrait;
use App\Repositories\HomeAdsRepository;

class VideoRecentRepository extends BaseRepository
{
    use HasAdsTrait;

    public function __construct(Video $video, HomeAdsRepository $adsRepo)
    {
        parent::__construct($video);
        $this->ads = $adsRepo;

        // $this->adsPerPage = 2;
        $this->pageIncrement = 1;
    }
}
