<?php

namespace App\Repositories;

use App\Model\Video;
use App\Repositories\BaseRepository;
use App\Repositories\Extensions\HasAdsTrait;
use App\Repositories\VideoListAdsRepository;
use Illuminate\Database\Eloquent\Builder;

class VideoRepository extends BaseRepository
{
    use HasAdsTrait;

    public function __construct(Video $video, VideoListAdsRepository $adsRepo)
    {
        parent::__construct($video);
        $this->ads = $adsRepo;

        // $this->adsPerPage = 1;
        // $this->pageIncrement = 0;
    }

    protected function getPlaylistVideos(array $queryList, $key)
    {
        $this->setAdditionalQuery(function (Builder $query) use ($key) {
            return $query->published()
                ->where('playlist_id', $key)
                ->orderBy('featured', 'desc')
                ->orderBy('upload_date', 'desc');
        });
        return $this->getIndex($queryList);
    }
}
