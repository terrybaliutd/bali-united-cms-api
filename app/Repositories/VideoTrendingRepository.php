<?php

namespace App\Repositories;

use App\Model\Video;
use App\Repositories\BaseRepository;

class VideoTrendingRepository extends BaseRepository
{
    public function __construct(Video $video)
    {
        parent::__construct($video);
    }
}
