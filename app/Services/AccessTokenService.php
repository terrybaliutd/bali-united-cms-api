<?php

namespace App\Services;

use Carbon\Carbon;
use App\Model\User;
use App\Model\AccessToken;

class AccessTokenService
{
    public function create(User $user)
    {
        $token = $this->generateToken();
        $refreshToken = hash('md5', $token);
        $accessToken = new AccessToken([
            'token' => $token,
            'refresh_token' => $refreshToken,
            'expired_at' => $this->newExpiredDate()
        ]);
        $accessToken->user()->associate($user);
        $accessToken->save();
        $accessToken->addVisible('refresh_token');

        $this->clearExpiredToken();

        return $accessToken;
    }

    public function findUser($accessToken)
    {
        $accessToken = AccessToken::notExpired()->whereToken($accessToken)->first();
        if ($accessToken == null) {
            return null;
        }

        return $accessToken->user;
    }

    public function renew($refreshToken)
    {
        $token = $this->generateToken();
        $accessToken = AccessToken::notExpired()->where('refresh_token', $refreshToken)->first();

        if ($accessToken == null) {
            return null;
        }
        $accessToken->token = $token;
        $accessToken->expired_at = $this->newExpiredDate();
        $accessToken->save();

        return $accessToken;
    }

    public function setExpired($accessToken)
    {
        $accessToken = AccessToken::notExpired()->whereToken($accessToken)->first();
        $accessToken->expired_at = Carbon::now();
        $accessToken->save();
    }

    public function clearExpiredToken()
    {
        AccessToken::expired()->delete();
    }

    protected function newExpiredDate()
    {
        return Carbon::now()->addDays(60);
    }

    protected function generateToken()
    {
        return hash('md5', uniqid() . time());
    }
}
