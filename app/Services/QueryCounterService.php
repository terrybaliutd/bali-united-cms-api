<?php

namespace App\Services;

use App\Model\QueryCounter;
use App\Services\AccessTokenService;
use Illuminate\Http\Request;

class QueryCounterService
{
    protected $count;

    public function incrementCounter()
    {
        $this->count++;
    }

    public function saveToDatabase(Request $request)
    {
        $data = [
            'url' => request()->url(),
            'count' => $this->count
        ];

        if ($data['count'] <= 10) {
            return null;
        }

        $queryCounter = QueryCounter::where('url', $data['url'])->first();

        $user = \UserAccess::user();
        $data['user_id'] = $user ? $user->id : null;
        $data['access_token'] = $request->access_token;

        if ($queryCounter) {
            if ($queryCounter->count < $data['count']) {
                $queryCounter->update($data);
            }
        } else {
            $newQueryCounter = new QueryCounter;
            $newQueryCounter->fill($data);
            $newQueryCounter->save();
            return $newQueryCounter;
        }

        return $queryCounter;
    }
}
