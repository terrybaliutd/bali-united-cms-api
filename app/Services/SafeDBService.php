<?php

namespace App\Services;

class SafeDBService
{
    /**
     * Determine if the given exception was caused by a deadlock.
     *
     * @param  \Exception  $e
     * @return bool
     */
    protected function causedByDeadlock(Exception $exp)
    {
        $message = $exp->getMessage();
        return Str::contains($message, [
            'Deadlock found when trying to get lock',
            'deadlock detected',
            'The database file is locked',
            'A table in the database is locked',
            'has been chosen as the deadlock victim',
        ]);
    }

    /**
     * Execute a Closure within a transaction.
     *
     * @param  \Closure  $callback
     * @param  int  $attempts
     * @return mixed
     *
     * @throws \Exception|\Throwable
     */
    public function transaction(\Closure $callback, $attempts = 1)
    {
        for ($i = 1; $i <= $attempts; $i++) {
            \DB::beginTransaction();

            try {
                $result = $callback($this);

                \DB::commit();
            } catch (Exception $e) {
                \DB::rollback();

                if ($this->causedByDeadlock($e) && ($i < $attempts)) {
                    continue;
                }

                throw $e;
            } catch (Throwable $e) {
                \DB::rollback();
                throw $e;
            }

            return $result;
        }

        return false;
    }
}
