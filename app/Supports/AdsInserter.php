<?php
namespace App\Supports;

class AdsInserter
{
    public static function getAds($ads, $page, $perPage)
    {
        return $ads->forPage($page%count($ads), $perPage)->first();
    }

    public static function insertAds($ads, $data, $perPage, $delta)
    {
        if ($ads->count() == 0) {
            return $data;
        }
        $page = ceil(count($data)/($perPage-1));
        $ret = $data;
        // $diff = $perPage - 1;
        $idx1 = -1;
        for ($i=0; $i < $page - 1; $i++) {
            $idx2 = $idx1 + 1;
            $idx1 = ($perPage-1) * ($i+1) + $i;
            $ret->splice(ceil(($idx2+$idx1)/2), 0, [static::getAds($ads, $i+$delta, 1)]);
        }
        return $ret;
    }
}
