<?php

namespace App\Supports\Attachment\Downloader;

use App\Supports\Attachment\Youtube;
use App\Supports\Attachment\ResourceNotFoundException;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\BadResponseException;
use Illuminate\Contracts\Filesystem\Filesystem;

class YoutubeDownloader extends UrlDownloader
{
    protected static $imageSize = [
        'maxresdefault',
        'hqdefault',
        'mqdefault',
        'sddefault',
        'default',
        '0',
        '1',
        '2',
        '3'
    ];

    public function download($url)
    {
        $code = $this->youtubeCode($url);

        if ($code === null) {
            throw new ResourceNotFoundException("Youtube code not found `{$url}`");
        }

        foreach (static::$imageSize as $size) {
            try {
                $fileAttachment = parent::download("http://img.youtube.com/vi/{$code}/{$size}.jpg");

                $result = new Youtube(
                    $fileAttachment->getUrlPath(),
                    $fileAttachment->getMimeType(),
                    $fileAttachment->getSize(),
                    $code
                );

                return $result;
            } catch (ResourceNotFoundException $e) {
                continue;
            }
        }
        throw new ResourceNotFoundException("Youtube video is not found `{$code}`");
    }

    public function validate($url)
    {
        return parent::validate($url) &&
               preg_match("/^https?:\/\/(?:www\.)?(?:m\.)?youtu\.be\/[^\?]+\??.*$/", $url) === 1 ||
               preg_match("/^https?:\/\/(?:www\.)?(?:m\.)?youtube\.com\/embed\/[^\?]+\??.*$/", $url) === 1 ||
               preg_match("/^https?:\/\/(?:www\.)?(?:m\.)?youtube\.com\/watch\/?\?.*?v=.+$/", $url) === 1;
    }


    public static function youtubeCode($url)
    {
        if ($pos = strpos($url, 'youtu.be')) {
            $uri = substr(explode('?', $url)[0], $pos + strlen('youtu.be'));
            $matches = [];
            if (preg_match('/\/?([^\/]+)\/?/', $uri, $matches)) {
                if (isset($matches[1])) {
                    return $matches[1];
                }
            }
        }

        if ($pos = strpos($url, 'youtube.com/embed')) {
            $uri = substr(explode('?', $url)[0], $pos + strlen('youtube.com/embed'));
            $matches = [];
            if (preg_match('/\/?([^\/]+)\/?/', $uri, $matches)) {
                if (isset($matches[1])) {
                    return $matches[1];
                }
            }
        }

        if ($pos = strpos($url, '?')) {
            $queryString = [];
            parse_str(substr($url, $pos + 1), $queryString);

            if (isset($queryString['v'])) {
                return $queryString['v'];
            }
        }

        return null;
    }
}
