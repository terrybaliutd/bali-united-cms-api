<?php

namespace App\Supports\Attachment;

class Youtube extends File
{
    protected $code;

    public function __construct($urlPath, $mimeType, $size, $code)
    {
        parent::__construct($urlPath, $mimeType, $size);

        $this->code = $code;
    }

    public function getCode()
    {
        return $this->code;
    }

    public function getEmbedUrl()
    {
        return "https://www.youtube.com/embed/".$this->getCode();
    }

    public function getRealUrl()
    {
        return "https://www.youtube.com/watch?v=".$this->getCode();
    }

    public function toArray()
    {
        $parent = parent::toArray();

        return array_merge(
            $parent,
            [
                'code' => $this->getCode(),
                'embed_url' => $this->getEmbedUrl(),
                'real_url' => $this->getRealUrl()
            ]
        );
    }
}
