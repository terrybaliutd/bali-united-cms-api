<?php

namespace App\Supports;

use App\Model\Setting;
use App\Model\User;
use Illuminate\Database\Eloquent\Model;

abstract class BasePointUpdater
{
    /**
     * Any model contains user activity records,
     * the model should have 'user_id' property / attribute.
     *
     * @var Illuminate\Database\Eloquent\Model
     */
    protected $model;

    /**
     * Define any key to retrieve the transaction
     * type's setting value
     *
     * @var string
     */
    protected $settingKey;

    /**
     * Class Constructor
     *
     * @param Illuminate\Database\Eloquent\Model $model
     */
    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    /**
     * Get transaction type by the given key name
     *
     * @param  string $name
     * @return mixed
     */
    protected function getTransactionType($name)
    {
        $client = new \GuzzleHttp\Client(['base_uri' => env('POINT_API_URL', '')]);
        $baseUrl = 'api/transaction-types';
        $url = $baseUrl;
        $response = $client->get($url, [
            'headers' => [
                'unique_code' => env('UNIQUE_CODE')
            ],
            'query' => [
                'filter_name' => $name
            ]
        ]);

        $stream = $response->getBody();
        $result = json_decode($stream->getContents(), true);
        if (count(array_values($result['result']['data'])) === 0) {
            return null;
        } else {
            $transactionType = array_values($result['result']['data'])[0];
            return $transactionType;
        }
    }

    /**
     * Execute video point updater process
     *
     * @return void
     */
    public function execute()
    {
        $user = User::find($this->model->user_id);
        $setting = Setting::findByName($this->settingKey);
        $transactionType = $this->getTransactionType($setting->value);
        $client = new \GuzzleHttp\Client(['base_uri' => env('POINT_API_URL', '')]);
        if ($transactionType !== null) {
            // Call transaction endpoint in point system
            $response = $client->post('api/transactions', [
                'headers' => [
                    'unique_code' => env('UNIQUE_CODE')
                ],
                'form_params' => [
                    'member_id' => $user->member_id,
                    'transaction_type_id' => $transactionType['id'],
                    'description' => 'Watch Video'
                ]
            ]);
            $stream = $response->getBody();
            $result = json_decode($stream->getContents(), true);
            $transaction = $result['result'];
            $user->update(['points' => $transaction['user']['points']]);
        }
    }
}
