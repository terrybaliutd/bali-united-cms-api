<?php
namespace App\Supports;

use App\Model\User;
use App\Model\City;

class CityProvinceFinder
{
    public static function find(User $user)
    {
        $cityName = $user->city;
        if ($cityName) {
            $city = City::where('name', $cityName)->first();
            if ($city) {
                return [
                    'city_id' => $city->rajaongkir_id,
                    'province_id' =>$city->province->rajaongkir_id
                ];
            }
        }
        return null;
    }
}
