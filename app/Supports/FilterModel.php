<?php
namespace App\Supports;

use App\Model\Translation\TranslateModel;
use Illuminate\Database\Eloquent\Model;

class FilterModel
{
    protected $model;

    protected $joins = [];

    protected static $validOperators = [
        '=',
        '<>',
        '>',
        '>=',
        '<=',
        'LIKE%%',
        'LIKE%',
        'LIKE_%',
        '!NULL',
        'NULL',
        'IN'
    ];

    public function __construct(Model $model)
    {
        $this->model = $model;
        $this->joins = [];
    }

    public function filterQuery($query, $search, $orders, array $scopes = [])
    {
        $this->joins = [];
        $query = $this->orderQuery($query, $orders);

        if (!empty($search)) {
            $query = $this->searchQuery($query, $search);
        }

        $query = $this->filterWhereQuery($query, $scopes);
        return $query;
    }

    public function orderQuery($query, $orders)
    {
        $query->select(["{$this->getModel()->getTable()}.*"]);
        foreach ($orders as $key => $order) {
            $splits = explode('.', $key);
            $qualifiedKey = $this->getModel()->getQualifiedColumn($key);
            if (count($splits) >= 2) {
                $relation = $splits[0];
                $column = $splits[1];
                $related = $this->getModel()->$relation()->getRelated();

                if (!in_array($column, $related->getColumnListing())) {
                    continue;
                }

                if (!isset($this->joins[$relation])) {
                    $query = $this->joinRelatedQuery($query, $relation);
                    $this->joins[$relation] = true;
                }

                $qualifiedKey = $this->getModel()->getQualifiedRelationColumn($relation, $column);
            } else {
                if (!in_array($key, $this->getModel()->getColumnListing())) {
                    continue;
                }
            }
            $query->orderBy($qualifiedKey, $order);
        }
        return $query;
    }

    public function searchQuery($query, $search)
    {
        $fields = $this->getModel()->getSearchField();
        $query->whereNested(function ($nestedQuery) use ($fields, $search, $query) {
            foreach ($fields as $field) {
                $splits = explode('.', $field);
                $field = $this->getModel()->getQualifiedColumn($field);
                if (count($splits) >= 2) {
                    $relation = $splits[0];
                    $column = $splits[1];
                    if (!isset($this->joins[$relation])) {
                        $query = $this->joinRelatedQuery($query, $relation);
                        $this->joins[$relation] = true;
                    }
                    $field = $this->getModel()->getQualifiedRelationColumn($relation, $column);
                }
                $nestedQuery->orWhere($field, 'LIKE', "%$search%");
            }
        });

        return $query;
    }

    protected function joinRelatedQuery($query, $relation)
    {
        $related = $this->getModel()->$relation()->getRelated();
        $foreignKey = $this->getModel()->$relation()->getForeignKey();
        $query->join(
            "{$related->getTable()} as $relation",
            "{$this->getModel()->getTable()}.{$foreignKey}",
            '=',
            "{$relation}.{$related->getKeyName()}",
            "left"
        );

        if ($related instanceof TranslateModel) {
            $transTableAlias = "{$relation}_translate";
            $transForeignField = "{$transTableAlias}.{$related->getTranslateForeignKey()}";
            $query->join(
                "{$related->getTranslateTable()} as {$transTableAlias}",
                "{$relation}.{$related->getKeyName()}",
                '=',
                $transForeignField,
                'left'
            )->whereNested(function ($query) use ($related, $foreignKey, $transTableAlias) {
                $query->where("{$transTableAlias}.lang", \App::getLocale())
                      ->orWhereNull("{$this->getModel()->getTable()}.{$foreignKey}");
            });
        }

        return $query;
    }

    public function filterWhereQuery($query, array $scopes)
    {
        $columnNames = $this->getModel()->getColumnListing();
        foreach ($scopes as $field => $value) {
            if (method_exists($this->getModel(), "scope$field") && is_string($value)) {
                $query->$field($value);
                continue;
            }
            if (!in_array($field, $columnNames)) {
                continue;
            }
            if (!is_array($value)) {
                $value = [$value];
            }
            $query->whereNested(function ($query) use ($field, $value) {
                foreach ($value as $singleValue) {
                    $this->filterSingleWhereQuery($query, $field, $singleValue);
                }
            });
        }
        return $query;
    }

    protected function filterSingleWhereQuery($query, $field, $value)
    {
        if (empty($value)) {
            $query->whereNested(function ($query) use ($field, $value) {
                $query->where($field, $value)->orWhereNull($field);
            });
            return $query;
        }
        $splitValues = explode('|', $value);
        $operator = $splitValues[0];
        if (count($splitValues) == 1 && $this->isValidOperator($operator)) {
            $query->where($field, $value);
            return $query;
        }
        unset($splitValues[0]);
        $value = implode('|', $splitValues);
        $query = $this->resolveDynamicWhereQuery($query, $operator, $field, $value);
        return $query;
    }

    protected function isValidOperator($operator)
    {
        return !in_array($operator, static::$validOperators);
    }

    protected function resolveDynamicWhereQuery($query, $operator, $field, $value)
    {
        switch ($operator) {
            case 'LIKE%%':
                $query->where($field, 'LIKE', "%$value%");
                break;
            case 'LIKE%':
                $query->where($field, 'LIKE', "%$value");
                break;
            case 'LIKE_%':
                $query->where($field, 'LIKE', "$value%");
                break;
            case '!NULL':
                $query->whereNotNull($field);
                break;
            case 'NULL':
                $query->whereNull($field);
                break;
            case 'IN':
                $query->whereIn($field, explode(',', $value));
                break;
            default:
                $query->where($field, $operator, $value);
        }
        return $query;
    }

    public function getModel()
    {
        return $this->model;
    }
}
