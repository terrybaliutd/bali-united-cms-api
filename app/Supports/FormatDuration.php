<?php
namespace App\Supports;

class FormatDuration
{
    public static function convert($interval)
    {
        $interval = new \DateInterval($interval);
        $result = "";
        if ($interval->y) {
            $result .= $interval->format("%Y years ");
        }
        if ($interval->m) {
            $result .= $interval->format("%M months ");
        }
        if ($interval->d) {
            $result .= $interval->format("%D days ");
        }
        if ($interval->h) {
            $result .= $interval->format("%H:");
        }
        if ($interval->i) {
            $result .= $interval->format("%I:");
        } else {
            $result .= $interval->format("00:");
        }
        if ($interval->s) {
            $result .= $interval->format("%S");
        } else {
            $result .= $interval->format("00");
        }

        return $result;
    }
}
