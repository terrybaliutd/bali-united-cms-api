<?php

namespace App\Supports;

class ImageRotator
{
    public static function fixOrientation($image)
    {
        if ($image->getMimeType() != 'image/jpeg') {
            return false;
        }

        $imagickObject = new \Imagick($image->getPathname());

        try {
            $orientation = $imagickObject->getImageOrientation();

            switch ($orientation) {
                case \Imagick::ORIENTATION_BOTTOMRIGHT:
                    $imagickObject->rotateimage("#000", 180); // rotate 180 degrees
                    break;

                case \Imagick::ORIENTATION_RIGHTTOP:
                    $imagickObject->rotateimage("#000", 90); // rotate 90 degrees CW
                    break;

                case \Imagick::ORIENTATION_LEFTBOTTOM:
                    $imagickObject->rotateimage("#000", -90); // rotate 90 degrees CCW
                    break;
            }

            $imagickObject->setImageOrientation(\Imagick::ORIENTATION_TOPLEFT);
            $imagickObject->writeImage($image->getPathname());
        } catch (Exception $e) {
            return false;
        }
    }
}
