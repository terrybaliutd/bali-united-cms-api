<?php

namespace App\Supports\Pagination;

use App\Repositories\BaseAdsRepository;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Collection;

class AdsPaginator extends LengthAwarePaginator
{
    /**
     * All available Ads
     *
     * @var Illuminate\Support\Collection
     */
    private $ads;

    /**
     * Count of all available ads
     *
     * @var integer
     */
    private $adsCount;

    /**
     * The number of Ads that would be inserted into the paginator
     *
     * @var integer
     */
    private $adsPerPage;

    /**
     * Base records per page
     *
     * @var integer
     */
    private $basePerPage;

    /**
     * Base total records count
     *
     * @var integer
     */
    private $baseTotal;

    /**
     * Ads page increment
     * This variable would be used only for the home ads
     *
     * @var integer
     */
    private $pageIncrement;

    /**
     * Class constructor
     *
     * @param BaseAdsRepository $repo
     * @param Collection        $items
     * @param integer           $total
     * @param integer           $perPage
     * @param integer           $currentPage
     * @param array             $options
     */
    public function __construct($repo, $items, $total, $perPage, $currentPage = null, array $options = [])
    {
        foreach ($options as $key => $value) {
            $this->{$key} = $value;
        }

        $this->ads = $repo->getAds();
        $this->adsCount = $this->ads->count();
        $this->basePerPage = $perPage;
        $this->perPage = $perPage + $this->adsPerPage;
        $this->total = $this->calculateTotalItems($total);

        $this->lastPage = (int) ceil($this->total / $this->perPage);
        $this->path = $this->path != '/' ? rtrim($this->path, '/') : $this->path;

        $this->currentPage = $this->setCurrentPage($currentPage, $this->lastPage);
        $this->items = $this->manipulateItems($items);
    }

    /**
     * Calculate total items count
     * after being inserted with Ads
     *
     * @param  integer $baseTotal
     * @return integer
     */
    private function calculateTotalItems($baseTotal)
    {
        $this->baseTotal = $baseTotal;

        if ($this->adsCount == 0) {
            return $baseTotal;
        }
        $pages = (int) ceil($baseTotal / $this->basePerPage);

        return $baseTotal + ($this->adsPerPage * $pages);
    }

    /**
     * Insert the Ads into the item list
     *
     * @param  Collection $items
     * @return Collection
     */
    private function manipulateItems(Collection $items)
    {
        if (($this->adsCount == 0) || $items->isEmpty()) {
            return $items;
        }
        $divisor = round($items->count() / ($this->adsPerPage + 1));

        for ($i = 1; $i <= $this->adsPerPage; $i++) {
            $index = ($divisor * $i) + ($i - 1);
            $page = $this->currentPage + $this->pageIncrement;
            $items->splice($index, 0, $this->getCurrentAds($page, $i-1));
        }

        return $items;
    }

    /**
     * Get current Ads
     *
     * @param  integer $page
     * @param  integer $index
     * @return Collection
     */
    private function getCurrentAds($page, $index)
    {
        $index = ($this->adsPerPage * $page) - ($this->adsPerPage - $index);
        $index = $index % $this->ads->count();

        return [$this->ads[$index]];
    }
}
