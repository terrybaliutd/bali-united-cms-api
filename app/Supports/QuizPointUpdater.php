<?php

namespace App\Supports;

use App\Model\Quiz;
use App\Model\User;

class QuizPointUpdater
{
    /**
     * Quiz Model
     *
     * @var App\Model\Quiz
     */
    protected $quiz;

    /**
     * User Model
     *
     * @var App\Model\User
     */
    protected $user;

    /**
     * Class Constructor
     *
     * @param App\Model\Quiz $quiz
     * @param App\Model\User $user
     */
    public function __construct(Quiz $quiz, User $user)
    {
        $this->quiz = $quiz;
        $this->user = $user;
    }

    /**
     * Execute quiz point updater process
     *
     * @return void
     */
    public function execute()
    {
        $client = new \GuzzleHttp\Client(['base_uri' => env('POINT_API_URL', '')]);

        // Call transaction endpoint in point system
        $response = $client->post('api/transactions', [
            'headers' => [
                'unique_code' => env('UNIQUE_CODE')
            ],
            'form_params' => [
                'member_id' => $this->user->member_id,
                'transaction_type_id' => $this->quiz->transaction_type_id,
                'description' => 'Answer quiz'
            ]
        ]);
        $stream = $response->getBody();
        $result = json_decode($stream->getContents(), true);
        $transaction = $result['result'];
        $this->user->update(['points' => $transaction['user']['points']]);
    }
}
