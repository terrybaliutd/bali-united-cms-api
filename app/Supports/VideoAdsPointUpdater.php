<?php

namespace App\Supports;

use App\Model\Setting;
use App\Model\User;
use App\Model\VideoAdsUser;

class VideoAdsPointUpdater extends BasePointUpdater
{
    /**
     * Class Constructor
     *
     * @param \App\Model\VideoAdsUser $model
     */
    public function __construct(VideoAdsUser $model)
    {
        parent::__construct($model);

        $this->settingKey = 'watch-ads-transaction-type';
    }
}
