<?php

namespace App\Supports;

use App\Model\Setting;
use App\Model\User;
use App\Model\VideoWatch;

class VideoPointUpdater extends BasePointUpdater
{
    /**
     * Class Constructor
     *
     * @param \App\Model\VideoWatch $model
     */
    public function __construct(VideoWatch $model)
    {
        parent::__construct($model);

        $this->settingKey = 'watch-transaction-type';
    }
}
