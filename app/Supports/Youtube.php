<?php

namespace App\Supports;

use Dawson\Youtube\Youtube as BaseYoutube;

class Youtube extends BaseYoutube
{
    public function __construct($development = false)
    {
        parent::__construct($development);
    }

    public function insertPlaylistItem($data)
    {
        $this->handleAccessToken();
        try {
            $resourceId = new \Google_Service_YouTube_ResourceId();
            $resourceId->setKind('youtube#video');
            if (array_key_exists('videoId', $data)) {
                $resourceId->setVideoId($data['videoId']);
            }

            $playlistItemSnippet = new \Google_Service_YouTube_PlaylistItemSnippet();
            if (array_key_exists('playlistId', $data)) {
                $playlistItemSnippet->setPlaylistId($data['playlistId']);
            }
            $playlistItemSnippet->setResourceId($resourceId);

            $playlistItem = new \Google_Service_YouTube_PlaylistItem();
            $playlistItem->setSnippet($playlistItemSnippet);

            $response = $this->youtube->playlistItems->insert('snippet', $playlistItem, []);

            return $response;
        } catch (\Google_Service_Exception $e) {
            \Log::error($e->getMessage());
        } catch (\Google_Exception $e) {
            \Log::error($e->getMessage());
        }
    }

    private function handleAccessToken()
    {
        $accessToken = $this->client->getAccessToken();

        if (!($accessToken)) {
            throw new \Exception('An access token is required');
        }

        if ($this->client->isAccessTokenExpired()) {
            $accessToken = json_decode($accessToken);
            $refreshToken = $accessToken->refresh_token;
            $this->client->refreshToken($refreshToken);
            $newAccessToken = $this->client->getAccessToken();
            $this->saveAccessTokenToDB($newAccessToken);
        }
    }
}
