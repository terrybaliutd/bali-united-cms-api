<?php

return [

    /*
    |--------------------------------------------------------------------------
    | List of routes which would be excluded using any http cookies
    |--------------------------------------------------------------------------
    |
    | Define your routes list here
    |
    */

    'excluded_routes' => [
        'api/*'
    ]
];
