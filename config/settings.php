<?php

return [
    'site-name' => [
        'title' => 'Site Name',
        'description' => 'Website Name',
        'type' => 'suitText',
        'value' => 'Suitcms'
    ],
    'site-description' => [
        'title' => 'Site Description',
        'Description' => 'Website description',
        'type' => 'suitTextarea',
        'value' => ''
    ],
    'site-image' => [
        'title' => 'Site Image',
        'Description' => 'Website image for social media',
        'type' => 'suitFileBrowser',
        'value' => ''
    ],
    'logo-url' => [
        'title' => 'Admin Logo Url',
        'type' => 'suitFileBrowser',
        'value' => 'assets/admin/img/logo.png'
    ],
    'social-facebook-username' => [
        'title' => 'Facebook username',
        'type' => 'suitText',
        'value' => ''
    ],
    'social-twitter-username' => [
        'title' => 'Twitter username',
        'type' => 'suitText',
        'value' => ''
    ],
    'citizen-journalism-playlist' => [
        'title' => 'Playlist key',
        'type' => 'suitText',
        'value' => ''
    ],
    'default-banner-image' => [
        'title' => 'Default Banner',
        'Description' => 'Default Banner for Mobile Application',
        'type' => 'suitFileBrowser',
        'value' => 'files/img_BU_banner2.jpg'
    ],
    'watch-transaction-type' => [
        'title' => 'Transaction Type Name for Watching Video',
        'description' => 'Default transaction type name for watching video',
        'type' => 'suitSelect',
        'value' => ''
    ],
    'watch-ads-transaction-type' => [
        'title' => 'Transaction Type Name for Watching Video Ads',
        'description' => 'Default transaction type name for watching video ads',
        'type' => 'suitSelect',
        'value' => ''
    ],
    'reward-shipping-origin-id' => [
        'title' => 'The city of origin that would be used for shipping any rewards',
        'type' => 'suitText',
        'value' => '1764'
    ],
    'reward-shipping-default-courier' => [
        'title' => 'The default courier which would be used for any merchandise shipment.',
        'type' => 'suitText',
        'value' => 'jne'
    ],
    'reward-shipping-origin-type' => [
        'title' => 'The origin type for shipping, the options are: city, subdistrict',
        'type' => 'suitText',
        'value' => 'subdistrict'
    ],
];
