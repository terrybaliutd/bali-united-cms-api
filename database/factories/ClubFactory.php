<?php

$factory->define(\App\Model\Club::class, function (\Faker\Generator $faker) {
    $shortName = $faker->numerify('Club-##');
    return [
        'name' => $shortName,
        'official_name' => $shortName,
        'abbreviation' => $faker->regexify('[A-Z]{3}'),
        'established_at' => $faker->date(),
        'home_base' => $faker->address,
        'manager' => $faker->name,
        'supporter_nick_name' => $faker->sentence(2),
        'stadium_name' => $faker->sentence(3),
        'published' => true,
    ];
});
