<?php

$factory->define(App\Model\Competition::class, function ($faker) {
    return [
        'name' => $faker->words(4, true),
        'long_name' => $faker->words(7, true),
        'order' => $faker->randomDigit,
        'is_default' => false,
        'published' => true,
    ];
});
