<?php

$factory->define(App\Model\MatchClubInfo::class, function ($faker) {
    return [
        'club_id' => function () {
            factory(App\Model\Club::class)->create()->id;
        },
        'match_id' => 12,
        'coach' => $faker->name(),
        'type' => 'Home',
        'formation' => '4-4-2',
        'score' => $faker->randomNumber(1),
    ];
});
