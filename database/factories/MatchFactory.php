<?php

$factory->define(App\Model\Match::class, function ($faker) {
    return [
        'competition_id' => function () {
            return factory(App\Model\Competition::class)->create()->id;
        },
        'season_id' => function () {
            return factory(App\Model\Season::class)->create()->id;
        },
        'start_at' => $faker->dateTime(),
        'venue' => $faker->name(),
        'tv_info' => $faker->paragraph(),
        'published' => $faker->dateTime(),
        'status' => $faker->randomElement($array = array('Schedule', 'Playing', 'Finished', 'Postponed', 'Suspended')),
        'prediction_home_win' => $faker->numberBetween(1, 100),
        'prediction_away_win' => $faker->numberBetween(1, 100),
        'prediction_draw' => $faker->numberBetween(1, 100),
        'referees' => $faker->name(),
        'preview_match_text' => $faker->paragraph(),
        'post_match_text' => $faker->paragraph(),
        'audience' => $faker->numberBetween(1, 1000),
        'live_streaming_url' => $faker->imageUrl(),
    ];
});