<?php

$factory->define(App\Model\MatchHighlight::class, function ($faker) {
    return [
        'match_id' => function () {
            factory(App\Model\Match::class)->create()->id;
        },
        'player_name' => $faker->name(),
        'support_player_name' => $faker->name(4),
        'time_in_second' => $faker->numberBetween(0, 59),
        'time_in_minute' => $faker->numberBetween(0, 89),
        'event_description' => $faker->randomElement($array = ['goal', 'assist', 'own_goal', 'red_card', 'yellow_card', 'substitution', 'penalty_goal', 'penalty_missed']),
        'stage_name' => $faker->name(),
        'club_type' => $faker->randomElement($array = ['home', 'away'])
    ];
});
