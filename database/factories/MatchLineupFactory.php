<?php

$factory->define(App\Model\MatchLineUp::class, function ($faker) {
    $firstName = $this->faker->firstName();
    $familyName = $this->faker->lastName();
    return [
        'match_club_info_id' => function () {
            factory(\App\Model\MatchClubInfo::class)->create()->id;
        },
        'player_name' => "$firstName $familyName",
        'position_code' => $this->faker->randomElement(array_keys(App\Model\Player::POSISTION_CODES)),
        'shirt_number' => $this->faker->randomNumber(2),
        'is_primary' => true,
    ];
});
