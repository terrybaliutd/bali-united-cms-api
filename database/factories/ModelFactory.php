<?php
/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

require_once 'CompetitionFactory.php';
require_once 'ClubFactory.php';
require_once 'MatchFactory.php';
require_once 'MatchHighlightFactory.php';
require_once 'MatchLineupFactory.php';
require_once 'MatchStatisticFactory.php';
require_once 'NewsFactory.php';
require_once 'PlayerFactory.php';
require_once 'QuizFactory.php';
require_once 'SeasonFactory.php';
require_once 'StickerFactory.php';
require_once 'StandingFactory.php';
