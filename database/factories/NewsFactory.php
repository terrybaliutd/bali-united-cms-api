<?php

$factory->define(App\Model\News::class, function ($faker) {
    return [
        'title' => $faker->sentence($faker->numberBetween(4, 10)),
        'description' => $faker->paragraph(),
        'content' => implode('<br>', $faker->paragraphs(3)),
        'published' => true,
        'featured' => $faker->boolean(10),
        'news_date' => $faker->dateTime(),
        'tags' => implode(',', $faker->words(4))
    ];
});
