<?php

$factory->define(App\Model\Player::class, function ($faker) {
    $firstName = $this->faker->firstName();
    $familyName = $this->faker->lastName();
    return [
        'name' => "$firstName $familyName",
        'short_name' => "$firstName[0] $familyName",
        'position_code' => $this->faker->randomElement(array_keys(App\Model\Player::POSISTION_CODES)),
        'shirt_number' => $this->faker->randomNumber(2),
        'published' => true,
        'birth_place' => $this->faker->city(),
        'birth_date' => $this->faker->date(),
        'weight' => $this->faker->randomNumber(2),
        'height' => $this->faker->numberBetween(160, 200),
        'nationality' => $this->faker->randomElement(array_keys(trans('site.nationality')))
    ];
});
