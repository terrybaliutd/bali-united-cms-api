<?php

$factory->define(App\Model\Quiz::class, function ($faker) {
    return [
        'video_id' => function () {
            factory(\App\Model\Video::class)->create()->id;
        },
        'title' => $faker->sentence(2),
        'description' => $faker->paragraph(3),
        'question' => $faker->paragraph(3),
        'option_1' => $faker->word,
        'option_2' => $faker->word,
        'option_3' => $faker->word,
        'option_4' => $faker->word,
        'answer' => $faker->numberBetween(1, 4),
        'points' => 10,
        'published' => true,
    ];
});
