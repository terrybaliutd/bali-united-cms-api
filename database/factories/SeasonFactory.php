<?php

$factory->define(App\Model\Season::class, function ($faker) {
    return [
        'name' => $faker->words(4, true),
        'order' => $faker->randomDigit,
        'is_default' => false,
        'published' => true,
    ];
});