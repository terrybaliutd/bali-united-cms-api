<?php

$factory->define(App\Model\Standing::class, function ($faker) {
    return [
        'competition_id' => function () {
            return factory(App\Model\Competition::class)->create()->id;
        },
        'season_id' => function () {
            return factory(App\Model\Season::class)->create()->id;
        },
        'club_id' => function () {
            return factory(App\Model\Club::class)->create()->id;
        },
        'position' => $this->faker->numberBetween(1, 20),
        'matches' => $this->faker->numberBetween(1, 20),
        'goal' => $this->faker->numberBetween(1, 20),
        'goal_conceded' => $this->faker->numberBetween(1, 20),
        'goal_diff' => $this->faker->numberBetween(1, 20),
        'win' => $this->faker->numberBetween(1, 20),
        'lose' => $this->faker->numberBetween(1, 20),
        'draw' => $this->faker->numberBetween(1, 20),
        'point' => $this->faker->numberBetween(1, 20),
        'priority' => $this->faker->numberBetween(1, 20),
        'published' => true
    ];
});
