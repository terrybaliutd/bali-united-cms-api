<?php

$factory->define(App\Model\StickerCategory::class, function ($faker) {
    return [
        'title' => $faker->words(4, true),
        'published' => true,
    ];
});

$factory->define(\App\Model\StickerItem::class, function (\Faker\Generator $faker) {
    return [
        'sticker_category_id' => function () {
            factory(\App\Model\StickerCategory::class)->create()->id;
        },
        'title' => $faker->words(4, true),
        // 'image' => asset('assets/admin/img/logo-big.png')
        // 'image' => $faker->imageUrl()
    ];
});
