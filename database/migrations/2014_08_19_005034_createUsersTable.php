<?php

use App\Model\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateUsersTable extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->tinyInteger('group_type')->index()->default(User::USER);
            $table->string('username')->nullable();
            $table->string('email')->unique();
            $table->string('password', 63);
            $table->string('slug')->nullable()->index();

            $table->string('address')->nullable();
            $table->string('city')->nullable();
            $table->string('province')->nullable();
            $table->integer('zip_code')->nullable();
            $table->string('phone_number')->nullable();

            $table->string('name')->default('Anonymous');
            $table->text('about')->nullable();
            $table->dateTime('date_of_birth')->nullable();

            $table->string('attachment')->nullable();
            $table->tinyInteger('attachment_type')->default(0);
            $table->text('attachment_info')->default('');

            $table->boolean('active')->default(true);
            $table->integer('log_in_count')->default(0);
            $table->dateTime('current_log_in_at')->nullable();
            $table->dateTime('last_log_in_at')->nullable();
            $table->string('current_log_in_ip')->default('');
            $table->string('last_log_in_ip')->default('');

            $table->string('remember_token')->default('');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::dropIfExists('users');
    }
}
