<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClubTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clubs', function (Blueprint $table) {
            $table->increments('id');
            $table->string('api_id')->nullable()->index();
            $table->string('name', 200);
            $table->string('nick_name', 100)->default('');
            $table->string('official_name', 200);
            $table->string('abbreviation', 5);

            $table->date('established_at')->nullable();
            $table->string('home_base', 200)->default('');
            $table->string('manager', 200)->default('');
            $table->string('supporter_nick_name', 200)->default('');
            $table->string('stadium_name', 200)->default('');

            $table->string('logo_url', 500)->nullable();
            $table->tinyInteger('logo_url_type')->default(0);
            $table->text('logo_url_info')->default('');

            $table->string('slug')->unique();
            $table->dateTime('published_at')->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('clubs');
    }
}
