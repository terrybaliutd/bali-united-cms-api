<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlayerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('players', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('api_id')->nullable()->index();
            $table->string('name', 200);
            $table->string('short_name', 100);
            $table->string('position_code', 5);
            $table->string('shirt_number', 5);
            $table->string('birth_place')->default('');
            $table->string('birth_date')->default('');
            $table->integer('weight')->unsigned()->nullable();
            $table->integer('height')->unsigned()->nullable();
            $table->string('nationality', 5)->default('');

            $table->string('photo_profile', 500)->nullable();
            $table->tinyInteger('photo_profile_type')->default(0);
            $table->text('photo_profile_info')->default('');

            $table->string('photo_action', 500)->nullable();
            $table->tinyInteger('photo_action_type')->default(0);
            $table->text('photo_action_info')->default('');

            $table->string('slug')->unique();
            $table->dateTime('published_at')->nullable();
            $table->nullableTimestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('players');
    }
}
