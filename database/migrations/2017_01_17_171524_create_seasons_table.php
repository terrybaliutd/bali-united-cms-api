<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSeasonsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('seasons', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name');
            $table->integer('order')->default(0);
            $table->boolean('is_default')->default(false);
            
            $table->string('slug')->unique();
            $table->dateTime('published_at')->nullable();
            $table->nullableTimestamps();
        });

        \Schema::create('competition_season', function (Blueprint $table) {
            $table->bigInteger('competition_id')->unsigned();
            $table->bigInteger('season_id')->unsigned();

            $table->primary(['competition_id', 'season_id']);
            $table->foreign('competition_id')
                  ->references('id')->on('competitions')
                  ->onDelete('cascade');
            $table->foreign('season_id')
                  ->references('id')->on('seasons')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('competition_season');
        Schema::drop('seasons');
    }
}
