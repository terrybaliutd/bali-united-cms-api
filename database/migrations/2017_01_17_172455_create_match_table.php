<?php

use App\Model\Match;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateMatchTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('matches', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('competition_id')->unsigned();
            $table->bigInteger('season_id')->unsigned()->nullable();
            $table->string('api_id')->nullable()->index();
            $table->datetime('start_at');
            $table->string('status')->default(Match::STATUS_SCHEDULE)->index();
            $table->string('venue');
            $table->integer('prediction_home_win');
            $table->integer('prediction_draw');
            $table->integer('prediction_away_win');
            $table->string('referees');
            $table->text('preview_match_text');
            $table->text('post_match_text');
            $table->integer('audience');
            $table->text('tv_info');
            $table->string('live_streaming_url');
            $table->dateTime('published_at')->nullable();
            $table->nullableTimestamps();

            $table->string('banner')->nullable();
            $table->tinyInteger('banner_type')->default(0)->nullable();
            $table->text('banner_info')->default('')->nullable();

            $table->foreign('competition_id')->references('id')->on('competitions')->onDelete('cascade');
            $table->foreign('season_id')->references('id')->on('seasons')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('matches');
    }
}
