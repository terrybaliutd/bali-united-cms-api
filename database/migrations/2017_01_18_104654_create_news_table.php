<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('match_id')->nullable();
            $table->string('slug')->unique();
            $table->dateTime('news_date');
            $table->dateTime('published_at')->nullable();

            $table->boolean('featured')->default(false);
            $table->string('title', 200)->nullable();
            $table->string('description', 500)->nullable();
            $table->mediumText('content')->nullable();

            $table->string('image', 500)->nullable();
            $table->tinyInteger('image_type')->default(0);
            $table->text('image_info')->default('');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('news');
    }
}
