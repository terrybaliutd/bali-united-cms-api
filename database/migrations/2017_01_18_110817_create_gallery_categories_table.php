<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGalleryCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gallery_categories', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('parent_id')->unsigned()->nullable();
            $table->string('title');
            $table->string('slug');
            $table->dateTime('published_at')->nullable();
            $table->string('image');
            $table->tinyInteger('image_type')->default(0);
            $table->text('image_info')->default('');
            
            $table->timestamps();

            $table->foreign('parent_id')
                  ->references('id')->on('gallery_categories')
                  ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gallery_categories');
    }
}
