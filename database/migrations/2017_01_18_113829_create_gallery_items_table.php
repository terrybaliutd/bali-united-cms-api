<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGalleryItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('gallery_items', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('gallery_category_id')->unsigned()->nullable();
            $table->string('title');
            $table->string('type');
            $table->boolean('featured')->default(false);
            $table->string('slug')->unique();

            $table->string('attachment');
            $table->tinyInteger('attachment_type')->default(0);
            $table->text('attachment_info')->default('');

            $table->dateTime('published_at')->nullable();
            $table->timestamps();

            $table->foreign('gallery_category_id')
                  ->references('id')->on('gallery_categories')
                  ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('gallery_items');
    }
}
