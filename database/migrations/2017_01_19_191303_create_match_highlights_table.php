<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchHighlightsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('match_highlights', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('api_id')->index();
            $table->bigInteger('match_id')->unsigned();
            $table->string('player_name')->nullable();
            $table->string('support_player_name')->nullable();
            $table->integer('time_in_second')->default(0);
            $table->integer('time_in_minute')->default(0);
            $table->string('event_description')->index(); // ex: goal, yellow_card
            $table->string('stage_name'); // 1: first half, 2: second half, 3: penalty, 4: first extra time, 5: second extra time, ...
            $table->nullableTimestamps();

            $table->foreign('match_id')
                  ->references('id')->on('matches')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('match_highlights');
    }
}
