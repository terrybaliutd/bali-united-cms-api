<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchClubInfosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('match_club_infos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('match_id')->unsigned();
            $table->integer('club_id')->unsigned();
            $table->string('type', 10);
            $table->string('coach');
            $table->string('formation', 10);
            $table->integer('score');
            $table->tinyInteger('is_default');
            $table->timestamps();

            $table->foreign('match_id')
                  ->references('id')->on('matches')
                  ->onDelete('cascade');
            $table->foreign('club_id')
                  ->references('id')->on('clubs')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('match_club_infos');
    }
}
