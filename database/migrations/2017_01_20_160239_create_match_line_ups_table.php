<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMatchLineUpsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('match_line_ups', function (Blueprint $table) {
            $table->string('id');
            $table->bigInteger('match_club_info_id')->unsigned();
            $table->string('player_name');
            $table->string('position_code', 5);
            $table->string('shirt_number', 5);
            $table->boolean('is_primary')->default(true);

            $table->primary(['id']);
            $table->foreign('match_club_info_id')
                  ->references('id')->on('match_club_infos')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('match_line_ups');
    }
}
