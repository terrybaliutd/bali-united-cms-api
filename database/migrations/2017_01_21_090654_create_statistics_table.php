<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStatisticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('match_statistics', function (Blueprint $table) {
            $table->string('id');
            $table->bigInteger('match_club_info_id')->unsigned();
            $table->integer('assist')->default(0);
            $table->integer('block_cross')->default(0);
            $table->integer('block')->default(0);
            $table->integer('body_charge')->default(0);
            $table->integer('clearance')->default(0);
            $table->integer('corner')->default(0);
            $table->integer('create_chance')->default(0);
            $table->integer('cross_blocked')->default(0);
            $table->integer('cross_failed')->default(0);
            $table->integer('cross_success')->default(0);
            $table->integer('dribble_failed')->default(0);
            $table->integer('dribble_success')->default(0);
            $table->integer('foul')->default(0);
            $table->integer('fouled')->default(0);
            $table->integer('free_kick')->default(0);
            $table->integer('goal')->default(0);
            $table->integer('handball')->default(0);
            $table->integer('header_failed')->default(0);
            $table->integer('header_success')->default(0);
            $table->integer('intercept_failed')->default(0);
            $table->integer('intercept_success')->default(0);
            $table->integer('loose_ball')->default(0);
            $table->integer('matches')->default(0);
            $table->integer('offside')->default(0);
            $table->integer('pass_failed')->default(0);
            $table->integer('pass_success')->default(0);
            $table->integer('red_card')->default(0);
            $table->integer('yellow_card')->default(0);
            $table->integer('save')->default(0);
            $table->integer('shoot_blocked')->default(0);
            $table->integer('shoot_off_target')->default(0);
            $table->integer('shoot_on_target')->default(0);
            $table->integer('substitution')->default(0);
            $table->integer('tackle_failed')->default(0);
            $table->integer('tackle_success')->default(0);
            $table->integer('throw_in')->default(0);
            $table->integer('penalty_goal')->default(0);
            $table->integer('penalty_missed')->default(0);
            $table->integer('minute_of_play')->default(0);
            $table->integer('own_goal')->default(0);
            $table->integer('ball_possession')->default(0);
            $table->timestamps();

            $table->primary('id');
            $table->foreign('match_club_info_id')
                  ->references('id')->on('match_club_infos')
                  ->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('match_statistics');
    }
}
