<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStandingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('standings', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('club_id')->unsigned();
            $table->bigInteger('competition_id')->unsigned()->nullable();
            $table->bigInteger('season_id')->unsigned()->nullable();
            $table->integer('position')->nullable();
            $table->integer('matches')->unsigned()->default(0);
            $table->integer('goal')->unsigned()->default(0);
            $table->integer('goal_conceded')->unsigned()->default(0);
            $table->integer('goal_diff')->default(0);
            $table->integer('win')->unsigned()->default(0);
            $table->integer('lose')->unsigned()->default(0);
            $table->integer('draw')->unsigned()->default(0);
            $table->integer('point')->unsigned()->default(0);
            $table->integer('priority')->unsigned()->default(99);
            
            $table->dateTime('published_at')->nullable();
            $table->nullableTimestamps();

            $table->foreign('club_id')
                  ->references('id')->on('clubs')
                  ->onDelete('cascade');
            $table->foreign('competition_id')
                  ->references('id')->on('competitions')
                  ->onDelete('set null');
            $table->foreign('season_id')
                  ->references('id')->on('seasons')
                  ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('standings');
    }
}
