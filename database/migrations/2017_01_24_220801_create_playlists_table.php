<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePlaylistsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('playlists', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('key')->unique()->nullable();
            $table->string('title');
            $table->string('description')->default('');
            $table->dateTime('published_at')->nullable();

            $table->string('attachment');
            $table->tinyInteger('attachment_type')->default(0);
            $table->text('attachment_info')->default('');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('playlists');
    }
}
