<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('videos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('publisher_id')->unsigned();
            $table->bigInteger('match_id')->unsigned()->nullable()->default(null);
            $table->string('key')->unique();
            $table->string('description')->default('');
            $table->bigInteger('playlist_id')->unsigned()->nullable()->default(null);
            $table->string('title');
            $table->string('duration');
            $table->boolean('featured')->default(false);

            $table->string('attachment')->nullable();
            $table->tinyInteger('attachment_type')->default(0)->nullable();
            $table->text('attachment_info')->default('')->nullable();

            $table->dateTime('published_at')->nullable();
            $table->timestamps();

            $table->foreign('match_id')
                  ->references('id')->on('matches')
                  ->onDelete('SET NULL');
            $table->foreign('playlist_id')
                  ->references('id')->on('playlists')
                  ->onDelete('CASCADE');
            $table->foreign('publisher_id')
                  ->references('id')->on('users')
                  ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('videos');
    }
}
