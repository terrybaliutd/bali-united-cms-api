<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuizTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('quizzes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('video_id')->unsigned();
            $table->string('title');
            $table->text('description');
            $table->text('question');
            $table->string('option_1');
            $table->string('option_2');
            $table->string('option_3');
            $table->string('option_4')->default('');
            $table->tinyInteger('answer');
            $table->integer('points');
            $table->dateTime('published_at')->nullable();
            $table->timestamps();

            $table->foreign('video_id')
                  ->references('id')->on('videos')
                  ->onDelete('CASCADE');
        });

        \Schema::create('quiz_users', function (Blueprint $table) {
            $table->bigInteger('quiz_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->tinyInteger('answer');
            $table->timestamps();

            $table->foreign('quiz_id')
                  ->references('id')->on('quizzes')
                  ->onDelete('CASCADE');
            $table->foreign('user_id')
                  ->references('id')->on('users')
                  ->onDelete('CASCADE');

            $table->primary(['quiz_id', 'user_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('quiz_users');
        Schema::drop('quizzes');
    }
}
