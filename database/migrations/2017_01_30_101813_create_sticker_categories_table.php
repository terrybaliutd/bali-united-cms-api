<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStickerCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sticker_categories', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');

            $table->string('image');
            $table->tinyInteger('image_type')->default(0);
            $table->text('image_info')->default('');

            $table->string('slug');
            $table->dateTime('expired_date');
            $table->dateTime('published_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sticker_categories');
    }
}
