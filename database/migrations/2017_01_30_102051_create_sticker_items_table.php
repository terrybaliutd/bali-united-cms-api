<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStickerItemsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sticker_items', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('sticker_category_id')->unsigned();
            $table->string('title');

            $table->string('image');
            $table->tinyInteger('image_type')->default(0);
            $table->text('image_info')->default('');

            $table->timestamps();

            $table->foreign('sticker_category_id')
                  ->references('id')->on('sticker_categories')
                  ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('sticker_items');
    }
}
