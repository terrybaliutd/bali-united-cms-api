<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('comments', function (Blueprint $table) {
            $table->string('id');
            $table->bigInteger('video_id')->unsigned();
            $table->integer('user_id')->unsigned();
            $table->string('type');
            $table->integer('sticker_item_id')->nullable()->unsigned();
            $table->text('message')->nullable();
            
            $table->timestamps();

            $table->foreign('video_id')
                  ->references('id')->on('videos')
                  ->onDelete('CASCADE');
            $table->foreign('user_id')
                  ->references('id')->on('users')
                  ->onDelete('CASCADE');
            $table->foreign('sticker_item_id')
                  ->references('id')->on('sticker_items')
                  ->onDelete('CASCADE');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('comments');
    }
}
