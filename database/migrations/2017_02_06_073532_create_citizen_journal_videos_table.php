<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCitizenJournalVideosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('citizen_journal_videos', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('user_id')->unsigned();
            $table->string('title');
            $table->text('description');
            $table->string('status', 10)->default('uploaded');

            $table->string('video');
            $table->tinyInteger('video_type')->default(0);
            $table->text('video_info')->default('');

            $table->string('converted_video')->nullable();
            $table->tinyInteger('converted_video_type')->default(0)->nullable();
            $table->text('converted_video_info')->default('')->nullable();

            $table->bigInteger('video_id')->unsigned()->nullable();

            $table->timestamps();

            $table->foreign('user_id')
                  ->references('id')->on('users')
                  ->onDelete('CASCADE');
            $table->foreign('video_id')
                  ->references('id')->on('videos')
                  ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('citizen_journal_videos');
    }
}
