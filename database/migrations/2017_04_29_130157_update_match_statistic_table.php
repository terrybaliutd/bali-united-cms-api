<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateMatchStatisticTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('match_statistics', function (Blueprint $table) {
            $table->integer('total_shots')->default(0);
            $table->integer('shot_on_goal')->default(0);
            $table->integer('passing_accuracy')->default(0);
            $table->dropColumn('assist');
            $table->dropColumn('block_cross');
            $table->dropColumn('block');
            $table->dropColumn('body_charge');
            $table->dropColumn('clearance');
            $table->dropColumn('create_chance');
            $table->dropColumn('cross_blocked');
            $table->dropColumn('cross_failed');
            $table->dropColumn('cross_success');
            $table->dropColumn('dribble_failed');
            $table->dropColumn('dribble_success');
            $table->dropColumn('fouled');
            $table->dropColumn('free_kick');
            $table->dropColumn('goal');
            $table->dropColumn('handball');
            $table->dropColumn('header_failed');
            $table->dropColumn('header_success');
            $table->dropColumn('intercept_failed');
            $table->dropColumn('intercept_success');
            $table->dropColumn('loose_ball');
            $table->dropColumn('matches');
            $table->dropColumn('pass_failed');
            $table->dropColumn('pass_success');
            $table->dropColumn('save');
            $table->dropColumn('shoot_blocked');
            $table->dropColumn('shoot_off_target');
            $table->dropColumn('shoot_on_target');
            $table->dropColumn('substitution');
            $table->dropColumn('tackle_failed');
            $table->dropColumn('throw_in');
            $table->dropColumn('penalty_goal');
            $table->dropColumn('penalty_missed');
            $table->dropColumn('minute_of_play');
            $table->dropColumn('own_goal');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('match_statistics', function (Blueprint $table) {
            $table->dropColumn('total_shots');
            $table->dropColumn('shots_on_goal');
            $table->dropColumn('passing_accuracy');
            $table->integer('assist')->default(0);
            $table->integer('block_cross')->default(0);
            $table->integer('block')->default(0);
            $table->integer('body_charge')->default(0);
            $table->integer('clearance')->default(0);
            $table->integer('create_chance')->default(0);
            $table->integer('cross_blocked')->default(0);
            $table->integer('cross_failed')->default(0);
            $table->integer('cross_success')->default(0);
            $table->integer('dribble_failed')->default(0);
            $table->integer('dribble_success')->default(0);
            $table->integer('fouled')->default(0);
            $table->integer('free_kick')->default(0);
            $table->integer('goal')->default(0);
            $table->integer('handball')->default(0);
            $table->integer('header_failed')->default(0);
            $table->integer('header_success')->default(0);
            $table->integer('intercept_failed')->default(0);
            $table->integer('intercept_success')->default(0);
            $table->integer('loose_ball')->default(0);
            $table->integer('matches')->default(0);
            $table->integer('pass_failed')->default(0);
            $table->integer('pass_success')->default(0);
            $table->integer('save')->default(0);
            $table->integer('shoot_blocked')->default(0);
            $table->integer('shoot_off_target')->default(0);
            $table->integer('shoot_on_target')->default(0);
            $table->integer('substitution')->default(0);
            $table->integer('tackle_failed')->default(0);
            $table->integer('throw_in')->default(0);
            $table->integer('penalty_goal')->default(0);
            $table->integer('penalty_missed')->default(0);
            $table->integer('minute_of_play')->default(0);
            $table->integer('own_goal')->default(0);
        });
    }
}
