<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRewardAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reward_ads', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('title');
            $table->string("url");
            $table->string('image');
            $table->tinyInteger('image_type')->default(0);
            $table->text('image_info')->default('');
            $table->integer('priority')->default(0);
            $table->dateTime('published_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('reward_ads');
    }
}
