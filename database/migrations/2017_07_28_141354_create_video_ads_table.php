<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVideoAdsTable extends Migration
{
    public function up()
    {
        Schema::create('video_ads', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('key')->unique();
            $table->string('description')->default('');
            $table->string('title');
            $table->string('duration');

            $table->string('attachment')->nullable();
            $table->tinyInteger('attachment_type')->default(0)->nullable();
            $table->text('attachment_info')->default('')->nullable();

            $table->integer('priority')->default(0);
            $table->dateTime('published_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('video_ads');
    }
}
