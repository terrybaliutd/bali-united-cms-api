<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateNewsListAdsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('news_list_ads', function (Blueprint $table) {
            $table->increments('id');
            $table->string("title");
            $table->string("url");
            $table->string('attachment');
            $table->tinyInteger('attachment_type')->default(0);
            $table->text('attachment_info')->default('');
            $table->integer('priority')->default(0);
            $table->dateTime('published_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('news_list_ads');
    }
}
