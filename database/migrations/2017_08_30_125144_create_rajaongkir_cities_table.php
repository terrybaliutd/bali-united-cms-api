<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRajaongkirCitiesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rajaongkir_cities', function (Blueprint $table) {
            $table->integer('id')->unsigned();
            $table->integer('province_id')->unsigned()->nullable();
            $table->string('name');
            $table->timestamps();

            $table->primary('id');

            $table->foreign('province_id')
                  ->references('id')->on('rajaongkir_provinces')
                  ->onDelete('set null');

            $table->index('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rajaongkir_cities');
    }
}
