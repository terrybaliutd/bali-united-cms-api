<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddProvinceAndCityIdToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('city_id')->unsigned()->nullable();
            $table->integer('province_id')->unsigned()->nullable();

            $table->foreign('city_id')
                  ->references('id')->on('rajaongkir_cities')
                  ->onDelete('set null');

            $table->foreign('province_id')
                  ->references('id')->on('rajaongkir_provinces')
                  ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_city_id_foreign');
            $table->dropForeign('users_province_id_foreign');

            $table->dropColumn('city_id');
            $table->dropColumn('province_id');
        });
    }
}
