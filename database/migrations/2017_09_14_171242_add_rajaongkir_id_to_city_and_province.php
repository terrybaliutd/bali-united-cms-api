<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddRajaongkirIdToCityAndProvince extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('cities', function (Blueprint $table) {
            $table->integer('rajaongkir_id')->unsigned()->nullable();
        });

        Schema::table('provinces', function (Blueprint $table) {
            $table->integer('rajaongkir_id')->unsigned()->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('cities', function (Blueprint $table) {
            $table->dropColumn('rajaongkir_id');
        });

        Schema::table('provinces', function (Blueprint $table) {
            $table->dropColumn('rajaongkir_id');
        });
    }
}
