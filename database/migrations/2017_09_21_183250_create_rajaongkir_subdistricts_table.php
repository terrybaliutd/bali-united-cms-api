<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRajaongkirSubdistrictsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rajaongkir_subdistricts', function (Blueprint $table) {
            $table->integer('id')->unsigned();
            $table->integer('city_id')->unsigned()->nullable();
            $table->string('name');
            $table->timestamps();

            $table->primary('id');

            $table->foreign('city_id')
                  ->references('id')->on('rajaongkir_cities')
                  ->onDelete('set null');

            $table->index('name');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('rajaongkir_subdistricts');
    }
}
