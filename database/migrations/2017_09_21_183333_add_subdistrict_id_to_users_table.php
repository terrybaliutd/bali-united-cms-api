<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddSubdistrictIdToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer('subdistrict_id')->unsigned()->nullable();

            $table->foreign('subdistrict_id')
                  ->references('id')->on('rajaongkir_subdistricts')
                  ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropForeign('users_subdistrict_id_foreign');
            
            $table->dropColumn('subdistrict_id');
        });
    }
}
