<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('owner_id')->unsigned();
            $table->string('type', 8)->index();
            $table->string('title');
            $table->string('slug');
            $table->string('image', 500)->nullable();
            $table->tinyInteger('image_type')->default(0);
            $table->text('image_info')->default('');
            $table->string('excerpt')->nullable();
            $table->text('content')->nullable();
            $table->string('video_url')->nullable();
            $table->integer('total_like')->default(0);
            $table->dateTime('published_at')->nullable();
            $table->timestamps();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();
            
            $table->string('video_status', 16);
            $table->text('video_status_description');

            $table->foreign('owner_id')
                  ->references('id')->on('users')
                  ->onDelete('CASCADE');

            $table->foreign('created_by')
                  ->references('id')->on('users')
                  ->onDelete('SET NULL');

            $table->foreign('updated_by')
                  ->references('id')->on('users')
                  ->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('posts');
    }
}
