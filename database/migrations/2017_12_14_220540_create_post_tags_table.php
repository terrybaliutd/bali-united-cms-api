<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostTagsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_tags', function (Blueprint $table) {
            $table->integer('tag_id')->unsigned();
            $table->integer('post_id')->unsigned();

            $table->timestamps();

            $table->foreign('tag_id')
                  ->references('id')->on('tags')
                  ->onDelete('CASCADE');

            $table->foreign('post_id')
                  ->references('id')->on('posts')
                  ->onDelete('CASCADE');

            $table->unique(array('post_id', 'tag_id'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('post_tags');
    }
}
