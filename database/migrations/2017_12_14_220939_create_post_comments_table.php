<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePostCommentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('post_comments', function (Blueprint $table) {
            $table->increments('id');
            $table->string('content');
            $table->integer('user_id')->unsigned();
            $table->integer('post_id')->unsigned();
            $table->integer('total_like');
            $table->integer('parent_id')->unsigned()->nullable();
            $table->timestamps();
            $table->integer('created_by')->unsigned()->nullable();
            $table->integer('updated_by')->unsigned()->nullable();

            $table->foreign('user_id')
                  ->references('id')->on('users')
                  ->onDelete('CASCADE');

            $table->foreign('post_id')
                  ->references('id')->on('posts')
                  ->onDelete('CASCADE');

            $table->foreign('created_by')
                  ->references('id')->on('users')
                  ->onDelete('SET NULL');

            $table->foreign('updated_by')
                  ->references('id')->on('users')
                  ->onDelete('SET NULL');

            $table->foreign('parent_id')
                  ->references('id')->on('post_comments')
                  ->onDelete('SET NULL');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('post_comments');
    }
}
