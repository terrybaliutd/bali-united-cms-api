<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQueryCountersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('query_counters', function (Blueprint $table) {
            $table->increments('id');
            $table->string('url');
            $table->integer('count');
            $table->integer('user_id')->unsigned()->nullable();
            $table->string('access_token')->nullable();
            $table->timestamps();

            $table->index('url');
            $table->index('access_token');

            $table->foreign('user_id')
                  ->references('id')->on('users')
                  ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('query_counters');
    }
}
