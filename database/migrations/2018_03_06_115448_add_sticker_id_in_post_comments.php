<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddStickerIdInPostComments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('post_comments', function (Blueprint $table) {
            $table->integer('sticker_id')->unsigned()->nullable()->after('parent_id');

            $table->foreign('sticker_id')
                  ->references('id')->on('sticker_items')
                  ->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('post_comments', function (Blueprint $table) {
            $table->dropForeign(['sticker_id']);
            $table->dropColumn('sticker_id');
        });
    }
}
