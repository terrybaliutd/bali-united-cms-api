<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();
        // $this->call(UserTableSeeder::class);
        $this->call(PostSeeder::class);
        $this->call(TagSeeder::class);
        $this->call(PostTagSeeder::class);
        $this->call(PostCommentSeeder::class);
        $this->call(PostReportSeeder::class);
        $this->call(PostCommentReportSeeder::class);
        $this->call(MembershipSeeder::class);
        Model::reguard();
    }
}
