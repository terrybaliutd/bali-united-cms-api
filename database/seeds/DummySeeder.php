<?php

use App\Model\Club;
use App\Model\Competition;
use App\Model\Match;
use App\Model\MatchClubInfo;
use App\Model\MatchHighlight;
use App\Model\MatchLineUp;
use App\Model\MatchStatistic;
use App\Model\News;
use App\Model\Playlist;
use App\Model\Quiz;
use App\Model\Season;
use App\Model\Standing;
use App\Model\StickerCategory;
use App\Model\StickerItem;
use Illuminate\Database\Seeder;

class DummySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $clubs = Club::get();
        News::getQuery()->delete();

        // News seeder
        factory(News::class, 20)->create();

        Season::getQuery()->delete();
        Competition::getQuery()->delete();

        // Competitions seeder
        $competition1 = factory(Competition::class)->create([
            'name' => 'Kompetisi 1',
            'long_name' => 'Kompetisi Nomor 1',
            'order' => 1,
            'is_default' => true,
        ]);

        $competition2 = factory(Competition::class)->create([
            'name' => 'Kompetisi 2',
            'long_name' => 'Kompetisi Nomor 2',
            'order' => 2,
        ]);

        $season = factory(Season::class)->create([
            'name' => '2016/2017',
            'is_default' => true
        ]);
        $season->competitions()->sync([$competition1->id, $competition2->id]);

        // Match Seeder
        factory(Match::class, 10)->create([
            'season_id' => $season->id,
            'competition_id' => $competition1->id
        ])->each(function ($match) use ($clubs) {
            $clubMatchs = $clubs->random(2);

            factory(MatchHighlight::class, 3)->create([
                'match_id' => $match->id
            ]);

            $homeClub = factory(MatchClubInfo::class)->create([
                'match_id' => $match->id,
                'club_id' => $clubMatchs->first()->id,
                'type' => 'Home'
            ]);
            factory(MatchLineUp::class, 11)->create([
                'match_club_info_id' => $homeClub->id
            ]);
            factory(MatchLineUp::class, 5)->create([
                'match_club_info_id' => $homeClub->id,
                'is_primary' => false
            ]);
            factory(MatchStatistic::class)->create([
                'match_club_info_id' => $homeClub->id
            ]);

            $awayClub = factory(MatchClubInfo::class)->create([
                'match_id' => $match->id,
                'club_id' => $clubMatchs->last()->id,
                'type' => 'Away'
            ]);
            factory(MatchLineUp::class, 11)->create([
                'match_club_info_id' => $awayClub->id
            ]);
            factory(MatchLineUp::class, 5)->create([
                'match_club_info_id' => $awayClub->id,
                'is_primary' => false
            ]);
            factory(MatchStatistic::class)->create([
                'match_club_info_id' => $awayClub->id
            ]);
        });

        factory(Match::class, 10)->create([
            'season_id' => $season->id,
            'competition_id' => $competition2->id
        ])->each(function ($match) use ($clubs) {
            $clubMatchs = $clubs->random(2);

            factory(MatchClubInfo::class)->create([
                'match_id' => $match->id,
                'club_id' => $clubMatchs->first()->id,
                'type' => 'Home'
            ]);
            factory(MatchClubInfo::class)->create([
                'match_id' => $match->id,
                'club_id' => $clubMatchs->last()->id,
                'type' => 'Away'
            ]);
        });

        // Sticker seeder
        StickerCategory::getQuery()->delete();
        $sticker = factory(StickerCategory::class)->create();

        $items = factory(StickerItem::class, 5)->create([
            'sticker_category_id' => $sticker->id,
            'image' => 'http://www.baliutd.com/wp-content/themes/balirevamp/images/logo.png',
            'expired_date' => Carbon\Carbon::now()->addMonths(3)
        ]);

        foreach ($items as $item) {
            $item->image = 'http://www.baliutd.com/wp-content/themes/balirevamp/images/logo.png';
            $item->save();
        }

        // Stading seeder
        Standing::getQuery()->delete();
        $clubStandings = $clubs->slice(0, 10);
        foreach ($clubStandings as $club) {
            factory(Standing::class)->create([
                'competition_id' => $competition1->id,
                'season_id' => $season->id,
                'club_id' => $club->id
            ]);
        }
        $clubStandings = $clubs->slice(10, 20);
        foreach ($clubStandings as $club) {
            factory(Standing::class)->create([
                'competition_id' => $competition2->id,
                'season_id' => $season->id,
                'club_id' => $club->id
            ]);
        }
    }
}
