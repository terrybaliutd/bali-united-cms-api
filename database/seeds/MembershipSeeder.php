<?php

use App\Model\Playlist;
use App\Model\Quiz;
use App\Model\User;
use App\Model\Video;
use Illuminate\Database\Seeder;

class MembershipSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $membership = new \App\Model\Membership();
        $membership->name = "Smartfren";
        $membership->prefix = "0811,0812";
        $membership->save();
    }
}
