<?php

use App\Model\PostCommentReport;
use Illuminate\Database\Seeder;

class PostCommentReportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('post_comment_reports')->delete();

        PostCommentReport::create([
        	'id' => 1,
        	'user_id' => 1,
        	'post_comment_id' => 1,
        	'type' => 'post comment',
        	'reason' => 'inappropriate content',
        ]);

        PostCommentReport::create([
            'id' => 2,
        	'user_id' => 1,
        	'post_comment_id' => 3,
        	'type' => 'post comment',
        	'reason' => 'negative content',
        ]);

        PostCommentReport::create([
        	'id' => 3,
        	'user_id' => 1,
        	'post_comment_id' => 8,
        	'type' => 'post comment',
        	'reason' => 'inappropriate content',
        ]);

        PostCommentReport::create([
            'id' => 4,
        	'user_id' => 1,
        	'post_comment_id' => 11,
        	'type' => 'post comment',
        	'reason' => 'negative content',
        ]);
    }
}
