<?php

use App\Model\PostComment;
use Illuminate\Database\Seeder;

class PostCommentSeeder extends Seeder
{
    /**
     * Run the database seeds.
     * 
     * @return void
     */
    public function run()
    {
        DB::table('post_comments')->delete();

        PostComment::create([
        	'id' => 1,
        	'user_id' => 1,
        	'post_id' => 1,
        	'parent_id' => null,
        	'content' => 'post comment'
        ]);

        PostComment::create([
        	'id' => 2,
            'user_id' => 1,
        	'post_id' => 1,
        	'parent_id' => 1,
        	'content' => 'reply post comment'
        ]);

        PostComment::create([
        	'id' => 3,
        	'user_id' => 1,
        	'post_id' => 2,
        	'parent_id' => null,
        	'content' => 'post comment'
        ]);

        PostComment::create([
        	'id' => 4,
            'user_id' => 1,
        	'post_id' => 2,
        	'parent_id' => 3,
        	'content' => 'reply post comment'
        ]);

        PostComment::create([
        	'id' => 5,
        	'user_id' => 1,
        	'post_id' => 3,
        	'parent_id' => null,
        	'content' => 'post comment'
        ]);

        PostComment::create([
        	'id' => 6,
            'user_id' => 1,
        	'post_id' => 3,
        	'parent_id' => 5,
        	'content' => 'reply post comment'
        ]);

        PostComment::create([
        	'id' => 7,
        	'user_id' => 1,
        	'post_id' => 4,
        	'parent_id' => null,
        	'content' => 'post comment'
        ]);

        PostComment::create([
        	'id' => 8,
            'user_id' => 1,
        	'post_id' => 4,
        	'parent_id' => 7,
        	'content' => 'reply post comment'
        ]);

        PostComment::create([
        	'id' => 9,
        	'user_id' => 1,
        	'post_id' => 5,
        	'parent_id' => null,
        	'content' => 'post comment'
        ]);

        PostComment::create([
        	'id' => 10,
            'user_id' => 1,
        	'post_id' => 5,
        	'parent_id' => 9,
        	'content' => 'reply post comment'
        ]);

        PostComment::create([
        	'id' => 11,
        	'user_id' => 1,
        	'post_id' => 6,
        	'parent_id' => null,
        	'content' => 'post comment'
        ]);

        PostComment::create([
        	'id' => 12,
            'user_id' => 1,
        	'post_id' => 6,
        	'parent_id' => 11,
        	'content' => 'reply post comment'
        ]);

        PostComment::create([
        	'id' => 13,
        	'user_id' => 1,
        	'post_id' => 7,
        	'parent_id' => null,
        	'content' => 'post comment'
        ]);

        PostComment::create([
        	'id' => 14,
            'user_id' => 1,
        	'post_id' => 7,
        	'parent_id' => 13,
        	'content' => 'reply post comment'
        ]);

        PostComment::create([
        	'id' => 15,
        	'user_id' => 1,
        	'post_id' => 8,
        	'parent_id' => null,
        	'content' => 'post comment'
        ]);

        PostComment::create([
        	'id' => 16,
            'user_id' => 1,
        	'post_id' => 8,
        	'parent_id' => 15,
        	'content' => 'reply post comment'
        ]);
    }
}
