<?php

use App\Model\PostReport;
use Illuminate\Database\Seeder;

class PostReportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('post_reports')->delete();

        PostReport::create([
        	'id' => 1,
        	'user_id' => 1,
        	'post_id' => 1,
        	'type' => 'post',
        	'reason' => 'inappropriate content',
        ]);

        PostReport::create([
            'id' => 2,
        	'user_id' => 1,
        	'post_id' => 3,
        	'type' => 'post',
        	'reason' => 'negative content',
        ]);

        PostReport::create([
        	'id' => 3,
        	'user_id' => 1,
        	'post_id' => 7,
        	'type' => 'post',
        	'reason' => 'inappropriate content',
        ]);

        PostReport::create([
            'id' => 4,
        	'user_id' => 1,
        	'post_id' => 8,
        	'type' => 'post',
        	'reason' => 'negative content',
        ]);
    }
}
