<?php

use App\Model\Post;
use Illuminate\Database\Seeder;

class PostSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('posts')->delete();

        Post::create([
        	'id' => 1,
        	'owner_id' => 1,
        	'type' => 'photo',
        	'title' => 'seed photo 1',
        	'slug' => 'seed-photo-1',
        	'published' => true
        ]);

        Post::create([
        	'id' => 2,
        	'owner_id' => 1,
        	'type' => 'photo',
        	'title' => 'seed photo 2',
        	'slug' => 'seed-photo-2',
        	'published' => true
        ]);

        Post::create([
        	'id' => 3,
        	'owner_id' => 1,
        	'type' => 'meme',
        	'title' => 'seed meme 1',
        	'slug' => 'seed-meme-1',
        	'published' => true
        ]);

        Post::create([
        	'id' => 4,
        	'owner_id' => 1,
        	'type' => 'meme',
        	'title' => 'seed meme 2',
        	'slug' => 'seed-meme-2',
        	'published' => true
        ]);

        Post::create([
        	'id' => 5,
        	'owner_id' => 1,
        	'type' => 'video',
        	'title' => 'seed video 1',
        	'slug' => 'seed-video-1',
        	'video_url' => 'https://www.youtube.com/watch?v=mX4MTTyZhSU',
        	'published' => true
        ]);

        Post::create([
        	'id' => 6,
        	'owner_id' => 1,
        	'type' => 'video',
        	'title' => 'seed video 2',
        	'slug' => 'seed-video-2',
        	'video_url' => 'https://www.youtube.com/watch?v=mX4MTTyZhSU',
        	'published' => true
        ]);

        Post::create([
        	'id' => 7,
        	'owner_id' => 1,
        	'type' => 'article',
        	'title' => 'seed article 1',
        	'slug' => 'seed-article-1',
        	'excerpt' => 'excerpt seed article 1',
        	'content' => 'content article content article content article content article content article content article',
        	'published' => true
        ]);

        Post::create([
        	'id' => 8,
        	'owner_id' => 1,
        	'type' => 'article',
        	'title' => 'seed article 2',
        	'slug' => 'seed-article-2',
        	'excerpt' => 'excerpt seed article 2',
        	'content' => 'content article content article content article content article content article content article',
        	'published' => true
        ]);
    }
}
