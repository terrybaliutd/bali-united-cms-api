<?php

use App\Model\PostTag;
use Illuminate\Database\Seeder;

class PostTagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('post_tags')->delete();

        PostTag::create([
        	'post_id' => 1,
            'tag_id' => 1
        ]);

        PostTag::create([
            'post_id' => 1,
            'tag_id' => 2
        ]);

        PostTag::create([
            'post_id' => 2,
            'tag_id' => 1
        ]);

        PostTag::create([
            'post_id' => 4,
            'tag_id' => 1
        ]);

        PostTag::create([
            'post_id' => 5,
            'tag_id' => 1
        ]);

        PostTag::create([
            'post_id' => 5,
            'tag_id' => 2
        ]);

        PostTag::create([
            'post_id' => 6,
            'tag_id' => 1
        ]);

        PostTag::create([
            'post_id' => 8,
            'tag_id' => 1
        ]);

        PostTag::create([
            'post_id' => 8,
            'tag_id' => 2
        ]);
    }
}
