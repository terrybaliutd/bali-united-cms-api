<?php

use Illuminate\Database\Seeder;

class ProvinceSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        \DB::table('provinces')->delete();

        \DB::table('provinces')->insert([
            0 => [
                'id' => 85,
                'code' => '11',
                'name' => 'ACEH',
                'created_at' => '2016-01-22 03:30:25',
                'updated_at' => '2016-01-22 03:30:25',
            ],
            1 => [
                'id' => 86,
                'code' => '12',
                'name' => 'SUMATERA UTARA',
                'created_at' => '2016-01-22 03:30:25',
                'updated_at' => '2016-06-23 06:49:38',
            ],
            2 => [
                'id' => 87,
                'code' => '13',
                'name' => 'SUMATERA BARAT',
                'created_at' => '2016-01-22 03:30:25',
                'updated_at' => '2016-06-23 06:49:38',
            ],
            3 => [
                'id' => 88,
                'code' => '14',
                'name' => 'RIAU',
                'created_at' => '2016-01-22 03:30:25',
                'updated_at' => '2016-06-23 06:49:38',
            ],
            4 => [
                'id' => 89,
                'code' => '15',
                'name' => 'JAMBI',
                'created_at' => '2016-01-22 03:30:25',
                'updated_at' => '2016-06-23 06:49:37',
            ],
            5 => [
                'id' => 90,
                'code' => '16',
                'name' => 'SUMATERA SELATAN',
                'created_at' => '2016-01-22 03:30:25',
                'updated_at' => '2016-06-23 06:49:38',
            ],
            6 => [
                'id' => 91,
                'code' => '17',
                'name' => 'BENGKULU',
                'created_at' => '2016-01-22 03:30:25',
                'updated_at' => '2016-06-23 06:49:37',
            ],
            7 => [
                'id' => 92,
                'code' => '18',
                'name' => 'LAMPUNG',
                'created_at' => '2016-01-22 03:30:25',
                'updated_at' => '2016-06-23 06:49:38',
            ],
            8 => [
                'id' => 93,
                'code' => '19',
                'name' => 'KEPULAUAN BANGKA BELITUNG',
                'created_at' => '2016-01-22 03:30:25',
                'updated_at' => '2016-01-22 03:30:25',
            ],
            9 => [
                'id' => 94,
                'code' => '21',
                'name' => 'KEPULAUAN RIAU',
                'created_at' => '2016-01-22 03:30:25',
                'updated_at' => '2016-06-23 06:49:38',
            ],
            10 => [
                'id' => 95,
                'code' => '31',
                'name' => 'DKI JAKARTA',
                'created_at' => '2016-01-22 03:30:25',
                'updated_at' => '2016-06-23 06:49:37',
            ],
            11 => [
                'id' => 96,
                'code' => '32',
                'name' => 'JAWA BARAT',
                'created_at' => '2016-01-22 03:30:25',
                'updated_at' => '2016-06-23 06:49:37',
            ],
            12 => [
                'id' => 97,
                'code' => '33',
                'name' => 'JAWA TENGAH',
                'created_at' => '2016-01-22 03:30:25',
                'updated_at' => '2016-06-23 06:49:37',
            ],
            13 => [
                'id' => 98,
                'code' => '34',
                'name' => 'DI YOGYAKARTA',
                'created_at' => '2016-01-22 03:30:25',
                'updated_at' => '2016-06-23 06:49:37',
            ],
            14 => [
                'id' => 99,
                'code' => '35',
                'name' => 'JAWA TIMUR',
                'created_at' => '2016-01-22 03:30:25',
                'updated_at' => '2016-06-23 06:49:37',
            ],
            15 => [
                'id' => 100,
                'code' => '36',
                'name' => 'BANTEN',
                'created_at' => '2016-01-22 03:30:25',
                'updated_at' => '2016-06-23 06:49:37',
            ],
            16 => [
                'id' => 101,
                'code' => '51',
                'name' => 'BALI',
                'created_at' => '2016-01-22 03:30:25',
                'updated_at' => '2016-06-23 06:49:37',
            ],
            17 => [
                'id' => 102,
                'code' => '52',
                'name' => 'NUSA TENGGARA BARAT',
                'created_at' => '2016-01-22 03:30:25',
                'updated_at' => '2016-01-22 03:30:25',
            ],
            18 => [
                'id' => 103,
                'code' => '53',
                'name' => 'NUSA TENGGARA TIMUR',
                'created_at' => '2016-01-22 03:30:25',
                'updated_at' => '2016-01-22 03:30:25',
            ],
            19 => [
                'id' => 104,
                'code' => '61',
                'name' => 'KALIMANTAN BARAT',
                'created_at' => '2016-01-22 03:30:25',
                'updated_at' => '2016-06-23 06:49:37',
            ],
            20 => [
                'id' => 105,
                'code' => '62',
                'name' => 'KALIMANTAN TENGAH',
                'created_at' => '2016-01-22 03:30:25',
                'updated_at' => '2016-06-23 06:49:38',
            ],
            21 => [
                'id' => 106,
                'code' => '63',
                'name' => 'KALIMANTAN SELATAN',
                'created_at' => '2016-01-22 03:30:25',
                'updated_at' => '2016-06-23 06:49:37',
            ],
            22 => [
                'id' => 107,
                'code' => '64',
                'name' => 'KALIMANTAN TIMUR',
                'created_at' => '2016-01-22 03:30:25',
                'updated_at' => '2016-06-23 06:49:38',
            ],
            23 => [
                'id' => 108,
                'code' => '65',
                'name' => 'KALIMANTAN UTARA',
                'created_at' => '2016-01-22 03:30:25',
                'updated_at' => '2016-06-23 06:49:38',
            ],
            24 => [
                'id' => 109,
                'code' => '71',
                'name' => 'SULAWESI UTARA',
                'created_at' => '2016-01-22 03:30:25',
                'updated_at' => '2016-06-23 06:49:38',
            ],
            25 => [
                'id' => 110,
                'code' => '72',
                'name' => 'SULAWESI TENGAH',
                'created_at' => '2016-01-22 03:30:25',
                'updated_at' => '2016-06-23 06:49:38',
            ],
            26 => [
                'id' => 111,
                'code' => '73',
                'name' => 'SULAWESI SELATAN',
                'created_at' => '2016-01-22 03:30:25',
                'updated_at' => '2016-06-23 06:49:38',
            ],
            27 => [
                'id' => 112,
                'code' => '74',
                'name' => 'SULAWESI TENGGARA',
                'created_at' => '2016-01-22 03:30:25',
                'updated_at' => '2016-06-23 06:49:38',
            ],
            28 => [
                'id' => 113,
                'code' => '75',
                'name' => 'GORONTALO',
                'created_at' => '2016-01-22 03:30:25',
                'updated_at' => '2016-06-23 06:49:37',
            ],
            29 => [
                'id' => 114,
                'code' => '76',
                'name' => 'SULAWESI BARAT',
                'created_at' => '2016-01-22 03:30:25',
                'updated_at' => '2016-06-23 06:49:38',
            ],
            30 => [
                'id' => 115,
                'code' => '81',
                'name' => 'MALUKU',
                'created_at' => '2016-01-22 03:30:25',
                'updated_at' => '2016-06-23 06:49:38',
            ],
            31 => [
                'id' => 116,
                'code' => '82',
                'name' => 'MALUKU UTARA',
                'created_at' => '2016-01-22 03:30:25',
                'updated_at' => '2016-06-23 06:49:38',
            ],
            32 => [
                'id' => 117,
                'code' => '91',
                'name' => 'PAPUA BARAT',
                'created_at' => '2016-01-22 03:30:25',
                'updated_at' => '2016-06-23 06:49:38',
            ],
            33 => [
                'id' => 118,
                'code' => '94',
                'name' => 'PAPUA',
                'created_at' => '2016-01-22 03:30:25',
                'updated_at' => '2016-06-23 06:49:38',
            ],
        ]);
    }
}
