<?php

use App\Model\Tag;
use Illuminate\Database\Seeder;

class TagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tags')->delete();

        Tag::create([
        	'id' => 1,
        	'name' => 'tag 1',
            'description' => 'description 1'
        ]);

        Tag::create([
            'id' => 2,
            'name' => 'tag 2',
            'description' => 'description 2'
        ]);
    }
}
