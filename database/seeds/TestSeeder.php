<?php

use Illuminate\Database\Seeder;

class TestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Video
        DB::table('videos')->delete();
        DB::table('videos')->insert([
        [
            'publisher_id' => 1,
            'url' => 'a',
            'description' => '',
            'title' => 'Video 1',
            'featured' => false,
            'attachment' => '',
            'attachment_type' => 0,
            'attachment_info' => '',
            'published_at' => date('Y-m-d H:i:s'),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ],
        [
            'publisher_id' => 1,
            'url' => 'b',
            'description' => '',
            'title' => 'Video 1',
            'featured' => false,
            'attachment' => '',
            'attachment_type' => 0,
            'attachment_info' => '',
            'published_at' => date('Y-m-d H:i:s'),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]]);

        // Sticker
        DB::table('sticker_categories')->delete();
        DB::table('sticker_categories')->insert([
        [
            'title' => 'Kategori 1',
            'slug' => 'kategori-1',
            'published_at' => date('Y-m-d H:i:s'),
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]]);

        DB::table('sticker_items')->delete();

        $categoryId = DB::table('sticker_categories')->orderBy('created_at')->first()->id;
        $item = new \App\Model\StickerItem;
        $item->sticker_category_id = $categoryId;
        $item->image = '.gitignore';
        $item->title = 'Sticker 1';
        $item->save();


        // Comment
        $videoId = DB::table('videos')->orderBy('created_at')->first()->id;
        $stcikerItemId = DB::table('sticker_items')->orderBy('created_at')->first()->id;

        DB::table('comments')->delete();
        DB::table('comments')->insert([
        [
            'id' => uniqid(hash('md5', $videoId), false),
            'video_id' => $videoId,
            'user_id' => 1,
            'sticker_item_id' => null,
            'type' => 'message',
            'message' => 'Begitulah',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ],
        [
            'id' => uniqid(hash('md5', $videoId), false),
            'video_id' => $videoId,
            'user_id' => 1,
            'sticker_item_id' => $stcikerItemId,
            'type' => 'sticker',
            'message' => null,
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ],
        [
            'id' => uniqid(hash('md5', $videoId + 1), false),
            'video_id' => $videoId + 1,
            'user_id' => 1,
            'sticker_item_id' => null,
            'type' => 'message',
            'message' => 'Begitulah 2',
            'created_at' => date('Y-m-d H:i:s'),
            'updated_at' => date('Y-m-d H:i:s')
        ]]);

        
    }
}
