<?php

use App\Model\Playlist;
use App\Model\Quiz;
use App\Model\User;
use App\Model\Video;
use Illuminate\Database\Seeder;

class VideoDummySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = User::first();
        \Auth::loginUsingId($user->id);
        // Playlist Seeder
        Playlist::getQuery()->delete();
        $playlist = Playlist::createFromUrl('https://www.youtube.com/watch?v=r7kGJmQuC5I&list=PLD-H-tvwIL5fNj4ublvdMLxjM0_BlCEMg');
        $playlist->published = true;
        $playlist->save();
        $playlist->synch();

        $playlist2 = Playlist::createFromUrl('https://www.youtube.com/watch?v=8RA9fnqF6Mk&list=PLD-H-tvwIL5dMY50LasSq-09IP7yL-N5_');
        $playlist2->published = true;
        $playlist2->save();
        $playlist2->synch();

        $video = Video::first();

        // Quiz seeder
        Quiz::getQuery()->delete();
        factory(Quiz::class, 10)->create([
            'video_id' => $video->id
        ]);

        $videos = Video::get();
        foreach ($videos as $video) {
            $video->published = true;
            $video->save();
        }
    }
}
