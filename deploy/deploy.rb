# config valid only for current version of Capistrano
lock '3.10.0'

set :application, 'baliutd'
set :repo_url, 'git@gitlab.com:suitmedia/baliutd.git'

# Default branch is :master
ask :branch, `git tag`.split("\n").last

# Default deploy_to directory is /var/www/my_app_name
# set :deploy_to, '/var/www/my_app_name'

# Default value for :scm is :git
# set :scm, :git

# Default value for :format is :pretty
# set :format, :pretty

# Default value for :log_level is :debug
# set :log_level, :debug

# Default value for :pty is false
# set :pty, true

# Default value for :linked_files is []
set :linked_files, fetch(:linked_files, []).push('.env')

# Default value for linked_dirs is []
set :linked_dirs, fetch(:linked_dirs, []).push('public/files', 'storage/logs', 'storage/framework/cache')

# Default value for default_env is {}
# set :default_env, { path: "/opt/ruby/bin:$PATH" }

# Default value for keep_releases is 5
set :keep_releases, 5

namespace :deploy do

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence, wait: 5 do
      execute "chmod 0777 #{release_path.join('storage')} -R"
      execute "cd '#{release_path}'; composer install"
      execute "cd '#{release_path}'; php artisan migrate -n --force"

      # Please disable these lines when you are deploying to staging environment
      execute "sudo service php7.0-fpm restart"
      execute "sudo service nginx restart"
    end
  end

  before :publishing, :restart
end
