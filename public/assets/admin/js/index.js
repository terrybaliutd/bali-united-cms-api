var Index = function() {

    function formPost(target, params) {
        var form = document.createElement('form');

        form.setAttribute('method', 'POST');
        form.setAttribute('action', target);
        for(var key in params) {
            var hiddenField = document.createElement('input');
            hiddenField.setAttribute('name', key);
            hiddenField.setAttribute('value', params[key]);
            hiddenField.setAttribute('type', 'hidden');

            form.appendChild(hiddenField);
        }

        $(form).appendTo("body").submit();
    }

    function initDeleteForm() {
        var element = $('tbody');

        if (!element.length) {
            return;
        }
        element.delegate('a.delete', 'click', function(event) {

            var href = this.getAttribute('href', '#'),
                token = this.getAttribute('data-token'),
                message = this.getAttribute('data-message');

            var params = {
                _token: token,
                _method: 'DELETE'
            };

            if (message == null) {
                message = 'Are sure want to delete this?';
            }

            if (confirm(message)) {
                formPost(href, params);
            }
            event.preventDefault();
        });
    }

    return {
        init: function() {
            initDeleteForm();
        }
    };
}();
