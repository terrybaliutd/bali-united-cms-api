var TableAdvanced = function () {

    var initHideShowColumn = function () {
        var table = $('#hide_show_column');
        var tableWrapper = $('#hide_show_column_wrapper'); // datatable creates the table wrapper by adding with id {your_table_jd}_wrapper
        var tableColumnToggler = $('#hide_show_column_column_toggler');
        var tableHeaderRow = $('#table-header-row');
        var columns = [];

        $('input[type="checkbox"]', tableColumnToggler).each(function(index, el) {
            columns.push({
                "name": $(el).attr('data-name')
            });
            $(el).attr("data-column", index);
            tableHeaderRow.append($('<th>' + $(el).parent().parent().parent().text() + '</th>'));
        });
        columns.push({"name": ""});
        tableHeaderRow.append($('<th>Action</th>'));

        var oTable = table.dataTable({

            // Internationalisation. For more info refer to http://datatables.net/manual/i18n
            "language": {
                "aria": {
                    "sortAscending": ": activate to sort column ascending",
                    "sortDescending": ": activate to sort column descending"
                },
                "emptyTable": "No data available in table",
                "info": "Showing _START_ to _END_ of _TOTAL_ entries",
                "infoEmpty": "No entries found",
                "infoFiltered": "(filtered from _MAX_ total entries)",
                "lengthMenu": "Show _MENU_ entries",
                "search": "Search:",
                "zeroRecords": "No matching records found"
            },

            "columnDefs": [{
                "orderable": false,
                "targets": [-1]
            }],

            "columns": columns,
            "aaSorting": [],

            "lengthMenu": [
                [10, 25, 50, 100, 250, 500, 1000, -1],
                [10, 25, 50, 100, 250, 500, 1000, "All"] // change per page values here
            ],

            "serverSide": true,
            "processing": true,
            "ajax": []
        });


        /* modify datatable control inputs */
        tableWrapper.find('.dataTables_length select').select2(); // initialize select2 dropdown

        /* init hidden column*/
        $('input[type="checkbox"]', tableColumnToggler).each(function(index, el) {
            var checked = el.checked;
            if (!checked) {
                var iCol = index;
                oTable.fnSetColumnVis(iCol, false);
            }
        });

        /* handle show/hide columns*/
        $('input[type="checkbox"]', tableColumnToggler).change(function () {
            /* Get the DataTables object again - this is not a recreation, just a get of the object */
            var iCol = parseInt($(this).attr("data-column"));
            var bVis = oTable.fnSettings().aoColumns[iCol].bVisible;
            oTable.fnSetColumnVis(iCol, (bVis ? false : true));
        });
    }

    return {

        //main function to initiate the module
        init: function () {

            if (!jQuery().dataTable) {
                return;
            }
            initHideShowColumn();
        }

    };

}();
