<?php

return [
    'messages' => [
        'login_success' => 'Welcome :name!. You have logon successfully.',
        'login_fail' => 'Incorrect username/email or password',
        'logout_success' => 'You have successfully logout.',
        'login_header' => 'Please Log In',
        'reminder_header' => 'Forgot Password?',
        'reset_header' => 'Reset Password',
        'not_authorized' => 'You are not authorized to access this page!',
        'password_success' => 'New password saved.',
    ],
    'resources' => [
        'success_add' => 'Success to add new :item.',
        'success_edit' => 'Success to edit the :item.',
        'success_delete' => 'Success to delete a :item.',
        'invalid_field' => 'Please enter valid value for each field',
        'not_found' => 'Failed to find :item.',
    ],
    'views' => [
        'submit' => 'Submit',
        'login' => 'Login',
        'logout' => 'Logout',
        'reset_password' => 'Forget password?'
    ],
    'passwords' => [
        "password" => "Passwords must be at least eight characters and match the confirmation.",
        "user" => "We can't find a user with that e-mail address.",
        "token" => "This password reset token is invalid.",
        "sent" => "Password reminder sent!",
        "reset" => "Your password has been reset."
    ]
];
