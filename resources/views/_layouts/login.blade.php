@extends('_layouts._base', [
    'bodyClasses' => 'login'
])

{{-- DOC: Basic admin page layout. --}}
@section('site-content')
    {{-- BEGIN LOGO --}}
    <div class="logo">
        <img src="{{ asset(Setting::get('logo-url', 'assets/admin/img/logo.png')) }}" alt="logo">
    </div> <!-- /.logo -->

    {{-- BEGIN LOGIN --}}
    <div class="content">
        @yield('page-content')
    </div> <!-- /.content -->

    @include('admins._partials.footer-copyright', ['classes' => ''])
@endsection

{{-- DOC: Basic admin global head. --}}
@section('site-head')
    @include('admins._partials.head')
@endsection

{{-- DOC: Basic admin global foot. --}}
@section('site-foot')
    @include('admins._partials.foot')

    {{-- DOC: Begin page specific initialization script. --}}
    <script id="init-script">
    jQuery(document).ready(function() {
        @section('init-scripts')
        Metronic.init(); // init metronic core componets
        Layout.init(); // init layout
        @show
    });
    </script>
@endsection
