@extends('_layouts._base')

{{-- DOC: Basic web page layout --}}
@section('site-content')
@endsection

{{-- DOC: Basic web global head. --}}
@section('site-head')
    <link rel="apple-touch-icon" href="{{ asset('assets/apple-icon.png') }}">
    <link rel="icon" type="image/png" href="{{ asset('assets/favicon.png') }}">

    {{-- DOC: Begin web global stylesheets. --}}
    {{-- <link rel="stylesheet" href="{{ asset('path/to/style.css') }}"> --}}

    {{-- DOC: Begin web page specific stylesheets. --}}
    @yield('page-styles')
@endsection

{{-- DOC: Basic web global foot. --}}
@section('site-foot')
    {{-- DOC: Begin web global scripts. --}}
    {{-- <script src="{{ asset('path/to/script.js') }}"></script> --}}

    {{-- DOC: Begin web page specific scripts. --}}
    @yield('page-scripts')

    <script id="init-script">
    $(document).ready(function() {
        @yield('init-scripts')
    });
    </script>

    {{-- DOC: Include analytic script only on web (front-end). --}}
    @if (env('GOOGLE_ANALYTIC_ID'))
        @include('_partials.google-analytic')
    @endif
@endsection
