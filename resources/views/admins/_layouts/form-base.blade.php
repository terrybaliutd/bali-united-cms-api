@extends('_layouts.admin')

@section('page-content')
<div class="row">
    <div class="col-md-12">
        <div class="portlet light">
            <div class="portlet-title">
                <div class="caption">
                    @section('form-title')
                        <i class="fa fa-grid"></i> Form
                    @show
                </div> <!-- /.caption -->

                <div class="tools">
                    @yield('form-tools')
                </div> <!-- /.tools -->
            </div> <!-- /.portlet-title -->

            <div class="portlet-body form">
                @yield('form-body')
            </div> <!-- /.portlet-body -->
        </div> <!-- /.portlet -->
    </div>
</div>
@endsection

@section('page-styles')
    <link rel="stylesheet" href="{{ asset('assets/admin/vendor/bootstrap-fileinput/bootstrap-fileinput.css')}}"/>
    <link rel="stylesheet" href="{{ asset('assets/admin/vendor/select2/select2.css') }}"/>
    <link rel="stylesheet" href="{{ asset('assets/admin/vendor/bootstrap-datepicker/css/datepicker.css') }}"/>
    <link rel="stylesheet" href="{{ asset('assets/admin/vendor/bootstrap-datetimepicker/css/bootstrap-datetimepicker.css') }}"/>
    <link rel="stylesheet" href="{{ asset('assets/admin/vendor/colorbox/theme/colorbox.css') }}"/>
@endsection

@section('page-scripts')
    <script src="{{ asset('assets/admin/vendor/bootstrap-fileinput/bootstrap-fileinput.js') }}"></script>
    <script src="{{ asset('assets/admin/vendor/select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/admin/vendor/moment.min.js') }}"></script>
    <script src="{{ asset('assets/admin/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('assets/admin/vendor/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js') }}"></script>
    <script src="{{ asset('assets/admin/vendor/tinymce/tinymce.min.js') }}"></script>
    <script src="{{ asset('assets/admin/vendor/colorbox/jquery.colorbox-min.js') }}"></script>
    <script src="{{ asset('packages/barryvdh/elfinder/js/standalonepopup.min.js') }}"></script>
    <script src="https://maps.googleapis.com/maps/api/js?libraries=places"> </script>

    <script src="{{ asset('assets/admin/js/form.js') }}"></script>
@endsection

@section('init-scripts')
    @parent
    FormField.init();
@endsection
