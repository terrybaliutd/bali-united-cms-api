@extends('_layouts.admin')

@section('page-content')
    <div class="row">
        <div class="col-md-12">
            {{-- BEGIN EXAMPLE TABLE PORTLET --}}
            <div class="portlet light">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-globe"></i> @yield('table-title')
                    </div>

                    <div class="actions">
                        <div class="btn-group">
                            <a class="btn default" href="#" data-toggle="dropdown">
                                Columns <i class="fa fa-angle-down"></i>
                            </a>
                            <div id="hide_show_column_column_toggler" class="dropdown-menu hold-on-click dropdown-checkboxes pull-right">
                                @yield('table-column-checkbox')
                            </div>
                        </div>
                    </div>
                    <div class="tools"></div>
                </div>

                <div class="portlet-body">
                    <table class="table table-striped table-bordered table-hover table-condensed" id="hide_show_column">
                        <thead>
                            <tr id="table-header-row">
                                @yield('table-column-header')
                            </tr>
                        </thead>

                        <tbody>
                            @yield('table-rows')
                        </tbody>
                    </table>
                </div>
            </div>
            {{-- END EXAMPLE TABLE PORTLET --}}
            {{-- BEGIN EXAMPLE TABLE PORTLET --}}
        </div>
    </div>
@endsection

@section('page-styles')
    <link rel="stylesheet" href="{{ asset('assets/admin/vendor/select2/select2.css') }}"/>
    <link rel="stylesheet" href="{{ asset('assets/admin/vendor/datatables/extensions/Scroller/css/dataTables.scroller.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('assets/admin/vendor/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('assets/admin/vendor/datatables/plugins/bootstrap/dataTables.bootstrap.css') }}"/>
@endsection

@section('page-scripts')
    <script src="{{ asset('assets/admin/vendor/select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/admin/vendor/moment.min.js') }}"></script>
    <script src="{{ asset('assets/admin/vendor/select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/admin/vendor/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/admin/vendor/datatables/extensions/TableTools/js/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ asset('assets/admin/vendor/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js') }}"></script>
    <script src="{{ asset('assets/admin/vendor/datatables/extensions/Scroller/js/dataTables.scroller.min.js') }}"></script>
    <script src="{{ asset('assets/admin/vendor/datatables/plugins/bootstrap/dataTables.bootstrap.js') }}"></script>

    <script src="{{ asset('assets/admin/js/table-advanced.js') }}"></script>
    <script src="{{ asset('assets/admin/js/index.js') }}"></script>
@endsection

@section('init-scripts')
    @parent
    TableAdvanced.init();
    Index.init();
@endsection
