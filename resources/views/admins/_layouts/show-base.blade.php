@extends('_layouts.admin')

@section('page-content')
    <div class="row">
        <div class="col-md-12">
            {{-- BEGIN EXAMPLE TABLE PORTLET --}}
            <div class="portlet light">
                <ul class="nav nav-tabs ">
                    @yield('tab-menus')
                </ul>
                <!--<div class="portlet-title"></div>-->
                <div class="portlet-body">
                        <div class="tab-content">
                            @yield('tab-contents')
                        </div>
                </div>
            </div>
            {{-- END EXAMPLE TABLE PORTLET --}}
            {{-- BEGIN EXAMPLE TABLE PORTLET --}}
        </div>
    </div>
@endsection

@section('page-styles')
    <link rel="stylesheet" href="{{ asset('assets/admin/vendor/select2/select2.css') }}"/>
    <link rel="stylesheet" href="{{ asset('assets/admin/vendor/datatables/extensions/Scroller/css/dataTables.scroller.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('assets/admin/vendor/datatables/extensions/ColReorder/css/dataTables.colReorder.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('assets/admin/vendor/datatables/plugins/bootstrap/dataTables.bootstrap.css') }}"/>
    <link rel="stylesheet" href="{{ asset('assets/admin/vendor/bootstrap-switch/css/bootstrap-switch.css') }}"/>
@endsection

@section('page-scripts')
    <script src="{{ asset('assets/admin/vendor/select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/admin/vendor/moment.min.js') }}"></script>
    <script src="{{ asset('assets/admin/vendor/select2/select2.min.js') }}"></script>
    <script src="{{ asset('assets/admin/vendor/datatables/media/js/jquery.dataTables.min.js') }}"></script>
    <script src="{{ asset('assets/admin/vendor/datatables/extensions/TableTools/js/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ asset('assets/admin/vendor/datatables/extensions/ColReorder/js/dataTables.colReorder.min.js') }}"></script>
    <script src="{{ asset('assets/admin/vendor/datatables/extensions/Scroller/js/dataTables.scroller.min.js') }}"></script>
    <script src="{{ asset('assets/admin/vendor/datatables/plugins/bootstrap/dataTables.bootstrap.js') }}"></script>
    <script src="{{ asset('assets/admin/vendor/bootstrap-switch/js/bootstrap-switch.js') }}"></script>

    <script src="{{ asset('assets/admin/js/index.js') }}"></script>
    <script src="{{ asset('assets/admin/js/table-advanced.js') }}"></script>
@endsection
