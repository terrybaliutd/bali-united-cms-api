@extends('admins._layouts.sidebar')

@section('sidebar-menus')
<li{!! $routePrefix == 'clubs' ? ' class="active"' : '' !!}>
    <a href="{{ suitRoute('clubs.index') }}">
        <i class="fa fa-users"></i>
        <span class="title">Club</span>
    </a>
</li>
<li{!! $routePrefix == 'players' ? ' class="active"' : '' !!}>
    <a href="{{ suitRoute('players.index') }}">
        <i class="fa fa-user"></i>
        <span class="title">Player</span>
    </a>
</li>
<li{!! $routePrefix == 'competitions' || $routePrefix == 'seasons' ? ' class="active"' : '' !!}>
    <a href="javascript:;" class="nav-link nav-toggle">
        <i class="fa fa-flag"></i>
        <span class="title">Competitions</span>
        <span class="arrow"></span>
    </a>
    <ul class="sub-menu">
        <li class="nav-item {!! $routePrefix == 'competitions' ? 'active open' : '' !!}">
            <a href="{{ suitRoute('competitions.index') }}">
                <i class="fa fa-flag-o"></i>
                <span class="title">Competition</span>
            </a>
        </li>
        <li class="nav-item {!! $routePrefix == 'seasons' ? 'active open' : '' !!}">
            <a href="{{ suitRoute('seasons.index') }}">
                <i class="fa fa-calendar"></i>
                <span class="title">Season</span>
            </a>
        </li>
    </ul>
</li>
<li{!! $routePrefix == 'matches' ? ' class="active"' : '' !!}>
    <a href="{{ suitRoute('matches.index') }}">
        <i class="fa fa-calendar-o"></i>
        <span class="title">Matches</span>
    </a>
</li>
<li{!! $routePrefix == 'standings' ? ' class="active"' : '' !!}>
    <a href="{{ suitRoute('standings.index') }}">
        <i class="fa fa-bar-chart"></i>
        <span class="title">Standing</span>
    </a>
</li>
<li{!! $routePrefix == 'playlists' ? ' class="active"' : '' !!}>
    <a href="{{ suitRoute('playlists.index') }}">
        <i class="fa fa-play-circle"></i>
        <span class="title">Playlist</span>
    </a>
</li>
<li{!! $routePrefix == 'videos' ? ' class="active"' : '' !!}>
    <a href="{{ suitRoute('videos.index') }}">
        <i class="fa fa-picture-o"></i>
        <span class="title">Video</span>
    </a>
</li>
{{-- <li{!! $routePrefix == 'citizen-journal-videos' ? ' class="active"' : '' !!}>
    <a href="{{ suitRoute('citizen-journal-videos.index') }}">
        <i class="fa fa-video-camera"></i>
        <span class="title">Citizen Journalism</span>
    </a>
</li> --}}
<li{!! $routePrefix == 'reward-ads' || $routePrefix == 'home-ads' || $routePrefix == 'video-ads' || $routePrefix == 'news-list-ads' || $routePrefix == 'video-list-ads' ? ' class="active"' : '' !!}>
    <a href="javascript:;" class="nav-link nav-toggle">
        <i class="fa fa-money"></i>
        <span class="title">Ads</span>
        <span class="arrow"></span>
    </a>
    <ul class="sub-menu">
        <li class="nav-item {!! $routePrefix == 'home-ads' ? 'active open' : '' !!}">
            <a href="{{ suitRoute('home-ads.index') }}">
                <i class="fa fa-home"></i>
                <span class="title">Home</span>
            </a>
        </li>
        <li class="nav-item {!! $routePrefix == 'news-list-ads' ? 'active open' : '' !!}">
            <a href="{{ suitRoute('news-list-ads.index') }}">
                <i class="fa fa-newspaper-o"></i>
                <span class="title">News List</span>
            </a>
        </li>
        <li class="nav-item {!! $routePrefix == 'video-list-ads' ? 'active open' : '' !!}">
            <a href="{{ suitRoute('video-list-ads.index') }}">
                <i class="fa fa-video-camera"></i>
                <span class="title">Video List</span>
            </a>
        </li>
        <li class="nav-item {!! $routePrefix == 'video-ads' ? 'active open' : '' !!}">
            <a href="{{ suitRoute('video-ads.index') }}">
                <i class="fa fa-video-camera"></i>
                <span class="title">Video Ads</span>
            </a>
        </li>
        <li class="nav-item {!! $routePrefix == 'reward-ads' ? 'active open' : '' !!}">
            <a href="{{ suitRoute('reward-ads.index') }}">
                <i class="fa fa-gift"></i>
                <span class="title">Reward Ads</span>
            </a>
        </li>
    </ul>
</li>
<li{!! $routePrefix == 'quizzes' ? ' class="active"' : '' !!}>
    <a href="{{ suitRoute('quizzes.index') }}">
        <i class="fa fa-question-circle"></i>
        <span class="title">Quiz</span>
    </a>
</li>
<li{!! $routePrefix == 'comments' ? ' class="active"' : '' !!}>
    <a href="{{ suitRoute('comments.index') }}">
        <i class="fa fa-comment"></i>
        <span class="title">Comment</span>
    </a>
</li>
<li{!! $routePrefix == 'sticker-categories' ? ' class="active"' : '' !!}>
    <a href="{{ suitRoute('sticker-categories.index') }}">
        <i class="icon-emoticon-smile"></i>
        <span class="title">Sticker</span>
    </a>
</li>
<li{!! $routePrefix == 'news' ? ' class="active"' : '' !!}>
    <a href="{{ suitRoute('news.index') }}">
        <i class="fa fa-newspaper-o"></i>
        <span class="title">News</span>
    </a>
</li>
<li{!! $routePrefix == 'custom-notifications' ? ' class="active"' : '' !!}>
    <a href="{{ suitRoute('custom-notifications.index') }}">
        <i class="fa fa-newspaper-o"></i>
        <span class="title">Custom Notification</span>
    </a>
</li>
<li{!! $routePrefix == 'bank-accounts' ? ' class="active"' : '' !!}>
    <a href="{{ suitRoute('bank-accounts.index') }}">
        <i class="fa fa-university"></i>
        <span class="title">Bank Account</span>
    </a>
</li>
<li{!! $routePrefix == 'memberships' ? ' class="active"' : '' !!}>
    <a href="{{ suitRoute('memberships.index') }}">
        <i class="fa fa-user"></i>
        <span class="title">Membership</span>
    </a>
</li>
<li{!! $routePrefix == 'provinces' || $routePrefix == 'cities' ? ' class="active"' : '' !!}>
    <a href="javascript:;" class="nav-link nav-toggle">
        <i class="fa fa-globe"></i>
        <span class="title">Address</span>
        <span class="arrow"></span>
    </a>
    <ul class="sub-menu">
        <li class="nav-item {!! $routePrefix == 'provinces' ? 'active open' : '' !!}">
            <a href="{{ suitRoute('provinces.index') }}">
                <i class="fa fa-test"></i>
                <span class="title">Province</span>
            </a>
        </li>
        <li class="nav-item {!! $routePrefix == 'cities' ? 'active open' : '' !!}">
            <a href="{{ suitRoute('cities.index') }}">
                <i class="fa fa-test"></i>
                <span class="title">City</span>
            </a>
        </li>
    </ul>
</li>
<li{!! in_array($routePrefix, ['posts', 'post-comments']) ? ' class="active"' : '' !!}>
    <a href="{{ suitRoute('posts.index') }}">
        <i class="icon-globe"></i>
        <span class="title">Post</span>
    </a>
</li>
<li{!! $routePrefix == 'tags' ? ' class="active"' : '' !!}>
    <a href="{{ suitRoute('tags.index') }}">
        <i class="icon-tag"></i>
        <span class="title">Post Tag</span>
    </a>
</li>
<li{!! $routePrefix == 'post-reports' || $routePrefix == 'post-comment-reports'? ' class="active"' : '' !!}>
    <a href="javascript:;" class="nav-link nav-toggle">
        <i class="fa fa-gift"></i>
        <span class="title">Report</span>
        <span class="arrow"></span>
    </a>
    <ul class="sub-menu">
        <li class="nav-item {!! $routePrefix == 'post-reports' ? 'active open' : '' !!}">
            <a href="{{ suitRoute('post-reports.index') }}">
                <i class="fa fa-ticket"></i>
                <span class="title">Post Report</span>
            </a>
        </li>
        <li class="nav-item {!! $routePrefix == 'post-comment-reports' ? 'active open' : '' !!}">
            <a href="{{ suitRoute('post-comment-reports.index') }}">
                <i class="fa fa-gift"></i>
                <span class="title">Post Comment Report</span>
            </a>
        </li>
    </ul>
</li>
<li{!! $routePrefix == 'users' ? ' class="active"' : '' !!}>
    <a href="{{ suitRoute('users.index') }}">
        <i class="icon-user"></i>
        <span class="title">User</span>
    </a>
</li>
{{-- <li{!! $routePrefix == 'html-templates' ? ' class="active"' : '' !!}>
    <a href="{{ suitRoute('html-templates.index') }}">
        <i class="fa fa-html5"></i>
        <span class="title">Html Template</span>
    </a>
</li>
<li{!! $routePrefix == 'menus' ? ' class="active"' : '' !!}>
    <a href="{{ suitRoute('menus.index') }}">
        <i class="icon-directions"></i>
        <span class="title">Menu</span>
    </a>
</li>
<li{!! $routePrefix == 'pages' ? ' class="active"' : '' !!}>
    <a href="{{ suitRoute('pages.index') }}">
        <i class="icon-grid"></i>
        <span class="title">Page</span>
    </a>
</li> --}}
@endsection
