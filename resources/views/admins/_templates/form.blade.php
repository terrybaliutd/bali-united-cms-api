@extends('admins._layouts.form-base')

@section('form-title')
    {{ $pageName }} Form
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        <a href="{{ suitRoute($routePrefix.'.index') }}">{{ $pageName }}</a>
    </li>
@endsection

@section('form-body')
{!! Form::suitModel($model, ['prefix' => $routePrefix]) !!}
    <div class="form-body">

        @include(suitViewName('_templates._translate-header'))

        {!! Form::suitSection('My First Section') !!}
        {!! Form::suitText('text', 'Text') !!}
        {!! Form::suitPassword('Password', 'password') !!}
        {!! Form::suitNumber('number', 'Number') !!}
        {!! Form::suitTextarea('textarea', 'Text Area') !!}
        {!! Form::suitSelect('select', 'Select With Placeholder', ['Select 1', 'Select 2'], 'Placeholder') !!}
        {!! Form::suitSelect('select', 'Select Without Placeholder', ['Select 1', 'Select 2']) !!}
        {!! Form::suitMultiSelect('multiselect[]', 'Multiselect', [ 1=>'Select 1', 2=>'Select 2', 'Group' => [3=>'Select 3', 4=>'Select 4']])!!}
        {!! Form::suitTokenField('token', 'TokenField') !!}
        {!! Form::suitDate('datepicker', 'DatePicker') !!}
        {!! Form::suitFileInput('fileinput', 'File Input') !!}
        {!! Form::suitFileBrowser('filebrowser', 'File Browser')!!}
        {!! Form::suitWysiwyg('wysiwyg', 'WYSIWYG') !!}
        {{-- Location bebas ditaruh dimana saja --}}
        <div class="tab-content" >
          @foreach (config('suitcms.lang') as $lang => $value)
              <div role="tabpanel" class="tab-pane {{ ($lang == config('app.fallback_locale'))?'fade in active':'fade' }}" id="{{ $lang }}">
                {!! Form::suitText("{$lang}[title]", 'Title') !!}
                {!! Form::suitWysiwyg('wysiwyg', 'WYSIWYG') !!}
              </div>
          @endforeach
        </div>
    </div>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-offset-2 col-md-10">
                {!! Form::suitSubmit() !!}
                {!! Form::suitReset() !!}
                {!! Form::suitBack() !!}
            </div>
        </div>
    </div>
{!! Form::close() !!}
@endsection
