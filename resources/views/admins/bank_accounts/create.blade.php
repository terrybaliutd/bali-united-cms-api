@extends(suitViewName("$viewPrefix.form"))

@section('page-title')
    New {{ $pageName }}
@stop

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        New {{ $pageName }}
    </li>
@stop
