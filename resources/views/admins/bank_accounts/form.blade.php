@extends('admins._layouts.form-base')

@section('form-title')
    {{ $pageName }}
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        <a href="{{ suitRoute("$routePrefix.index") }}"">{{ $pageName }}</a>
    </li>
@endsection

@section('form-body')

<h3>Playlist Data</h3>
{!! Form::suitModel($model, ['prefix' => $routePrefix]) !!}
    <div class="form-body">
        {!! Form::suitText('bank_name', 'Bank Name') !!}
        {!! Form::suitText('account_name', 'Account Name') !!}
        {!! Form::suitText('account_number', 'Account Number') !!}
        {!! Form::suitSelect("published", 'Published', ['No', 'Yes']) !!}
    </div>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-offset-2 col-md-10">
                {!! Form::suitSubmit() !!}
                {!! Form::suitReset() !!}
                {!! Form::suitBack() !!}
            </div>
        </div>
    </div>
{!! Form::close() !!}

@endsection
