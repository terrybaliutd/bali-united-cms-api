@extends(suitViewName('_layouts.index-base'))

@section('page-title')
    {{ $pageName }}
@stop

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        {{ $pageName }}
    </li>
@stop

@section('table-title')
    {{ $pageName }} Table
@stop

@section('page-header-toolbar')
    <div class="btn-group pull-right">
        <a href="{{ suitRoute($routePrefix.'.create') }}" class="btn btn-sm btn-primary">
        <i class="glyphicon glyphicon-plus"></i> Add New
        </a>
    </div>
@stop

@section('table-column-checkbox')
    <label><input type="checkbox" data-name="id">#</label>
    <label><input type="checkbox" data-name="url">Bank Name</label>
    <label><input type="checkbox" checked data-name="title">Account Name</label>
    <label><input type="checkbox" checked data-name="priority">Account Number</label>
    <label><input type="checkbox" checked data-name="published">Published</label>
    <label><input type="checkbox" checked data-name="created_at">Created At</label>
    <label><input type="checkbox" data-name="updated_at">Updated At</label>
@stop
