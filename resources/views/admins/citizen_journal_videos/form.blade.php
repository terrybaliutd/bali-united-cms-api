@extends('admins._layouts.form-base')

@section('page-title')
    {{ $pageName }} Form
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        <a href="{{ suitRoute($routePrefix.'.index') }}">{{ $pageName }}</a>
    </li>
@endsection

@section('form-body')
{!! Form::model($model, ['prefix' => $routePrefix, 'class' => 'form-horizontal']) !!}
    <div class="form-body">
        {!! Form::suitText('user_id', 'User', $users[$model->user_id],['disabled']) !!}
        {!! Form::suitText('title', 'Title', null,['disabled']) !!}
        {!! Form::suitTextarea('description', 'Description', null,['disabled']) !!}
        {!! Form::suitText('status', 'Status', null,['disabled']) !!}
        <div class="form-group">
            <label class="control-label col-md-2">Video</label>
            <div class="col-md-10">
                <video width="720" controls buffered>
                    <source src="{{ asset($model->converted_video) }}" type="video/mp4">
                </video>
            </div>
        </div>
    </div>
{!! Form::close() !!}
    <div class="form-actions">
        <div class="row">
            <div class="col-md-offset-2 col-md-10">
                <div class="col-sm-1" style="margin:5px">
                    {!! Form::model($model, [ 'url' => suitPath('citizen-journal-videos/accept'), 'method' => 'POST']) !!}
                    <input type="hidden" name="id" value={{ $model->id }}>
                    <button type="submit" name="accept" class="btn green">Accept</button>
                    {!! Form::close() !!}
                </div>
                <div class="col-sm-1" style="margin:5px">
                    {!! Form::model($model, [ 'url' => suitPath('citizen-journal-videos/reject'), 'method' => 'POST']) !!}
                    <input type="hidden" name="id" value={{ $model->id }}>
                    <button type="submit" name="accept" class="btn yellow">Reject</button>
                    {!! Form::close() !!}
                </div>
                <div class="col-sm-1" style="margin:5px">
                    <a href="{{ suitRoute($routePrefix.'.index') }}" class="btn red">Back</a>
                </div>
            </div>
        </div>
    </div>

@endsection
