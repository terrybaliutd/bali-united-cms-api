{
    "draw": {{ \Input::get('draw', 1) }},
    "recordsTotal": {{ $total }},
    "recordsFiltered": {{ $models->total() }},
    "data" : [
        @foreach($models as $key => $model)
        [
            "{{ stringJson($model->getKey()) }}",
            "{{ stringJson($model->user->name) }}",
            "{{ stringJson($model->title) }}",
            "{{ stringJson(mb_strimwidth($model->description, 0, 50, "...")) }}",
            @if ($model->status == 'published')
                "<span class=\"label label-success\">Published</span>",
            @elseif ($model->status == 'converted')
                "<span class=\"label label-warning\">Converted</span>",
            @elseif ($model->status == 'accepted')
                "<span class=\"label label-info\">Accepted</span>",
            @elseif ($model->status == 'rejected')
                "<span class=\"label label-danger\">Rejected</span>",
            @else
                "<span class=\"label label-default\">Uploaded</span>",
            @endif
            "{{ $model->created_at}}",
            "{{ $model->updated_at}}",
            "{!! sprintf(
                "<a href='%s' class='btn default btn-xs blue'><i class='fa fa-eye'></i> View </a><a href='%s' class='btn default btn-xs red delete' data-token='%s'><i class='fa fa-trash-o'></i> Delete </a>",
                 suitRoute($routePrefix.'.show', $model),
                 suitRoute($routePrefix.'.destroy', $model),
                 \Session::token()
             )!!}"
        ]{{ ($models->count()-1 === $key) ? '' : ',' }}
        @endforeach
    ]
}
