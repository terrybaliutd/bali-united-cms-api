@extends(suitViewName('_layouts.index-base'))

@section('page-title')
    {{ $pageName }}
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        {{ $pageName }}
    </li>
@endsection

@section('table-title')
    {{ $pageName }} Table
@endsection

@section('page-header-toolbar')
    <div class="btn-group pull-right">
        <a href="{{ suitRoute("$routePrefix.create") }}" class="btn btn-fit-height btn-primary"><i class="glyphicon glyphicon-plus"></i>&nbsp;Add New</a>
    </div>
@endsection

@section('table-column-checkbox')
    <label><input type="checkbox" data-name="id">#</label>
    <label><input type="checkbox" data-name="api_id">Labbola#</label>
    <label><input type="checkbox" checked data-name="name" data-sort="asc,0">Name</label>
    <label><input type="checkbox" data-name="nick_name">Nick Name</label>
    <label><input type="checkbox" data-name="official_name">Official Name</label>
    <label><input type="checkbox" data-name="code">Code</label>
    <label><input type="checkbox" data-name="base">Base</label>
    <label><input type="checkbox" data-name="manager">Manager</label>
    <label><input type="checkbox" data-name="supporter_nick_name">Nick Name</label>
    <label><input type="checkbox" checked data-name="stadium_name">Stadium Name</label>
    <label><input type="checkbox" checked data-name="published_at">Published</label>
    <label><input type="checkbox" checked data-name="created_at">Created At</label>
    <label><input type="checkbox" data-name="updated_at">Updated At</label>
@endsection
