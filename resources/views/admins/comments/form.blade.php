@extends('admins._layouts.form-base')

@section('page-title')
    {{ $pageName }} Form
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        <a href="{{ suitRoute($routePrefix.'.index') }}">{{ $pageName }}</a>
    </li>
@endsection

@section('form-body')
{!! Form::model($model, ['prefix' => $routePrefix, 'class' => 'form-horizontal']) !!}
    <div class="form-body">
        {!! Form::suitText('video_title', 'Video Title', null, ['disabled']) !!}
        {!! Form::suitText('username', 'Username', null, ['disabled']) !!}
        {!! Form::suitText('type', 'Type', null, ['disabled']) !!}
        @if ($model->isMessage())
            {!! Form::suitTextarea('message', 'Message', null, ['disabled']) !!}
        @elseif ($model->isSticker())
            {!! Form::suitFileBrowser('sticker', 'Sticker', $model->stickerItem->image, ['disabled']) !!}
        @endif
    </div>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-offset-2 col-md-10">
                {!! Form::suitBack() !!}
            </div>
        </div>
    </div>
{!! Form::close() !!}
@endsection
