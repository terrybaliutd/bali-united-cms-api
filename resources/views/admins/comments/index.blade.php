@extends(suitViewName('_layouts.index-base'))

@section('page-title')
    {{ $pageName }}
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        {{ $pageName }}
    </li>
@endsection

@section('table-title')
    {{ $pageName }} Table
@endsection

@section('table-column-checkbox')
    <label><input type="checkbox" data-name="id">#</label>
    <label><input type="checkbox" checked data-name="video_title">Video Title</label>
    <label><input type="checkbox" checked data-name="username">Username</label>
    <label><input type="checkbox" checked data-name="type">Type</label>
    <label><input type="checkbox" checked data-name="created_at">Created At</label>
    <label><input type="checkbox" data-name="updated_at">Updated At</label>
@endsection
