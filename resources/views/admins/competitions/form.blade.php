@extends(suitViewName('_layouts.form-base'))

@section('page-title')
    {{ $pageName }} Form
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        <a href="{{ suitRoute("$routePrefix.index") }}">{{ $pageName }}</a>
    </li>
@endsection

@section('form-body')
{!! Form::suitModel($model,array('route'=>($model->exists?[suitRouteName("$routePrefix.update"),$model]:suitRouteName("$routePrefix.store")), 'method'=>($model->exists?'PUT':'POST'))) !!}
    @yield('head-action')
    <div class="form-body">
        {!! Form::suitText('api_id', 'Labbola ID') !!}
        {!! Form::suitText('name', 'Name') !!}
        {!! Form::suitText('long_name', 'Long Name') !!}
        {!! Form::suitText('slug', 'Slug', null, ['info' => 'Auto generated if being left blank']) !!}
        {!! Form::suitNumber('order', 'Order') !!}
        {!! Form::suitSelect('is_default', 'Is Default', ['No', 'Yes']) !!}
        {!! Form::suitSelect('published', 'Published', ['No', 'Yes']) !!}
    </div>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-offset-3 col-md-9">
                {!! Form::suitSubmit() !!}
                {!! Form::suitReset() !!}
                {!! Form::suitBack() !!}
            </div>
        </div>
        @yield('other-action')
    </div>
{!! Form::close() !!}
@endsection
