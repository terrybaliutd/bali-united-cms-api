@extends(suitViewName('_layouts.form-base'))

@section('page-title')
    {{ $pageName }} Form
@stop

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        <a href="{{ suitRoute("$routePrefix.index") }}">{{ $pageName }}</a>
    </li>
@stop

@section('form-body')
<template id="gallery-item-template">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box yellow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-grid"></i>
                        Gallery Item # {#}
                    </div>
                    <div class="tools">
                        <a href="javascript:void(0)" class="remove"></a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <div class="form-body">
                        {!! Form::suitText("items[{#}][title]", 'Title') !!}
                        {!! Form::suitSelect("items[{#}][type]", 'Media Type', $typeList) !!}
                        {!! Form::suitFileBrowser("items[{#}][attachment]", 'Media File', null, ['info' => 'Form Image Minimum 1280x720. or Youtube Url, e.g. https://www.youtube.com/watch?v=QVYOXyvuBdw']) !!}
                        {!! Form::suitSelect("items[{#}][featured]", 'Featured', ['No', 'Yes']) !!}
                        {!! Form::suitSelect("items[{#}][published]", 'Published', ['No', 'Yes']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</template>
{!! Form::suitModel($model,array('route'=>($model->exists?[suitRouteName("$routePrefix.update"),$model]:suitRouteName("$routePrefix.store")), 'method'=>($model->exists?'PUT':'POST'))) !!}
    <div class="form-body">
        {!! Form::suitSelect('parent_id', 'Parent Category', $parentList, '-- No Parent --') !!}
        {!! Form::suitText('title', 'Title') !!}
        {!! Form::suitFileBrowser('image', 'Image') !!}
        {!! Form::suitSelect('published', 'Published', ['No', 'Yes']) !!}
        <div id="gallery-item-container">
            @foreach ($model->items as $i => $item)
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet box yellow">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-grid"></i>
                                Gallery Item # {{ ++$i }}
                            </div>
                            <div class="tools">
                                <a href="javascript:void(0)" class="remove"></a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <div class="form-body">
                                {!! Form::hidden("items[{$i}][id]", $item->id) !!}
                                {!! Form::suitText("items[{$i}][title]", 'Title', $item['title']) !!}
                                {!! Form::suitSelect("items[{$i}][type]", 'Media Type', $typeList, null, $item->type) !!}
                                {!! Form::suitFileBrowser("items[{$i}][attachment]", 'Media File', $item->attachment, ['info' => 'Form Image Minimum 1280x720. or Youtube Url, e.g. https://www.youtube.com/watch?v=QVYOXyvuBdw']) !!}
                                {!! Form::suitSelect("items[{$i}][featured]", 'Featured', ['No', 'Yes'], null, $item->featured) !!}
                                {!! Form::suitSelect("items[{$i}][published]", 'Published', ['No', 'Yes'], null, $item->published) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            @if(is_array(Input::old('items')))
                @foreach(Input::old('items') as $i => $item)
                @if (!array_key_exists('id', $item))
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet box yellow">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-grid"></i>
                                    Gallery Item # {{ $i }}
                                </div>
                                <div class="tools">
                                    <a href="javascript:void(0)" class="remove"></a>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <div class="form-body">
                                    {!! Form::suitText("items[{$i}][title]", 'Title', $item['title']) !!}
                                    {!! Form::suitSelect("items[{$i}][type]", 'Media Type', $typeList, null, $item['type']) !!}
                                    {!! Form::suitFileBrowser("items[{$i}][attachment]", 'Media File', $item['attachment'], ['info' => 'Form Image Minimum 1280x720. or Youtube Url, e.g. https://www.youtube.com/watch?v=QVYOXyvuBdw']) !!}
                                    {!! Form::suitSelect("items[{$i}][featured]", 'Featured', ['No', 'Yes'], null, $item['featured']) !!}
                                    {!! Form::suitSelect("items[{$i}][published]", 'Published', ['No', 'Yes'], null, $item['published']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                @endforeach
            @endif
        </div>
        <a href="#" id="gallery-item-button" class="btn btn-primary">Add New Gallery Item</a>
    </div>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-offset-3 col-md-9">
                {!! Form::suitSubmit() !!}
                {!! Form::suitReset() !!}
                {!! Form::suitBack() !!}
            </div>
        </div>
    </div>
{!! Form::close() !!}
@stop

@section('page-scripts')
    @parent
    <script type="text/javascript">
        jQuery('#gallery-item-container .match-id-button').live("click", function(e){
            e.preventDefault();
            console.log('click');
            var container = jQuery(this).parent();
            var manualContainer = container.next('.manual-match-container');
            container.remove();
            manualContainer.show();
        });
        function initTemplate(template, container, button) {
            var container = $(container),
                template = $(template)[0].innerHTML;
            $(button).click(function (e) {
                e.preventDefault();
                var tempTemplate = template.replace(new RegExp('{#}', 'g'), container.children().length + 1);
                container.append($(tempTemplate));
                var multiSelect = container.find('select');
                multiSelect.select2();
            });
        }
        initTemplate('#gallery-item-template', '#gallery-item-container', '#gallery-item-button');
    </script>
@endsection