@extends('_layouts.login')

@section('page-styles')
    @parent
    <link rel="stylesheet" href="{{ asset('assets/admin/css/login.css') }}">
@endsection

@section('page-scripts')
    @parent
    <script src="{{ asset('assets/admin/js/login.js') }}" type="text/javascript"></script>
@endsection

@section('init-scripts')
    @parent
    Login.init();
@endsection

@section('page-content')
    {{-- BEGIN LOGIN FORM --}}
    {!! Form::open(['route' => suitRouteName('login'), 'class' => 'login-form']) !!}
        <h3 class="form-title">Sign In</h3>

        @include('admins._partials.flash')

        <div class="form-group">
            <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
            <label class="control-label visible-ie8 visible-ie9">Username</label>
            <input class="form-control form-control-solid placeholder-no-fix" type="text" autocomplete="off" placeholder="Username or Email" name="username" value="{{ \Input::old('username') }}"/>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Password</label>
            <input class="form-control form-control-solid placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password"/>
        </div>
        <div class="form-group">
            <label class="control-label visible-ie8 visible-ie9">Captcha</label>
            <img src="{{ Captcha::url() }}" width="100%" alt="Captcha">
            <input type="text" class="form-control form-control-solid placeholder-no-fix" name="captcha" placeholder="Enter Captcha"value="">
        </div>
        <div class="form-actions">
            <input type="submit" class="btn btn-success uppercase" value="Login"/>
            <a href="javascript:;" id="forget-password" class="forget-password">Forgot Password?</a>
        </div>
    {!! Form::close() !!}
    {{-- END LOGIN FORM --}}
    {{-- BEGIN FORGOT PASSWORD FORM --}}
    {!! Form::open(['action' => 'Admin\RemindersController@postRemind', 'class' => 'forget-form']) !!}
        <h3>Forget Password ?</h3>
        <p>
             Enter your e-mail address below to reset your password.
        </p>
        <div class="form-group">
            <input class="form-control placeholder-no-fix" type="text" autocomplete="off" placeholder="Email" name="email"/>
        </div>
        <div class="form-actions">
            <button type="button" id="back-btn" class="btn btn-default">Back</button>
            <button type="submit" class="btn btn-success uppercase pull-right">Submit</button>
        </div>
    {!! Form::close() !!}
    {{-- END FORGOT PASSWORD FORM --}}
@endsection
