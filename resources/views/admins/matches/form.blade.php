@extends(suitViewName('_layouts.form-base'))

@section('page-title')
    {{ $pageName }} Form
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        <a href="{{ suitRoute("$routePrefix.index") }}">{{ $pageName }}</a>
    </li>
@endsection

@section('form-body')
{{-- Highlight Template --}}
<template id="highlight-template">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box yellow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-grid"></i>
                        Highlight # {#}
                    </div>
                    <div class="tools">
                        <a href="javascript:void(0)" class="remove"></a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <div class="form-body">
                    {!! Form::suitSelect("highlights[{#}][club_type]", 'Club Type', ['home' => 'Home', 'away' => 'Away']) !!}
                    {!! Form::suitNumber("highlights[{#}][time_in_minute]", 'Minute') !!}
                    {!! Form::suitSelect("highlights[{#}][event_description]", 'Event', $events) !!}
                    {!! Form::suitText("highlights[{#}][player_name]", 'Player Name') !!}
                    {!! Form::suitText("highlights[{#}][support_player_name]", 'Support Player Name') !!}
                    {!! Form::suitText("highlights[{#}][stage_name]", 'Stage Name') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</template>

{{-- Home club player lineup template --}}
<template id="home-club-player-template">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box yellow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-grid"></i>
                        Home Player # {#}
                    </div>
                    <div class="tools">
                        <a href="javascript:void(0)" class="remove"></a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <div class="form-body">
                    {!! Form::suitText("home_lineups[{#}][player_name]", 'Player Name') !!}
                    {!! Form::suitSelect("home_lineups[{#}][position_code]", 'Position Code', $position_codes) !!}
                    {!! Form::suitNumber("home_lineups[{#}][shirt_number]", 'Shirt Number') !!}
                    {!! Form::suitSelect("home_lineups[{#}][is_primary]", 'Starting Member', ['No', 'Yes']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</template>

{{-- Away club player lineup template --}}
<template id="away-club-player-template">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box yellow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-grid"></i>
                        Away Player # {#}
                    </div>
                    <div class="tools">
                        <a href="javascript:void(0)" class="remove"></a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <div class="form-body">
                    {!! Form::suitText("away_lineups[{#}][player_name]", 'Player Name') !!}
                    {!! Form::suitSelect("away_lineups[{#}][position_code]", 'Position Code', $position_codes) !!}
                    {!! Form::suitNumber("away_lineups[{#}][shirt_number]", 'Shirt Number') !!}
                    {!! Form::suitSelect("away_lineups[{#}][is_primary]", 'Starting Member', ['No', 'Yes']) !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</template>
{!! Form::suitModel($model,array('route'=>($model->exists?[suitRouteName("$routePrefix.update"),$model]:suitRouteName("$routePrefix.store")), 'method'=>($model->exists?'PUT':'POST'))) !!}
    @yield('head-action')
    <div class="form-body">
        <ul class="nav nav-tabs" role="tablist">
            <li class="active"><a href="#general" role="tab" data-toggle="tab">General</a></li>
            <li><a href="#club-home" role="tab" data-toggle="tab">Home Club</a></li>
            <li><a href="#club-away" role="tab" data-toggle="tab">Away Club</a></li>
            <li><a href="#highlight" role="tab" data-toggle="tab">Highlight</a></li>
            <li><a href="#home-statistic" role="tab" data-toggle="tab">Home Statistic</a></li>
            <li><a href="#away-statistic" role="tab" data-toggle="tab">Away Statistic</a></li>
        </ul>
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane fade-in active" id="general">
                {!! Form::suitText('api_id', 'Api ID') !!}
                {!! Form::suitDateTime('start_at', 'Kick-off Date and Time') !!}
                {!! Form::suitSelect('competition_id', 'Competition', $competitions) !!}
                {!! Form::suitSelect('season_id', 'Season', $seasons) !!}
                {!! Form::suitSelect('live_video_id', 'Live Video', $model->video()->lists('title', 'id'), null, null, ['placeholder' => '-- No Live Video --']) !!}
                {!! Form::suitSelect('status', 'Match Status', $statusList) !!}
                {!! Form::suitText('venue', 'Venue (Stadium, Field)') !!}
                {!! Form::suitText('tv_info', 'TV Info', null, ['info' => 'For tv information. e.g. "Live On SCTV"']) !!}
                {!! Form::suitTokenField('referees', 'Referees') !!}
                {!! Form::suitNumber('audience', 'Audience') !!}
                {!! Form::suitWysiwyg('preview_match_text', 'Preview Text') !!}
                {!! Form::suitWysiwyg('post_match_text', 'Post Match Text') !!}
                {!! Form::suitNumber('prediction_home_win', 'Prediction Home') !!}
                {!! Form::suitNumber('prediction_draw', 'Prediction Draw') !!}
                {!! Form::suitNumber('prediction_away_win', 'Prediction Away') !!}
                {!! Form::suitTokenField('suspended_players', 'Suspended Players') !!}
                {!! Form::suitFileBrowser("banner", 'Banner Image', $model->attachment) !!}
                {!! Form::suitText('ticket_url', 'Ticket URL') !!}
                {!! Form::suitSelect('published', 'Published', ['No', 'Yes']) !!}
            </div>

            <div role='tabpanel' class='tab-pane fade' id='away-statistic'>
            @if ($model->away_club)
                @if ($model->away_club->statisticForm())
                    {!! Form::hidden("away_id", $model->away_club->id) !!}
                    {!! Form::suitSelect('away_club_id', 'Away Club', $clubs, null, $model->away_club->club_id) !!}
                    {!! Form::suitNumber('away_statistic_ball_possession', 'Ball Possesion', $model->away_club->statisticForm()->ball_possession ) !!}
                    {!! Form::suitNumber('away_statistic_shot_on_goal', 'Shot On Goal', $model->away_club->statisticForm()->shot_on_goal ) !!}
                    {!! Form::suitNumber('away_statistic_total_shots', 'Total Shots', $model->away_club->statisticForm()->total_shots ) !!}
                    {!! Form::suitNumber('away_statistic_passing_accuracy', 'Passing Accuracy', $model->home_club->statisticForm()->passing_accuracy ) !!}
                    {!! Form::suitNumber('away_statistic_tackle_success', 'Tackle Success', $model->away_club->statisticForm()->tackle_success ) !!}
                    {!! Form::suitNumber('away_statistic_corner', 'Corner', $model->away_club->statisticForm()->corner ) !!}
                    {!! Form::suitNumber('away_statistic_foul', 'Foul', $model->away_club->statisticForm()->foul ) !!}
                    {!! Form::suitNumber('away_statistic_offside', 'Offside', $model->away_club->statisticForm()->offside ) !!}
                    {!! Form::suitNumber('away_statistic_red_card', 'Red Card', $model->away_club->statisticForm()->red_card ) !!}
                    {!! Form::suitNumber('away_statistic_yellow_card', 'Yellow Card', $model->away_club->statisticForm()->yellow_card ) !!}
                @else
                    {!! Form::suitNumber('away_statistic_ball_possession', 'Ball Possesion') !!}
                    {!! Form::suitNumber('away_statistic_shot_on_goal', 'Shot On Goal') !!}
                    {!! Form::suitNumber('away_statistic_total_shots', 'Total Shots') !!}
                    {!! Form::suitNumber('away_statistic_passing_accuracy', 'Passing Accuracy') !!}
                    {!! Form::suitNumber('away_statistic_tackle_success', 'Tackle Success') !!}
                    {!! Form::suitNumber('away_statistic_corner', 'Corner') !!}
                    {!! Form::suitNumber('away_statistic_foul', 'Foul') !!}
                    {!! Form::suitNumber('away_statistic_offside', 'Offside') !!}
                    {!! Form::suitNumber('away_statistic_red_card', 'Red Card') !!}
                    {!! Form::suitNumber('away_statistic_yellow_card', 'Yellow Card') !!}
                @endif
            @else
                {!! Form::suitSelect('away_club_id', 'Away Club', $clubs) !!}
                {!! Form::suitNumber('away_statistic_ball_possession', 'Ball Possesion') !!}
                {!! Form::suitNumber('away_statistic_shot_on_goal', 'Shot On Goal') !!}
                {!! Form::suitNumber('away_statistic_total_shots', 'Total Shots') !!}
                {!! Form::suitNumber('away_statistic_passing_accuracy', 'Passing Accuracy') !!}
                {!! Form::suitNumber('away_statistic_tackle_success', 'Tackle Success') !!}
                {!! Form::suitNumber('away_statistic_corner', 'Corner') !!}
                {!! Form::suitNumber('away_statistic_foul', 'Foul') !!}
                {!! Form::suitNumber('away_statistic_offside', 'Offside') !!}
                {!! Form::suitNumber('away_statistic_red_card', 'Red Card') !!}
                {!! Form::suitNumber('away_statistic_yellow_card', 'Yellow Card') !!}
            @endif
            </div>
            <div role='tabpanel' class='tab-pane fade' id='home-statistic'>
            @if ($model->home_club)
                @if ($model->home_club->statisticForm())
                    {!! Form::hidden("home_id", $model->home_club->id) !!}
                    {!! Form::suitSelect('home_club_id', 'Home Club', $clubs, null, $model->home_club->club_id) !!}
                    {!! Form::suitNumber('home_statistic_ball_possession', 'Ball Possesion', $model->home_club->statisticForm()->ball_possession ) !!}
                    {!! Form::suitNumber('home_statistic_shot_on_goal', 'Shot On Goal', $model->home_club->statisticForm()->shot_on_goal ) !!}
                    {!! Form::suitNumber('home_statistic_total_shots', 'Total Shots', $model->home_club->statisticForm()->total_shots ) !!}
                    {!! Form::suitNumber('home_statistic_passing_accuracy', 'Passing Accuracy', $model->home_club->statisticForm()->passing_accuracy ) !!}
                    {!! Form::suitNumber('home_statistic_tackle_success', 'Tackle Success', $model->home_club->statisticForm()->tackle_success ) !!}
                    {!! Form::suitNumber('home_statistic_corner', 'Corner', $model->home_club->statisticForm()->corner ) !!}
                    {!! Form::suitNumber('home_statistic_foul', 'Foul', $model->home_club->statisticForm()->foul ) !!}
                    {!! Form::suitNumber('home_statistic_offside', 'Offside', $model->home_club->statisticForm()->offside ) !!}
                    {!! Form::suitNumber('home_statistic_red_card', 'Red Card', $model->home_club->statisticForm()->red_card ) !!}
                    {!! Form::suitNumber('home_statistic_yellow_card', 'Yellow Card', $model->home_club->statisticForm()->yellow_card ) !!}
                @else
                    {!! Form::suitNumber('home_statistic_ball_possession', 'Ball Possesion') !!}
                    {!! Form::suitNumber('home_statistic_shot_on_goal', 'Shot On Goal') !!}
                    {!! Form::suitNumber('home_statistic_total_shots', 'Total Shots') !!}
                    {!! Form::suitNumber('home_statistic_passing_accuracy', 'Passing Accuracy') !!}
                    {!! Form::suitNumber('home_statistic_tackle_success', 'Tackle Success') !!}
                    {!! Form::suitNumber('home_statistic_corner', 'Corner') !!}
                    {!! Form::suitNumber('home_statistic_foul', 'Foul') !!}
                    {!! Form::suitNumber('home_statistic_offside', 'Offside') !!}
                    {!! Form::suitNumber('home_statistic_red_card', 'Red Card') !!}
                    {!! Form::suitNumber('home_statistic_yellow_card', 'Yellow Card') !!}
                @endif
            @else
                {!! Form::suitSelect('home_club_id', 'Home Club', $clubs) !!}
                {!! Form::suitNumber('home_statistic_ball_possession', 'Ball Possesion') !!}
                {!! Form::suitNumber('home_statistic_shot_on_goal', 'Shot On Goal') !!}
                {!! Form::suitNumber('home_statistic_total_shots', 'Total Shots') !!}
                {!! Form::suitNumber('home_statistic_passing_accuracy', 'Passing Accuracy') !!}
                {!! Form::suitNumber('home_statistic_tackle_success', 'Tackle Success') !!}
                {!! Form::suitNumber('home_statistic_corner', 'Corner') !!}
                {!! Form::suitNumber('home_statistic_foul', 'Foul') !!}
                {!! Form::suitNumber('home_statistic_offside', 'Offside') !!}
                {!! Form::suitNumber('home_statistic_red_card', 'Red Card') !!}
                {!! Form::suitNumber('home_statistic_yellow_card', 'Yellow Card') !!}
            @endif
            </div>
            <div role="tabpanel" class="tab-pane fade" id="club-home">
            @if ($model->home_club)
                {!! Form::hidden("home_id", $model->home_club->id) !!}
                {!! Form::suitSelect('home_club_id', 'Home Club', $clubs, null, $model->home_club->club_id) !!}
                {!! Form::suitText('home_coach', 'Coach', $model->home_club->coach) !!}
                {!! Form::suitText('home_formation', 'Formation', $model->home_club->formation) !!}
                {!! Form::suitNumber('home_score', 'Score', $model->home_club->score) !!}
            @else
                {!! Form::suitSelect('home_club_id', 'Home Club', $clubs) !!}
                {!! Form::suitText('home_coach', 'Coach') !!}
                {!! Form::suitText('home_formation', 'Formation') !!}
                {!! Form::suitNumber('home_score', 'Score') !!}
            @endif
            {!! Form::suitSection('Match Lineup') !!}
            <div id="home-club-player-container">
            @if ($model->home_club)
                @foreach ($model->home_club->lineups as $i => $lineup)
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet box yellow">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-grid"></i>
                                    Home Player # {{ ++$i }}
                                </div>
                                <div class="tools">
                                    <a href="javascript:void(0)" class="remove"></a>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <div class="form-body">
                                {!! Form::hidden("home_lineups[{$i}][id]", $lineup->id) !!}
                                {!! Form::suitText("home_lineups[{$i}][player_name]", 'Player Name', $lineup->player_name) !!}
                                {!! Form::suitSelect("home_lineups[{$i}][position_code]", 'Position Code', $position_codes, null, $lineup->position_code) !!}
                                {!! Form::suitNumber("home_lineups[{$i}][shirt_number]", 'Shirt Number', $lineup->shirt_number) !!}
                                {!! Form::suitSelect("home_lineups[{$i}][is_primary]", 'Starting Member', ['No', 'Yes'], null, $lineup->is_primary) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            @endif
                @if (is_array(Input::old('home_lineups')))
                    @foreach (Input::old('home_lineups') as $i => $lineup)
                    @if (!isset($lineup['id']))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box yellow">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-grid"></i>
                                        Home Player # {{ $i }}
                                    </div>
                                    <div class="tools">
                                        <a href="javascript:void(0)" class="remove"></a>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <div class="form-body">
                                    {!! Form::suitText("home_lineups[{$i}][player_name]", 'Player Name') !!}
                                    {!! Form::suitSelect("home_lineups[{$i}][position_code]", 'Position Code', $position_codes) !!}
                                    {!! Form::suitNumber("home_lineups[{$i}][shirt_number]", 'Shirt Number') !!}
                                    {!! Form::suitSelect("home_lineups[{$i}][is_primary]", 'Starting Member', ['No', 'Yes']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    @endforeach
                @endif
            </div>
            <a href="#" id="home-club-player-button" class="btn btn-primary">Add New Player</a>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="club-away">
            @if ($model->away_club)
                {!! Form::hidden("away_id", $model->away_club->id) !!}
                {!! Form::suitSelect('away_club_id', 'Away Club', $clubs, null, $model->away_club->club_id) !!}
                {!! Form::suitText('away_coach', 'Coach', $model->away_club->coach) !!}
                {!! Form::suitText('away_formation', 'Formation', $model->away_club->formation) !!}
                {!! Form::suitNumber('away_score', 'Score', $model->away_club->score) !!}
            @else
                {!! Form::suitSelect('away_club_id', 'Away Club', $clubs) !!}
                {!! Form::suitText('away_coach', 'Coach') !!}
                {!! Form::suitText('away_formation', 'Formation') !!}
                {!! Form::suitNumber('away_score', 'Score') !!}
            @endif
            {!! Form::suitSection('Match Lineup') !!}
            <div id="away-club-player-container">
            @if ($model->away_club)
                @foreach ($model->away_club->lineups as $i => $lineup)
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet box yellow">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-grid"></i>
                                    Away Player # {{ ++$i }}
                                </div>
                                <div class="tools">
                                    <a href="javascript:void(0)" class="remove"></a>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <div class="form-body">
                                {!! Form::hidden("away_lineups[{$i}][id]", $lineup->id) !!}
                                {!! Form::suitText("away_lineups[{$i}][player_name]", 'Player Name', $lineup->player_name) !!}
                                {!! Form::suitSelect("away_lineups[{$i}][position_code]", 'Position Code', $position_codes, null, $lineup->position_code) !!}
                                {!! Form::suitNumber("away_lineups[{$i}][shirt_number]", 'Shirt Number', $lineup->shirt_number) !!}
                                {!! Form::suitSelect("away_lineups[{$i}][is_primary]", 'Starting Member', ['No', 'Yes'], null, $lineup->is_primary) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            @endif
                @if (is_array(Input::old('away_lineups')))
                    @foreach (Input::old('away_lineups') as $i => $lineup)
                    @if (!isset($lineup['id']))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box yellow">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-grid"></i>
                                        Away Player # {{ $i }}
                                    </div>
                                    <div class="tools">
                                        <a href="javascript:void(0)" class="remove"></a>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <div class="form-body">
                                    {!! Form::suitText("away_lineups[{$i}][player_name]", 'Player Name') !!}
                                    {!! Form::suitSelect("away_lineups[{$i}][position_code]", 'Position Code', $position_codes) !!}
                                    {!! Form::suitNumber("away_lineups[{$i}][shirt_number]", 'Shirt Number') !!}
                                    {!! Form::suitSelect("away_lineups[{$i}][is_primary]", 'Starting Member', ['No', 'Yes']) !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    @endforeach
                @endif
            </div>
            <a href="#" id="away-club-player-button" class="btn btn-primary">Add New Player</a>
            </div>
            <div role="tabpanel" class="tab-pane fade" id="highlight">
                <div id="highlight-container">
                    <?php $nb=0 ?>
                    @foreach ($model->highlights->sortBy('time_in_second') as $i => $highlight)
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box yellow">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-grid"></i>
                                        Highlight # {{ ++$nb }}
                                    </div>
                                    <div class="tools">
                                        <a href="javascript:void(0)" class="remove"></a>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <div class="form-body">
                                    {!! Form::hidden("highlights[{$i}][id]", $highlight->id) !!}
                                    {!! Form::suitSelect("highlights[{$i}][club_type]", 'Club Type', ['home' => 'Home', 'away' => 'Away'], null, $highlight->club_type) !!}
                                    {!! Form::suitNumber("highlights[{$i}][time_in_minute]", 'Minute', $highlight->time_in_minute) !!}
                                    {!! Form::suitSelect("highlights[{$i}][event_description]", 'Event', $events, null, $highlight->event_description) !!}
                                    {!! Form::suitText("highlights[{$i}][player_name]", 'Player Name', $highlight->player_name) !!}
                                    {!! Form::suitText("highlights[{$i}][support_player_name]", 'Support Player Name', $highlight->support_player_name) !!}
                                    {!! Form::suitText("highlights[{$i}][stage_name]", 'Stage Name', $highlight->stage_name) !!}

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endforeach
                @if (is_array(Input::old('highlights')))
                    @foreach(Input::old('highlights') as $i => $highlight)
                    @if (!isset($highlight['id']))
                    <div class="row">
                        <div class="col-md-12">
                            <div class="portlet box yellow">
                                <div class="portlet-title">
                                    <div class="caption">
                                        <i class="fa fa-grid"></i>
                                        Highlight # {{ $i }}
                                    </div>
                                    <div class="tools">
                                        <a href="javascript:void(0)" class="remove"></a>
                                    </div>
                                </div>
                                <div class="portlet-body form">
                                    <div class="form-body">
                                    {!! Form::suitSelect("highlights[{$i}][club_type]", 'Club Type', ['home' => 'Home', 'away' => 'Away']) !!}
                                    {!! Form::suitNumber("highlights[{$i}][time_in_minute]", 'Minute') !!}
                                    {!! Form::suitSelect("highlights[{$i}][event_description]", 'Event', $events) !!}
                                    {!! Form::suitText("highlights[{$i}][player_name]", 'Player Name') !!}
                                    {!! Form::suitText("highlights[{$i}][support_player_name]", 'Support Player Name') !!}
                                    {!! Form::suitText("highlights[{$i}][stage_name]", 'Stage Name') !!}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    @endif
                    @endforeach
                @endif
                </div>
                <a href="#" id="highlight-button" class="btn btn-primary">Add New Highlight</a>
            </div>
        </div>
    </div>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-offset-3 col-md-9">
                {!! Form::suitSubmit() !!}
                {!! Form::suitReset() !!}
                {!! Form::suitBack() !!}
            </div>
        </div>
    </div>
{!! Form::close() !!}
@endsection

@section('page-scripts')
    @parent
    <script type="text/javascript">
        FormField.initTemplate('#highlight-template', '#highlight-container', '#highlight-button');
        FormField.initTemplate('#home-club-player-template', '#home-club-player-container', '#home-club-player-button');
        FormField.initTemplate('#away-club-player-template', '#away-club-player-container', '#away-club-player-button');
        FormField.initAjaxSelect('{{ route(\Config::get('suitcms.prefix_url').'.season.list', ['competition_id' => '']) }}', '#competition_id', '#season_id');
    </script>
@endsection
