@extends(suitViewName('_layouts.index-base'))

@section('page-title')
    {{ $pageName }}
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        {{ $pageName }}
    </li>
@endsection

@section('table-title')
    {{ $pageName }} Table
@endsection

{{-- @if (\Auth::user()->isSuper()) --}}
@section('page-header-toolbar')
    <div class="btn-group pull-right">
        <a href="{{ suitRoute("$routePrefix.create") }}" class="btn btn-fit-height btn-primary"><i class="glyphicon glyphicon-plus"></i>&nbsp;Add New</a>
    </div>
@endsection
{{-- @endif --}}

@section('table-column-checkbox')
    <label><input type="checkbox" data-name="id" data-searchable="false">#</label>
    <label><input type="checkbox" data-name="api_id">#LabBola</label>
    <label><input type="checkbox" checked data-name="start_at">Start At</label>
    <label><input type="checkbox" checked data-name="competition.long_name">Competition</label>
    <label><input type="checkbox" data-name="group.name">Group</label>
    <label><input type="checkbox" checked data-name="published_at">Published</label>
    <label><input type="checkbox" data-name="created_at">Created At</label>
    <label><input type="checkbox" data-name="updated_at">Updated At</label>
@endsection

@section('table-filter')
    <div class="row">
        <div class="col-md-4">
            {!! Form::select('competitions', $competitions->toArray(), '', ['class' => 'form-control select2me', 'placeholder' => 'Select Competition', 'data-column-filter' => 'competition']) !!}
        </div>
        {{-- <div class="col-md-4">
            {!! Form::select('clubs', $clubs->toArray(), '', ['class' => 'form-control select2me', 'placeholder' => 'Select Home Club', 'data-column-filter' => 'homeClub']) !!}
        </div>
        <div class="col-md-4">
            {!! Form::select('clubs', $clubs->toArray(), '', ['class' => 'form-control select2me', 'placeholder' => 'Select Away Club', 'data-column-filter' => 'awayClub']) !!}
        </div> --}}
    </div>
    <hr>
@endsection
