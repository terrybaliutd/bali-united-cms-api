@extends('admins._layouts.form-base')

@section('form-title')
    {{ $pageName }}
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        <a href="{{ suitRoute("$routePrefix.index") }}">{{ $pageName }}</a>
    </li>
@endsection

@section('form-body')

<h3>Membership</h3>
{!! Form::suitModel($model, ['prefix' => $routePrefix]) !!}
    <div class="form-body">
        {!! Form::suitText('name', 'Name') !!}
        {!! Form::suitText('prefix', 'Prefix') !!}
        {!! Form::suitText('ribbon_path', 'Ribbon Image Path (Full)') !!}
        {!! Form::suitText('text_label', 'Text Label') !!}
        {!! Form::suitSelect("is_reward", 'Reward Active', ['No', 'Yes']) !!}
    </div>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-offset-2 col-md-10">
                {!! Form::suitSubmit() !!}
                {!! Form::suitReset() !!}
                {!! Form::suitBack() !!}
            </div>
        </div>
    </div>
{!! Form::close() !!}

@endsection
