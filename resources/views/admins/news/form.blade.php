@extends(suitViewName('_layouts.form-base'))

@section('page-title')
    {{ $pageName }} Form
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        <a href="{{ suitRoute("$routePrefix.index") }}">{{ $pageName }}</a>
    </li>
@endsection

@section('form-body')

{!! Form::suitModel($model,array('route'=>($model->exists?[suitRouteName("$routePrefix.update"),$model]:suitRouteName("$routePrefix.store")), 'method'=>($model->exists?'PUT':'POST'))) !!}
    <div class="form-body">
        {!! Form::suitText('title', 'Title') !!}
        {!! Form::suitSelect('published', 'Published', ['No', 'Yes']) !!}
        {!! Form::suitDateTime('news_date', 'News Date') !!}
        {!! Form::suitSelect('featured', 'Featured', ['No', 'Yes'], null, null, ['info' => 'Featured news will be shown on home screen']) !!}
        {!! Form::suitText('slug', 'Slug', null, ['info' => 'Leave blank will auto-generate slug from title']) !!}
        {!! Form::suitWysiwyg('description', 'Description') !!}
        {!! Form::suitWysiwyg('content', 'Content') !!}
        {!! Form::suitFileBrowser('image', 'News Image') !!}
        {!! Form::suitSelect('membership_id', 'Membership Content', $memberships, null, null, ['placeholder' => '-- Membership Content --']) !!}
        {!! Form::suitTokenField('tags', 'Tags', $model->getStringTags()) !!}
    </div>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-offset-3 col-md-9">
                {!! Form::suitSubmit() !!}
                {!! Form::suitReset() !!}
                {!! Form::suitBack() !!}
            </div>
        </div>
    </div>
{!! Form::close() !!}
@endsection
