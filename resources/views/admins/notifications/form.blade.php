@extends(suitViewName('_layouts.form-base'))

@section('page-title')
    {{ $pageName }} Form
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        <a href="{{ suitRoute("$routePrefix.index") }}">{{ $pageName }}</a>
    </li>
@endsection

@section('form-body')
{!! Form::suitModel($model,array('route'=>($model->exists?[suitRouteName("$routePrefix.update"),$model]:suitRouteName("$routePrefix.store")), 'method'=>($model->exists?'PUT':'POST'))) !!}
    @yield('head-action')
    <div class="form-body">
        {!! Form::suitText('user_id', 'User Id') !!}
        {!! Form::suitSelect('latest_match', 'Latest Match', ['No', 'Yes']) !!}
        {!! Form::suitSelect('upcoming_match', 'Upcoming Match', ['No', 'Yes']) !!}
        {!! Form::suitSelect('new_news', 'New News', ['No', 'Yes']) !!}
        {!! Form::suitSelect('new_video', 'New Video', ['No', 'Yes']) !!}
        {!! Form::suitSelect('live_video', 'Live Video', ['No', 'Yes']) !!}
    </div>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-offset-3 col-md-9">
                {!! Form::suitSubmit() !!}
                {!! Form::suitReset() !!}
                {!! Form::suitBack() !!}
            </div>
        </div>
        @yield('other-action')
    </div>
{!! Form::close() !!}
@endsection
