@extends(suitViewName('_layouts.index-base'))

@section('page-title')
    {{ $pageName }}
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        {{ $pageName }}
    </li>
@endsection

@section('table-title')
    {{ $pageName }} Table
@endsection

@section('table-column-checkbox')
    <label><input type="checkbox" data-name="id">#</label>
    <label><input type="checkbox" checked data-name="user_id">User Id</label>
    <label><input type="checkbox" checked data-name="latest_match">Latest Match</label>
    <label><input type="checkbox" checked data-name="upcoming_match">Upcoming Match</label>
    <label><input type="checkbox" checked data-name="new_news">New News</label>
    <label><input type="checkbox" checked data-name="new_video">New Video</label>
    <label><input type="checkbox" checked data-name="live_video">Live Video</label>
    <label><input type="checkbox" checked data-name="created_at">Created At</label>
    <label><input type="checkbox" data-name="updated_at">Updated At</label>
@endsection
