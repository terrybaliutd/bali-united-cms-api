@extends(suitViewName('_layouts.form-base'))

@section('page-title')
    {{ $pageName }} Form
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        <a href="{{ suitRoute("$routePrefix.index") }}">{{ $pageName }}</a>
    </li>
@endsection

@section('form-body')

{!! Form::suitModel($model,array('route'=>($model->exists?[suitRouteName("$routePrefix.update"),$model]:suitRouteName("$routePrefix.store")), 'method'=>($model->exists?'PUT':'POST'))) !!}
    <div class="form-body">
        {!! Form::suitText('api_id', 'Labbola ID') !!}
        {!! Form::suitText('name', 'Full Name') !!}
        {!! Form::suitText('short_name', 'Short Name') !!}
        {!! Form::suitText('slug', 'Slug', null, ['info' => 'Leave blank to auto generate from name']) !!}
        {!! Form::suitSelect('position_code', 'Position Code', $position_codes) !!}
        {!! Form::suitNumber('shirt_number', 'Shirt Number') !!}
        {!! Form::suitText('birth_place', 'Birth Place') !!}
        {!! Form::suitDate('birth_date', 'Birth Date') !!}
        {!! Form::suitText('nationality', 'Nationality') !!}

        {!! Form::suitFileBrowser('photo_profile', 'Profile Photo') !!}
        {!! Form::suitFileBrowser('photo_action', 'Action Photo') !!}
        {!! Form::suitSelect('published', 'Published', ['No', 'Yes']) !!}
    </div>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-offset-3 col-md-9">
                {!! Form::suitSubmit() !!}
                {!! Form::suitReset() !!}
                {!! Form::suitBack() !!}
            </div>
        </div>
    </div>
{!! Form::close() !!}
@endsection
