@extends('admins._layouts.form-base')

@section('form-title')
    {{ $pageName }}
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        <a href="{{ suitRoute("$routePrefix.index") }}"">{{ $pageName }}</a>
    </li>
@endsection

@section('form-body')
@if(empty($url))
    {{$url = null}}
@endif

<h3> Generate Playlist Data from URL</h3>
{!! Form::suitOpen([ 'url' => suitPath('playlists/create'), 'method' => 'POST']) !!}
    {!! Form::suitText('url', 'URL', $url ? $url : old('url')) !!}
    <div class="col-md-offset-2 col-md-10">
        {!! Form::suitSubmit() !!}
    </div>
    </div>
    {!! Form::close() !!}
    <br>
    <br>

@if($model->key != null)
<h3>Playlist Data</h3>
{!! Form::suitModel($model, ['prefix' => $routePrefix]) !!}
    <div class="form-body">
        {!! Form::suitText('key', 'Key', null, [ 'readonly']) !!}
        {!! Form::suitText('title', 'Title') !!}
        {!! Form::suitNumber('position', 'Position') !!}
        {!! Form::suitText('video_count', "Video Count", $model->videos->count() , [ 'readonly']) !!}
        {!! Form::suitSelect("published", 'Published', ['No', 'Yes']) !!}
        {!! Form::suitFileBrowser("attachment", 'Image', $model->attachment) !!}
        {!! Form::suitTextarea('description', 'Description') !!}
    </div>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-offset-2 col-md-10">
                {!! Form::suitSubmit() !!}
                {!! Form::suitReset() !!}
                {!! Form::suitBack() !!}
            </div>
        </div>
    </div>
{!! Form::close() !!}
@endif

@endsection
