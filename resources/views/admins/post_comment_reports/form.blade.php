@extends('admins._layouts.form-base')

@section('page-title')
    {{ $pageName }} Form
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        <a href="{{ suitRoute($routePrefix.'.index') }}">{{ $pageName }}</a>
    </li>
@endsection

@section('form-body')
{!! Form::suitModel($model, ['prefix' => $routePrefix, 'class' => 'form-horizontal']) !!}
    <div class="form-body">
        {!! Form::suitText('user_id', 'User', $model->user->name, ['disabled']) !!}
        {!! Form::suitText('postComment_id', 'Post Comment', $model->postComment->content, ['disabled']) !!}
        {!! Form::suitText('post_id', 'Post Title', $model->postComment->post->title, ['disabled']) !!}
        {!! Form::suitText('type', 'Type', null, ['disabled']) !!}
        {!! Form::suitText('reason', 'Reason', null, ['disabled']) !!}
    </div>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-offset-2 col-md-10">
                {!! Form::suitBack() !!}
            </div>
        </div>
    </div>
{!! Form::close() !!}
@endsection
