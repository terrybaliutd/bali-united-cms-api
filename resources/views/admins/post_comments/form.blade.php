@extends(suitViewName('_layouts.form-base'))

@section('page-title')
    {{ $pageName }} Form
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        <a href="{{ suitRoute("posts.index") }}">Post</a>
    </li>
    <li>
        <i class="fa fa-angle-right"></i>
        <a href="{{ suitRoute("posts.show", $post) }}">{{ $post->title }}</a>
    </li>
@endsection

@section('form-body')
{!! Form::suitModel($model, ['prefix' => $routePrefix, 'class' => 'form-horizontal']) !!}
    <div class="form-body">
        {!! Form::hidden('user_id', $user->id) !!}
        {!! Form::hidden('post_id', $post->id) !!}
        @if ($model->exists)
            {!! Form::suitText('user_id', 'User', $post->owner->name, ['disabled']) !!}
            {!! Form::suitText('post_id', 'Post', $post->title, ['disabled']) !!}
            {!! Form::suitText('parent_id', 'Parent', $model->parent ? $model->parent->content : '-', ['disabled']) !!}
            {!! Form::suitTextarea('content', 'content', null) !!}
        @else
            {!! Form::suitSelect('parent_id', 'Parent', $postComments) !!}
            {!! Form::suitTextarea('content', 'Comment') !!}
        @endif
    </div>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-offset-2 col-md-10">
                    {!! Form::suitSubmit() !!}
                    {!! Form::suitReset() !!}
                    {!! Form::suitBack() !!}
            </div>
        </div>
    </div>
{!! Form::close() !!}
@endsection
