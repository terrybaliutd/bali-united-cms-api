{
    "draw": {{ \Input::get('draw', 1) }},
    "recordsTotal": {{ $total }},
    "recordsFiltered": {{ $models->total() }},
    "data" : [
        @foreach($models as $key => $model)
        [
            "{{ $model->getKey() }}",
            "{{ $model->user ? stringJson($model->user->name) : '-' }}",
            "{{ $model->parent ? stringJson($model->parent->content) : '-' }}",
            "{{ stringJson($model->content) }}",
            "{{ $model->created_at }}",
            "{{ $model->updated_at }}",
            "{!! sprintf(
                "<a href='%s' class='btn default btn-xs green' data-action='update'><i class='fa fa-edit'></i> Edit </a><a href='%s' class='btn default btn-xs red delete' data-action='delete' data-token='%s'><i class='fa fa-trash-o'></i> Delete </a>",
                 suitRoute("$routePrefix.edit", $model),
                 suitRoute("$routePrefix.destroy", $model),
                 \Session::token()
             )!!}"
        ]{{ ($models->count()-1 === $key) ? '' : ',' }}
        @endforeach
    ]
}
