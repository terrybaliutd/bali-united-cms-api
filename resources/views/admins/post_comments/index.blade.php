@extends(suitViewName('_layouts.index-base'))

@section('page-title')
    <a href="{{ suitRoute("posts.index") }}" class="btn btn-icon-only gray-cascade" title="Back to Competition List">
        <i class="fa fa-arrow-left"></i>
    </a>
    {{ $post->title }}
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        <a href="{{ suitRoute('posts.show', $post) }}">{{ $post->title }}</a>
    </li>
    <li>
        <i class="fa fa-angle-right"></i>
        {{ $pageName }}s
    </li>
@endsection

@section('page-content')
    <div class="row">
        <div class="col-md-12">
            <div class="portlet light">
                <ul class="nav nav-tabs ">
                    <li><a href="{{ suitRoute('posts.show', $post) }}">Detail</a></li>
                    <li class="active"><a href="#">Comments</a></li>
                </ul>
                <div class="note note-info" style="margin-bottom: 5px">
                    List of post comments
                </div>
                <div class="portlet-title">
                    <div class="caption">
                        <a href="{{ suitRoute('post-comments.create', $post) }}" class="btn btn-success btn-sm" role="button">
                            <i class="glyphicon glyphicon-plus"></i> Add New Comment
                        </a>
                    </div>
                    <div class="actions">
                        <div class="btn-group">
                            <a class="btn default" href="#" data-toggle="dropdown">
                                Columns <i class="fa fa-angle-down"></i>
                            </a>
                            <div id="hide_show_column_column_toggler" class="dropdown-menu hold-on-click dropdown-checkboxes pull-right">
                                    <label><input type="checkbox" data-name="id">#</label>
                                    <label><input type="checkbox" checked data-name="name">User</label>
                                    <label><input type="checkbox" checked data-name="parent">Parent</label>
                                    <label><input type="checkbox" checked data-name="content">Content</label>
                                    <label><input type="checkbox" data-name="updated_at">Updated Date</label>
                                    <label><input type="checkbox" data-name="created_at">Created Date</label>
                            </div>
                        </div>
                    </div>
                    <div class="tools"></div>
                </div>

                <div class="portlet-body">
                    <div class="tab-content">
                        <div class="tab-pane active">
                            <table class="table table-striped table-bordered table-hover table-condensed" id="hide_show_column">
                                <thead>
                                <tr id="table-header-row">
                                    @yield('table-column-header')
                                </tr>
                                </thead>

                                <tbody>
                                @yield('table-rows')
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

