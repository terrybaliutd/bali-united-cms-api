@extends('admins._layouts.index-base')

@section('page-title')
    {{ $pageName }} Form
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i> {{ $pageName }}
    </li>
@endsection

@section('page-header-toolbar')
    <div class="btn-group pull-right">
    </div>
@endsection

@section('table-title')
    {{ $pageName }} Table
@endsection

@section('table-column-checkbox')
    <label><input type="checkbox" data-name="id">#</label>
    <label><input type="checkbox" checked data-name="user_id">User</label>
    <label><input type="checkbox" checked data-name="post_id">Post Title</label>
    <label><input type="checkbox" checked data-name="type">Type</label>
    <label><input type="checkbox" checked data-name="reason">Reason</label>
    <label><input type="checkbox" data-name="updated_at">Updated Date</label>
    <label><input type="checkbox" data-name="created_at">Created Date</label>
@endsection
