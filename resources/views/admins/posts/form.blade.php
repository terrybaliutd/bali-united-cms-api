@extends('admins._layouts.form-base')

@section('page-title')
    {{ $pageName }} Form
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        <a href="{{ suitRoute($routePrefix.'.index') }}">{{ $pageName }}</a>
    </li>
@endsection

@section('form-body')
{!! Form::suitModel($model, ['prefix' => $routePrefix, 'class' => 'form-horizontal']) !!}
    <div class="form-body">
        {!! Form::suitSelect('owner_id', 'Owner', $users) !!}
        {!! Form::suitSelect('type', 'Type', $types) !!}
        {!! Form::suitText('title', 'Title') !!}
        {!! Form::suitText('slug', 'Slug', null, ['info' => 'Leave blank will auto-generate slug from title']) !!}
        {!! Form::suitFileBrowser('image', 'Image', $model->attachment) !!}
        {!! Form::suitTextarea('excerpt', 'Excerpt') !!}
        {!! Form::suitWysiwyg('content', 'Content') !!}
        {!! Form::suitText('video_key', 'Video Key') !!}
        {!! Form::suitText('video_status', 'Video Status', null, ['disabled']) !!}
        {!! Form::suitNumber('total_like', 'Total Like', null, ['disabled']) !!}
        {!! Form::suitSelect('published', 'Published', ['No', 'Yes']) !!}
        {!! Form::suitMultiSelect('tags[]', 'Tags', $tags) !!}
    </div>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-offset-2 col-md-10">
                {!! Form::suitSubmit() !!}
                {!! Form::suitReset() !!}
                {!! Form::suitBack() !!}
            </div>
        </div>
    </div>
{!! Form::close() !!}
@endsection
