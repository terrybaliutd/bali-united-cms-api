{
    "draw": {{ \Input::get('draw', 1) }},
    "recordsTotal": {{ $total }},
    "recordsFiltered": {{ $models->total() }},
    "data" : [
        @foreach($models as $key => $model)
        [
            "{{ $model->getKey() }}",
            "{{ $model->owner ? stringJson($model->owner->name) : '-' }}",
            "{{ $model->type }}",
            "{{ $model->title }}",
            "{{ $model->slug }}",
            "{{ $model->excerpt }}",
            "{{ $model->video_url }}",
            "{{ $model->total_like }}",
            "{{ $model->total_comment }}",
            @if ($model->published)
                "<span class=\"label label-success\">Published</span>",
            @else
                "<span class=\"label label-danger\">Unpublished</span>",
            @endif
            "{{ $model->created_at}}",
            "{{ $model->updated_at}}",
            "{!! sprintf(
                "<a href='%s' class='btn default btn-xs blue-chambray' data-action='show'><i class='fa fa-eye'></i> View </a><a href='%s' class='btn default btn-xs green' data-action='update'><i class='fa fa-edit'></i> Edit </a><a href='%s' class='btn default btn-xs red delete' data-action='delete' data-token='%s'><i class='fa fa-trash-o'></i> Delete </a>",
                 suitRoute("$routePrefix.show", $model),
                 suitRoute("$routePrefix.edit", $model),
                 suitRoute("$routePrefix.destroy", $model),
                 \Session::token()
             )!!}"
        ]{{ ($models->count()-1 === $key)?'':',' }}
        @endforeach
    ]
}

