@extends(suitViewName('_layouts.show-base'))

@section('page-title')
    <a href="{{ suitRoute("posts.index") }}" class="btn btn-icon-only gray-cascade" title="Back to Post List">
        <i class="fa fa-arrow-left"></i>
    </a>
    {{ $post->title }}
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        <a href="{{ suitRoute('posts.index') }}">Post</a>
    </li>
    <li>
        <i class="fa fa-angle-right"></i>
        {{ $post->title }}
    </li>
@endsection

@section('tab-menus')
    <li class="active"><a href="#">Detail</a></li>
    <li><a href="{{ suitRoute('post-comments.index', $post) }}">Comment</a></li>
@endsection
@section('tab-contents')
	<div class="portlet-title">
        <div class="caption">
            <a href="{{ suitRoute('posts.edit', $post) }}" class="btn btn-success btn-sm" role="button">
                <i class="fa fa-edit"></i> Edit post
            </a>
        </div>
    </div>
    <div class="portlet-body">
        <div class="tab-content">
            <div class="tab-pane active">
                <table class="table table-striped table-bordered table-hover table-condensed">
                    <tr>
                        <td>
                            <strong>Owner</strong>
                        </td>
                        <td>
                            {{ $post->owner->name }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Type</strong>
                        </td>
                        <td>
                            {{ isset($post->getTypeOptions()[$post->type]) ? $post->getTypeOptions()[$post->type] : "-" }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Title</strong>
                        </td>
                        <td>
                            {{ $post->title }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Slug</strong>
                        </td>
                        <td>
                            {{ $post->slug }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Image</strong>
                        </td>
                        <td>
                            <a href="{{ asset($post->image) }}" target="_blank">
                            	<img src="{{ $post->getThumbnail('image', 'small_cover') }}">
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Excerpt</strong>
                        </td>
                        <td>
                            {{ $post->excerpt }}
                        </td>
                    </tr>
                        <td>
                            <strong>Video Key</strong>
                        </td>
                        <td>
                            {{ $post->video_key }}
                        </td>
                    </tr>
                    </tr>
                        <td>
                            <strong>Video Status</strong>
                        </td>
                        <td>
                            {{ $post->video_status }}
                        </td>
                    </tr>
                    </tr>
                        <td>
                            <strong>Video Status Description</strong>
                        </td>
                        <td>
                            {{ $post->video_status_description }}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Published</strong>
                        </td>
                        <td>
                            {!! $post->published ? "<span class=\"label label-success\">Yes</span>" : "<span class=\"label label-danger\">No</span>" !!}
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Content</strong>
                        </td>
                        <td>
                            {!! $post->content !!}
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <strong>Tags</strong>
                        </td>
                        <td>
                            {!! $postTags !!}
                        </td>
                    </tr>
                </table>
            </div>
        </div>
    </div>
@endsection