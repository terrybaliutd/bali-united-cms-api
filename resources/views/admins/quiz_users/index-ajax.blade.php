{
    "draw": {{ \Input::get('draw', 1) }},
    "recordsTotal": {{ $total }},
    "recordsFiltered": {{ $models->total() }},
    "data" : [
        @foreach($models as $key => $model)
        [
            "{{ stringJson($model->quiz->title) }}",
            "{{ stringJson($model->user->name) }}",
            "{{ stringJson($model->answer) }}",
            "{{ $model->created_at}}",
            "{{ $model->updated_at}}",
            "{!! sprintf("-")!!}"
        ]{{ ($models->count()-1 === $key) ? '' : ',' }}
        @endforeach
    ]
}
