@extends(suitViewName('_layouts.form-base'))

@section('page-title')
    {{ $pageName }} Form
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        <a href="{{ suitRoute("$routePrefix.index") }}">{{ $pageName }}</a>
    </li>
@endsection

@section('form-body')

{!! Form::suitModel($model,array('route'=>($model->exists?[suitRouteName("$routePrefix.update"),$model]:suitRouteName("$routePrefix.store")), 'method'=>($model->exists?'PUT':'POST'))) !!}
    <div class="form-body">
        {!! Form::suitSelect('video_id', 'Video', $videos) !!}
        {!! Form::suitText('title', 'Title') !!}
        {!! Form::suitTextarea('description', 'Description') !!}
        {!! Form::suitTextarea('question', 'Question') !!}
        {!! Form::suitFileBrowser('image', 'Image') !!}
        {!! Form::suitText('option_1', 'Option #1') !!}
        {!! Form::suitText('option_2', 'Option #2') !!}
        {!! Form::suitText('option_3', 'Option #3') !!}
        {!! Form::suitText('option_4', 'Option #4') !!}
        {!! Form::suitNumber('answer', 'Correct Answer', null, ['info' => 'The option number that represents the correct answer (1, 2, 3, or 4)'])!!}
        {!! Form::suitSelect('transaction_type_id', 'Points', $transaction_types) !!}
        {!! Form::suitSelect('published', 'Published', ['No', 'Yes']) !!}
    </div>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-offset-3 col-md-9">
                {!! Form::suitSubmit() !!}
                {!! Form::suitReset() !!}
                {!! Form::suitBack() !!}
            </div>
        </div>
    </div>
{!! Form::close() !!}
@endsection
