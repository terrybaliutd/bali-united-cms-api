@extends(suitViewName('_layouts.form-base'))

@section('page-title')
    {{ $pageName }} Import Form
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        <a href="{{ suitRoute("$routePrefix.index") }}">{{ $pageName }}</a>
    </li>
@endsection

@section('form-body')

    {!! Form::suitOpen([ 'url' => suitPath('quizzes/import'), 'method' => 'POST', 'files' => true]) !!}
        {!! Form::suitFileInput('file', 'Excel') !!}
        <div class="col-md-offset-2 col-md-10">
        {!! Form::suitSubmit() !!}

    {!! Form::close() !!}

@endsection
