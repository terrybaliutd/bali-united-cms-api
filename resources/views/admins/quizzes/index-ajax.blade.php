{
    "draw": {{ \Input::get('draw', 1) }},
    "recordsTotal": {{ $total }},
    "recordsFiltered": {{ $models->total() }},
    "data" : [
        @foreach($models as $key => $model)
        [
            "{{ stringJson($model->getKey()) }}",
            "{{ stringJson($model->video->title) }}",
            "{{ stringJson($model->title) }}",
            "{{ stringJson(mb_strimwidth($model->description, 0, 50, "...")) }}",
            "{{ stringJson(mb_strimwidth($model->question, 0, 50, "...")) }}",
            "{{ stringJson($model->point) }}",
            @if ($model->published_at)
                "<span class=\"label label-success\">Published</span>",
            @else
                "<span class=\"label label-danger\">Unpublished</span>",
            @endif
            "{{ $model->created_at}}",
            "{{ $model->updated_at}}",
            "{!! sprintf(
                "<a href='%s' class='btn default btn-xs green'><i class='fa fa-edit'></i> Edit </a><a href='%s' class='btn default btn-xs red delete' data-token='%s'><i class='fa fa-trash-o'></i> Delete </a><a href='%s' class='btn default btn-xs yellow'><i class='fa fa-info-circle'></i> Answers </a>",
                 suitRoute($routePrefix.'.edit', $model),
                 suitRoute($routePrefix.'.destroy', $model),
                 \Session::token(),
                 suitRoute('quiz-users.index', ['__quiz_id' => $model])
             )!!}"
        ]{{ ($models->count()-1 === $key) ? '' : ',' }}
        @endforeach
    ]
}
