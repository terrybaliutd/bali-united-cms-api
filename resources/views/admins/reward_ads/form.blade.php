@extends(suitViewName('_layouts.form-base'))

@section('page-title')
    {{ $pageName }} Form
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        <a href="{{ suitRoute("$routePrefix.index") }}">{{ $pageName }}</a>
    </li>
@endsection

@section('form-body')

{!! Form::suitModel($model,array('route'=>($model->exists?[suitRouteName("$routePrefix.update"),$model]:suitRouteName("$routePrefix.store")), 'method'=>($model->exists?'PUT':'POST'))) !!}
    <div class="form-body">
        {!! Form::suitText('title', 'Title') !!}
        {!! Form::suitText('url', 'Ads Url') !!}
        {!! Form::suitNumber('priority', 'Priority') !!}
        {!! Form::suitSelect('published', 'Published', ['No', 'Yes']) !!}
        {!! Form::suitFileBrowser('image', 'Ads Image', null, ['info' => 'The expected image resolution is 1364 x 456 pixels.']) !!}
    </div>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-offset-3 col-md-9">
                {!! Form::suitSubmit() !!}
                {!! Form::suitReset() !!}
                {!! Form::suitBack() !!}
            </div>
        </div>
    </div>
{!! Form::close() !!}
@endsection