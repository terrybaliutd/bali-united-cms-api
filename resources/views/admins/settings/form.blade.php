@extends('admins._layouts.form-base')

@section('form-title')
    {{ $pageName }} Form
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        <a href="{{ suitRoute($routePrefix.'.index') }}">{{ $pageName }}</a>
    </li>
@endsection

@section('form-body')
{!! Form::suitModel($model, ['prefix' => $routePrefix]) !!}
    <div class="form-body">
    @if (($model->name === 'watch-transaction-type') || ($model->name === 'watch-ads-transaction-type'))
        {!! Form::suitText('name', 'Name', null,['disabled']) !!}
        {!! Form::suitText('title', 'Title') !!}
        {!! Form::suitText('description', 'Description') !!}
        {!! forward_static_call_array(['Form', $model->type], ['value', 'Value', $transaction_types]) !!}
    @else 
        {!! Form::suitText('name', 'Name', null,['disabled']) !!}
        {!! Form::suitText('title', 'Title') !!}
        {!! Form::suitText('description', 'Description') !!}
        {!! forward_static_call_array(['Form', $model->type], ['value', 'Value']) !!}
    @endif   
    </div>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-offset-2 col-md-10">
                {!! Form::suitSubmit() !!}
                {!! Form::suitReset() !!}
                {!! Form::suitBack() !!}
            </div>
        </div>
    </div>
{!! Form::close() !!}
@endsection
