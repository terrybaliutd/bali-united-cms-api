@extends(suitViewName('_layouts.form-base'))

@section('page-title')
    {{ $pageName }} Form
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        <a href="{{ suitRoute("$routePrefix.index") }}">{{ $pageName }}</a>
    </li>
@endsection

@section('form-body')
{!! Form::suitModel($model,array('route'=>($model->exists?[suitRouteName("$routePrefix.update"),$model]:suitRouteName("$routePrefix.store")), 'method'=>($model->exists?'PUT':'POST'))) !!}
    <div class="form-body">
        {!! Form::suitSelect('club_id', 'Club', $clubs) !!}
        {!! Form::suitSelect('season_id', 'Season', $seasons) !!}
        {!! Form::suitSelect('competition_id', 'Competition', $competitions) !!}
        {!! Form::suitNumber('matches', 'Matches') !!}
        {!! Form::suitNumber('goal', 'Goal') !!}
        {!! Form::suitNumber('goal_conceded', 'Goal Conceded') !!}
        {!! Form::suitNumber('win', 'Win') !!}
        {!! Form::suitNumber('lose', 'Lose') !!}
        {!! Form::suitNumber('draw', 'Draw') !!}
        {!! Form::suitNumber('point', 'Point') !!}
        {!! Form::suitNumber('position', 'Position') !!}
        {!! Form::suitSelect('published', 'Published', ['No', 'Yes']) !!}
    </div>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-offset-3 col-md-9">
                {!! Form::suitSubmit() !!}
                {!! Form::suitReset() !!}
                {!! Form::suitBack() !!}
            </div>
        </div>
    </div>
{!! Form::close() !!}
@endsection
