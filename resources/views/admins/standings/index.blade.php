@extends(suitViewName('_layouts.index-base'))

@section('page-title')
    {{ $pageName }}
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        {{ $pageName }}
    </li>
@endsection

@section('table-title')
    {{ $pageName }} Table
@endsection

@section('page-header-toolbar')
    <div class="btn-group pull-right">
        <a href="{{ suitRoute("$routePrefix.create") }}" class="btn btn-fit-height btn-primary"><i class="glyphicon glyphicon-plus"></i>&nbsp;Add New</a>
    </div>
@endsection

@section('table-column-checkbox')
    <label><input type="checkbox" data-name="id">#</label>
    <label><input type="checkbox" checked data-name="season.name">Season</label>
    <label><input type="checkbox" checked data-name="competition.long_name">Competition</label>
    <label><input type="checkbox" checked data-name="club.official_name">Club</label>
    <label><input type="checkbox" checked data-name="matches">Matches</label>
    <label><input type="checkbox" checked data-name="goal">Goal</label>
    <label><input type="checkbox" checked data-name="goal_conceded">Goal Conceded</label>
    <label><input type="checkbox" checked data-name="win">Win</label>
    <label><input type="checkbox" checked data-name="lose">Lose</label>
    <label><input type="checkbox" checked data-name="draw">Draw</label>
    <label><input type="checkbox" checked data-name="point">Point</label>
    <label><input type="checkbox" checked data-name="position" data-searchable="false" data-orderable="false">Position</label>
    <label><input type="checkbox" data-name="created_at">Created At</label>
    <label><input type="checkbox" data-name="updated_at">Updated At</label>
@endsection

@section('table-filter')
    <div class="row">
        <div class="col-md-6">
            {!! Form::select('competitions', $competitions->toArray(), '', ['class' => 'form-control select2me', 'placeholder' => 'Select Competition', 'data-column-filter' => 'competition']) !!}
        </div>
        <div class="col-md-6">
            {!! Form::select('club', $clubs->toArray(), '', ['class' => 'form-control select2me', 'placeholder' => 'Select Club',  'data-column-filter' => 'club']) !!}
        </div>
    </div>
    <hr>
@stop
