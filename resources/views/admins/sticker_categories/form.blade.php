@extends(suitViewName('_layouts.form-base'))

@section('page-title')
    {{ $pageName }} Form
@stop

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        <a href="{{ suitRoute("$routePrefix.index") }}">{{ $pageName }}</a>
    </li>
@stop

@section('form-body')
<template id="sticker-item-template">
    <div class="row">
        <div class="col-md-12">
            <div class="portlet box yellow">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-grid"></i>
                        Sticker Item # {#}
                    </div>
                    <div class="tools">
                        <a href="javascript:void(0)" class="remove"></a>
                    </div>
                </div>
                <div class="portlet-body form">
                    <div class="form-body">
                        {!! Form::suitText("items[{#}][title]", 'Title') !!}
                        {!! Form::suitFileBrowser("items[{#}][image]", 'Sticker Image') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</template>
{!! Form::suitModel($model,array('route'=>($model->exists?[suitRouteName("$routePrefix.update"),$model]:suitRouteName("$routePrefix.store")), 'method'=>($model->exists?'PUT':'POST'))) !!}
    <div class="form-body">
        {!! Form::suitText('title', 'Title') !!}
        {!! Form::suitText('slug', 'Slug', null, ['info' => 'Must be alphanumeric. Leave blank to automatic generated']) !!}
        {!! Form::suitDateTime('expired_date', 'Expired Date') !!}
        {!! Form::suitSelect('published', 'Published', ['No', 'Yes']) !!}
        {!! Form::suitFileBrowser('image', 'Sticker Category Image') !!}
        <div id="sticker-item-container">
            @foreach ($model->items as $i => $item)
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet box yellow">
                        <div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-grid"></i>
                                Sticker Item # {{ ++$i }}
                            </div>
                            <div class="tools">
                                <a href="javascript:void(0)" class="remove"></a>
                            </div>
                        </div>
                        <div class="portlet-body form">
                            <div class="form-body">
                                {!! Form::hidden("items[{$i}][id]", $item->id) !!}
                                {!! Form::suitText("items[{$i}][title]", 'Title', $item['title']) !!}
                                {!! Form::suitFileBrowser("items[{$i}][image]", 'Sticker Image', $item->image) !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
            @if(is_array(Input::old('items')))
                @foreach(Input::old('items') as $i => $item)
                @if (!array_key_exists('id', $item))
                <div class="row">
                    <div class="col-md-12">
                        <div class="portlet box yellow">
                            <div class="portlet-title">
                                <div class="caption">
                                    <i class="fa fa-grid"></i>
                                    Sticker Item # {{ $i }}
                                </div>
                                <div class="tools">
                                    <a href="javascript:void(0)" class="remove"></a>
                                </div>
                            </div>
                            <div class="portlet-body form">
                                <div class="form-body">
                                    {!! Form::suitText("items[{$i}][title]", 'Title', $item['title']) !!}
                                    {!! Form::suitFileBrowser("items[{$i}][image]", 'Sticker Image', $item['image']) !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                @endif
                @endforeach
            @endif
        </div>
        <a href="#" id="sticker-item-button" class="btn btn-primary">Add New Sticker Item</a>
    </div>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-offset-3 col-md-9">
                {!! Form::suitSubmit() !!}
                {!! Form::suitReset() !!}
                {!! Form::suitBack() !!}
            </div>
        </div>
    </div>
{!! Form::close() !!}
@stop

@section('page-scripts')
    @parent
    <script type="text/javascript">
        jQuery('#sticker-item-container .match-id-button').live("click", function(e){
            e.preventDefault();
            console.log('click');
            var container = jQuery(this).parent();
            var manualContainer = container.next('.manual-match-container');
            container.remove();
            manualContainer.show();
        });
        function initTemplate(template, container, button) {
            var container = $(container),
                template = $(template)[0].innerHTML;
            $(button).click(function (e) {
                e.preventDefault();
                var tempTemplate = template.replace(new RegExp('{#}', 'g'), container.children().length + 1);
                container.append($(tempTemplate));
                var multiSelect = container.find('select');
                multiSelect.select2();
            });
        }
        initTemplate('#sticker-item-template', '#sticker-item-container', '#sticker-item-button');
    </script>
@endsection