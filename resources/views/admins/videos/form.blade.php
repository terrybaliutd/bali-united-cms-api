@extends('admins._layouts.form-base')

@section('form-title')
    {{ $pageName }}
@endsection

@section('page-breadcrumb')
    @parent
    <li>
        <i class="fa fa-angle-right"></i>
        <a href="{{ suitRoute("$routePrefix.index") }}"">{{ $pageName }}</a>
    </li>
@endsection

@section('form-body')
@if(empty($url))
    {{$url = null}}
@endif

<h3> Generate Video Data from URL</h3>
{!! Form::suitOpen([ 'url' => suitPath('videos/create'), 'method' => 'POST']) !!}
    {!! Form::suitText('url', 'URL', $url ? $url : old('url')) !!}
    <div class="col-md-offset-2 col-md-10">
        {!! Form::suitSubmit() !!}
    </div>
    </div>
    {!! Form::close() !!}
    <br>
    <br>

@if($model->key != null)
<h3>Video Data</h3>
{!! Form::suitModel($model, ['prefix' => $routePrefix]) !!}
    <div class="form-body">
        {!! Form::suitText('key', 'Key', null, [ 'readonly']) !!}
        {!! Form::suitText('title', 'Title', null, ['info' => 'Judul Video maksimum 80 karakter']) !!}
        {!! Form::suitDateTime('upload_date', 'Upload Date', null,  ['readonly']) !!}
        {!! Form::suitSelect('playlist_id', 'Playlist', $playlists) !!}
        {!! Form::suitText(
            'show_duration',
            'Duration',
            $model->duration ? App\Supports\FormatDuration::convert($model->duration) : null,
            [ 'readonly'])
            !!}
        {!! Form::hidden('duration', $model->duration) !!}
        {!! Form::suitSelect('match_id', 'Match', $matches, null, null, ['placeholder' => 'Select Match']) !!}
        {!! Form::suitText('publisher_name', 'Publisher', $model->publisher ? $model->publisher : Auth::user()->name, ['disabled']) !!}
        {!! Form::suitFileBrowser("attachment", 'Image', $model->attachment) !!}
        {!! Form::suitTextarea('description', 'Description') !!}
        {!! Form::suitSelect("featured", 'Featured', ['No', 'Yes']) !!}
        {!! Form::suitSelect("published", 'Published', ['No', 'Yes']) !!}
        {!! Form::suitSelect("is_live", 'Live Video', ['No', 'Yes']) !!}
        {!! Form::suitSelect('membership_id', 'Membership Content', $memberships, null, null, ['placeholder' => '-- Membership Content --']) !!}
        {!! Form::suitDateTime("start_time", "Streaming Start Time", null, ['disabled']) !!}

    </div>
    <div class="form-actions">
        <div class="row">
            <div class="col-md-offset-2 col-md-10">
                {!! Form::suitSubmit() !!}
                {!! Form::suitReset() !!}
                {!! Form::suitBack() !!}
            </div>
        </div>
    </div>
{!! Form::close() !!}
@endif

@endsection

@section('init-scripts')
    @parent
    if ($('#is_live').val() == 1) {
       $('#start_time').prop("disabled", false);
    } else {
       $('#start_time').prop("disabled", true);
    }
    $('#is_live').on('change', function (event) {
        console.log($(this).val());
        if ($(this).val() == 1) {
           $('#start_time').prop("disabled", false);
        } else {
           $('#start_time').prop("disabled", true);
        }
    });
@endsection
