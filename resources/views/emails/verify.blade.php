<!DOCTYPE html>
<html lang="en-US">
    <head>
        <meta charset="utf-8">
    </head>
    <style>
    p.big {
        line-height: 200%;
    }
    </style>
    <body>
        <div>
            <p class = "big">   
                Halo {{ $user }},<br>
            </p> 
            Terima kasih sudah mendaftar / membuat akun di aplikasi resmi Bali United. <br>
            Silakan ikuti link berikut untuk verifikasi alamat email anda 
            <a href="{{ URL::to('register/verify/' . $confirmation_code) }}">
                {{ URL::to('register/verify/' . $confirmation_code) }}
            </a>.
            <br/>
        </div>

    </body>
</html>
