@extends('_layouts.web')

@section('site-content')
<ul>
    @if ($datas instanceof Illuminate\Database\Eloquent\Model)
        <p>{{ $datas }}</p>
    @else
        @foreach($datas as $data)
        <li>
            {{ $data }}
        </li>
        @endforeach
    @endif
</ul>
@endsection
