@extends('_layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                @if(Session::has('message'))
                <div class="alert">
                    <a class="close" data-close="alert"></a>
                    <span> {{ Session::get('message') }}</span>
                </div>
                @endif
            </div>
        </div>
    </div>
</div>
@endsection
