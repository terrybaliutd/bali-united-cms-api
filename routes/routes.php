<?php

use Suitcms\Model\Menu;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function () {
    return View::make('menu', ['menus' => App\Model\Menu::getAllMenu()]);
});

Route::get('/home', function () {
    return redirect('http://www.baliutd.com');
});

Route::get('/password/reset/{token}', 'PasswordController@getReset');
Route::post('/password/reset/', 'PasswordController@postReset');

Route::get('register/verify/{confirmationCode}', [
    'as' => 'register.verify',
    'uses' => 'UserController@verify'
]);

Route::group(['prefix' => 'api', 'namespace' => 'Api'], function () {
    /* User API */
    Route::resource('users', 'UserController', ['only' => ['show', 'index']]);
    Route::post('users/login', 'UserController@login');
    Route::post('users/login/{provider}', 'UserController@loginBySocialAcc');
    Route::group(['middleware' => 'api.access'], function () {
        Route::post('users/logout', 'UserController@logout');
        Route::post('users/set-password', 'UserController@setPassword');
        Route::post('users/update', 'UserController@update');
    });
    Route::post('users/register', 'UserController@register');
    Route::post('users/forgotPassword', 'UserController@forgotPassword');

    Route::resource('menus', 'MenuController', ['only' => ['show', 'index']]);
    Route::resource('settings', 'SettingController', ['only' => ['show', 'index']]);
    Route::resource('pages', 'PageController', ['only' => ['show', 'index']]);
    Route::resource('news', 'NewsController', ['only' => ['show', 'index']]);
    Route::resource('clubs', 'ClubController', ['only' => ['show', 'index']]);
    Route::resource('competitions', 'CompetitionController', ['only' => ['show', 'index']]);
    Route::resource('seasons', 'SeasonController', ['only' => ['show', 'index']]);
    Route::resource('standings', 'StandingRepoController', ['only' => ['show', 'index']]);
    Route::resource('playlists', 'PlaylistRepoController', ['only' => ['show', 'index']]);
    Route::resource('provinces', 'ProvinceController', ['only' => ['show', 'index']]);
    Route::resource('cities', 'CityController', ['only' => ['show', 'index']]);
    Route::resource('bank-accounts', 'BankAccountController', ['only' => ['index']]);
    Route::get('citizen-jurnals', 'CitizenJournalVideoController@index');

    // Search API
    Route::group(['prefix' => 'search'], function () {
        Route::get('/', 'SearchController@index');
        Route::get('video', 'SearchController@video');
        Route::get('news', 'SearchController@news');
    });


    /* Match API */
    Route::group(['prefix' => 'match'], function () {
        Route::get('matches', 'MatchController@matches');
        Route::get('{id}/highlight', 'MatchHighlightController@highlight');
        Route::get('{id}/lineup', 'MatchLineUpController@lineUp');
        Route::get('{id}/videos', 'VideoController@matchVideos');
        Route::get('{id}/statistic', 'MatchStatisticController@statistic');
        Route::get('results', 'MatchResultRepoController@index');
        Route::get('fixtures', 'MatchFixturesRepoController@index');
        Route::get('tickets', 'MatchTicketsRepoController@index');
        Route::get('highlights', 'MatchHighlightController@index');
        Route::get('lineups', 'MatchLineUpController@index');
        Route::get('statistics', 'MatchStatisticController@index');
    });

    Route::group(['prefix' => 'shipping'], function () {
        Route::post('calculate', 'ShippingController@calculateCost');
        Route::group(['prefix' => 'list'], function () {
            Route::get('provinces', 'RajaongkirProvinceRepoController@index');
            Route::get('cities', 'RajaongkirCityRepoController@index');
            Route::get('subdistricts', 'RajaongkirSubdistrictRepoController@index');
            Route::get('couriers', 'ShippingController@getCouriers');
        });
    });

    /* Video API */
    Route::group(['prefix' => 'video', 'middleware' => 'api.access'], function () {
        Route::get('{id}', 'VideoController@show');
        Route::post('{id}/like', 'VideoController@like');
        Route::post('{id}/watch', 'VideoController@watch');
    });

    /* Videos API */
    Route::group(['prefix' => 'videos'], function () {
        Route::get('/', 'VideoIndexRepoController@index');
        // Please Remove Me Later !!!!!
        Route::get('recent', 'VideoRecentRepoController@index');
        // End of Remove Request
        Route::get('trending', 'VideoTrendingRepoController@index');
        Route::get('live', 'VideoLiveRepoController@live');
    });
    Route::group(['middleware' => 'api.access'], function () {
        Route::post('videos/post', 'CitizenJournalVideoController@postVideo');
    });

    /* Playlist API */
    Route::group(['prefix' => 'playlists'], function () {
        Route::get('{id}/videos', 'VideoRepoController@playlistVideos');
    });

    Route::group(['prefix' => 'latest'], function () {
        Route::get('news', 'NewsRecentController@index');
        Route::get('videos', 'VideoRecentRepoController@index');
    });

    /* Comment API */
    Route::resource('comments', 'CommentController', ['only' => ['index']]);
    Route::group(['middleware' => 'api.access'], function () {
        Route::post('comments/post', 'CommentController@postComment');
    });
    Route::resource('stickers', 'StickerController', ['only' => ['index']]);

    Route::get('smartfren/import', 'CronController@import_smartfren', ['only' => ['index']]);

    /* OTP API */
    Route::group(['middleware' => 'api.access'], function () {
        Route::post('otp/generate', 'OtpController@generateOtp');
        Route::post('otp/verification', 'OtpController@verifyOtp');
    });

    /* Quiz API */
    Route::group(['middleware' => 'api.access'], function () {
        Route::post('quizzes/post', 'QuizController@postQuiz');
        Route::resource('quizzes', 'QuizController', ['only' => ['show', 'index']]);
    });

    /* Reward API */
    Route::group(['middleware' => 'api.access'], function () {
        Route::resource('user-rewards', 'UserRewardController', ['only' => ['index', 'show']]);
        Route::post('user-rewards/redeem', 'UserRewardController@redeemVoucher');
    });
    Route::get('rewards', 'RewardController@index');
    Route::get('rewards/{key}', 'RewardController@show');

    /* Transaction API */
    Route::group(['middleware' => 'api.access'], function () {
        Route::resource('transactions', 'TransactionController', ['only' => ['index', 'show', 'store']]);
    });
    Route::group(['prefix' => 'transactions', 'middleware' => 'api.access'], function () {
        Route::post('voucher', 'TransactionController@storeVoucher');
        Route::post('merchandise', 'TransactionController@storeMerchandise');
        Route::get('history/earned', 'TransactionController@creditHistory');
        Route::get('history/spent', 'TransactionController@debitHistory');
    });

    Route::group(['prefix' => 'ads'], function () {
        Route::resource('reward-ads', 'RewardAdsController', ['only' => ['index']]);
        Route::get('video-ads', 'VideoAdsController@index')->middleware('api.access');
        Route::post('video-ads/{id}/watch', 'VideoAdsController@watch')->middleware('api.access');
    });

    /* Payment API */
    Route::group(['prefix' => 'transactions/{key}/payments', 'middleware' => 'api.access'], function () {
        Route::post('/', 'PaymentController@store');
        Route::patch('/', 'PaymentController@update');
        Route::get('/', 'PaymentController@show');
    });
});

Route::get('{url}', ['uses' => 'PageController@show'])->where('url', '.+');
