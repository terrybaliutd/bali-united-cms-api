<?php

namespace TestApp\Supports\Attachment\Downloader;

use App\Supports\Attachment\Downloader\AbstractDownloader;
use Illuminate\Contracts\Filesystem\Filesystem;
use Mockery;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeExtensionGuesser;
use TestApp\TestCase;

class AbstractDownloaderTest extends TestCase
{
    protected $filesystem;

    protected $downloader;

    public function setUp()
    {
        parent::setUp();

        $filesystem = Mockery::mock(Filesystem::class);
        $downloader = new StubDownloader($filesystem, new MimeTypeExtensionGuesser);

        $this->filesystem = $filesystem;
        $this->downloader = $downloader;
    }

    public function testFilename()
    {
        $this->assertRegExp('/test\.jpg/', $this->downloader->filename('test.jpg'));
        $this->assertRegExp('/test\.jpg/', $this->downloader->filename('test.JPG'));
        $this->assertRegExp('/test-space\.jpg/', $this->downloader->filename('test space.jpg'));
        $this->assertRegExp('/test/', $this->downloader->filename('test'));
        $this->assertRegExp('/test-space/', $this->downloader->filename('test space'));
        $this->assertRegExp('/test-space/', $this->downloader->filename('test%20space'));
        $this->assertRegExp('/testspace/', $this->downloader->filename('test&space'));
        $this->assertRegExp('/test-space/', $this->downloader->filename('test+space'));
        $this->assertRegExp('/12345678901234567890123456789012345678901234567890/', $this->downloader->filename('1234567890123456789012345678901234567890123456789012345678901234567890'));
        $this->assertRegExp('/test\.1234567890/', $this->downloader->filename('test.1234567890123456789012345678901234567890'));
        $this->assertRegExp('/test\.jpeg/', $this->downloader->filename('test', 'image/jpeg'));
        $this->assertRegExp('/test/', $this->downloader->filename('test', null));
    }

    public function testSetRootPath()
    {
        $this->downloader->setRootPath('test');
        $this->assertEquals('test', $this->downloader->getRootPath());
        $this->downloader->setRootPath('test/');
        $this->assertEquals('test', $this->downloader->getRootPath());
        $this->downloader->setRootPath('/test folder/test/');
        $this->assertEquals('test folder/test', $this->downloader->getRootPath());
    }

    public function testSavePath()
    {
        $this->downloader->setRootPath('test');
        $this->assertRegExp('/^test\/[0-9]{4}\/[A-Za-z]{3}\/[0-9]{2}\/[a-z0-9]{13}\/test.png$/', $this->downloader->savePath('test.png'));
    }
}

class StubDownloader extends AbstractDownloader
{
    public function download($path)
    {

    }

    public function validate($path)
    {

    }
}
