<?php

namespace TestApp\Supports\Attachment\Downloader;

use App\Supports\Attachment\Downloader\LocalDownloader;
use Illuminate\Contracts\Filesystem\Filesystem;
use Mockery;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeExtensionGuesser;
use TestApp\TestCase;

class LocalDownloaderTest extends TestCase
{
    protected $filesystem;

    protected $downloader;

    public function setUp()
    {
        parent::setUp();

        $filesystem = Mockery::mock(Filesystem::class);
        $extensionGuesser = new MimeTypeExtensionGuesser;

        $downloader = new LocalDownloader($filesystem, $extensionGuesser);

        $this->filesystem = $filesystem;
        $this->downloader = $downloader;
    }

    public function testDownloadWhenFileExists()
    {
        $this->filesystem->shouldReceive('exists')->andReturn(true);
        $this->filesystem->shouldReceive('mimeType')->andReturn('mime');
        $this->filesystem->shouldReceive('size')->andReturn(1000);
        $this->filesystem->shouldReceive('copy')->once();

        $attachment = $this->downloader->download('path/to/file');

        $this->assertEquals('1000', $attachment->getSize());
        $this->assertEquals('mime', $attachment->getMimeType());
        $this->assertRegExp('/[0-9]{4}\/[A-Za-z]{3}\/[0-9]{2}\/[a-z0-9]{13}\/file/', $attachment->getUrlPath());
    }

    /**
     * @expectedException \App\Supports\Attachment\ResourceNotFoundException
     */
    public function testDownloadWhenFileNotExists()
    {
        $this->filesystem->shouldReceive('exists')->andReturn(false);
        $this->filesystem->shouldReceive('mimeType')->never();
        $this->filesystem->shouldReceive('size')->never();
        $this->filesystem->shouldReceive('copy')->never();

        $this->downloader->download('path/to/file');
    }

    public function testValidate()
    {
        $this->filesystem->shouldReceive('exists')->andReturn(true);
        $this->assertTrue($this->downloader->validate('path/to/file'));
    }

    public function testValidateNotValid()
    {
        $this->assertFalse($this->downloader->validate(Mockery::mock(Mockery::class)));
        $this->assertFalse($this->downloader->validate(1));
    }
}
