<?php

namespace TestApp\Supports\Attachment;

use App\Supports\Attachment\Manager;
use App\Supports\Attachment\Downloader\AbstractDownloader;
use App\Supports\Attachment\InvalidDownloaderException;
use App\Supports\Attachment\ResourceNotFoundException;
use App\Supports\Attachment\FormatNotSupportException;
use Mockery;
use TestApp\TestCase;

class ManagerTest extends TestCase
{
    public function testConstructWithEmptyDownloader()
    {
        $manager = new Manager([]);
        $this->assertInstanceOf(Manager::class, $manager);
    }

    public function testConstructWithValidDownloader()
    {
        $manager = new Manager([Mockery::mock(AbstractDownloader::class), Mockery::mock(AbstractDownloader::class)]);
        $this->assertInstanceOf(Manager::class, $manager);
    }

    /**
     * @expectedException \App\Supports\Attachment\InvalidDownloaderException
     */
    public function testConstructWithInValidDownloader()
    {
        $manager = new Manager([Mockery::mock(AbstractDownloader::class), Mockery::mock(Mockery::class)]);
        $this->assertInstanceOf(Manager::class, $manager);
    }

    public function testDownload()
    {
        $manager = new Manager([
            $this->mockSuccessDownloader('first'),
            $this->mockSuccessDownloader('second')
        ]);

        $this->assertEquals('first', $manager->download('file', 'path'));
    }

    public function testDownloadWithPartialInvalid()
    {
        $manager = new Manager([
            $this->mockInvalidDownloader(),
            $this->mockSuccessDownloader('second')
        ]);

        $this->assertEquals('second', $manager->download('file', 'path'));
    }

    /**
     * @expectedException \App\Supports\Attachment\FormatNotSupportException
     */
    public function testDownloadWithAllInvalid()
    {
        $manager = new Manager([
            $this->mockInvalidDownloader(),
            $this->mockInvalidDownloader()
        ]);

        $manager->download('file', 'path');
    }

    /**
     * @expectedException \App\Supports\Attachment\ResourceNotFoundException
     */
    public function testDownloadWithPartialInvalidAndFail()
    {
        $manager = new Manager([
            $this->mockInvalidDownloader(),
            $this->mockFailDownloader()
        ]);

        $manager->download('file', 'path');
    }

    /**
     * @expectedException \App\Supports\Attachment\ResourceNotFoundException
     */
    public function testDownloadWithAllFail()
    {
        $manager = new Manager([
            $this->mockFailDownloader(),
            $this->mockFailDownloader()
        ]);

        $manager->download('file', 'path');
    }


    protected function mockSuccessDownloader($value)
    {
        $downloader = Mockery::mock(AbstractDownloader::class);
        $downloader->shouldReceive('validate')->with('file')->andReturn(true);
        $downloader->shouldReceive('setRootPath')->with('path');
        $downloader->shouldReceive('download')->andReturn($value);
        return $downloader;
    }

    protected function mockInvalidDownloader()
    {
        $downloader = Mockery::mock(AbstractDownloader::class);
        $downloader->shouldReceive('validate')->andReturn(false);
        return $downloader;
    }

    public function mockFailDownloader()
    {
        $exception = Mockery::mock(ResourceNotFoundException::class);
        $exception->shouldReceive('getMessage')->andReturn('error.');

        $downloader = Mockery::mock(AbstractDownloader::class);
        $downloader->shouldReceive('validate')->andReturn(true);
        $downloader->shouldReceive('setRootPath');
        $downloader->shouldReceive('download')->andThrow($exception);
        return $downloader;
    }
}
